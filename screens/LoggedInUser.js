import React, {Component} from 'react';
import {AppRegistry,Platform, StyleSheet, Text, View, Image,TouchableOpacity,StatusBar,ScrollView,TextInput,KeyboardAvoidingView} from 'react-native';
import StarRating from 'react-native-star-rating';
import { Actions } from 'react-native-router-flux';
import styles from '../components/styles';
import color from '../constants/Colors';
const like = require('../assets/images/like.png');
const dislike = require('../assets/images/dislike.png');



class LoggedInUser extends Component {



constructor(props) {
    super(props)
    this.state = { 
        value: 18 ,
        fundimage :true,
        investimage :false,
        starCount:2.5,
        error: null,
        emailtext:null,
        pwd:null,
        hideDrawerButton: false,
        hideNavBar:false,
        hideTabBar:false,
    }
} 





renderfundimage(){
    var imgSource = this.state.fundimage? like : dislike;
return (
  <Image
  style={{width: 40, height: 40,resizeMode:"contain",marginTop:10}} 
    source={ imgSource }
  />
    );
}

renderinvestimage(){
    var imgSource = this.state.investimage? like : dislike;
return (
  <Image
  style={{width: 40, height: 40,resizeMode:"contain",marginTop:10,marginLeft:25}}
    source={ imgSource }
  />
    );
}


onStarRatingPress(rating) {
    this.setState({
      starCount: rating         
    });
   // console.warn(rating);
}

  render () {
   
    return (
        <KeyboardAvoidingView style={styles.containerFlex} behavior="padding">
        <View>
            <StatusBar
        backgroundColor={color.toolbar}
        barStyle="light-content"/>
        <ScrollView behavior="padding">
<View style={styles.loginContainer}>
          <View style={{flexDirection:"row",marginTop:10}}>
            <Image source={require('../assets/images/like.png')}
                style={{width: 80, height: 80, borderRadius: 80/2,alignSelf:"center",marginTop:15,padding:5,resizeMode:"contain",marginLeft:15}} />

            <View style={{flexDirection:"column",marginLeft:15}}>
            <Text style={[styles.welcome,{textAlign: 'center',marginTop:15}]}>BNP Paribas Multi Cap Fund (G)</Text>
                <Text style={{marginTop:5,marginEnd:10,color:'black',fontSize:20}}>Nav
                        <Text style={{color: 'green',fontStyle:"italic"}}  >
                        {'\t'}{'\u20B9'}44.634
                        </Text>
                </Text>
                <Text style={{color:'grey'}}>(28/06/2018)</Text>
            </View>

          </View>
       
            <View style={[styles.dash,{marginTop:25}]}/>  
            <View style={{backgroundColor:"#FFFFFF"}}> 
            <View style={{ flexDirection: 'row',marginTop:15}}>          

                    <View style={{flex:1}} >

                        <View style={styles.box}>
                        <TouchableOpacity activeOpacity = { .5 }  >
                            <Text style={styles.flexboxtext}>Fund</Text>
                            {/* <Image source={require('../assets/images/like.png')}
                            style={{width: 40, height: 40,resizeMode:"contain",marginTop:10}} /> */}
                            {this.renderfundimage()}
                            </TouchableOpacity>
                        </View>
                    </View>

            <View style={[styles.dashcolumn]}/>   

                    <View style={{flex:1}} >
                    <View style={styles.box}>
                            <Text style={styles.flexboxtext}>Rank</Text>
                            {/* <Image source={require('../assets/rating.png')}
                            style={{width: 60, height: 40,resizeMode:"contain",marginTop:10}} /> */}
                           <StarRating
                                starStyle={{marginTop:10}}
                                fullStar= {require('../assets/images/star.png')}
                                emptyStar= {require('../assets/images/emptystar.png')}
                                disabled={true}
                                maxStars={5}
                                starSize={28}
                                halfStarEnabled={true}
                                rating={this.state.starCount}
                                selectedStar={(rating) => this.onStarRatingPress(rating)}
                                fullStarColor={'red'}                                
                            />
                        </View>
                    </View>

                     <View style={[styles.dashcolumn]}/> 

                    <View style={{flex:1}} >
                    <View style={styles.box}>
                    <TouchableOpacity activeOpacity = { .5 }  >
                            <Text style={styles.flexboxtext}>Time to Invest</Text>
                            {/* <Image source={require('../assets/images/dislike.png')}
                            style={{width: 40, height: 40,resizeMode:"contain",marginTop:10}} /> */}
                            {this.renderinvestimage()}
                            </TouchableOpacity>
                        </View>
                    </View>    
                </View> 
                <View style={[styles.dash,{marginTop:15}]}/> 
                <View style={{ flexDirection: 'row',marginTop:5}}>
                   <View style={{flex:1}} >
                    <View style={styles.box}>
                 
                            <Text style={styles.flexboxtext}>Expense Ratio</Text>
                            <Text style={[styles.welcome,{textAlign:"center"}]}>2.54%</Text>
                           
                        </View>
                    </View>    
                    <View style={[styles.dashcolumn]}/> 

                    <View style={{flex:1}} >
                    <View style={styles.box}>
                 
                            <Text style={styles.flexboxtext}>Rank</Text>
                            <Text style={[styles.welcome,{textAlign:"center"}]}>10</Text>
                           
                        </View>
                    </View> 
                    </View>
                    <View style={[styles.dash,{marginTop:5}]}/> 
                <View style={{ flexDirection: 'row',marginTop:5}}>
                   <View style={{flex:1}} >
                    <View style={styles.box}>
                 
                            <Text style={styles.flexboxtext}>Type</Text>
                            <Text style={[styles.welcome,{textAlign:"center"}]}>Equity</Text>
                           
                        </View>
                    </View>    
                    <View style={[styles.dashcolumn]}/> 

                    <View style={{flex:1}} >
                    <View style={styles.box}>
                 
                            <Text style={styles.flexboxtext}>Exit Load</Text>
                            <Text style={[styles.welcome,{textAlign:"center"}]}>1%</Text>
                           
                        </View>
                    </View> 
                    </View>
                    <View style={[styles.dash,{marginTop:5}]}/> 
                </View>
              
                <View style={{ flexDirection: 'column',alignItems:"center",justifyContent:"center",flex:1,marginTop:20}}>

                        <Text style={[styles.welcome,{textAlign:"center"}]}>Amount</Text>
                        <TextInput style = {styles.input} 
                            autoCapitalize="none" 
                            autoCorrect={false} 
                            keyboardType='numeric' 
                            returnKeyType="done" 
                            width={200}
                            underlineColorAndroid="transparent"
                            onChangeText={(text) => this.setState({amount:text})}/>
                            
                </View>
                <View style={[styles.dash,{marginTop:15}]}/> 
                <View style={[styles.buttonContainer,{marginTop:10}]}>
                    <TouchableOpacity  onPress={() => Actions.Buy2()} style={[styles.button,{backgroundColor:color.toolbar}]} >
                        <Text style={styles.instructions}>CONFIRM AND CONTINUE</Text>
                    </TouchableOpacity> 
                    </View>
                    
                    
      </View>
      </ScrollView>
                    </View>
                    </KeyboardAvoidingView>
    );
  }
}


export default LoggedInUser;