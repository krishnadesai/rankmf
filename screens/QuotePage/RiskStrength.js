import React, {Component} from 'react';
import { StyleSheet,Platform, Text,  View,ScrollView,Image,Picker,Alert,AsyncStorage,KeyboardAvoidingView} from 'react-native';
import { Card } from 'react-native-elements';
import Table from 'react-native-simple-table';
import styles from '../../components/styles';
import Apis from '../../constants/Apis';
import color from '../../constants/Colors';

const thumbsup = require('../../assets/images/thumbsup.png');
const thumbsdown = require('../../assets/images/thumbsdown.png');


class RiskStrength extends Component {

  constructor(props) {
    super(props)
    this.state = { 
        aggregate:{},
        title:'',
        risk_strength:[],
        riskcolor:''
    }
  
}

  componentDidMount(){
    this.retrieveData()
  }
  
  fetchData(){
   // alert(this.props.unique_code);
   unique_code = this.props.unique_code;
    fetch(Apis.BaseURL, {
      method: 'POST',
      headers: new Headers({
                  'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
          }),
      body: `api=1.2&access_token=${access_token}&user=${clientid}&unique_code=${this.props.unique_code}&type=risk_factor`// <-- Post parameters
          })
          .then((response) => response.json())
          .then((responseJson) => {
            for (var key in responseJson["data"]["risk_strength"]) {
                this.state.risk_strength.push(responseJson["data"]["risk_strength"][key])
               
            }

            
                this.setState({riskcolor:responseJson["data"]["response"][0].Color})
              
             
          
          this.setState({ aggregate:responseJson["data"]["aggregate"],})
            var riskscore = responseJson["data"]["aggregate"].risk_score
          if(riskscore === "0") {
            this.setState({title:"Very Low Strength"});
          
           }else if(riskscore === "1"){
            this.setState({title:"Low Strength"});
              
           }else if(riskscore === "2"){
            this.setState({title:"Moderate Strength"});
             
          }else if(riskscore === "3"){
            this.setState({title:"High Strength"});
              
          }else if(riskscore === "4"){
            this.setState({title:"Very High Strength"});
             
          }
          })
          .catch((error) => {
              console.error(error);
          });
  }

  retrieveData = async () => {
    try {
     clienttype = await AsyncStorage.getItem('clienttype');
     clientid = await AsyncStorage.getItem('clientid');
     access_token =await AsyncStorage.getItem('access_token');  
     if(clienttype === "client"){
       this.setState({showFund:true})
     }else{
      this.setState({showFund:false})
     }
     this.fetchData();
     } catch (error) {
       // Error retrieving data
       //console.warn("1");
     }
  }

  renderStrength(riskscore){       
       
    if(riskscore === "0") {
     
     return (
        
             <Image source={require('../../assets/images/very_low_strength_large.png')} 
             style={{width:140, height:140,resizeMode:"contain",alignItems:"center",marginTop:9,alignSelf:"center"}}                   />
       
         );
     }else if(riskscore === "1"){
     
         return (
            <Image source={require('../../assets/images/low_strength_large.png')}
            style={{width:140, height:140,resizeMode:"contain",alignItems:"center",marginTop:9,alignSelf:"center"}}   />
             );
     }else if(riskscore === "2"){
      
        return (
           <Image source={require('../../assets/images/moderate_strength_large.png')}
           style={{width:140, height:140,resizeMode:"contain",alignItems:"center",marginTop:9,alignSelf:"center"}}   />
            );
    }else if(riskscore === "3"){
      
        return (
           <Image
           source={require('../../assets/images/high_strength_large.png')}
           style={{width:140, height:140,resizeMode:"contain",alignItems:"center",marginTop:9,alignSelf:"center"}}   />
            );
    }else if(riskscore === "4"){
     
        return (
           <Image
           source={require('../../assets/images/very_high_strength_large.png')}
           style={{width:140, height:140,resizeMode:"contain",alignItems:"center",marginTop:9,alignSelf:"center"}}      />
            );
    }
     }

     renderStrengthTooltip(portfolio_quality,risk_volatility,corelation_becnhmark,asset_type){

      const arr =[thumbsdown,thumbsup];
      const arr1=[color.RED,color.GREEN];

      if(asset_type != "Debt"){
          return(
              <View style={{padding:Platform.OS ==='ios'? 5 :3,marginTop:15}}>
       
                  <View style={{flexDirection:"row"}}>
                     <View style={[styles.circle,{backgroundColor:arr1[risk_volatility],alignItems:"center",height:20,width:20}]}>
                          <Image
                          style={{width: 10, height: 10,resizeMode:"contain",alignItems:"center",marginTop:5}} 
                              source={arr[risk_volatility]}
                          />
                          </View>
                          <Text style={[styles.flexboxtext,{marginLeft:15,textAlign:"left"}]}>Resilience in volatility</Text>
                  </View>
                  <View style={{flexDirection:"row",marginTop:10}}>
                     <View style={[styles.circle,{backgroundColor:arr1[corelation_becnhmark],alignItems:"center",height:20,width:20}]}>
                          <Image
                          style={{width: 10, height: 10,resizeMode:"contain",alignItems:"center",marginTop:5}} 
                              source={arr[corelation_becnhmark]}
                          />
                          </View>
                          <Text style={[styles.flexboxtext,{marginLeft:15,textAlign:"left"}]}>Ability to generate higher return at lower risk</Text>
                  </View>
                  { asset_type === "Equity" || asset_type === "Hybrid"    &&  <View style={{flexDirection:"row",marginTop:10}}>
                     <View style={[styles.circle,{backgroundColor:arr1[portfolio_quality],alignItems:"center",height:20,width:20}]}>
                          <Image
                          style={{width: 10, height: 10,resizeMode:"contain",alignItems:"center",marginTop:5}} 
                              source={arr[portfolio_quality]}
                          />
                          </View>
                          <Text style={[styles.flexboxtext,{marginLeft:15,textAlign:"left"}]}>Quality of Portfolio</Text>
                  </View>}
              </View>
          );
      }else{
          return(
              <View style={{padding:5,marginTop:15}}>
       
       <View style={{flexDirection:"row"}}>
                     <View style={[styles.circle,{backgroundColor:arr1[portfolio_quality],alignItems:"center",height:20,width:20}]}>
                          <Image
                          style={{width: 10, height: 10,resizeMode:"contain",alignItems:"center",marginTop:5}} 
                              source={arr[portfolio_quality]}
                          />
                          </View>
                          <Text style={[styles.flexboxtext,{marginLeft:15,textAlign:"left"}]}>Quality of Portfolio</Text>
                  </View>
                  <View style={{flexDirection:"row",marginTop:10}}>
                     <View style={[styles.circle,{backgroundColor:arr1[corelation_becnhmark],alignItems:"center",height:20,width:20}]}>
                          <Image
                          style={{width: 10, height: 10,resizeMode:"contain",alignItems:"center",marginTop:5}} 
                              source={arr[corelation_becnhmark]}
                          />
                          </View>
                          <Text style={[styles.flexboxtext,{marginLeft:15,textAlign:"left"}]}>Interest Rate Risk</Text>
                  </View>
                  
              </View>
          );
      }
   }

  render () {

    
    return (
     
      <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.container}>
      <Card >
        {clienttype==="client" &&<View> 
          {this.renderStrength(this.state.aggregate.risk_score)}
           <Text style={{fontWeight:"bold",color:'black',fontSize:20,marginTop:15,alignSelf:"center"}}>{this.state.title}</Text>
           
           {this.renderStrengthTooltip(this.state.aggregate.portfolio_quality,this.state.aggregate.risk_volatility,this.state.aggregate.corelation_becnhmark,this.state.aggregate.asset_type)}
           </View>
}

{clienttype==="lead" &&<View style={{justifyContent:"center"}}>
<View style={{opacity:0.05}}>
 <View style={{alignItems:"center"}}>
          {this.renderStrength(this.state.aggregate.risk_score)}
           <Text style={{fontWeight:"bold",color:'black',fontSize:20,marginTop:15}}>{this.state.title}</Text>
           </View>
           {this.renderStrengthTooltip(this.state.aggregate.portfolio_quality,this.state.aggregate.risk_volatility,this.state.aggregate.corelation_becnhmark,this.state.aggregate.asset_type)}
           
           </View>
                    <Image source={require('../../assets/images/Complete-Your-Application.png')} style={{position:"absolute",resizeMode:"contain",alignItems:"center",alignSelf:"center"}}/>
           </View>
} 
           <View style={{marginTop:20,alignItems:"center"}}>

            <View style={{flexDirection:"row",flex:1}}>
              <Text style={{fontWeight:"bold",color:'black',fontSize:15,flex:1}}>Alpha</Text>
              <Text style={{color:'black',fontSize:15,flex:1}}>{this.state.risk_strength[3]}</Text>
            </View>
            <View style={{flexDirection:"row",flex:1}}>
              <Text style={{fontWeight:"bold",color:'black',fontSize:15,flex:1}}>Beta</Text>
              <Text style={{color:'black',fontSize:15,flex:1}}>{this.state.risk_strength[1]}</Text>
            </View>
            <View style={{flexDirection:"row",flex:1}}>
              <Text style={{fontWeight:"bold",color:'black',fontSize:15,flex:1}}>R-Squared</Text>
              <Text style={{color:'black',fontSize:15,flex:1}}>{this.state.risk_strength[5]}</Text>
            </View>
            <View style={{flexDirection:"row",flex:1}}>
              <Text style={{fontWeight:"bold",color:'black',fontSize:15,flex:1}}>Info Ratio</Text>
              <Text style={{color:'black',fontSize:15,flex:1}}>{this.state.risk_strength[7]}</Text>
            </View>
            <View style={{flexDirection:"row",flex:1}}>
              <Text style={{fontWeight:"bold",color:'black',fontSize:15,flex:1}}>Tracking Err</Text>
              <Text style={{color:'black',fontSize:15,flex:1}}>{this.state.risk_strength[6]}</Text>
            </View>
            <View style={{flexDirection:"row",flex:1}}>
              <Text style={{fontWeight:"bold",color:'black',fontSize:15,flex:1}}>Sortino</Text>
              <Text style={{color:'black',fontSize:15,flex:1}}>{this.state.risk_strength[4]}</Text>
            </View>
            <View style={{flexDirection:"row",flex:1}}>
              <Text style={{fontWeight:"bold",color:'black',fontSize:15,flex:1}}>Sharpe</Text>
              <Text style={{color:'black',fontSize:15,flex:1}}>{this.state.risk_strength[2]}</Text>
            </View>
            <View style={{flexDirection:"row",flex:1}}>
              <Text style={{fontWeight:"bold",color:'black',fontSize:15,flex:1}}>Std Dev</Text>
              <Text style={{color:'black',fontSize:15,flex:1}}>{this.state.risk_strength[0]}</Text>
            </View>
            <View style={{flexDirection:"row",flex:1}}>
              <Text style={{fontWeight:"bold",color:'black',fontSize:15,flex:1}}>Risk Color</Text>
              <Text style={{color:'black',fontSize:15,flex:1}}>{this.state.riskcolor}</Text>
            </View>
           </View>
      </Card>
      </ScrollView>
      
    );
  }
}


export default RiskStrength;