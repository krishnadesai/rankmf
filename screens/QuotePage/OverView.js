import React, { Component } from "react";
import {
  AsyncStorage,
  Image,
  Platform,
  ScrollView,
  Text,
  View
} from "react-native";
import { Card } from "react-native-elements";
import ChartView from "react-native-highcharts";
import { SlideAnimation } from "react-native-popup-dialog";
import { Actions } from "react-native-router-flux";
import styles from "../../components/styles";
import Apis from "../../constants/Apis";
import color from "../../constants/Colors";

const slideAnimation = new SlideAnimation({ slideFrom: "bottom" });

class Overview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      dialogShow: false,
      sampleDataState: "",
      response: [],
      category_data: [],
      aggregate: {},
      index: [],
      Objective: "",
      category: "",
      aum: [],
      expenceRatio: [],
      turnoverRatio: [],
      load: {},
      chartdata: [],
      navdata: ""
    };
  }

  componentDidMount() {
    //this.props.passRefUpward(this.refs);
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    // alert(this.props.unique_code);
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.2&access_token=${access_token}&user=${clientid}&unique_code=${
        this.props.unique_code
      }&type=nav` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson["data"]["expenceRatio"] != false) {
          this.setState({
            expenceRatio: responseJson["data"]["expenceRatio"][0]
          });
        } else {
          this.setState({ expenceRatio: responseJson["data"]["expenceRatio"] });
        }
        this.setState({
          response: responseJson["data"]["response"],
          category_data: responseJson["data"]["category_data"],
          aggregate: responseJson["data"]["aggregate"],
          index: responseJson["data"]["index"],
          Objective: responseJson["data"]["response"][0].OBJECTIVE,
          category: responseJson["data"]["category_data"][0].classname,
          aum: responseJson["data"]["aum"][0],
          load: responseJson["data"]["load"],
          low: responseJson["data"]["highLow52Week"][0].fTwoWeekLow,
          high: responseJson["data"]["highLow52Week"][0].fTwoWeekHigh
        });

        if (responseJson["data"]["turnoverRatio"] != false) {
          this.setState({
            turnoverRatio: responseJson["data"]["turnoverRatio"][0]
          });
        } else {
          this.setState({
            turnoverRatio: responseJson["data"]["turnoverRatio"]
          });
        }
        this.fetchChart();
      })
      .catch(error => {
        console.error(error);
      });
  }

  fetchChart() {
    // alert(`api=1.3&access_token=${access_token}&user=${clientid}&scheme_unique_code=${this.props.unique_code}&tab_type=nav&type=3&scheme_code=${this.state.aggregate.schemecode}&year_selector_container1=${this.props.chartyr}&classcode=${this.state.aggregate.classcode}&nav_since_select=${this.props.nav}`)
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.3&access_token=${access_token}&user=${clientid}&scheme_unique_code=${
        this.props.unique_code
      }&tab_type=nav&type=3&scheme_code=${
        this.state.aggregate.schemecode
      }&year_selector_container1=${this.props.chartyr}&classcode=${
        this.state.aggregate.classcode
      }&nav_since_select=${this.props.nav}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson["data"]["get_scheme_nav"] != false) {
          this.setState({
            navdata: responseJson["data"]["get_scheme_nav"][0].NAV
          });
        } else {
          this.setState({ navdata: responseJson["data"]["get_scheme_nav"] });
        }
        this.setState({ chartdata: responseJson["data"]["scheme_json"][0] });
      })
      .catch(error => {
        console.error(error);
      });
  }

  renderColor(item) {
    if (item < 0) {
      return color.RED;
    } else {
      return color.GREEN;
    }
  }

  render() {
    var Highcharts = "Highcharts";
    var conf = {
      chart: {
        type: "spline",
        animation: Highcharts.svg, // don't animate in old IE
        marginRight: 10,
        events: {
          load: function() {}
        }
      },
      credits: {
        enabled: false
      },
      title: {
        text: ""
      },
      xAxis: {
        type: "datetime",
        dateTimeLabelFormats: {
          // don't display the dummy year
          month: "%b %y"
        },
        tickPixelInterval: 50
      },
      yAxis: {
        title: "",
        plotLines: [
          {
            value: 0,
            width: 1,
            color: "#808080"
          }
        ]
      },
      tooltip: {
        headerFormat: "<b>{series.name}</b><br>",
        pointFormat: "{point.x:%e %b %y }: {point.y:.2f} "
      },
      plotOptions: {
        spline: {
          marker: {
            enabled: false
          }
        }
      },
      legend: {
        enabled: false
      },
      exporting: {
        enabled: false
      },
      series: this.state.chartdata
    };

    const options = {
      global: {
        useUTC: false
      },
      lang: {
        decimalPoint: ".",
        thousandsSep: ","
      }
    };

    return (
      <ScrollView
        style={styles.containerFlex}
        contentInsetAdjustmentBehavior="automatic"
        
      >
        <View>
          <Card title="Investment Objective" >
            <Text>{this.state.Objective} </Text>
          </Card>
          <Card>
            <Text
              style={{ fontWeight: "bold", fontSize: 17, color: "#404040" }}
            >
              Category :{" "}
              <Text style={styles.carddetail}>{this.state.category}</Text>{" "}
            </Text>
          </Card>
          <Card>
            <View style={{ flexDirection: "column" }}>
              <Text
                style={{ fontWeight: "bold", fontSize: 17, color: "#404040" }}
              >
                Benchmark :{" "}
              </Text>

              {this.state.index.map((item, i) => (
                <Text
                  style={{
                    fontWeight: "normal",
                    fontSize: 13,
                    color: "#000000"
                  }}
                >
                  {item.IndexName}
                </Text>
              ))}
            </View>
          </Card>
          <Card title="Returns">
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <Text style={{ flex: 1 }}>1M</Text>
                <Text
                  style={{
                    color: this.renderColor(this.state.aggregate.oneMONTHRET),
                    flex: 1
                  }}
                >
                  {this.state.aggregate.oneMONTHRET != ""
                    ? parseFloat(this.state.aggregate.oneMONTHRET).toFixed(2)
                    : "-"}
                  %
                </Text>
              </View>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <Text style={{ flex: 1 }}>1Y</Text>
                <Text
                  style={{
                    color: this.renderColor(this.state.aggregate.oneYRRET),
                    flex: 1
                  }}
                >
                  {this.state.aggregate.oneYRRET != ""
                    ? parseFloat(this.state.aggregate.oneYRRET).toFixed(2)
                    : "-"}
                  %
                </Text>
              </View>
            </View>
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <Text style={{ flex: 1 }}>3M</Text>
                <Text
                  style={{
                    color: this.renderColor(this.state.aggregate.threeMONTHRET),
                    flex: 1
                  }}
                >
                  {this.state.aggregate.threeMONTHRET != ""
                    ? parseFloat(this.state.aggregate.threeMONTHRET).toFixed(2)
                    : "-"}
                  %
                </Text>
              </View>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <Text style={{ flex: 1 }}>3Y</Text>
                <Text
                  style={{
                    color: this.renderColor(this.state.aggregate.threeYEARRET),
                    flex: 1
                  }}
                >
                  {this.state.aggregate.threeYEARRET != ""
                    ? parseFloat(this.state.aggregate.threeYEARRET).toFixed(2)
                    : "-"}
                  %
                </Text>
              </View>
            </View>
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <Text style={{ flex: 1 }}>6M</Text>
                <Text
                  style={{
                    color: this.renderColor(this.state.aggregate.sixMONTHRET),
                    flex: 1
                  }}
                >
                  {this.state.aggregate.sixMONTHRET != ""
                    ? parseFloat(this.state.aggregate.sixMONTHRET).toFixed(2)
                    : "-"}
                  %
                </Text>
              </View>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <Text style={{ flex: 1 }}>5Y</Text>
                <Text
                  style={{
                    color: this.renderColor(this.state.aggregate.fiveYEARRET),
                    flex: 1
                  }}
                >
                  {this.state.aggregate.fiveYEARRET != ""
                    ? parseFloat(this.state.aggregate.fiveYEARRET).toFixed(2)
                    : "-"}
                  %
                </Text>
              </View>
            </View>
          </Card>
          <View>
            <Card>
              <View style={{ flexDirection: "column" }}>
                <Text
                  style={{ fontWeight: "bold", fontSize: 17, color: "#404040" }}
                >
                  Historical NAV :{" "}
                </Text>

                <Text
                  style={{
                    fontWeight: "normal",
                    fontSize: 13,
                    color: "#000000",
                    marginTop: 5
                  }}
                >
                  NAV for scheme {this.state.aggregate.Scheme_Name}
                  <Text
                    style={styles.input}
                    onPress={() => Actions.page1({ dialog: "nav" })}
                  >
                    {" "}
                    {this.props.initpriced}
                    <Image
                      source={require("../../assets/images/drop-down-arrow.png")}
                      style={{
                        height: Platform.OS == "ios" ? 10 : 30,
                        width: Platform.OS == "ios" ? 10 : 30,
                        resizeMode: "contain",
                        alignitemcharts: "center",
                        marginLeft: Platform.OS == "ios" ? 0 : 10
                      }}
                    />
                  </Text>{" "}
                  is {"\u20B9"}
                  {this.state.navdata}
                </Text>
              </View>
            </Card>

            <Card title="Growth of 10000 since inception of the fund">
              <Text
                style={styles.input}
                onPress={() => Actions.page1({ dialog: "OverViewGro" })}
              >
                {this.props.throttlemode}
              </Text>

              <View style={styles.itemchart}>
                <ChartView
                  style={{ height: 300 }}
                  config={conf}
                  options={options}
                />
              </View>
            </Card>
          </View>

          <Card>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 17,
                  color: "#404040",
                  flex: 1
                }}
              >
                AUM{" "}
              </Text>
              <Text
                style={{
                  fontWeight: "normal",
                  fontSize: 13,
                  color: "green",
                  textAlign: "right",
                  alignItems: "flex-end",
                  flex: 1
                }}
              >
                {" "}
                {"\u20B9"}
                {parseFloat(this.state.aum.Total).toFixed(2)} Cr{" "}
              </Text>
            </View>
            <Text
              style={{
                fontWeight: "normal",
                fontSize: 13,
                color: "#000000",
                marginTop: 5
              }}
            >
              {this.state.aggregate.Scheme_Name} AUM is{" "}
              {parseFloat(this.state.aum.aum_perc).toFixed(0)}% higher than
              other schemes in the category
            </Text>
          </Card>

          <Card>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 17,
                  color: "#404040",
                  flex: 1
                }}
              >
                Expense Ratio{" "}
              </Text>
              <Text
                style={{
                  fontWeight: "normal",
                  fontSize: 13,
                  color: "green",
                  textAlign: "right",
                  alignItems: "flex-end",
                  flex: 1
                }}
              >
                {this.state.expenceRatio != false
                  ? parseFloat(this.state.expenceRatio.expratio).toFixed(2) +
                    `%`
                  : `NA`}{" "}
              </Text>
            </View>
            {this.state.expenceRatio != false ? (
              <Text
                style={{
                  fontWeight: "normal",
                  fontSize: 13,
                  color: "#000000",
                  marginTop: 5
                }}
              >
                {this.state.aggregate.Scheme_Name} Expense Ratio is{" "}
                {parseFloat(this.state.expenceRatio.expratio_perc).toFixed(0)}%
                lower than other schemes in the category
              </Text>
            ) : null}
          </Card>

          <Card>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: 17,
                  color: "#404040",
                  flex: 1
                }}
              >
                Turnover Ratio{" "}
              </Text>
              <Text
                style={{
                  fontWeight: "normal",
                  fontSize: 13,
                  color: "green",
                  textAlign: "right",
                  alignItems: "flex-end",
                  flex: 1
                }}
              >
                {this.state.turnoverRatio != false
                  ? parseFloat(this.state.turnoverRatio.TURNOVER_RATIO).toFixed(
                      2
                    ) + `%`
                  : `NA`}{" "}
              </Text>
            </View>
            {this.state.turnoverRatio != false ? (
              <Text
                style={{
                  fontWeight: "normal",
                  fontSize: 13,
                  color: "#000000",
                  marginTop: 5
                }}
              >
                {this.state.aggregate.Scheme_Name} Turnover Ratio is{" "}
                {parseFloat(this.state.turnoverRatio.turnover_perc).toFixed(0)}%
                higher than other schemes in the category
              </Text>
            ) : null}
          </Card>

          <Card>
            <Text
              style={{
                fontWeight: "bold",
                fontSize: 17,
                color: "#404040",
                flex: 1
              }}
            >
              Load{" "}
            </Text>
            <View style={{ flexDirection: "column", marginTop: 5 }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <Text style={{ flex: 1 }}>Entry Load:</Text>
                <Text style={{ color: color.BLACK, flex: 1 }}>
                  {this.state.load.ENTRYLOAD === "0"
                    ? "NIL"
                    : this.state.load.ENTRYLOAD}
                </Text>
              </View>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <Text style={{ flex: 1 }}>Exit Load:</Text>
                <Text style={{ color: color.BLACK, flex: 1 }}>
                  {this.state.load.REMARKS}
                </Text>
              </View>
            </View>
          </Card>
        </View>
      </ScrollView>
    );
  }
}

export default Overview;
