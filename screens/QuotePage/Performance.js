import React, { Component } from "react";
import {
  Dimensions,
  Platform,
  Text,
  View,
  ScrollView,
  Image,
  Picker,
  TouchableOpacity,
  AsyncStorage,
  FlatList
} from "react-native";
import { Card, Button } from "react-native-elements";
import PopupDialog, {
  DialogTitle,
  SlideAnimation
} from "react-native-popup-dialog";
import ChartView from "react-native-highcharts";
import Autocomplete from "react-native-autocomplete-input";
import SimpleTable from "../../components/SimpleTable";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";
import { Actions } from "react-native-router-flux";
const like = require("../../assets/images/thumbsup.png");
const dislike = require("../../assets/images/thumbsdown.png");

const slideAnimation = new SlideAnimation({ slideFrom: "bottom" });
const DEVICE_HEIGHT = Dimensions.get("window").height;
const DEVICE_WIDTH = Dimensions.get("window").width;
const clienttype = "";

class Performance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      dialogShow: false,
      backtable: "#D3D3D3",
      backchart: color.toolbar,
      chartortable: "chart",
      charthide: false,
      tablehide: true,
      chartdata: [],
      aggregate: {},
      avgChart: [],
      avgTableColumns: [{ title: "", dataIndex: "head", width: 90, index: 0 }],
      avgTableData: [],
      schemedata: [],
      categorydata: [],
      financialdata: [],
      performancedata: {},
      border: "",
      good_bad: "",
      good_bad_ic: "",
      categoryfunddata: [],
      compare_scheme: [],
      text1: "",
      unique_codes: [],
      compare_scheme_text: [],
      avgfunddata: [],
      avg_scheme: [],
      text2: "",
      unique_codes_avg: [],
      avg_scheme_text: [],
      finfunddata: [],
      fin_scheme: [],
      text3: "",
      unique_codes_fin: [],
      fin_scheme_text: []
    };
  }

  componentDidMount() {
    //this.props.passRefUpward(this.refs);
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      if (clienttype === "client") {
        this.setState({ showFund: true });
      } else {
        this.setState({ showFund: false });
      }
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    // alert(this.props.unique_code);
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.2&access_token=${access_token}&user=${clientid}&unique_code=${
        this.props.unique_code
      }&type=returns_and_performance` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({ aggregate: responseJson["data"]["aggregate"] });
        this.fetchLineChart();
        this.fetchPerformanceChart();
      })
      .catch(error => {
        console.error(error);
      });
  }

  searchCategoryFund(text) {
    if (text.length >= 3) {
      fetch(Apis.BaseURL, {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
        }),
        body: `api=1.4&access_token=${access_token}&user=${clientid}&keyword=${text.toUpperCase()}&classcode=${
          this.state.aggregate.classcode
        }&schemes=${this.props.unique_code}&asset_type=${
          this.state.aggregate.asset_type
        }` // <-- Post parameters
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({ categoryfunddata: responseJson["data"]["data"] });

          //alert(responseJson["data"])
        })
        .catch(error => {
          console.error(error);
        });
    }
    //this.setState({text1:""})
  }

  renderAutoComplete(text, id, asset_type) {
    // alert(text)
    return (
      <TouchableOpacity
        onPress={() => {
          if (this.state.compare_scheme_text.indexOf(text) === -1) {
            if (this.state.compare_scheme.length < 3) {
              this.state.compare_scheme_text.push(text);
              this.state.compare_scheme.push(asset_type);
              this.state.unique_codes.push(id);

              this.setState({ text1: "" });
              this.searchCategoryFund("no match");
            } else {
              alert("Maximum 3 schemes can be compared");
              this.searchCategoryFund("no match");
            }
          } else {
            alert(`${text} is already added`);
          }
          
          setTimeout(() => {
            //  this.props.navigation.navigate('TutorialSlider');

            this.fetchLineChart();
          }, 1000);
        }}
      >
        <Text style={{ margin: 5, fontSize: 10 }}>{text}</Text>
        <View style={styles.dash} />
      </TouchableOpacity>
    );
  }

  renderRowLine(item, index) {
    return (
      <View
        style={{
          backgroundColor: color.bgcolor,
          flexDirection: "row",
          borderRadius: 5,
          margin: 3
        }}
      >
        <Text style={[styles.flexboxtext, { flex: 1.8, fontSize: 10 }]}>
          {item}
        </Text>
        <TouchableOpacity
          style={{ flex: 0.2, marginLeft: 5, marginTop: 5 }}
          onPress={() => {
            this.state.compare_scheme.splice(index, 1);
            this.state.compare_scheme_text.pop(item);
            this.state.unique_codes.splice(index, 1);
            setTimeout(() => {
              this.fetchLineChart();
            }, 1000);
          }}
        >
          <Image
            source={require("../../assets/images/cross.png")}
            style={{ height: 10, width: 10, resizeMode: "contain" }}
          />
        </TouchableOpacity>
      </View>
    );
  }

  fetchLineChart() {
    var compare_scheme_container = "";

    this.state.compare_scheme.map((item, i) => {
      compare_scheme_container += `compare_scheme_container1[${i}]=${item}&`;
    });
    //  compare_scheme_container.substring(0, compare_scheme_container.length-1)

    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.3&access_token=${access_token}&user=${clientid}&scheme_unique_code=${
        this.props.unique_code
      }&tab_type=returns_and_performance&type=3&scheme_code=${
        this.state.aggregate.schemecode
      }&${compare_scheme_container}unique_codes=${
        this.state.unique_codes
      }&year_selector_container1=${this.props.chartyr}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        // alert(responseJson["scheme_json"][0])
        this.setState({ chartdata: responseJson["data"]["scheme_json"][0] });
        this.fetchAvgChart();
        this.fetchFinanceChart();
      })
      .catch(error => {
        console.error(error);
      });
  }

  fetchAvgChart() {
    var compare_scheme_container = "";
    this.setState({
      avgTableColumns: [{ title: "", dataIndex: "head", width: 90, index: 0 }],
      avgTableData: []
    });

    this.state.avg_scheme.map((item, i) => {
      compare_scheme_container += `compare_scheme[${i}]=${item}&`;
    });

    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.3&access_token=${access_token}&user=${clientid}&scheme_unique_code=${
        this.props.unique_code
      }&tab_type=returns_and_performance&type=1&scheme_code=${
        this.state.aggregate.schemecode
      }&${compare_scheme_container}unique_codes=${
        this.state.unique_codes_avg
      }&period=${this.props.per_period}&classcode=${
        this.state.aggregate.classcode
      }` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        for (key in responseJson["data"]["scheme_json"]) {
          this.setState({ avgChart: responseJson["data"]["scheme_json"][key] });
        }

        //this.setState({schemedata:this.state.avgChart[0][0].data,categorydata:this.state.avgChart[0][1].data})
        if (this.state.avgChart.length > 0) {
          this.state.avgChart[0].map((item, i) => {
            var key = item.name.split(" ")[0];
            var colu = {
              title: item.name,
              dataIndex: key,
              width: 110,
              index: i + 1
            };

            this.state.avgTableColumns.push(colu);
          });

          this.state.avgChart[1].map((item, i) => {
            var data = {};
            data["head"] = item;
            this.state.avgChart[0].map((item1, index) => {
              var key = item1.name.split(" ")[0];
              data[key] = parseFloat(item1.data[i]).toFixed(2);
            });
            this.state.avgTableData.push(data);
          });

          this.setState({ chartortable: "chart" });
          this.ChangeChartView;
        }
       // alert(this.state.avgTableData.length)
      })
      .catch(error => {
        console.error(error);
      });
  }

  searchAvgFund(text) {
    if (text.length >= 3) {
      fetch(Apis.BaseURL, {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
        }),
        body: `api=1.4&access_token=${access_token}&user=${clientid}&keyword=${text.toUpperCase()}&schemes=${
          this.props.unique_code
        }` // <-- Post parameters
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({ avgfunddata: responseJson["data"]["data"] });

          // alert(responseJson["data"])
        })
        .catch(error => {
          console.error(error);
        });
    }
    this.setState({ text2: "" });
  }

  renderAutoCompleteAvg(text, id, asset_type) {
    // alert(text)
    return (
      <TouchableOpacity
        onPress={() => {
          if (this.state.avg_scheme_text.indexOf(text) === -1) {
            if (this.state.avg_scheme.length < 3) {
              this.state.avg_scheme_text.push(text);
              this.state.avg_scheme.push(asset_type);
              this.state.unique_codes_avg.push(id);

              this.setState({ text1: "" });
              this.searchAvgFund("no match");
            } else {
              alert("Maximum 3 schemes can be compared");
              this.searchAvgFund("no match");
            }
          } else {
            alert(`${text} is already added`);
          }

          setTimeout(() => {
            //  this.props.navigation.navigate('TutorialSlider');

            this.fetchAvgChart();
          }, 1000);
        }}
      >
        <Text style={{ margin: 5, fontSize: 10 }}>{text}</Text>
        <View style={styles.dash} />
      </TouchableOpacity>
    );
  }

  renderRowLineAvg(item, index) {
    return (
      <View
        style={{
          backgroundColor: color.bgcolor,
          flexDirection: "row",
          borderRadius: 5,
          margin: 3
        }}
      >
        <Text style={[styles.flexboxtext, { flex: 1.8, fontSize: 10 }]}>
          {item}
        </Text>
        <TouchableOpacity
          style={{ flex: 0.2, marginLeft: 5, marginTop: 5 }}
          onPress={() => {
            this.state.avg_scheme.splice(index, 1);
            this.state.avg_scheme_text.pop(item);
            this.state.unique_codes_avg.splice(index, 1);
            setTimeout(() => {
              this.fetchAvgChart();
            }, 1000);
          }}
        >
          <Image
            source={require("../../assets/images/cross.png")}
            style={{ height: 10, width: 10, resizeMode: "contain" }}
          />
        </TouchableOpacity>
      </View>
    );
  }

  fetchFinanceChart() {
    var compare_scheme_container = "";

    this.state.fin_scheme.map((item, i) => {
      compare_scheme_container += `compare_scheme_year[${i}]=${item}&`;
    });

    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.3&access_token=${access_token}&user=${clientid}&scheme_unique_code=${
        this.props.unique_code
      }&tab_type=returns_and_performance&type=2&scheme_code=${
        this.state.aggregate.schemecode
      }&${compare_scheme_container}unique_codes=${
        this.state.unique_codes_fin
      }&period_year=${this.props.period_year}&classcode=${
        this.state.aggregate.classcode
      }` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        for (key in responseJson["data"]["scheme_json"]) {
          this.setState({
            financialdata: responseJson["data"]["scheme_json"][key]
          });
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  searchFinFund(text) {
    if (text.length >= 3) {
      fetch(Apis.BaseURL, {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
        }),
        body: `api=1.4&access_token=${access_token}&user=${clientid}&keyword=${text.toUpperCase()}&schemes=${
          this.props.unique_code
        }` // <-- Post parameters
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({ finfunddata: responseJson["data"]["data"] });

          // alert(responseJson["data"])
        })
        .catch(error => {
          console.error(error);
        });
    }
    this.setState({ text3: "" });
  }

  renderAutoCompleteFin(text, id, asset_type) {
    // alert(text)
    return (
      <TouchableOpacity
        onPress={() => {
          if (this.state.fin_scheme_text.indexOf(text) === -1) {
            if (this.state.fin_scheme.length < 3) {
              this.state.fin_scheme_text.push(text);
              this.state.fin_scheme.push(asset_type);
              this.state.unique_codes_fin.push(id);

              this.setState({ text1: "" });
              this.searchFinFund("no match");
            } else {
              alert("Maximum 3 schemes can be compared");
              this.searchFinFund("no match");
            }
          } else {
            alert(`${text} is already added`);
          }

          setTimeout(() => {
            //  this.props.navigation.navigate('TutorialSlider');

            this.fetchFinanceChart();
          }, 1000);
        }}
      >
        <Text style={{ margin: 5, fontSize: 10 }}>{text}</Text>
        <View style={styles.dash} />
      </TouchableOpacity>
    );
  }

  renderRowLineFin(item, index) {
    return (
      <View
        style={{
          backgroundColor: color.bgcolor,
          flexDirection: "row",
          borderRadius: 5,
          margin: 3
        }}
      >
        <Text style={[styles.flexboxtext, { flex: 1.8, fontSize: 10 }]}>
          {item}
        </Text>
        <TouchableOpacity
          style={{ flex: 0.2, marginLeft: 5, marginTop: 5 }}
          onPress={() => {
            this.state.fin_scheme.splice(index, 1);
            this.state.fin_scheme_text.pop(item);
            this.state.unique_codes_fin.splice(index, 1);
            setTimeout(() => {
              this.fetchFinanceChart();
            }, 1000);
          }}
        >
          <Image
            source={require("../../assets/images/cross.png")}
            style={{ height: 10, width: 10, resizeMode: "contain" }}
          />
        </TouchableOpacity>
      </View>
    );
  }

  fetchPerformanceChart() {
    // alert(this.state.period_year)
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.3&access_token=${access_token}&user=${clientid}&scheme_unique_code=${
        this.props.unique_code
      }&tab_type=returns_and_performance&type=4&scheme_code=${
        this.state.aggregate.schemecode
      }&dateYear=${this.props.dateYear}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({ performancedata: responseJson });

        // alert(responseJson)

        if (
          parseFloat(this.state.performancedata.gainAfter10Years) / 100 >
          parseFloat(this.state.performancedata.indexgainAfter) / 100
        ) {
          this.setState({
            good_bad: "Good",
            good_bad_ic: like,
            border: color.GREEN
          });
        } else {
          this.setState({
            good_bad: "Bad",
            good_bad_ic: dislike,
            border: color.RED
          });
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  ChangeChartView = () => {
    if (this.state.chartortable === "chart") {
      this.setState({ chartortable: "table" });
      this.setState({ tablehide: false });
      this.setState({ charthide: true });
      this.setState({ backtable: color.toolbar });
      this.setState({ backchart: "#D3D3D3" });
    } else if (this.state.chartortable === "table") {
      this.setState({ chartortable: "chart" });
      this.setState({ backchart: color.toolbar });
      this.setState({ backtable: "#D3D3D3" });
      this.setState({ tablehide: true });
      this.setState({ charthide: false });
    }
  };

  render() {
    var Highcharts = "Highcharts";
    var configwaterfall = {
      chart: {
        type: "waterfall"
      },
      credits: {
        enabled: false
      },
      title: {
        text: ""
      },

      xAxis: {
        type: "category"
      },

      yAxis: {
        title: {
          text: ""
        }
      },

      legend: {
        enabled: false
      },

      tooltip: {
        pointFormat: "<b>Rs.{point.y:,.2f}</b> "
      },
      exporting: {
        enabled: false
      },
      series: [
        {
          data: [
            {
              name: `Your investment ${this.props.performance} ago`,
              y: this.state.performancedata.starNav,
              color: "#3b71b6"
            },
            {
              name: `Profit/Loss in ${this.props.performance}`,
              y: this.state.performancedata.gainAfter10Years,
              color: "#fdd835"
            },
            {
              name: "Todays value",
              isSum: true,
              color: "#30bf46"
            }
          ],
          dataLabels: {
            enabled: true,
            formatter: function() {
              return Highcharts.numberFormat(this.y);
            },
            style: {
              fontWeight: "bold"
            }
          },
          pointPadding: 0
        }
      ]
    };

    var configwaterfallindex = {
      chart: {
        type: "waterfall"
      },
      credits: {
        enabled: false
      },
      title: {
        text: ""
      },

      xAxis: {
        type: "category"
      },

      yAxis: {
        title: {
          text: ""
        }
      },

      legend: {
        enabled: false
      },

      tooltip: {
        pointFormat: "<b>Rs.{point.y:,.2f}</b> "
      },
      exporting: {
        enabled: false
      },
      series: [
        {
          data: [
            {
              name: `Your investment ${this.props.performance} ago`,
              y: this.state.performancedata.starNav,
              color: "#3b71b6"
            },
            {
              name: `Profit/Loss in ${this.props.performance}`,
              y: this.state.performancedata.indexgainAfter,
              color: "#fdd835"
            },
            {
              name: "Todays value",
              isSum: true,
              color: "#30bf46"
            }
          ],
          dataLabels: {
            enabled: true,
            formatter: function() {
              return Highcharts.numberFormat(this.y);
            },
            style: {
              fontWeight: "bold"
            }
          },
          pointPadding: 0
        }
      ]
    };
    var conf = {
      chart: {
        type: "spline",
        animation: Highcharts.svg, // don't animate in old IE
        marginRight: 10,
        events: {
          load: function() {}
        }
      },
      credits: {
        enabled: false
      },
      title: {
        text: ""
      },
      xAxis: {
        type: "datetime",
        dateTimeLabelFormats: {
          month: "%b %y"
        },
        tickPixelInterval: 50
      },
      yAxis: {
        title: "",
        plotLines: [
          {
            value: 0,
            width: 1,
            color: "#808080"
          }
        ]
      },
      tooltip: {
        headerFormat: "<b>{series.name}</b><br>",
        pointFormat: "{point.x:%e %b %y }: {point.y:.2f} "
      },
      plotOptions: {
        spline: {
          marker: {
            enabled: false
          }
        }
      },
      legend: {
        enabled: true
      },
      exporting: {
        enabled: false
      },
      series: this.state.chartdata
    };

    var myChart = {
      chart: {
        type: "bar"
      },
      title: {
        text: ""
      },
      credits: {
        enabled: false
      },
      chart: {
        type: "column"
      },
      tooltip: {
        headerFormat: "<b>{series.name}</b><br>",
        pointFormat: "{point.y:,.2f}% "
      },
      xAxis: {
        categories: this.state.avgChart[1]
      },
      yAxis: {
        title: {
          text: "Return(%)"
        }
      },
      exporting: {
        enabled: false
      },
      legend: {
        enabled: true
      },
      series: this.state.avgChart[0]
    };

    var myChartfinancial = {
      chart: {
        type: "bar"
      },
      credits: {
        enabled: false
      },
      title: {
        text: ""
      },
      chart: {
        type: "column"
      },
      legend: {
        enabled: true
      },
      tooltip: {
        headerFormat: "<b>{series.name}</b><br>",
        pointFormat: "{point.y:,.2f}% "
      },
      xAxis: {
        categories: this.state.financialdata[1]
      },
      yAxis: {
        title: {
          text: "Return(%)"
        }
      },
      exporting: {
        enabled: false
      },
      series: this.state.financialdata[0]
    };

    const options = {
      global: {
        useUTC: false
      },
      lang: {
        decimalPoint: ".",
        thousandsSep: ","
      }
    };

    return (
      <ScrollView ref="_ScrollView">
        <View>
          <Card>
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  fontWeight: "bold",
                  color: "black",
                  fontSize: 15,
                  flex: 1
                }}
              >
                Growth of {"\u20B9"}10,000
              </Text>
            </View>
            <Autocomplete
            ref={(c) => this.ac1 =c}
              autoCapitalize="none"
              clearButtonMode={'always'}
              autoCorrect={false}
              clearTextOnFocus={true}
              containerStyle={{
                borderBottomColor: Platform.OS == "ios" ? "black" : color.WHITE,
                borderBottomWidth: Platform.OS == "ios" ? 1 : 0,
                textAlign: "center"
              }}
              defaultValue={this.state.text1}
              data={this.state.categoryfunddata}
              listContainerStyle={{ position: "relative" }}
              onChangeText={text => this.searchCategoryFund(text)}
              placeholder="Add Category Fund"
              renderItem={({ text, id, asset_type }) =>
                this.renderAutoComplete(text, id, asset_type)
              }
            />

            {this.state.compare_scheme.length > 0 ? (
              <FlatList
                scrollEnabled={false}
                showsVerticalScrollIndicator={false}
                style={{
                  marginTop: 10,
                  height: this.state.compare_scheme_text.length * 40,
                  minHeight: this.state.compare_scheme_text.length * 40
                }}
                data={this.state.compare_scheme_text}
                renderItem={({ item, index }) =>
                  this.renderRowLine(item, index)
                }
              />
            ) : null}

            <View style={{ marginTop: 10, flexDirection: "column" }}>
              <Text style={{ color: "black", fontSize: 13 }}>
                How your investments would have grown in
              </Text>
              <Text
                onPress={() => Actions.page1({ dialog: "line" })}
                style={[styles.input]}
              >
                {this.props.line}
              </Text>
            </View>
            <View style={styles.itemchart}>
              <ChartView
                style={{ height: 300 }}
                config={conf}
                options={options}
              />
            </View>

            <Text
              style={{
                fontWeight: "bold",
                color: "black",
                fontSize: 15,
                marginTop: 15
              }}
            >
              Performance of the Fund
            </Text>
            {clienttype === "client" && (
              <View>
                <View style={{ marginTop: 10, flexDirection: "column" }}>
                  <Text style={{ color: "black", fontSize: 13 }}>
                    How your investments would have grown in
                  </Text>
                  <Text
                    onPress={() => Actions.page1({ dialog: "performance" })}
                    style={[styles.input]}
                  >
                    {this.props.performance}
                  </Text>
                </View>

                <View style={{ paddingRight: 5, marginTop: 15 }}>
                  <View
                    style={{
                      borderColor: this.state.border,
                      borderWidth: 1,
                      marginTop: 10
                    }}
                  >
                    <View
                      style={[
                        styles.itemchart,
                        { backgroundColor: "white", marginTop: 20 }
                      ]}
                    >
                      <Text
                        style={{
                          color: "black",
                          fontSize: 11,
                          marginBottom: 10,
                          textAlign: "center"
                        }}
                      >
                        {this.state.aggregate.Scheme_Name}{" "}
                        <Text style={{ color: "green", fontSize: 11 }}>
                          {"\t"}
                          {parseFloat(
                            this.state.performancedata.gainAfter10Years / 100
                          ).toFixed(2) > 0
                            ? `+` +
                              parseFloat(
                                this.state.performancedata.gainAfter10Years /
                                  100
                              ).toFixed(2)
                            : parseFloat(
                                this.state.performancedata.gainAfter10Years /
                                  100
                              ).toFixed(2)}
                          %
                        </Text>
                      </Text>

                      <ChartView
                        style={{ height: 300, width: 300 }}
                        config={configwaterfall}
                        options={options}
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      backgroundColor: this.state.border,
                      borderRadius: 10,
                      flexDirection: "row",
                      position: "absolute",
                      right: 0,
                      top: 0
                    }}
                  >
                    <Text style={{ color: "white", fontSize: 10, padding: 5 }}>
                      {this.state.good_bad} Performance
                    </Text>
                    <Image
                      source={this.state.good_bad_ic}
                      style={{
                        height: 10,
                        width: 10,
                        resizeMode: "contain",
                        paddingLeft: 2,
                        marginTop: 5,
                        marginRight: 5
                      }}
                    />
                  </View>
                </View>
                <View style={[styles.itemchart, { backgroundColor: "white" }]}>
                  <Text
                    style={{
                      color: "black",
                      fontSize: 11,
                      marginTop: 20,
                      marginBottom: 10,
                      textAlign: "center"
                    }}
                  >
                    {this.state.performancedata.index_name}
                    <Text style={{ color: "green", fontSize: 11 }}>
                      {"\t"}
                      {parseFloat(
                        this.state.performancedata.indexgainAfter / 100
                      ).toFixed(2) > 0
                        ? `+` +
                          parseFloat(
                            this.state.performancedata.indexgainAfter / 100
                          ).toFixed(2)
                        : parseFloat(
                            this.state.performancedata.indexgainAfter / 100
                          ).toFixed(2)}
                      %
                    </Text>
                  </Text>
                  <ChartView
                    style={{ height: 300 }}
                    config={configwaterfallindex}
                    options={options}
                  />
                </View>
              </View>
            )}

            {clienttype === "lead" && (
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <View
                  style={{ opacity: Platform.OS == "android" ? 0.3 : 0.05 }}
                >
                  <View style={{ marginTop: 10, flexDirection: "row" }}>
                    <Text style={{ color: "black", fontSize: 13 }}>
                      How your investments would have grown in
                    </Text>
                    <Text style={[styles.input, { marginLeft: 10 }]}>
                      {this.props.performance}
                    </Text>
                  </View>

                  <View style={{ paddingRight: 5, marginTop: 15 }}>
                    <View
                      style={{
                        borderColor: this.state.border,
                        borderWidth: 1,
                        marginTop: 10
                      }}
                    >
                      <View
                        style={[
                          styles.itemchart,
                          { backgroundColor: "white", marginTop: 20 }
                        ]}
                      >
                        <Text
                          style={{
                            color: "black",
                            fontSize: 11,
                            marginBottom: 10,
                            textAlign: "center"
                          }}
                        >
                          {this.state.aggregate.Scheme_Name}{" "}
                          <Text style={{ color: "green", fontSize: 11 }}>
                            {"\t"}
                            {parseFloat(
                              this.state.performancedata.gainAfter10Years / 100
                            ).toFixed(2) > 0
                              ? `+` +
                                parseFloat(
                                  this.state.performancedata.gainAfter10Years /
                                    100
                                ).toFixed(2)
                              : parseFloat(
                                  this.state.performancedata.gainAfter10Years /
                                    100
                                ).toFixed(2)}
                            %
                          </Text>
                        </Text>

                        <ChartView
                          style={{
                            height: 300,
                            width: 300,
                            opacity: Platform.OS == "android" ? 0.3 : 0.05
                          }}
                          config={configwaterfall}
                          options={options}
                        />
                      </View>
                    </View>
                    <View
                      style={{
                        backgroundColor: this.state.border,
                        borderRadius: 10,
                        flexDirection: "row",
                        position: "absolute",
                        right: 0,
                        top: 0
                      }}
                    >
                      <Text
                        style={{ color: "white", fontSize: 10, padding: 5 }}
                      >
                        {this.state.good_bad} Performance
                      </Text>
                      <Image
                        source={this.state.good_bad_ic}
                        style={{
                          height: 10,
                          width: 10,
                          resizeMode: "contain",
                          paddingLeft: 2,
                          marginTop: 5,
                          marginRight: 5
                        }}
                      />
                    </View>
                  </View>
                  <View
                    style={[styles.itemchart, { backgroundColor: "white" }]}
                  >
                    <Text
                      style={{
                        color: "black",
                        fontSize: 11,
                        marginTop: 20,
                        marginBottom: 10,
                        textAlign: "center"
                      }}
                    >
                      {this.state.performancedata.index_name}
                      <Text style={{ color: "green", fontSize: 11 }}>
                        {"\t"}
                        {parseFloat(
                          this.state.performancedata.indexgainAfter / 100
                        ).toFixed(2) > 0
                          ? `+` +
                            parseFloat(
                              this.state.performancedata.indexgainAfter / 100
                            ).toFixed(2)
                          : parseFloat(
                              this.state.performancedata.indexgainAfter / 100
                            ).toFixed(2)}
                        %
                      </Text>
                    </Text>

                    <ChartView
                      style={{
                        height: 300,
                        opacity:
                          Platform.OS == "android" &&
                          (Platform.OS == "android" ? 0.3 : 0.05)
                      }}
                      config={configwaterfallindex}
                      options={options}
                    />
                  </View>
                </View>
                <View
                  style={{
                    position: "absolute",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Image
                    source={require("../../assets/images/Complete-Your-Application.png")}
                  />
                </View>
              </View>
            )}
            <Text
              style={{
                fontWeight: "bold",
                color: "black",
                fontSize: 15,
                marginTop: 15
              }}
            >
              What are the average return of the fund and how do they compare?
            </Text>
            {clienttype === "client" && (
              <View>
                <View style={{ flexDirection: "row" }}>
                  <Text
                    onPress={() => Actions.page1({ dialog: "bar1" })}
                    style={[
                      styles.input,
                      { marginLeft: 5, marginTop: 10, flex: 1 }
                    ]}
                  >
                    {this.props.bar1}
                  </Text>
                </View>
                <Autocomplete
                  autoCapitalize="none"
                  autoCorrect={false}
                  clearButtonMode="while-editing"
                  clearTextOnFocus={true}
                  containerStyle={{
                    borderBottomColor:
                      Platform.OS == "ios" ? "black" : color.WHITE,
                    borderBottomWidth: Platform.OS == "ios" ? 1 : 0,
                    textAlign: "center"
                  }}
                  data={this.state.avgfunddata}
                  listContainerStyle={{ position: "relative" }}
                  onChangeText={text => this.searchAvgFund(text)}
                  placeholder="Add To Compare"
                  renderItem={({ text, id, asset_type }) =>
                    this.renderAutoCompleteAvg(text, id, asset_type)
                  }
                />
                {this.state.avg_scheme.length > 0 && (
                  <View>
                    <FlatList
                      scrollEnabled={false}
                      showsVerticalScrollIndicator={false}
                      style={{
                        marginTop: 10,
                        height: this.state.avg_scheme_text.length * 40,
                        minHeight: this.state.avg_scheme_text.length * 40
                      }}
                      data={this.state.avg_scheme_text}
                      renderItem={({ item, index }) =>
                        this.renderRowLineAvg(item, index)
                      }
                    />
                  </View>
                )}
                <View style={{ alignItems: "center" }}>
                  <View
                    style={{
                      flexDirection: "row",
                      borderRadius: 10,
                      backgroundColor: "#D3D3D3",
                      alignItems: "center",
                      marginTop: 10,
                      overflow: "hidden"
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 14,
                        padding: 10,
                        color: "#FFFFFF",
                        backgroundColor: this.state.backchart,
                        borderBottomLeftRadius: 10,
                        borderBottomRightRadius: 0,
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 0
                      }}
                      onPress={this.ChangeChartView}
                    >
                      Chart View
                    </Text>

                    <Text
                      style={{
                        fontSize: 14,
                        padding: 10,
                        color: "#FFFFFF",
                        backgroundColor: this.state.backtable,
                        borderBottomLeftRadius: 0,
                        borderBottomRightRadius: 10,
                        borderTopLeftRadius: 0,
                        borderTopRightRadius: 10
                      }}
                      onPress={this.ChangeChartView}
                    >
                      Table View
                    </Text>
                  </View>
                </View>
                <View  style={{ height: 300, marginTop: 10 }}>
                {!this.state.charthide ? (
                  <ChartView   
                  style={{ height: 300 }}               
                    config={myChart}
                    options={options}
                  />
                ) : null}

                {!this.state.tablehide ? (
                  
                    <SimpleTable
                      height={'100%'}                      
                      columns={this.state.avgTableColumns}
                      dataSource={this.state.avgTableData}
                    />
                
                ) : null}
                </View>
              </View>
            )}

            {clienttype === "lead" && (
              <View style={{ justifyContent: "center" }}>
                <View
                  style={{ opacity: Platform.OS == "android" ? 0.3 : 0.05 }}
                >
                  <View>
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        onPress={this.showSlideAnimationDialog3}
                        style={[
                          styles.input,
                          { marginLeft: 5, marginTop: 10, flex: 1 }
                        ]}
                      >
                        {this.props.bar1}
                      </Text>
                    </View>
                    <Autocomplete
                      autoCapitalize="none"
                      autoCorrect={false}
                      clearTextOnFocus={true}
                      containerStyle={{
                        borderBottomColor:
                          Platform.OS == "ios" ? "black" : color.WHITE,
                        borderBottomWidth: Platform.OS == "ios" ? 1 : 0,
                        textAlign: "center"
                      }}
                      data={this.state.avgfunddata}
                      listContainerStyle={{ position: "relative" }}
                      onChangeText={text => this.searchAvgFund(text)}
                      placeholder="Add To Compare"
                      renderItem={({ text, id, asset_type }) =>
                        this.renderAutoCompleteAvg(text, id, asset_type)
                      }
                    />
                    {this.state.avg_scheme.length > 0 && (
                      <View>
                        <FlatList
                          scrollEnabled={false}
                          showsVerticalScrollIndicator={false}
                          style={{
                            marginTop: 10,
                            height: this.state.avg_scheme_text.length * 40,
                            minHeight: this.state.avg_scheme_text.length * 40
                          }}
                          data={this.state.avg_scheme_text}
                          renderItem={({ item, index }) =>
                            this.renderRowLineAvg(item, index)
                          }
                        />
                      </View>
                    )}
                    <View style={{ alignItems: "center" }}>
                      <View
                        style={{
                          flexDirection: "row",
                          borderRadius: 10,
                          backgroundColor: "#D3D3D3",
                          width: 180,
                          alignItems: "center",
                          marginTop: 10,
                          overflow: "hidden"
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 14,
                            padding: 10,
                            color: "#FFFFFF",
                            backgroundColor: this.state.backchart,
                            borderBottomLeftRadius: 10,
                            borderBottomRightRadius: 0,
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 0
                          }}
                          onPress={this.ChangeChartView}
                        >
                          Chart View
                        </Text>

                        <Text
                          style={{
                            fontSize: 14,
                            padding: 10,
                            color: "#FFFFFF",
                            backgroundColor: this.state.backtable,
                            borderBottomLeftRadius: 0,
                            borderBottomRightRadius: 10,
                            borderTopLeftRadius: 0,
                            borderTopRightRadius: 10
                          }}
                          onPress={this.ChangeChartView}
                        >
                          Table View
                        </Text>
                      </View>
                    </View>

                    {!this.state.charthide ? (
                      <ChartView
                        style={{
                          height: 300,
                          marginTop: 10,
                          opacity:
                            Platform.OS == "android" &&
                            (Platform.OS == "android" ? 0.3 : 0.05)
                        }}
                        config={myChart}
                        options={options}
                      />
                    ) : null}

                    {!this.state.tablehide ? (
                     
                        <SimpleTable
                          columns={this.state.avgTableColumns}
                          dataSource={this.state.avgTableData}
                        />
                     
                    ) : null}
                  </View>
                </View>
                <View
                  style={{
                    position: "absolute",
                    alignItems: "center",
                    justifyContent: "center",
                    alignSelf: "center"
                  }}
                >
                  <Image
                    source={require("../../assets/images/Complete-Your-Application.png")}
                  />
                </View>
              </View>
            )}
            <Text
              style={{
                fontWeight: "bold",
                color: "black",
                fontSize: 15,
                marginTop: 15
              }}
            >
              What's the Fund's performance in Calendar/Financial Year?{" "}
            </Text>
            {clienttype === "client" && (
              <View style={{ backgroundColor: "white" }}>
                <View style={{ flexDirection: "row" }}>
                  <Text
                    onPress={() => Actions.page1({ dialog: "year" })}
                    style={[
                      styles.input,
                      { marginLeft: 5, marginTop: 10, flex: 1 }
                    ]}
                  >
                    {this.props.year}
                  </Text>
                </View>
                <Autocomplete
                  autoCapitalize="none"
                  autoCorrect={false}
                  clearTextOnFocus={true}
                  containerStyle={{
                    borderBottomColor:
                      Platform.OS == "ios" ? "black" : color.WHITE,
                    borderBottomWidth: Platform.OS == "ios" ? 1 : 0,
                    textAlign: "center"
                  }}
                  data={this.state.finfunddata}
                  listContainerStyle={{ position: "relative" }}
                  onChangeText={text => this.searchFinFund(text)}
                  placeholder="Add To Compare"
                  renderItem={({ text, id, asset_type }) =>
                    this.renderAutoCompleteFin(text, id, asset_type)
                  }
                />
                {this.state.fin_scheme.length > 0 && (
                  <View>
                    <FlatList
                      scrollEnabled={false}
                      showsVerticalScrollIndicator={false}
                      style={{
                        marginTop: 10,
                        height: this.state.fin_scheme_text.length * 40,
                        minHeight: this.state.fin_scheme_text.length * 40
                      }}
                      data={this.state.fin_scheme_text}
                      renderItem={({ item, index }) =>
                        this.renderRowLineFin(item, index)
                      }
                    />
                  </View>
                )}
                <ChartView
                  style={{ height: 300, marginTop: 10 }}
                  config={myChartfinancial}
                  options={options}
                />
              </View>
            )}

            {clienttype === "lead" && (
              <View style={{ justifyContent: "center" }}>
                <View
                  style={{ opacity: Platform.OS == "android" ? 0.3 : 0.05 }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <Text
                      onPress={this.showSlideAnimationDialog4}
                      style={[
                        styles.input,
                        { marginLeft: 5, marginTop: 10, flex: 1 }
                      ]}
                    >
                      {this.props.year}
                    </Text>
                  </View>
                  <Autocomplete
                    autoCapitalize="none"
                    autoCorrect={false}
                    clearTextOnFocus={true}
                    containerStyle={{
                      borderBottomColor:
                        Platform.OS == "ios" ? "black" : color.WHITE,
                      borderBottomWidth: Platform.OS == "ios" ? 1 : 0,
                      textAlign: "center"
                    }}
                    data={this.state.finfunddata}
                    listContainerStyle={{ position: "relative" }}
                    onChangeText={text => this.searchFinFund(text)}
                    placeholder="Add To Compare"
                    renderItem={({ text, id, asset_type }) =>
                      this.renderAutoCompleteFin(text, id, asset_type)
                    }
                  />
                  {this.state.fin_scheme.length > 0 && (
                    <View>
                      <FlatList
                        scrollEnabled={false}
                        showsVerticalScrollIndicator={false}
                        style={{
                          marginTop: 10,
                          height: this.state.fin_scheme_text.length * 40,
                          minHeight: this.state.fin_scheme_text.length * 40
                        }}
                        data={this.state.fin_scheme_text}
                        renderItem={({ item, index }) =>
                          this.renderRowLineFin(item, index)
                        }
                      />
                    </View>
                  )}
                  <ChartView
                    style={{
                      height: 300,
                      marginTop: 10,
                      opacity:
                        Platform.OS == "android" &&
                        (Platform.OS == "android" ? 0.3 : 0.05)
                    }}
                    config={myChartfinancial}
                    options={options}
                  />
                </View>
                <View
                  style={{
                    position: "absolute",
                    alignItems: "center",
                    justifyContent: "center",
                    alignSelf: "center"
                  }}
                >
                  <Image
                    source={require("../../assets/images/Complete-Your-Application.png")}
                  />
                </View>
              </View>
            )}
          </Card>
        </View>
      </ScrollView>
    );
  }
}

export default Performance;
