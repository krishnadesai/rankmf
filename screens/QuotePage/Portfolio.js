import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  KeyboardAvoidingView,
  TouchableOpacity,
  FlatList,
  AsyncStorage
} from "react-native";
import { Card, Button } from "react-native-elements";
import PopupDialog, {
  DialogTitle,
  SlideAnimation
} from "react-native-popup-dialog";
import ChartView from "react-native-highcharts";
import randomColor from "randomcolor";
import styles from "../../components/styles";
import Apis from "../../constants/Apis";
import color from "../../constants/Colors";
const like = require("../../assets/images/thumbsup.png");
const dislike = require("../../assets/images/thumbsdown.png");
const slideAnimation = new SlideAnimation({ slideFrom: "bottom" });
const unique_code = "",
  clienttype = "";

class Portfolio extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dialogShow: false,
      aggregate: {},
      Equity: [],
      Debt: [],
      Other: [],
      itemList: [],
      itemListten: [],
      border: [color.LIGHT_GREY, color.LIGHT_GREY, color.LIGHT_GREY],
      stockdata: {},
      EquityChart: [],
      DebtChart: [],
      OtherChart: [],
      chart: [],
      piechart: [],
      borderChart: [color.LIGHT_GREY, color.LIGHT_GREY, color.LIGHT_GREY],
      borderStyleBox: [color.LIGHT_GREY, color.LIGHT_GREY],
      styleBox: "",
      equity_style_box: "",
      debt_style_box: "",
      styleBoxEquityBack: [
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY
      ],
      styleBoxDebtBack: [
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY
      ],
      borderportfolio: [color.LIGHT_GREY, color.LIGHT_GREY],
      eqport: [],
      debtport: [],
      port: ""
    };
    this.fetchChart = this.fetchChart.bind(this);
  }

  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      if (clienttype === "client") {
        this.setState({ showFund: true });
      } else {
        this.setState({ showFund: false });
      }
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    // alert(this.props.unique_code);
    unique_code = this.props.unique_code;
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.2&access_token=${access_token}&user=${clientid}&unique_code=${
        this.props.unique_code
      }&type=portfolio_composition&pf_type=1` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        const border = this.state.border;
        var total = 0,
          e = 0,
          d = 0,
          o = 0;
        responseJson["data"]["top_holding"].map((item, i) => {
          if (item.asset === "Equity") {
            e += parseFloat(item.Holdpercentage);
            this.state.Equity.push(item);
          } else if (item.asset === "Debt") {
            d += parseFloat(item.Holdpercentage);
            this.state.Debt.push(item);
          } else if (item.asset === "Others") {
            o += parseFloat(item.Holdpercentage);
            this.state.Other.push(item);
            this.state.Other.map(item => {
              var color1 = randomColor();
              var found = false;
              for (var i = 0; i < this.state.OtherChart.length; i++) {
                if (this.state.OtherChart[i].name === item.Compname) {
                  found = true;
                  break;
                }
              }
              if (found === false) {
                var dataStorage = {
                  name: item.Compname,
                  value: parseFloat(item.Holdpercentage),
                  asset: "Debt",
                  color: color1 + "",
                  html: ""
                };
                this.state.OtherChart.push(dataStorage);
              }
            });
          }
          total += parseFloat(item.Holdpercentage);
        });

        if (e != 0) {
          if (d != 0 && o != 0) {
            var dataStorage = [
              { name: "Equity", y: (e * 100) / total },
              { name: "Debt", y: (d * 100) / total },
              { name: "Other", y: (o * 100) / total }
            ];
            this.setState({ piechart: dataStorage });
          } else if (d == 0 && o != 0) {
            var dataStorage = [
              { name: "Equity", y: (e * 100) / total },
              { name: "Other", y: (o * 100) / total }
            ];
            this.setState({ piechart: dataStorage });
          } else if (d != 0 && o == 0) {
            var dataStorage = [
              { name: "Equity", y: (e * 100) / total },
              { name: "Debt", y: (d * 100) / total }
            ];
            this.setState({ piechart: dataStorage });
          } else if (d == 0 && o == 0) {
            var dataStorage = [{ name: "Equity", y: (e * 100) / total }];
            this.setState({ piechart: dataStorage });
          }
        } else if (d != 0) {
          if (e == 0 && o != 0) {
            var dataStorage = [
              { name: "Debt", y: (d * 100) / total },
              { name: "Other", y: (o * 100) / total }
            ];
            this.setState({ piechart: dataStorage });
          } else if (e == 0 && o == 0) {
            var dataStorage = [{ name: "Debt", y: (d * 100) / total }];
            this.setState({ piechart: dataStorage });
          }
        } else if (o != 0) {
          if (e == 0 && d == 0) {
            var dataStorage = [{ name: "Other", y: (o * 100) / total }];
            this.setState({ piechart: dataStorage });
          }
        }

        if (this.state.Other.length > 0) {
          border[2] = color.toolbar;
          border[0] = color.LIGHT_GREY;
          border[1] = color.LIGHT_GREY;
          this.setState({ itemList: this.state.Other });
        }
        if (this.state.Debt.length > 0) {
          border[2] = color.LIGHT_GREY;
          border[0] = color.LIGHT_GREY;
          border[1] = color.toolbar;
          this.setState({ itemList: this.state.Debt });
        }
        if (this.state.Equity.length > 0) {
          border[2] = color.LIGHT_GREY;
          border[0] = color.toolbar;
          border[1] = color.LIGHT_GREY;
          this.setState({ itemList: this.state.Equity });
        }

        this.state.itemList.map((item, i) => {
          if (i < 10) {
            this.state.itemListten.push(item);
          }
        });

        this.setState({
          aggregate: responseJson["data"]["aggregate"],
          border,
          stockdata: responseJson["data"]["stock_data"]
        });
        if (clienttype != "client") {
          this.fetchChart();
        }
        this.fetchStylebox();
        this.fetchPortfolio();
      })
      .catch(error => {
        console.error(error);
      });
  }

  fetchChart() {
    //alert(unique_code)
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.2&access_token=${access_token}&user=${clientid}&unique_code=${unique_code}&type=portfolio_composition&pf_type=2` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        const borderChart = this.state.borderChart;

        for (var key in responseJson["data"]["sectors"]) {
          if (key === "Equity") {
            this.setState({
              EquityChart: responseJson["data"]["sectors"][key]
            });
          } else if (key === "Debt") {
            this.setState({ DebtChart: responseJson["data"]["sectors"][key] });
          } else if (key === "Others") {
            this.state.Other.map(item => {
              var color1 = randomColor();
              var found = false;
              for (var i = 0; i < this.state.OtherChart.length; i++) {
                if (this.state.OtherChart[i].name === item.Compname) {
                  found = true;
                  break;
                }
              }
              if (found === false) {
                var dataStorage = {
                  name: item.Compname,
                  value: parseFloat(item.Holdpercentage),
                  asset: "Debt",
                  color: color1 + "",
                  html: ""
                };
                this.state.OtherChart.push(dataStorage);
              }
            });
          }
        }

        // alert(this.state.OtherChart)
        if (this.state.OtherChart.length > 0) {
          borderChart[2] = color.toolbar;
          borderChart[0] = color.LIGHT_GREY;
          borderChart[1] = color.LIGHT_GREY;
          this.setState({ chart: this.state.OtherChart });
        }
        if (this.state.DebtChart.length > 0) {
          borderChart[2] = color.LIGHT_GREY;
          borderChart[0] = color.LIGHT_GREY;
          borderChart[1] = color.toolbar;
          this.setState({ chart: this.state.DebtChart });
        }
        if (this.state.EquityChart.length > 0) {
          borderChart[2] = color.LIGHT_GREY;
          borderChart[0] = color.toolbar;
          borderChart[1] = color.LIGHT_GREY;
          this.setState({ chart: this.state.EquityChart });
        }
        this.setState({
          aggregate: responseJson["data"]["aggregate"],
          borderChart
        });
        //alert(this.state.EquityChart)
      })

      .catch(error => {
        console.error(error);
      });
  }

  fetchStylebox() {
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.2&access_token=${access_token}&user=${clientid}&unique_code=${unique_code}&type=portfolio_composition&pf_type=3` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        const borderStyleBox = this.state.borderStyleBox;
        const styleBoxEquityBack = this.state.styleBoxEquityBack;
        const styleBoxDebtBack = this.state.styleBoxDebtBack;

        if (responseJson["data"]["debt_style_box"] != "0") {
          borderStyleBox[0] = color.LIGHT_GREY;
          borderStyleBox[1] = color.toolbar;
          styleBoxDebtBack[
            parseInt(responseJson["data"]["debt_style_box"]) - 1
          ] = color.GREEN;
          this.setState({
            styleBox: "Debt",
            debt_style_box: responseJson["data"]["debt_style_box"],
            styleBoxDebtBack
          });
        }
        if (responseJson["data"]["equity_style_box"] != "0") {
          borderStyleBox[0] = color.toolbar;
          borderStyleBox[1] = color.LIGHT_GREY;

          styleBoxEquityBack[
            parseInt(responseJson["data"]["equity_style_box"]) - 1
          ] = color.GREEN;

          this.setState({
            styleBox: "Equity",
            equity_style_box: responseJson["data"]["equity_style_box"],
            styleBoxEquityBack
          });
        }

        this.setState({ borderStyleBox });

        //alert(this.state.EquityChart)
      })

      //  this.fetchStylebox()
      .catch(error => {
        console.error(error);
      });
  }

  fetchPortfolio() {
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.2&access_token=${access_token}&user=${clientid}&unique_code=${unique_code}&type=portfolio_composition&pf_type=4` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        const borderportfolio = this.state.borderportfolio;
        for (var key in responseJson["data"]) {
          if (key === "debtDetails") {
            for (var key1 in responseJson["data"]["debtDetails"]) {
              this.state.debtport.push(
                responseJson["data"]["debtDetails"][key1]
              );
            }
          }
          if (key === "equityDetails") {
            for (var key1 in responseJson["data"]["equityDetails"]) {
              this.state.eqport.push(
                responseJson["data"]["equityDetails"][key1]
              );
            }
          }
        }

        //alert(this.state.eqport)

        if (this.state.debtport.length > 0) {
          borderportfolio[0] = color.LIGHT_GREY;
          borderportfolio[1] = color.toolbar;
          this.setState({ port: "Debt" });
        }
        if (this.state.eqport.length > 0) {
          borderportfolio[0] = color.toolbar;
          borderportfolio[1] = color.LIGHT_GREY;
          this.setState({ port: "Equity" });
        }

        this.setState({ borderportfolio });
      })

      .catch(error => {
        console.error(error);
      });
  }

  renderImage(item) {
    for (var key in this.state.stockdata) {
      if (key === item.ISIN) {
        var score = this.state.stockdata[key].score;
        if (score >= 50) {
          return (
            <View style={{ position: "absolute",zIndex:1}}>
              <View
                style={[
                  styles.circle,
                  {
                    backgroundColor: color.GREEN,
                    alignItems: "center",
                    height: 20,
                    width: 20,
                    borderRadius: 10
                  }
                ]}
              >
                <Image
                  style={{
                    width: 10,
                    height: 10,
                    resizeMode: "contain",
                    alignItems: "center",
                    marginTop: 5
                  }}
                  source={like}
                />
              </View>
            </View>
          );
        } else {
          return (
            <View style={{ position: "absolute",zIndex:1,right:0}}>
              <View
                style={[
                  styles.circle,
                  {
                    backgroundColor: color.RED,
                    alignItems: "center",
                    height: 20,
                    width: 20,
                    borderRadius: 10
                  }
                ]}
              >
                <Image
                  style={{
                    width: 10,
                    height: 10,
                    resizeMode: "contain",
                    alignItems: "center",
                    marginTop: 5
                  }}
                  source={dislike}
                />
              </View>
            </View>
          );
        }
      }
    }
  }

  renderRow(item,index) {
    return (
      <View style={{ marginTop: 10, flex: 1 }}>
      
        <View style={{ padding: 5, margin: 5, flex: 1 ,borderWidth:1,borderColor:color.LIGHT_GREY}}>
          <Text
            style={[
              styles.title_carddetail,
              {
                color: color.BLACK,
                marginLeft: 8,
                fontWeight: "normal",
                fontSize: 13
              }
            ]}
          >
            {item.Compname}
          </Text>

          <View style={{ flexDirection: "row", marginTop: 5 }}>
            <Text style={[styles.title, { flex: 1 }]}>% Weight : </Text>
            <Text
              style={[
                styles.title_carddetail,
                {
                  color: color.BLACK,
                  marginLeft: 8,
                  fontWeight: "normal",
                  fontSize: 10,
                  flex: 1
                }
              ]}
            >
              {parseFloat(item.Holdpercentage).toFixed(2)}%
            </Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={[styles.title, { flex: 1 }]}>Value : </Text>
            <Text
              style={[
                styles.title_carddetail,
                {
                  color: color.BLACK,
                  marginLeft: 8,
                  fontWeight: "normal",
                  fontSize: 10,
                  flex: 1
                }
              ]}
            >
              {parseFloat(item.Mktval / 100).toFixed(3)} Cr
            </Text>
          </View>
        </View>
        <View style={{ position: "absolute",right:0,height:20,width:20,flex:1,zIndex:1}}>
              <View
                style={[
                  styles.circle,
                  {
                    backgroundColor: color.GREEN,
                    alignItems: "center",
                    height: 20,
                    width: 20,
                    borderRadius: 10
                  }
                ]}
              >
                <Image
                  style={{
                    width: 10,
                    height: 10,
                    resizeMode: "contain",
                    alignItems: "center",
                    marginTop: 5
                  }}
                  source={like}
                />
              </View>
            </View>
            
      </View>
    );
  }

  ShowHoldingsTable = () => {
    this.slideAnimationDialog.show();
  };

  ChangeHoldingTable(name, i) {
    const border = this.state.border;
    border.map((item, ii) => {
      if (item === color.toolbar) {
        border[ii] = color.LIGHT_GREY;
      }
    });

    border[i] = color.toolbar;

    if (name === "Equity") {
      this.setState({ itemList: this.state.Equity });
    } else if (name === "Debt") {
      this.setState({ itemList: this.state.Debt });
    } else if (name === "Other") {
      this.setState({ itemList: this.state.Other });
    }

    this.setState({ border });
  }

  ChangeChart(name, i) {
    const borderChart = this.state.borderChart;
    borderChart.map((item, ii) => {
      if (item === color.toolbar) {
        borderChart[ii] = color.LIGHT_GREY;
      }
    });

    borderChart[i] = color.toolbar;

    if (name === "Equity") {
      this.setState({ chart: this.state.EquityChart });
    } else if (name === "Debt") {
      this.setState({ chart: this.state.DebtChart });
    } else if (name === "Other") {
      this.setState({ chart: this.state.OtherChart });
    }

    this.setState({ borderChart });
  }

  ChangeStyleBox(name, i) {
    const borderStyleBox = this.state.borderStyleBox;
    borderStyleBox.map((item, ii) => {
      if (item === color.toolbar) {
        borderStyleBox[ii] = color.LIGHT_GREY;
      }
    });

    borderStyleBox[i] = color.toolbar;

    if (name === "Equity") {
      this.setState({ styleBox: "Equity" });
    } else if (name === "Debt") {
      this.setState({ styleBox: "Debt" });
    }

    this.setState({ borderStyleBox });
  }

  ChangePortfolio(name, i) {
    const borderportfolio = this.state.borderportfolio;
    borderportfolio.map((item, ii) => {
      if (item === color.toolbar) {
        borderportfolio[ii] = color.LIGHT_GREY;
      }
    });
    borderportfolio[i] = color.toolbar;

    if (name === "Equity") {
      this.setState({ port: "Equity" });
    } else if (name === "Debt") {
      this.setState({ port: "Debt" });
    }

    this.setState({ borderportfolio });
  }

  render() {
    var Highcharts = "Highcharts";

    var conf = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie"
      },
      credits: {
        enabled: false
      },
      title: {
        text: `Asset Allocation for ${this.state.aggregate.Scheme_Name}`
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>"
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: true,
            format: "<b>{point.name}</b>: {point.percentage:.1f} %",
            style: {
              color:
                (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
                "black"
            }
          }
        }
      },
      exporting: {
        enabled: false
      },
      series: [
        {
          name: "Series 1",
          colorByPoint: true,
          data: this.state.piechart
        }
      ]
    };

    var treemapconfig = {
      series: [
        {
          type: "treemap",
          layoutAlgorithm: "squarified",
          data: this.state.chart
        }
      ],
      credits: {
        enabled: false
      },
      exporting: {
        enabled: false
      },
      title: {
        text: ""
      }
    };

    const options = {
      global: {
        useUTC: false
      },
      lang: {
        decimalPoint: ".",
        thousandsSep: ","
      }
    };

    return (
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.containerFlex}
      >
        <Card>
          <View>
            <ChartView
              style={{ height: 300 }}
              config={conf}
              options={options}
            />

            <Text
              style={{
                fontWeight: "bold",
                color: "black",
                fontSize: 14,
                marginTop: 10
              }}
            >
              Detailed Portfolio of Fund Growth
            </Text>

            {clienttype === "client" && (
              <View>
                <View style={{ flexDirection: "row", margin: 10 }}>
                  {this.state.Equity.length > 0 && (
                    <TouchableOpacity
                      style={{ flex: 1 }}
                      onPress={() => this.ChangeHoldingTable("Equity", 0)}
                    >
                      <Text style={[styles.flexboxtext]}>Equity </Text>
                      <View
                        style={[
                          styles.dash,
                          {
                            borderBottomColor: this.state.border[0],
                            borderBottomWidth: 1
                          }
                        ]}
                      />
                    </TouchableOpacity>
                  )}
                  {this.state.Debt.length > 0 && (
                    <TouchableOpacity
                      style={{ flex: 1 }}
                      onPress={() => this.ChangeHoldingTable("Debt", 1)}
                    >
                      <Text style={[styles.flexboxtext]}>Debt </Text>
                      <View
                        style={[
                          styles.dash,
                          {
                            borderBottomColor: this.state.border[1],
                            borderBottomWidth: 1
                          }
                        ]}
                      />
                    </TouchableOpacity>
                  )}
                  {this.state.Other.length > 0 && (
                    <TouchableOpacity
                      style={{ flex: 1 }}
                      onPress={() => this.ChangeHoldingTable("Other", 2)}
                    >
                      <Text style={[styles.flexboxtext]}>Other </Text>
                      <View
                        style={[
                          styles.dash,
                          {
                            borderBottomColor: this.state.border[2],
                            borderBottomWidth: 1
                          }
                        ]}
                      />
                    </TouchableOpacity>
                  )}
                </View>

                <FlatList
                  numColumns={2}
                  style={{ marginTop: 15, marginBottom: 25 }}
                  data={this.state.itemList}
                  renderItem={({ item, index }) => this.renderRow(item,index)}
                  onEndReached={this.fetchChart}
                  onEndReachedThreshold={0.5}
                />
              </View>
            )}

            {clienttype === "lead" && (
              <View style={{ justifyContent: "center" }}>
                <View style={{ opacity: 0.05 }}>
                  <View style={{ flexDirection: "row", margin: 10 }}>
                    {this.state.Equity.length > 0 && (
                      <TouchableOpacity style={{ flex: 1 }}>
                        <Text style={[styles.flexboxtext]}>Equity </Text>
                        <View
                          style={[
                            styles.dash,
                            {
                              borderBottomColor: this.state.border[0],
                              borderBottomWidth: 1
                            }
                          ]}
                        />
                      </TouchableOpacity>
                    )}
                    {this.state.Debt.length > 0 && (
                      <TouchableOpacity style={{ flex: 1 }}>
                        <Text style={[styles.flexboxtext]}>Debt </Text>
                        <View
                          style={[
                            styles.dash,
                            {
                              borderBottomColor: this.state.border[1],
                              borderBottomWidth: 1
                            }
                          ]}
                        />
                      </TouchableOpacity>
                    )}
                    {this.state.Other.length > 0 && (
                      <TouchableOpacity style={{ flex: 1 }}>
                        <Text style={[styles.flexboxtext]}>Other </Text>
                        <View
                          style={[
                            styles.dash,
                            {
                              borderBottomColor: this.state.border[2],
                              borderBottomWidth: 1
                            }
                          ]}
                        />
                      </TouchableOpacity>
                    )}
                  </View>

                  <FlatList
                    numColumns={2}
                    style={{ marginTop: 15, marginBottom: 25 }}
                    data={this.state.itemListten}
                    renderItem={({ item, index }) => this.renderRow(item)}
                    onEndReached={this.fetchChart}
                    onEndReachedThreshold={0.5}
                  />
                </View>
                <Image
                  source={require("../../assets/images/Complete-Your-Application.png")}
                  style={{
                    position: "absolute",
                    resizeMode: "contain",
                    alignItems: "center",
                    alignSelf: "center"
                  }}
                />
              </View>
            )}

            <Text
              style={{
                fontWeight: "bold",
                color: "black",
                fontSize: 14,
                marginTop: 10
              }}
            >
              Insight on the composition of the{" "}
              {this.state.aggregate.Scheme_Name}
            </Text>

            {clienttype === "client" && (
              <View>
                <View style={{ flexDirection: "row", margin: 10 }}>
                  {this.state.EquityChart.length > 0 && (
                    <TouchableOpacity
                      style={{ flex: 1 }}
                      onPress={() => this.ChangeChart("Equity", 0)}
                    >
                      <Text style={[styles.flexboxtext]}>Equity </Text>
                      <View
                        style={[
                          styles.dash,
                          {
                            borderBottomColor: this.state.borderChart[0],
                            borderBottomWidth: 1
                          }
                        ]}
                      />
                    </TouchableOpacity>
                  )}
                  {this.state.DebtChart.length > 0 && (
                    <TouchableOpacity
                      style={{ flex: 1 }}
                      onPress={() => this.ChangeChart("Debt", 1)}
                    >
                      <Text style={[styles.flexboxtext]}>Debt </Text>
                      <View
                        style={[
                          styles.dash,
                          {
                            borderBottomColor: this.state.borderChart[1],
                            borderBottomWidth: 1
                          }
                        ]}
                      />
                    </TouchableOpacity>
                  )}
                  {this.state.OtherChart.length > 0 && (
                    <TouchableOpacity
                      style={{ flex: 1 }}
                      onPress={() => this.ChangeChart("Other", 2)}
                    >
                      <Text style={[styles.flexboxtext]}>Other </Text>
                      <View
                        style={[
                          styles.dash,
                          {
                            borderBottomColor: this.state.borderChart[2],
                            borderBottomWidth: 1
                          }
                        ]}
                      />
                    </TouchableOpacity>
                  )}
                </View>

                <ChartView
                  style={{ height: 400 }}
                  config={treemapconfig}
                  options={options}
                />
              </View>
            )}
            {clienttype === "lead" && (
              <View style={{ justifyContent: "center" }}>
                <View style={{ opacity: 0.05 }}>
                  <View style={{ flexDirection: "row", margin: 10 }}>
                    {this.state.EquityChart.length > 0 && (
                      <TouchableOpacity style={{ flex: 1 }}>
                        <Text style={[styles.flexboxtext]}>Equity </Text>
                        <View
                          style={[
                            styles.dash,
                            {
                              borderBottomColor: this.state.borderChart[0],
                              borderBottomWidth: 1
                            }
                          ]}
                        />
                      </TouchableOpacity>
                    )}
                    {this.state.DebtChart.length > 0 && (
                      <TouchableOpacity style={{ flex: 1 }}>
                        <Text style={[styles.flexboxtext]}>Debt </Text>
                        <View
                          style={[
                            styles.dash,
                            {
                              borderBottomColor: this.state.borderChart[1],
                              borderBottomWidth: 1
                            }
                          ]}
                        />
                      </TouchableOpacity>
                    )}
                    {this.state.OtherChart.length > 0 && (
                      <TouchableOpacity style={{ flex: 1 }}>
                        <Text style={[styles.flexboxtext]}>Other </Text>
                        <View
                          style={[
                            styles.dash,
                            {
                              borderBottomColor: this.state.borderChart[2],
                              borderBottomWidth: 1
                            }
                          ]}
                        />
                      </TouchableOpacity>
                    )}
                  </View>
                  <ChartView
                    style={{ height: 400, opacity: 0.05 }}
                    config={treemapconfig}
                    options={options}
                  />
                </View>
                <View
                  style={{
                    position: "absolute",
                    alignItems: "center",
                    justifyContent: "center",
                    alignSelf: "center"
                  }}
                >
                  <Image
                    source={require("../../assets/images/Complete-Your-Application.png")}
                  />
                </View>
              </View>
            )}

            <Text
              style={{
                fontWeight: "bold",
                color: "black",
                fontSize: 14,
                marginTop: 10
              }}
            >
              Style box of the {this.state.aggregate.Scheme_Name}
            </Text>
            <View style={{ flexDirection: "row", margin: 10 }}>
              {this.state.equity_style_box != 0 && (
                <TouchableOpacity
                  style={{ flex: 1 }}
                  onPress={() => this.ChangeStyleBox("Equity", 0)}
                >
                  <Text style={[styles.flexboxtext]}>Equity </Text>
                  <View
                    style={[
                      styles.dash,
                      {
                        borderBottomColor: this.state.borderStyleBox[0],
                        borderBottomWidth: 1
                      }
                    ]}
                  />
                </TouchableOpacity>
              )}
              {this.state.debt_style_box != 0 && (
                <TouchableOpacity
                  style={{ flex: 1 }}
                  onPress={() => this.ChangeStyleBox("Debt", 1)}
                >
                  <Text style={[styles.flexboxtext]}>Debt </Text>
                  <View
                    style={[
                      styles.dash,
                      {
                        borderBottomColor: this.state.borderStyleBox[1],
                        borderBottomWidth: 1
                      }
                    ]}
                  />
                </TouchableOpacity>
              )}
            </View>

            {this.state.equity_style_box != 0 &&
              this.state.styleBox === "Equity" && (
                <View style={{ flexDirection: "column" }}>
                  <View style={{ flexDirection: "row", flex: 1 }}>
                    <View style={{ flex: 1, margin: 2 }} />
                    <View
                      style={{
                        flex: 1,
                        margin: 2,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text style={{ textAlign: "center", fontSize: 12 }}>
                        Value
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        margin: 2,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text style={{ textAlign: "center", fontSize: 12 }}>
                        Blend
                      </Text>
                    </View>
                    <View style={{ flex: 1, margin: 2 }}>
                      <Text style={{ textAlign: "center" }}>Growth</Text>
                    </View>
                    <View style={{ flex: 1, margin: 2 }} />
                  </View>
                  <View style={{ flexDirection: "row", flex: 1 }}>
                    <View
                      style={{
                        flex: 1,
                        margin: 2,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text style={{ textAlign: "center", fontSize: 12 }}>
                        Large
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        margin: 2,
                        height: 60,
                        width: 60,
                        backgroundColor: this.state.styleBoxEquityBack[0],
                        borderRadius: 5
                      }}
                    />
                    <View
                      style={{
                        flex: 1,
                        margin: 2,
                        height: 60,
                        width: 60,
                        backgroundColor: this.state.styleBoxEquityBack[1],
                        borderRadius: 5
                      }}
                    />
                    <View
                      style={{
                        flex: 1,
                        margin: 2,
                        height: 60,
                        width: 60,
                        backgroundColor: this.state.styleBoxEquityBack[2],
                        borderRadius: 5
                      }}
                    />
                    <View style={{ flex: 1, margin: 2 }} />
                  </View>
                  <View style={{ flexDirection: "row", flex: 1 }}>
                    <View
                      style={{
                        flex: 1,
                        margin: 2,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text style={{ textAlign: "center", fontSize: 12 }}>
                        Medium
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        margin: 2,
                        height: 60,
                        width: 60,
                        backgroundColor: this.state.styleBoxEquityBack[3],
                        borderRadius: 5
                      }}
                    />
                    <View
                      style={{
                        flex: 1,
                        margin: 2,
                        height: 60,
                        width: 60,
                        backgroundColor: this.state.styleBoxEquityBack[4],
                        borderRadius: 5
                      }}
                    />
                    <View
                      style={{
                        flex: 1,
                        margin: 2,
                        height: 60,
                        width: 60,
                        backgroundColor: this.state.styleBoxEquityBack[5],
                        borderRadius: 5
                      }}
                    />
                    <View
                      style={{
                        flex: 1,
                        margin: 2,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text style={{ textAlign: "center", fontSize: 12 }}>
                        Market Capitalization
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: "row", flex: 1 }}>
                    <View
                      style={{
                        flex: 1,
                        margin: 2,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text style={{ textAlign: "center", fontSize: 12 }}>
                        Small
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        margin: 2,
                        height: 60,
                        width: 60,
                        backgroundColor: this.state.styleBoxEquityBack[6],
                        borderRadius: 5
                      }}
                    />
                    <View
                      style={{
                        flex: 1,
                        margin: 2,
                        height: 60,
                        width: 60,
                        backgroundColor: this.state.styleBoxEquityBack[7],
                        borderRadius: 5
                      }}
                    />
                    <View
                      style={{
                        flex: 1,
                        margin: 2,
                        height: 60,
                        width: 60,
                        backgroundColor: this.state.styleBoxEquityBack[8],
                        borderRadius: 5
                      }}
                    />
                    <View style={{ flex: 1, margin: 2 }} />
                  </View>
                </View>
              )}

            {this.state.debt_style_box != 0 && this.state.styleBox === "Debt" && (
              <View style={{ flexDirection: "column" }}>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <View style={{ flex: 1, margin: 2 }} />
                  <View
                    style={{
                      flex: 1,
                      margin: 2,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{ textAlign: "center", fontSize: 12 }}>
                      Short
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      margin: 2,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{ textAlign: "center", fontSize: 12 }}>
                      Medium
                    </Text>
                  </View>
                  <View style={{ flex: 1, margin: 2 }}>
                    <Text style={{ textAlign: "center" }}>Long</Text>
                  </View>
                  <View style={{ flex: 1, margin: 2 }} />
                </View>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <View
                    style={{
                      flex: 1,
                      margin: 2,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{ textAlign: "center", fontSize: 12 }}>
                      Treasury/Agency
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      margin: 2,
                      height: 60,
                      width: 60,
                      backgroundColor: this.state.styleBoxDebtBack[0],
                      borderRadius: 5
                    }}
                  />
                  <View
                    style={{
                      flex: 1,
                      margin: 2,
                      height: 60,
                      width: 60,
                      backgroundColor: this.state.styleBoxDebtBack[1],
                      borderRadius: 5
                    }}
                  />
                  <View
                    style={{
                      flex: 1,
                      margin: 2,
                      height: 60,
                      width: 60,
                      backgroundColor: this.state.styleBoxDebtBack[2],
                      borderRadius: 5
                    }}
                  />
                  <View style={{ flex: 1, margin: 2 }} />
                </View>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <View
                    style={{
                      flex: 1,
                      margin: 2,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{ textAlign: "center", fontSize: 12 }}>
                      Investment-grade corporate
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      margin: 2,
                      height: 60,
                      width: 60,
                      backgroundColor: this.state.styleBoxDebtBack[3],
                      borderRadius: 5
                    }}
                  />
                  <View
                    style={{
                      flex: 1,
                      margin: 2,
                      height: 60,
                      width: 60,
                      backgroundColor: this.state.styleBoxDebtBack[4],
                      borderRadius: 5
                    }}
                  />
                  <View
                    style={{
                      flex: 1,
                      margin: 2,
                      height: 60,
                      width: 60,
                      backgroundColor: this.state.styleBoxDebtBack[5],
                      borderRadius: 5
                    }}
                  />
                  <View
                    style={{
                      flex: 1,
                      margin: 2,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{ textAlign: "center", fontSize: 12 }}>
                      Quality
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <View
                    style={{
                      flex: 1,
                      margin: 2,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text style={{ textAlign: "center", fontSize: 12 }}>
                      Below investment grade
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      margin: 2,
                      height: 60,
                      width: 60,
                      backgroundColor: this.state.styleBoxDebtBack[6],
                      borderRadius: 5
                    }}
                  />
                  <View
                    style={{
                      flex: 1,
                      margin: 2,
                      height: 60,
                      width: 60,
                      backgroundColor: this.state.styleBoxDebtBack[7],
                      borderRadius: 5
                    }}
                  />
                  <View
                    style={{
                      flex: 1,
                      margin: 2,
                      height: 60,
                      width: 60,
                      backgroundColor: this.state.styleBoxDebtBack[8],
                      borderRadius: 5
                    }}
                  />
                  <View style={{ flex: 1, margin: 2 }} />
                </View>
              </View>
            )}

            <Text
              style={{
                fontWeight: "bold",
                color: "black",
                fontSize: 14,
                marginTop: 15
              }}
            >
              Portfolio Characteristics of the{" "}
              {this.state.aggregate.Scheme_Name}
            </Text>
            <View style={{ flexDirection: "row", margin: 10 }}>
              {this.state.eqport.length > 0 && (
                <TouchableOpacity
                  style={{ flex: 1 }}
                  onPress={() => this.ChangePortfolio("Equity", 0)}
                >
                  <Text style={[styles.flexboxtext]}>Equity </Text>
                  <View
                    style={[
                      styles.dash,
                      {
                        borderBottomColor: this.state.borderportfolio[0],
                        borderBottomWidth: 1
                      }
                    ]}
                  />
                </TouchableOpacity>
              )}
              {this.state.debtport.length > 0 && (
                <TouchableOpacity
                  style={{ flex: 1 }}
                  onPress={() => this.ChangePortfolio("Debt", 1)}
                >
                  <Text style={[styles.flexboxtext]}>Debt </Text>
                  <View
                    style={[
                      styles.dash,
                      {
                        borderBottomColor: this.state.borderportfolio[1],
                        borderBottomWidth: 1
                      }
                    ]}
                  />
                </TouchableOpacity>
              )}
            </View>

            {this.state.eqport.length > 0 && this.state.port === "Equity" && (
              <View style={{ marginTop: 20, alignItems: "center" }}>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 15,
                      flex: 1
                    }}
                  >
                    Numbers of stocks
                  </Text>
                  <Text style={{ color: "black", fontSize: 15, flex: 1 }}>
                    {this.state.Equity.length}
                  </Text>
                </View>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 15,
                      flex: 1
                    }}
                  >
                    Median Market Cap
                  </Text>
                  <Text style={{ color: "black", fontSize: 15, flex: 1 }}>
                    {this.state.eqport[0]}
                  </Text>
                </View>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 15,
                      flex: 1
                    }}
                  >
                    P/E
                  </Text>
                  <Text style={{ color: "black", fontSize: 15, flex: 1 }}>
                    {this.state.eqport[1]}
                  </Text>
                </View>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 15,
                      flex: 1
                    }}
                  >
                    P/B
                  </Text>
                  <Text style={{ color: "black", fontSize: 15, flex: 1 }}>
                    {this.state.eqport[2]}
                  </Text>
                </View>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 15,
                      flex: 1
                    }}
                  >
                    RoE
                  </Text>
                  <Text style={{ color: "black", fontSize: 15, flex: 1 }}>
                    {this.state.eqport[3]}
                  </Text>
                </View>
              </View>
            )}
            {this.state.debtport.length > 0 && this.state.port === "Debt" && (
              <View style={{ marginTop: 20, alignItems: "center" }}>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 15,
                      flex: 1
                    }}
                  >
                    Numbers of bonds
                  </Text>
                  <Text style={{ color: "black", fontSize: 15, flex: 1 }}>
                    {this.state.Debt.length}
                  </Text>
                </View>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 15,
                      flex: 1
                    }}
                  >
                    YTM
                  </Text>
                  <Text style={{ color: "black", fontSize: 15, flex: 1 }}>
                    {this.state.debtport[0]}
                  </Text>
                </View>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 15,
                      flex: 1
                    }}
                  >
                    Avg. Coupon
                  </Text>
                  <Text style={{ color: "black", fontSize: 15, flex: 1 }} />
                </View>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 15,
                      flex: 1
                    }}
                  >
                    Avg. Eff. Maturity
                  </Text>
                  <Text style={{ color: "black", fontSize: 15, flex: 1 }}>
                    {this.state.debtport[1]} {this.state.debtport[2]}
                  </Text>
                </View>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 15,
                      flex: 1
                    }}
                  >
                    Modified Duration
                  </Text>
                  <Text style={{ color: "black", fontSize: 15, flex: 1 }}>
                    {this.state.debtport[3]} {this.state.debtport[4]}
                  </Text>
                </View>
              </View>
            )}
          </View>
        </Card>
      </ScrollView>
    );
  }
}

export default Portfolio;
