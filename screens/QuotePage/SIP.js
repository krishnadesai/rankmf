import React, { Component } from "react";
import {
  StyleSheet,
  Platform,
  Text,
  View,
  ScrollView,
  Image,
  Picker,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  AsyncStorage
} from "react-native";
import { Card, Button } from "react-native-elements";
import ModalDatePicker from "react-native-datepicker-modal";
import PopupDialog, {
  DialogTitle,
  SlideAnimation
} from "react-native-popup-dialog";
import { Actions } from "react-native-router-flux";
import ChartView from "react-native-highcharts";
import styles from "../../components/styles";
import Apis from "../../constants/Apis";
import color from "../../constants/Colors";
const thumbsup = require("../../assets/images/thumbsup.png");
const thumbsdown = require("../../assets/images/thumbsdown.png");
const doublelike = require("../../assets/images/double_like.png");
const newicon = require("../../assets/images/new-icon.png");
var date = new Date();
var day = date ? ("0" + date.getDate()).slice(-2) : "";
var month = date ? ("0" + (date.getMonth() + 1)).slice(-2) : "";
var year = date ? date.getFullYear() : "";
const clienttype = "";
const currentDate = year + "-" + month + "-" + day;
const datestart = year - 5 + "-" + month + "-" + day,
  dateend = currentDate;
const slideAnimation = new SlideAnimation({ slideFrom: "bottom" });

class Sip extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dialogShow: false,
      aggregate: {},
      sipamtgrowth: "5000",
      growthdata1: [],
      growthdata2: [],
      growthdata3: {},
      pregrowthdata1: [],
      pregrowthdata2: [],
      sipamtpregrowth: "5000",
      interest: "0"
    };
  }

  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      if (clienttype === "client") {
        this.setState({ showFund: true });
      } else {
        this.setState({ showFund: false });
      }
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    // alert(this.props.unique_code);
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.2&access_token=${access_token}&user=${clientid}&unique_code=${
        this.props.unique_code
      }&type=sip_returns` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({ aggregate: responseJson["data"]["aggregate"] });
        this.setState({
          interest: parseFloat(this.state.aggregate.INCRET).toFixed(0)
        });
        //alert(responseJson["aggregate"].Scheme_Name)
        this.fetchGrowthChart();
        this.fetchPreGrowthChart();
      })
      .catch(error => {
        console.error(error);
      });
  }

  fetchGrowthChart() {
    //  alert(datestart+"--"+dateend)
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.3&access_token=${access_token}&user=${clientid}&tab_type=sip_returns&chart_type=growth_cal&scheme_code=${
        this.state.aggregate.schemecode
      }&sip_amt=${
        this.state.sipamtgrowth
      }&from_date=${datestart}&to_date=${dateend}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          growthdata1: responseJson["data"]["sipGrowthCalculator"]["0"],
          growthdata2: responseJson["data"]["sipGrowthCalculator"]["1"],
          growthdata3: responseJson["data"]["sipGrowthCalculator"]["3"]
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  fetchPreGrowthChart() {
    //alert(`api=1.3&unique_code=${this.props.unique_code}&type=sip_returns&chart_type=pre_growth_cal&scheme_code=${this.state.aggregate.schemecode}&classcode=${this.state.aggregate.classcode}&sip_amt=${this.state.sipamtpregrowth}&interest=${this.state.interest}&period=${this.state.sipperiod}`)
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.3&access_token=${access_token}&user=${clientid}&tab_type=sip_returns&chart_type=pre_growth_cal&scheme_code=${
        this.state.aggregate.schemecode
      }&sip_amt=${this.state.sipamtpregrowth}&interest=${
        this.state.interest
      }&period=${this.props.sipperiod}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          pregrowthdata1:
            responseJson["data"]["sipPredictedGrowthCalculator"][0],
          pregrowthdata2:
            responseJson["data"]["sipPredictedGrowthCalculator"][1]
        });
        // alert(parseFloat(this.state.pregrowthdata1[(this.state.pregrowthdata1.length)-1][1]).toFixed(0))
      })
      .catch(error => {
        console.error(error);
      });
  }

  

  renderCorrectTime() {
    if (
      this.state.aggregate.time_tooptip !=
      "Enough data is not available for this scheme."
    ) {
      if (this.state.aggregate.existing_sip_flag === "0") {
        if (clienttype === "client") {
          return (
            <View>
              <View
                style={{
                  flexDirection: "row",
                  marginTop: 10,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <View
                  style={[
                    styles.circle,
                    {
                      backgroundColor: color.RED,
                      alignItems: "center",
                      marginLeft: Platform.OS === "android" ? 40 : 0
                    }
                  ]}
                >
                  <Image
                    style={{
                      width: 20,
                      height: 20,
                      resizeMode: "contain",
                      alignItems: "center",
                      marginTop: 9
                    }}
                    source={thumbsdown}
                  />
                </View>
                <Text
                  style={{
                    fontWeight: "bold",
                    color: "black",
                    fontSize: 13,
                    marginLeft: 10
                  }}
                >
                  {this.state.aggregate.conclusion}
                </Text>
              </View>
              {this.state.aggregate.asset_type === "Commodity" && (
                <Text
                  style={{
                    fontWeight: "bold",
                    color: "black",
                    fontSize: 13,
                    marginTop: 10
                  }}
                >
                  RANK MF research does not recommend SIP’s in commodity funds
                  since commodities are cyclical in nature and usually do not
                  create long term wealth via SIP.
                </Text>
              )}
              <Text
                style={{
                  fontWeight: "normal",
                  color: "black",
                  fontSize: 12,
                  marginTop: 10
                }}
              >
                {this.state.aggregate.sip_desc}
              </Text>
            </View>
          );
        } else if (clienttype === "lead") {
          return (
            <View style={{ alignItems: "center", justifyContent: "center" }}>
              <View style={{ opacity: 0.05 }}>
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 10,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <View
                    style={[
                      styles.circle,
                      { backgroundColor: color.RED, alignItems: "center",height:40,width:40 }
                    ]}
                  >
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: "contain",
                        alignItems: "center",
                        marginTop: 9
                      }}
                      source={thumbsdown}
                    />
                  </View>
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 13,
                      marginLeft: 10
                    }}
                  >
                    {this.state.aggregate.conclusion}
                  </Text>
                </View>
                {this.state.aggregate.asset_type === "Commodity" && (
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 13,
                      marginTop: 10
                    }}
                  >
                    RANK MF research does not recommend SIP’s in commodity funds
                    since commodities are cyclical in nature and usually do not
                    create long term wealth via SIP.
                  </Text>
                )}
                <Text
                  style={{
                    fontWeight: "normal",
                    color: "black",
                    fontSize: 12,
                    marginTop: 10
                  }}
                >
                  {this.state.aggregate.sip_desc}
                </Text>
              </View>
              <Image
                source={require("../../assets/images/Complete-Your-Application.png")}
                style={{ position: "absolute" }}
              />
            </View>
          );
        }
      } else if (this.state.aggregate.existing_sip_flag === "1") {
        if (clienttype === "client") {
          return (
            <View>
              <View
                style={{
                  flexDirection: "row",
                  marginTop: 10,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <View
                  style={[
                    styles.circle,
                    { backgroundColor: color.GREEN, alignItems: "center" }
                  ]}
                >
                  <Image
                    style={{
                      width: 20,
                      height: 20,
                      resizeMode: "contain",
                      alignItems: "center",
                      marginTop: 9
                    }}
                    source={thumbsup}
                  />
                </View>
                <Text
                  style={{
                    fontWeight: "bold",
                    color: "black",
                    fontSize: 13,
                    marginLeft: 10
                  }}
                >
                  {this.state.aggregate.conclusion}
                </Text>
              </View>
              {this.state.aggregate.asset_type === "Commodity" && (
                <Text
                  style={{
                    fontWeight: "bold",
                    color: "black",
                    fontSize: 13,
                    marginTop: 10
                  }}
                >
                  RANK MF research does not recommend SIP’s in commodity funds
                  since commodities are cyclical in nature and usually do not
                  create long term wealth via SIP.
                </Text>
              )}
              <Text
                style={{
                  fontWeight: "normal",
                  color: "black",
                  fontSize: 12,
                  marginTop: 10
                }}
              >
                {this.state.aggregate.sip_desc}
              </Text>
            </View>
          );
        } else if (clienttype === "lead") {
          return (
            <View style={{ alignItems: "center", justifyContent: "center" }}>
              <View style={{ opacity: 0.05 }}>
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 10,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <View
                    style={[
                      styles.circle,
                      { backgroundColor: color.GREEN, alignItems: "center" }
                    ]}
                  >
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: "contain",
                        alignItems: "center",
                        marginTop: 9
                      }}
                      source={thumbsup}
                    />
                  </View>
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 13,
                      marginLeft: 10
                    }}
                  >
                    {this.state.aggregate.conclusion}
                  </Text>
                </View>
                {this.state.aggregate.asset_type === "Commodity" && (
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 13,
                      marginTop: 10
                    }}
                  >
                    RANK MF research does not recommend SIP’s in commodity funds
                    since commodities are cyclical in nature and usually do not
                    create long term wealth via SIP.
                  </Text>
                )}
                <Text
                  style={{
                    fontWeight: "normal",
                    color: "black",
                    fontSize: 12,
                    marginTop: 10
                  }}
                >
                  {this.state.aggregate.sip_desc}
                </Text>
              </View>
              <Image
                source={require("../../assets/images/Complete-Your-Application.png")}
                style={{ position: "absolute" }}
              />
            </View>
          );
        }
      } else if (this.state.aggregate.existing_sip_flag === "2") {
        if (clienttype === "client") {
          return (
            <View>
              <View
                style={{
                  flexDirection: "row",
                  marginTop: 10,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <Image
                  style={{
                    width: 40,
                    height: 40,
                    resizeMode: "contain",
                    alignItems: "center"
                  }}
                  source={doublelike}
                />

                <Text
                  style={{
                    fontWeight: "bold",
                    color: "black",
                    fontSize: 13,
                    marginLeft: 10
                  }}
                >
                  {this.state.aggregate.conclusion}
                </Text>
              </View>
              {this.state.aggregate.asset_type === "Commodity" && (
                <Text
                  style={{
                    fontWeight: "bold",
                    color: "black",
                    fontSize: 13,
                    marginTop: 10
                  }}
                >
                  RANK MF research does not recommend SIP’s in commodity funds
                  since commodities are cyclical in nature and usually do not
                  create long term wealth via SIP.
                </Text>
              )}
              <Text
                style={{
                  fontWeight: "normal",
                  color: "black",
                  fontSize: 12,
                  marginTop: 10
                }}
              >
                {this.state.aggregate.sip_desc}
              </Text>
            </View>
          );
        } else if (clienttype === "lead") {
          return (
            <View style={{ alignItems: "center", justifyContent: "center" }}>
              <View style={{ opacity: 0.05 }}>
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 10,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Image
                    style={{
                      width: 40,
                      height: 40,
                      resizeMode: "contain",
                      alignItems: "center"
                    }}
                    source={doublelike}
                  />

                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 13,
                      marginLeft: 10
                    }}
                  >
                    {this.state.aggregate.conclusion}
                  </Text>
                </View>
                {this.state.aggregate.asset_type === "Commodity" && (
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 13,
                      marginTop: 10
                    }}
                  >
                    RANK MF research does not recommend SIP’s in commodity funds
                    since commodities are cyclical in nature and usually do not
                    create long term wealth via SIP.
                  </Text>
                )}
                <Text
                  style={{
                    fontWeight: "normal",
                    color: "black",
                    fontSize: 12,
                    marginTop: 10
                  }}
                >
                  {this.state.aggregate.sip_desc}
                </Text>
              </View>
              <Image
                source={require("../../assets/images/Complete-Your-Application.png")}
                style={{ position: "absolute" }}
              />
            </View>
          );
        }
      } else if (this.state.aggregate.existing_sip_flag === "3") {
        if (clienttype === "client") {
          return (
            <View>
              <View
                style={{
                  flexDirection: "row",
                  marginTop: 10,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <View
                  style={[
                    styles.circle,
                    { backgroundColor: color.RED, alignItems: "center" }
                  ]}
                >
                  <Image
                    style={{
                      width: 20,
                      height: 20,
                      resizeMode: "contain",
                      alignItems: "center",
                      marginTop: 9
                    }}
                    source={thumbsdown}
                  />
                </View>
                <Text
                  style={{
                    fontWeight: "bold",
                    color: "black",
                    fontSize: 13,
                    marginLeft: 10
                  }}
                >
                  {this.state.aggregate.conclusion}
                </Text>
              </View>
              {this.state.aggregate.asset_type === "Commodity" && (
                <Text
                  style={{
                    fontWeight: "bold",
                    color: "black",
                    fontSize: 13,
                    marginTop: 10
                  }}
                >
                  RANK MF research does not recommend SIP’s in commodity funds
                  since commodities are cyclical in nature and usually do not
                  create long term wealth via SIP.
                </Text>
              )}
              <Text
                style={{
                  fontWeight: "normal",
                  color: "black",
                  fontSize: 12,
                  marginTop: 10
                }}
              >
                {this.state.aggregate.sip_desc}
              </Text>
            </View>
          );
        } else if (clienttype === "lead") {
          return (
            <View style={{ alignItems: "center", justifyContent: "center" }}>
              <View style={{ opacity: 0.05 }}>
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 10,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <View
                    style={[
                      styles.circle,
                      { backgroundColor: color.RED, alignItems: "center" }
                    ]}
                  >
                    <Image
                      style={{
                        width: 20,
                        height: 20,
                        resizeMode: "contain",
                        alignItems: "center",
                        marginTop: 9
                      }}
                      source={thumbsdown}
                    />
                  </View>
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 13,
                      marginLeft: 10
                    }}
                  >
                    {this.state.aggregate.conclusion}
                  </Text>
                </View>
                {this.state.aggregate.asset_type === "Commodity" && (
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      fontSize: 13,
                      marginTop: 10
                    }}
                  >
                    RANK MF research does not recommend SIP’s in commodity funds
                    since commodities are cyclical in nature and usually do not
                    create long term wealth via SIP.
                  </Text>
                )}
                <Text
                  style={{
                    fontWeight: "normal",
                    color: "black",
                    fontSize: 12,
                    marginTop: 10
                  }}
                >
                  {this.state.aggregate.sip_desc}
                </Text>
              </View>
              <Image
                source={require("../../assets/images/Complete-Your-Application.png")}
                style={{ position: "absolute" }}
              />
            </View>
          );
        }
      }
    } else {
      if (clienttype === "client") {
        return (
          <Image
            style={{
              width: 40,
              height: 40,
              resizeMode: "contain",
              marginTop: 10
            }}
            source={newicon}
          />
        );
      } else if (clienttype === "lead") {
        return (
          <View style={{ alignItems: "center", justifyContent: "center" }}>
            <View style={{ opacity: 0.05 }}>
              <Image
                style={{
                  width: 40,
                  height: 40,
                  resizeMode: "contain",
                  marginTop: 10
                }}
                source={newicon}
              />
            </View>
            <Image
              source={require("../../assets/images/Complete-Your-Application.png")}
              style={{ position: "absolute" }}
            />
          </View>
        );
      }
    }
  }

  render() {
    var Highcharts = "Highcharts";

    var growth = {
      chart: {
        type: "spline",
        animation: Highcharts.svg, // don't animate in old IE
        marginRight: 10,
        events: {
          load: function() {}
        }
      },
      credits: {
        enabled: false
      },
      title: {
        text: ""
      },
      xAxis: {
        type: "datetime",
        dateTimeLabelFormats: {
          month: "%b %y"
        },
        tickPixelInterval: 50
      },
      yAxis: {
        title: "",
        plotLines: [
          {
            value: 0,
            width: 1,
            color: "#808080"
          }
        ]
      },
      tooltip: {
        headerFormat: "<b>{series.name}</b><br>",
        pointFormat: "{point.x:%e %b %y }: {point.y:.2f} "
      },
      plotOptions: {
        spline: {
          marker: {
            enabled: false
          }
        }
      },
      legend: {
        enabled: true
      },
      exporting: {
        enabled: false
      },
      series: [
        {
          name: `SIP at ${this.state.sipamtgrowth} per month`,
          data: this.state.growthdata1,
          color: color.ChartBlue
        },
        {
          name: "Total invested",
          data: this.state.growthdata2,
          color: color.ChartGreen
        }
      ]
    };

    var pregrowth = {
      chart: {
        type: "spline",
        animation: Highcharts.svg, // don't animate in old IE
        marginRight: 10,
        events: {
          load: function() {}
        }
      },
      credits: {
        enabled: false
      },
      title: {
        text: ""
      },
      xAxis: {
        type: "datetime",
        dateTimeLabelFormats: {
          month: "%b %y"
        },
        tickPixelInterval: 50
      },
      yAxis: {
        title: "",
        plotLines: [
          {
            value: 0,
            width: 1,
            color: "#808080"
          }
        ]
      },
      tooltip: {
        headerFormat: "<b>{series.name}</b><br>",
        pointFormat: "{point.x:%e %b %y }: {point.y:.2f} "
      },
      plotOptions: {
        spline: {
          marker: {
            enabled: false
          }
        }
      },
      legend: {
        enabled: true
      },
      exporting: {
        enabled: false
      },
      series: [
        {
          name: `SIP at ${this.state.sipamtpregrowth} per month`,
          data: this.state.pregrowthdata1,

          color: color.ChartBlue
        },
        {
          name: "Total invested",
          data: this.state.pregrowthdata2,
          color: color.ChartGreen
        }
      ]
    };
    const options = {
      global: {
        useUTC: false
      },
      lang: {
        decimalPoint: ".",
        thousandsSep: ","
      }
    };

    const DatePickerStart = ({ style, ...props }) => (
      <ModalDatePicker
        style={[styles.container, style]}
        renderDate={({ year, month, day, date }) => {
          if (!date) {
            return (
              <Text style={[styles.dateinput, { marginLeft: 5 }]}>
                {datestart}
              </Text>
            );
          }

          //dateFinal = `${day}-${month}-${year}`;
          datestart = `${year}-${month}-${day}`;

          return (
            <Text style={[styles.dateinput, { marginLeft: 5 }]}>
              {datestart}
            </Text>
          );
        }}
        {...props}
      />
    );
    const DatePickerEnd = ({ style, ...props }) => (
      <ModalDatePicker
        style={[styles.container, style]}
        renderDate={({ year, month, day, date }) => {
          if (!date) {
            return (
              <Text style={[styles.dateinput, { marginLeft: 7 }]}>
                {dateend}
              </Text>
            );
          }

          //  dateFinal = `${day}-${month}-${year}`
          dateend = `${year}-${month}-${day}`;

          return (
            <Text style={[styles.dateinput, { marginLeft: 7 }]}>{dateend}</Text>
          );
        }}
        {...props}
      />
    );
    return (
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.containerFlex}
      >
        <View>
          <Card>
            <View>
              <Text
                style={{
                  fontWeight: "bold",
                  color: "black",
                  fontSize: 15,
                  flex: 1
                }}
              >
                Is this the correct time to do SIP in{" "}
                {this.state.aggregate.Scheme_Name}?
              </Text>
              <View style={{ marginTop: 8 }}>{this.renderCorrectTime()}</View>
            </View>
            <View style={{ marginTop: 10, flexDirection: "row" }}>
              <Text
                style={{ fontWeight: "bold", color: "black", fontSize: 13 }}
              >
                How much money would you have made with an SIP in{" "}
                {this.state.aggregate.Scheme_Name}?
              </Text>
            </View>

            <View
              style={{
                flexDirection: "column",
                marginTop: 10,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <Text style={[styles.title, { flex: 1 }]}>
                  Monthly Amount :{" "}
                </Text>
                <TextInput
                returnKeyType="done"
                  placeholder="Monthly Amount"
                  underlineColorAndroid="transparent"
                  keyboardType="numeric"
                  defaultValue="5000"
                  onChangeText={text => this.setState({ sipamtgrowth: text })}
                  style={{
                    borderBottomColor: "black",
                    borderBottomWidth: 1,
                    flex: 1,
                    marginLeft: 10,
                    textAlign: "center"
                  }}
                />
              </View>
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <View style={{ flex: 1 }}>
                  <Text style={styles.title}>From</Text>
                  <DatePickerStart style={{ marginBottom: 15, marginTop: 3 }} />
                </View>
                <View style={{ flex: 1, marginLeft: 15 }}>
                  <Text style={styles.title}>To</Text>
                  <DatePickerEnd style={{ marginBottom: 15, marginTop: 3 }} />
                </View>
              </View>
              <TouchableOpacity
                onPress={() => this.fetchGrowthChart()}
                style={[
                  styles.button,
                  {
                    backgroundColor: color.toolbar,
                    width: 150,
                    alignItems: "center",
                    justifyContent: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    styles.instructions,
                    { fontSize: Platform.OS == "ios" ? 13 : 10 }
                  ]}
                >
                  CALCULATE
                </Text>
              </TouchableOpacity>
            </View>

            <View style={[styles.itemchart, { marginTop: 15 }]}>
              <ChartView
                style={{ height: 300 }}
                config={growth}
                options={options}
              />
            </View>

            <View style={{ flexDirection: "row" }}>
              <Card containerStyle={{ flex: 1 }}>
                <View style={{ flexDirection: "column" }}>
                  <Text style={{ color: "blue", textAlign: "center" }}>
                    {"\u20B9"}
                    {parseFloat(this.state.sipamtgrowth) *
                      parseFloat(this.state.growthdata3.tot_inst)}
                  </Text>
                  <Text style={{ marginTop: 5, textAlign: "center" }}>
                    Total Invested
                  </Text>
                </View>
              </Card>
              <Card containerStyle={{ flex: 1 }}>
                <View style={{ flexDirection: "column" }}>
                  <Text style={{ color: "blue", textAlign: "center" }}>
                    {"\u20B9"}
                    {parseFloat(this.state.growthdata3.calcnav).toFixed(0)}
                  </Text>
                  <Text style={{ marginTop: 5, textAlign: "center" }}>
                    Current Valuation
                  </Text>
                </View>
              </Card>
            </View>

            <View style={{ flexDirection: "row" }}>
              <Card containerStyle={{ flex: 1 }}>
                <View style={{ flexDirection: "column" }}>
                  <Text style={{ color: "blue", textAlign: "center" }}>
                    {"\u20B9"}
                    {parseFloat(
                      parseFloat(this.state.growthdata3.calcnav) -
                        parseFloat(
                          parseFloat(this.state.sipamtgrowth) *
                            parseFloat(this.state.growthdata3.tot_inst)
                        )
                    ).toFixed(0)}
                  </Text>
                  <Text style={{ marginTop: 5, textAlign: "center" }}>
                    Net Profit
                  </Text>
                </View>
              </Card>
              <Card containerStyle={{ flex: 1 }}>
                <View style={{ flexDirection: "column" }}>
                  <Text style={{ color: "blue", textAlign: "center" }}>
                    {parseFloat(
                      ((parseFloat(this.state.growthdata3.calcnav) -
                        parseFloat(
                          parseFloat(this.state.sipamtgrowth) *
                            parseFloat(this.state.growthdata3.tot_inst)
                        )) /
                        parseFloat(
                          parseFloat(this.state.sipamtgrowth) *
                            parseFloat(this.state.growthdata3.tot_inst)
                        )) *
                        100
                    ).toFixed(0)}
                    %
                  </Text>
                  <Text style={{ marginTop: 5, textAlign: "center" }}>
                    Absolute profit
                  </Text>
                </View>
              </Card>
            </View>

            <View style={{ flexDirection: "row" }}>
              <Card containerStyle={{ flex: 1 }}>
                <View style={{ flexDirection: "column" }}>
                  <Text style={{ color: "blue", textAlign: "center" }}>
                    {this.state.growthdata3.tot_inst}
                  </Text>
                  <Text style={{ marginTop: 5, textAlign: "center" }}>
                    Total installments
                  </Text>
                </View>
              </Card>
              <Card containerStyle={{ flex: 1 }}>
                <View style={{ flexDirection: "column" }}>
                  <Text style={{ color: "blue", textAlign: "center" }}>
                    {this.state.growthdata3.irr}%
                  </Text>
                  <Text style={{ marginTop: 5, textAlign: "center" }}>
                    Internal rate of return
                  </Text>
                </View>
              </Card>
            </View>

            <View style={{ flexDirection: "row" }}>
              <Card containerStyle={{ flex: 1 }}>
                <View style={{ flexDirection: "column" }}>
                  <Text style={{ color: "blue", textAlign: "center" }}>
                    {datestart}
                  </Text>
                  <Text style={{ marginTop: 5, textAlign: "center" }}>
                    SIP start date
                  </Text>
                </View>
              </Card>
              <Card containerStyle={{ flex: 1 }}>
                <View style={{ flexDirection: "column" }}>
                  <Text style={{ color: "blue", textAlign: "center" }}>
                    {dateend}
                  </Text>
                  <Text style={{ marginTop: 5, textAlign: "center" }}>
                    SIP end dates
                  </Text>
                </View>
              </Card>
            </View>

            <Text
              style={{
                fontWeight: "bold",
                fontSize: 15,
                color: "black",
                marginTop: 15
              }}
            >
              {this.state.aggregate.Scheme_Name} - Growth - SIP Calculator
            </Text>
            {clienttype === "client" && (
              <View>
                <View style={{ marginTop: 10, flexDirection: "row" }}>
                  <Text
                    style={{ fontWeight: "bold", color: "black", fontSize: 13 }}
                  >
                    How much money can you expect to make with an SIP in{" "}
                    {this.state.aggregate.Scheme_Name} - Growth in the future?
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "column",
                    marginTop: 10,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <View style={{ flexDirection: "row", marginTop: 10 }}>
                    <Text style={[styles.title, { flex: 1 }]}>
                      Monthly Amount :{" "}
                    </Text>
                    <TextInput
                    returnKeyType="done"
                      placeholder="SIP Amount"
                      underlineColorAndroid="transparent"
                      keyboardType="numeric"
                      defaultValue="5000"
                      onChangeText={text =>
                        this.setState({ sipamtpregrowth: text })
                      }
                      style={{
                        borderBottomColor: "black",
                        borderBottomWidth: 1,
                        flex: 1,
                        marginLeft: 10,
                        textAlign: "center"
                      }}
                    />
                  </View>
                  <View style={{ marginTop: 10, flexDirection: "row" }}>
                    <Text
                      style={[styles.title, { flex: 1 }]}
                      onPress={this.showSlideAnimationDialog}
                    >
                      SIP period (yrs) :{" "}
                    </Text>
                    <Text
                    onPress={() => Actions.page1({ dialog: "sipperiod" })}
                    style={[
                      styles.input,
                      { flex: 1 }
                    ]}
                  >
                    {this.props.sipperiod}
                  </Text>
                   
                  </View>
                  <View style={{ flexDirection: "row", marginTop: 10 }}>
                    <Text style={[styles.title, { flex: 1 }]}>
                      Expected Yearly Return :{" "}
                    </Text>
                    <View style={{ flexDirection: "row", flex: 1 }}>
                      <TextInput
                      returnKeyType="done"
                        placeholder="Expected Yearly Return"
                        underlineColorAndroid="transparent"
                        keyboardType="numeric"
                        value={parseFloat(this.state.aggregate.INCRET).toFixed(
                          0
                        )}
                        onChangeText={text => this.setState({ interest: text })}
                        style={{
                          borderBottomColor: "black",
                          borderBottomWidth: 1,
                          textAlign: "center",
                          flex: 0.8
                        }}
                      />
                      <Text style={{ fontSize: 10, flex: 1.2, marginLeft: 3 }}>
                        {" "}
                        %(Average Inception Return)
                      </Text>
                    </View>
                  </View>
                  <TouchableOpacity
                    onPress={() => this.fetchPreGrowthChart()}
                    style={[
                      styles.button,
                      {
                        backgroundColor: color.toolbar,
                        width: 150,
                        alignItems: "center",
                        justifyContent: "center",
                        marginTop: 10
                      }
                    ]}
                  >
                    <Text
                      style={[
                        styles.instructions,
                        { fontSize: Platform.OS == "ios" ? 13 : 10 }
                      ]}
                    >
                      CALCULATE
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.itemchart}>
                  <ChartView
                    style={{ height: 300 }}
                    config={pregrowth}
                    options={options}
                  />
                </View>
                {/* <View style={{flexDirection:"row",marginTop:3}}>
        <Text style={{color:'blue',fontSize:13,flex:1.3}}>-SIP at {this.state.sipamtpregrowth} per month</Text>
        <Text style={{color:'green',fontSize:13,flex:0.7}}>-Total Invested</Text>       
        </View> */}

                <View style={{ flexDirection: "row" }}>
                  <Card containerStyle={{ flex: 1 }}>
                    <View style={{ flexDirection: "column" }}>
                      <Text style={{ color: "blue", textAlign: "center" }}>
                        {"\u20B9"}
                        {parseFloat(this.state.sipamtpregrowth) *
                          parseFloat(this.state.growthdata3.tot_inst)}
                      </Text>
                      <Text style={{ marginTop: 5, textAlign: "center" }}>
                        Total Invested
                      </Text>
                    </View>
                  </Card>
                  <Card containerStyle={{ flex: 1 }}>
                    <View style={{ flexDirection: "column" }}>
                      {this.state.pregrowthdata1.length != 0 && (
                        <Text style={{ color: "blue", textAlign: "center" }}>
                          {"\u20B9"}
                          {parseFloat(
                            this.state.pregrowthdata1[
                              this.state.pregrowthdata1.length - 1
                            ][1]
                          ).toFixed(0)}
                        </Text>
                      )}
                      <Text style={{ marginTop: 5, textAlign: "center" }}>
                        Current Valuation
                      </Text>
                    </View>
                  </Card>
                </View>

                <View style={{ flexDirection: "row" }}>
                  <Card containerStyle={{ flex: 1 }}>
                    <View style={{ flexDirection: "column" }}>
                      {this.state.pregrowthdata1.length != 0 && (
                        <Text style={{ color: "blue", textAlign: "center" }}>
                          {"\u20B9"}
                          {parseFloat(
                            parseFloat(
                              this.state.pregrowthdata1[
                                this.state.pregrowthdata1.length - 1
                              ][1]
                            ) -
                              parseFloat(
                                parseFloat(this.state.sipamtpregrowth) *
                                  parseFloat(this.state.growthdata3.tot_inst)
                              )
                          ).toFixed(0)}
                        </Text>
                      )}
                      <Text style={{ marginTop: 5, textAlign: "center" }}>
                        Net Profit
                      </Text>
                    </View>
                  </Card>
                  <Card containerStyle={{ flex: 1 }}>
                    <View style={{ flexDirection: "column" }}>
                      {this.state.pregrowthdata1.length != 0 && (
                        <Text style={{ color: "blue", textAlign: "center" }}>
                          {parseFloat(
                            ((parseFloat(
                              this.state.pregrowthdata1[
                                this.state.pregrowthdata1.length - 1
                              ][1]
                            ) -
                              parseFloat(
                                parseFloat(this.state.sipamtpregrowth) *
                                  parseFloat(this.state.growthdata3.tot_inst)
                              )) /
                              parseFloat(
                                parseFloat(this.state.sipamtpregrowth) *
                                  parseFloat(this.state.growthdata3.tot_inst)
                              )) *
                              100
                          ).toFixed(0)}
                          %
                        </Text>
                      )}
                      <Text style={{ marginTop: 5, textAlign: "center" }}>
                        Absolute profit
                      </Text>
                    </View>
                  </Card>
                </View>
              </View>
            )}
            {clienttype === "lead" && (
              <View style={{ justifyContent: "center" }}>
                <View style={{ opacity: 0.05 }}>
                  <View style={{ marginTop: 10, flexDirection: "row" }}>
                    <Text
                      style={{
                        fontWeight: "bold",
                        color: "black",
                        fontSize: 13
                      }}
                    >
                      How much money can you expect to make with an SIP in{" "}
                      {this.state.aggregate.Scheme_Name} - Growth in the future?
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "column",
                      marginTop: 10,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <View style={{ flexDirection: "row", marginTop: 10 }}>
                      <Text style={[styles.title, { flex: 1 }]}>
                        Monthly Amount :{" "}
                      </Text>
                      <TextInput
                      returnKeyType="done"
                        placeholder="SIP Amount"
                        underlineColorAndroid="transparent"
                        keyboardType="numeric"
                        defaultValue="5000"
                        onChangeText={text =>
                          this.setState({ sipamtpregrowth: text })
                        }
                        style={{
                          borderBottomColor: "black",
                          borderBottomWidth: 1,
                          flex: 1,
                          marginLeft: 10,
                          textAlign: "center"
                        }}
                      />
                    </View>
                    <View style={{ marginTop: 10, flexDirection: "row" }}>
                      <Text style={[styles.title, { flex: 1 }]}>
                        SIP period (yrs) :{" "}
                      </Text>
                      <Text
                    onPress={() => Actions.page1({ dialog: "sipperiod" })}
                    style={[
                      styles.input,
                      { flex: 1 }
                    ]}
                  >
                    {this.props.sipperiod}
                  </Text>
                   
                    </View>
                    <View style={{ flexDirection: "row", marginTop: 10 }}>
                      <Text style={[styles.title, { flex: 1 }]}>
                        Expected Yearly Return :{" "}
                      </Text>
                      <View style={{ flexDirection: "row", flex: 1 }}>
                        <TextInput
                        returnKeyType="done"
                          placeholder="Expected Yearly Return"
                          underlineColorAndroid="transparent"
                          keyboardType="numeric"
                          defaultValue={parseFloat(
                            this.state.aggregate.INCRET
                          ).toFixed(0)}
                          onChangeText={text =>
                            this.setState({ interest: text })
                          }
                          style={{
                            borderBottomColor: "black",
                            borderBottomWidth: 1,
                            textAlign: "center",
                            flex: 0.8
                          }}
                        />
                        <Text
                          style={{ fontSize: 10, flex: 1.2, marginLeft: 3 }}
                        >
                          {" "}
                          %(Average Inception Return)
                        </Text>
                      </View>
                    </View>
                    <TouchableOpacity
                      style={[
                        styles.button,
                        {
                          backgroundColor: color.toolbar,
                          width: 150,
                          alignItems: "center",
                          justifyContent: "center",
                          marginTop: 10
                        }
                      ]}
                    >
                      <Text
                        style={[
                          styles.instructions,
                          { fontSize: Platform.OS == "ios" ? 13 : 10 }
                        ]}
                      >
                        CALCULATE
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.itemchart}>
                    <ChartView
                      style={{ height: 300 }}
                      config={pregrowth}
                      options={options}
                      style={{ opacity: 0.05 }}
                    />
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <Card containerStyle={{ flex: 1 }}>
                      <View style={{ flexDirection: "column" }}>
                        <Text style={{ color: "blue", textAlign: "center" }}>
                          {"\u20B9"}
                          {parseFloat(this.state.sipamtpregrowth) *
                            parseFloat(this.state.growthdata3.tot_inst)}
                        </Text>
                        <Text style={{ marginTop: 5, textAlign: "center" }}>
                          Total Invested
                        </Text>
                      </View>
                    </Card>
                    <Card containerStyle={{ flex: 1 }}>
                      <View style={{ flexDirection: "column" }}>
                        {this.state.pregrowthdata1.length != 0 && (
                          <Text style={{ color: "blue", textAlign: "center" }}>
                            {"\u20B9"}
                            {parseFloat(
                              this.state.pregrowthdata1[
                                this.state.pregrowthdata1.length - 1
                              ][1]
                            ).toFixed(0)}
                          </Text>
                        )}
                        <Text style={{ marginTop: 5, textAlign: "center" }}>
                          Current Valuation
                        </Text>
                      </View>
                    </Card>
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <Card containerStyle={{ flex: 1 }}>
                      <View style={{ flexDirection: "column" }}>
                        {this.state.pregrowthdata1.length != 0 && (
                          <Text style={{ color: "blue", textAlign: "center" }}>
                            {"\u20B9"}
                            {parseFloat(
                              parseFloat(
                                this.state.pregrowthdata1[
                                  this.state.pregrowthdata1.length - 1
                                ][1]
                              ) -
                                parseFloat(
                                  parseFloat(this.state.sipamtpregrowth) *
                                    parseFloat(this.state.growthdata3.tot_inst)
                                )
                            ).toFixed(0)}
                          </Text>
                        )}
                        <Text style={{ marginTop: 5, textAlign: "center" }}>
                          Net Profit
                        </Text>
                      </View>
                    </Card>
                    <Card containerStyle={{ flex: 1 }}>
                      <View style={{ flexDirection: "column" }}>
                        {this.state.pregrowthdata1.length != 0 && (
                          <Text style={{ color: "blue", textAlign: "center" }}>
                            {parseFloat(
                              ((parseFloat(
                                this.state.pregrowthdata1[
                                  this.state.pregrowthdata1.length - 1
                                ][1]
                              ) -
                                parseFloat(
                                  parseFloat(this.state.sipamtpregrowth) *
                                    parseFloat(this.state.growthdata3.tot_inst)
                                )) /
                                parseFloat(
                                  parseFloat(this.state.sipamtpregrowth) *
                                    parseFloat(this.state.growthdata3.tot_inst)
                                )) *
                                100
                            ).toFixed(0)}
                            %
                          </Text>
                        )}
                        <Text style={{ marginTop: 5, textAlign: "center" }}>
                          Absolute profit
                        </Text>
                      </View>
                    </Card>
                  </View>
                </View>
                <View
                  style={{
                    position: "absolute",
                    alignItems: "center",
                    justifyContent: "center",
                    alignSelf: "center"
                  }}
                >
                  <Image
                    source={require("../../assets/images/Complete-Your-Application.png")}
                  />
                </View>
              </View>
            )}
          </Card>
         
        </View>
      </ScrollView>
    );
  }
}

export default Sip;
