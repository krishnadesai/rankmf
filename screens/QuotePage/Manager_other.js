import React, {Component} from 'react';
import { StyleSheet,Platform, Text,  View,ScrollView,Image,Picker,AsyncStorage,FlatList,KeyboardAvoidingView} from 'react-native';
import { Card } from 'react-native-elements';
import styles from '../../components/styles';
import Apis from '../../constants/Apis';

class Manager extends Component {

  constructor(props) {
    super(props);
    this.state = {
      aggregate:{},
      response:{},
      mgr_details:[],
      sip_details:{},
      load:{}
    }
  }

  componentDidMount(){
    this.retrieveData()
  }

  retrieveData = async () => {
    try {
     clienttype = await AsyncStorage.getItem('clienttype');
     clientid = await AsyncStorage.getItem('clientid');
     access_token =await AsyncStorage.getItem('access_token');  
     if(clienttype === "client"){
       this.setState({showFund:true})
     }else{
      this.setState({showFund:false})
     }
     this.fetchData();
     } catch (error) {
       // Error retrieving data
       //console.warn("1");
     }
  }


  fetchData(){
   // alert(this.props.unique_code);
    fetch(Apis.BaseURL, {
      method: 'POST',
      headers: new Headers({
                  'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
          }),
      body: `api=1.2&access_token=${access_token}&user=${clientid}&unique_code=${this.props.unique_code}&type=fundmanager_rta_contact`// <-- Post parameters
          })
          .then((response) => response.json())
          .then((responseJson) => {
             
               this.setState({response:responseJson["data"]["response"][0],aggregate:responseJson["data"]["aggregate"],mgr_details:responseJson["data"]["mgr_details"],sip_details:responseJson["data"]["sip_details"][3],load:responseJson["data"]["load"]})
               //alert(responseJson["data"]["aggregate"].Scheme_Name)
             
          })
          .catch((error) => {
              console.error(error);
          });
  }

  renderManagers(item){
   // alert(this.state.mgr_details[0])
  return(   
        
            <Card >
            <Text style={{fontWeight:"bold",color:'black',fontSize:17}}>{item.FUNDMANAGER}</Text>
            <Text style={{fontWeight:"bold",fontSize:14,color:"#404040",marginTop:10}}>Education : 
            <Text style={styles.carddetail}>{item.QUALIFICATION}</Text></Text>       
            <Text style={{fontWeight:"bold",fontSize:14,color:"#404040",marginTop:5}}>Experience : 
            <Text style={styles.carddetail}>{item.BASICDETAILS}</Text>  </Text>       
           </Card>
          
      
  );
  }

  render () {
    return (
      
      <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.containerFlex}>
      <View>
      <Text style={{fontWeight:"bold",color:'black',fontSize:17,textAlign:"center",marginTop:15}}>Investment Details</Text>
      <Card>
        <View style={{flexDirection:"row"}}>
          <Text style={{fontWeight:"bold",fontSize:14,color:"#000000",flex:1.5}}>Minimum Investment (R)</Text>      
          <Text style={{fontWeight:"normal",fontSize:13,color:"#000000",flex:0.5,alignItems:"flex-end"}}>{this.state.response.mininvt}</Text> 
        </View>
        <View style={{flexDirection:"row"}}>
          <Text style={{fontWeight:"bold",fontSize:14,color:"#000000",flex:1.5}}>Minimum Addl Investment (R)</Text>      
          <Text style={{fontWeight:"normal",fontSize:13,color:"#000000",flex:0.5,alignItems:"flex-end"}}>{this.state.sip_details.SIPADDNINVEST}</Text> 
        </View>
        <View style={{flexDirection:"row"}}>
          <Text style={{fontWeight:"bold",fontSize:14,color:"#000000",flex:1.5}}>Minimum SIP Investment (R)</Text>      
          <Text style={{fontWeight:"normal",fontSize:13,color:"#000000",flex:0.5,alignItems:"flex-end"}}>{this.state.sip_details.SIPMININVEST}</Text> 
        </View>
       { this.state.load != false ? (<View style={{flexDirection:"row"}}>
          <Text style={{fontWeight:"bold",fontSize:14,color:"#000000",flex:1.5}}>Exit Load (%)</Text>      
          <Text style={{fontWeight:"normal",fontSize:13,color:"#000000",flex:0.5,alignItems:"flex-end"}}>{this.state.load.EXITLOAD}</Text> 
        </View> ) : null}
        </Card>
        <Card>
        <View style={{flexDirection:"row"}}>
        <Text style={{fontWeight:"bold",fontSize:14,color:"#000000",flex:0.5}}>ISIN</Text>
        <Text style={{fontWeight:"normal",fontSize:13,color:"#000000",flex:1.5,alignItems:"flex-start"}}>{this.state.aggregate.ISIN}</Text>
        </View> 
        </Card>

        <Text style={{fontWeight:"bold",color:'black',fontSize:17,textAlign:"center",marginTop:15}}>Fund Managers</Text>

        {this.state.mgr_details.length>0 ? (<FlatList
                    style={{marginTop:10,}}                   
                    data={this.state.mgr_details}
                    renderItem={( {item,index }) => this.renderManagers(item) }
                    
            />) :  null}
        
        

        <Card >
         <Text style={{fontWeight:"bold",color:'black',fontSize:17}}>Contact Information</Text>
         <Text style={{fontWeight:"bold",fontSize:14,color:"#404040",marginTop:10}}>AMC : 
         <Text style={styles.carddetail}>  {this.state.response.amc}
         </Text>  </Text>       
         <Text style={{fontWeight:"bold",fontSize:14,color:"#404040",marginTop:5}}>Address : 
         <Text style={styles.carddetail}>  {this.state.response.add1} {this.state.response.add2} {this.state.response.add3} {this.state.response.phone}</Text></Text>     
          <Text style={{fontWeight:"bold",fontSize:14,color:"#404040",marginTop:5}}>Website : 
         <Text style={styles.carddetail}>  {this.state.response.Website}</Text></Text>     
        </Card>

 <Card >
         <Text style={{fontWeight:"bold",color:'black',fontSize:17}}>Registrar & Transfer Agent</Text>
         <Text style={{fontWeight:"normal",color:'black',fontSize:14,marginTop:10}}>{this.state.response.Rt_name}</Text>
              
         <Text style={{fontWeight:"bold",fontSize:14,color:"#404040",marginTop:5}}>Address : 
         <Text style={styles.carddetail}>  {this.state.response.Address1}{this.state.response.Address2}{this.state.response.Address3}{this.state.response.Tel}</Text></Text>     
          <Text style={{fontWeight:"bold",fontSize:14,color:"#404040",marginTop:5}}>Email : 
         <Text style={styles.carddetail}>  {this.state.response.email}</Text></Text>  
          <Text style={{fontWeight:"bold",fontSize:14,color:"#404040",marginTop:5}}>Website : 
         <Text style={styles.carddetail}>  {this.state.response.Website}</Text></Text>     
        </Card>
        </View>
      </ScrollView>
     
    );
  }
}




export default Manager;