import React, {Component} from 'react';
import { StyleSheet, Text, View,Image,Button,TouchableOpacity,Picker } from 'react-native';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';


class Page3 extends Component {

  constructor(){
    super()
    this.state = {
        text: '',
        PickerValueHolder : ''
    }
    this.onSelect = this.onSelect.bind(this)
}

onSelect(index, value){
    this.setState({
    text: `Selected index: ${index} , value: ${value}`
    })  
}



GetSelectedPickerItem=()=>{

  alert(this.state.PickerValueHolder);
}


  render () {
    return (
      <View style={styles.container}>
                <RadioGroup
                    size={24}
                    thickness={2}
                    color={color.toolbar}
                    highlightColor='#FFFFFF'
                    selectedIndex={1}
                    onSelect = {(index, value) => this.onSelect(index, value)}
                >
                    <RadioButton 
                        style={{alignItems:'center'}}
                        value='Yo!! I am a cat.'                        
                    >
                        <Image
                            style={{width:100, height: 100,resizeMode:"contain"}}
                            source={require('../assets/images/samcologo.png')}
                        />
                    </RadioButton>

                    <RadioButton 
                        value='index1'
                    > 
                        <Text>Start from item index #1</Text>
                    </RadioButton>

                    <RadioButton 
                        value='red color'
                        color='red'
                    >
                        <Text>Red Dot</Text>
                    </RadioButton>

                    <RadioButton 
                        value='green color'
                        color='green'
                    >
                        <Text>Green Dot</Text>
                    </RadioButton>

                    <RadioButton 
                        value='blue color'
                        color='blue'
                    >
                        <Text>Blue Dot</Text>
                    </RadioButton>
                </RadioGroup>
              
                <Text style={styles.text}>{this.state.text}</Text>
                <Button title="Button" onPress={()=>alert(this.state.text)}/>
            
                <Picker
        selectedValue={this.state.PickerValueHolder}
 
        onValueChange={(itemValue, itemIndex) => this.setState({PickerValueHolder: itemValue})} >
 
        <Picker.Item label="React Native" value="React Native" />
        <Picker.Item label="Java" value="Java" />
        <Picker.Item label="Html" value="Html" />
        <Picker.Item label="Php" value="Php" />
        <Picker.Item label="C++" value="C++" />
        <Picker.Item label="JavaScript" value="JavaScript" />
 
      </Picker>
      <Button title="Get Selected Picker Value" onPress={ this.GetSelectedPickerItem } />
            </View>
    );
  }
}

let styles = StyleSheet.create({
  container: {
      marginTop: 40,
  },
  text: {
      padding: 10,
      fontSize: 14,
  },reverse: {
    transform: [{
      rotate: '180deg',
    }],
  },
})

export default Page3;