import React, { Component, Props } from "react";
import {
  Animated,
  Dimensions,
  Image,
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ScrollView,
  AsyncStorage,
  TouchableOpacity
} from "react-native";
import {
  Body,
  Header,
  List,
  ListItem as Item,
  ScrollableTab,
  Tab,
  TabHeading,
  Tabs,
  Title
} from "native-base";
import { Card } from "react-native-elements";
import PortfolioSnapshot from "../screens/ManageAccounts/PortfolioSnapshot";
import PortfolioSummary from "../screens/ManageAccounts/PortfolioSummary";
import PortfolioAnalytics from "../screens/ManageAccounts/PortfolioAnalytics";
import GainLossReport from "../screens/ManageAccounts/GainLossReport";
import SipHistory from "../screens/ManageAccounts/SipHistory";
import SipCalendar from "../screens/ManageAccounts/SipCalendar";
import AccountStatement from "../screens/ManageAccounts/AccountStatement";
import OrderHistory from "../screens/ManageAccounts/OrderHistory";
import MandateRegistration from "../screens/ManageAccounts/MandateRegistration";
import { Actions } from "react-native-router-flux";
import styles from "../components/styles";
import Apis from "../constants/Apis";
import color from "../constants/Colors";

const { width: SCREEN_WIDTH } = Dimensions.get("window");
const IMAGE_HEIGHT = Dimensions.get("window").height / 1.35;
const HEADER_HEIGHT = Platform.OS === "ios" ? 80 : 50;
const SCROLL_HEIGHT = Dimensions.get("window").height / 1.35;
const THEME_COLOR = color.toolbar;
const FADED_THEME_COLOR = color.toolbar;

class ManageAccount extends Component {
  nScroll = new Animated.Value(0);
  scroll = new Animated.Value(0);

  textColor = this.scroll.interpolate({
    inputRange: [0, SCROLL_HEIGHT / 5, SCROLL_HEIGHT],
    outputRange: [THEME_COLOR, FADED_THEME_COLOR, "white"],
    extrapolate: "clamp"
  });
  tabBg = this.scroll.interpolate({
    inputRange: [0, SCROLL_HEIGHT],
    outputRange: ["white", THEME_COLOR],
    extrapolate: "clamp"
  });
  tabY = this.nScroll.interpolate({
    inputRange: [0, SCROLL_HEIGHT, SCROLL_HEIGHT + 1],
    outputRange: [0, 0, 1]
  });
  headerBg = this.scroll.interpolate({
    inputRange: [0, SCROLL_HEIGHT, SCROLL_HEIGHT + 1],
    outputRange: ["transparent", "transparent", THEME_COLOR],
    extrapolate: "clamp"
  });
  imgScale = this.nScroll.interpolate({
    inputRange: [-25, 0],
    outputRange: [1.1, 1],
    extrapolateRight: "clamp"
  });
  imgOpacity = this.nScroll.interpolate({
    inputRange: [0, SCROLL_HEIGHT],
    outputRange: [1, 0]
  });
  tabContent = (x, i) => (
    <View style={{ height: this.state.height }}>
      <List
        onLayout={({
          nativeEvent: {
            layout: { height }
          }
        }) => {
          this.heights[i] = height;
          if (this.state.activeTab === i) this.setState({ height });
        }}
      >
        {new Array(x).fill(null).map((_, i) => (
          <Item key={i}>
            <Text>Item {i}</Text>
          </Item>
        ))}
      </List>
    </View>
  );
  heights = [500, 500];
  state = {
    activeTab: 0,
    height: 500
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading:true,
      opacity:0,
      error: null,
      emailtext: null,
      pwd: null,
      hideNavBar: true,
      hideDrawerButton: true,
      mfname: "",
      enabled: true,
      client_info: {},
      client_bank: {}
    };
    this.nScroll.addListener(
      Animated.event([{ value: this.scroll }], { useNativeDriver: false })
    );
  }

  toggleNavBar = () => {
    //this.setState(prevState => ({ hideNavBar: prevState.hideNavBar }), () => Actions.refresh({ hideNavBar: false }));
    Actions.refresh({ hideNavBar: false });
  };

  componentDidMount() {
    this.retrieveData();
    this.toggleNavBar();
    this.props.navigation.addListener("didFocus", payload => {
      this.toggleNavBar();
      Actions.refresh({ hideDrawerButton: false });
    });
    // BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      if (clienttype === "client") {
        this.setState({ showFund: true });
      } else {
        this.setState({ showFund: false });
      }
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    //alert(`api=1.1&client_id=${clientid}&invest_type=portfolio_summary&from_date=${dateFinal}`);
    this.setState({ summary: [], master_array: [] });
    fetch(Apis.Minvest, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&invest_type=manage_account` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          client_info: responseJson["data"]["manage_account"]["client_info"][0],
          client_bank: responseJson["data"]["manage_account"]["client_bank"][0],
          isLoading:false,opacity:1
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    return (
      <View>
        <StatusBar backgroundColor={color.toolbar} barStyle="light-content" />
        {/* <Animated.View style={{position: "absolute", width: "100%", backgroundColor: this.headerBg, zIndex: 1}}>
          <Header style={{backgroundColor: "transparent"}} hasTabs>
            <Body>
            <Title>
              <Animated.Text style={{color: this.textColor, fontWeight: "bold"}}>
               {this.state.mfname}
              </Animated.Text>
            </Title>
            </Body>
          </Header>
        </Animated.View> */}
        <Animated.ScrollView
          ref={c => {
            this.scrollnest = c;
          }}
          scrollEventThrottle={5}
          showsVerticalScrollIndicator={true}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.nScroll } } }],
            { useNativeDriver: true }
          )}
          style={{ zIndex: 0 , opacity:this.state.opacity}}
        >
          <Animated.View
            style={{
              transform: [
                { translateY: Animated.multiply(this.nScroll, 0.65) },
                { scale: this.imgScale }
              ],
              backgroundColor: THEME_COLOR
            }}
          >
            <Animated.View
              style={{
                height: IMAGE_HEIGHT,
                width: "100%",
                opacity: this.imgOpacity,
                backgroundColor: "white"
              }}
            >
              <ScrollView>
                <View>
                  <Card
                    title="Investment Account Info"
                    titleStyle={{
                      color: THEME_COLOR,
                      marginBottom: Platform.OS == "android" ? 5 : 15
                    }}
                    dividerStyle={{
                      marginBottom: Platform.OS == "android" ? 5 : 15
                    }}
                  >
                    <Text style={styles.title_carddetail}>
                      Client Code :{" "}
                      <Text style={styles.carddetail}>
                        {" "}
                        {this.state.client_info.client_id}{" "}
                      </Text>{" "}
                    </Text>
                    <Text
                      style={[
                        styles.title_carddetail,
                        { marginTop: Platform.OS == "android" ? 3 : 7 }
                      ]}
                    >
                      PAN :{" "}
                      <Text style={styles.carddetail}>
                        {" "}
                        {this.state.client_info.client_pan}{" "}
                      </Text>{" "}
                    </Text>
                  </Card>

                  <Card
                    title="Personal Info"
                    titleStyle={{
                      color: THEME_COLOR,
                      marginBottom: Platform.OS == "android" ? 5 : 15
                    }}
                    containerStyle={{
                      marginTop:  10
                    }}
                    dividerStyle={{
                      marginBottom: Platform.OS == "android" ? 5 : 15
                    }}
                  >
                    <Text style={styles.title_carddetail}>
                      Name :{" "}
                      <Text style={styles.carddetail}>
                        {" "}
                        {this.state.client_info.client_name}{" "}
                      </Text>{" "}
                    </Text>
                    <Text
                      style={[
                        styles.title_carddetail,
                        { marginTop: Platform.OS == "android" ? 3 : 7 }
                      ]}
                    >
                      Email :{" "}
                      <Text style={styles.carddetail}>
                        {" "}
                        {this.state.client_info.client_email}{" "}
                      </Text>{" "}
                    </Text>
                    <Text
                      style={[
                        styles.title_carddetail,
                        { marginTop: Platform.OS == "android" ? 3 : 7 }
                      ]}
                    >
                      Phone :{" "}
                      <Text style={styles.carddetail}>
                        {" "}
                        {this.state.client_info.client_mobile}{" "}
                      </Text>{" "}
                    </Text>
                  </Card>

                  <Card
                    title="Bank Details"
                    titleStyle={{
                      color: THEME_COLOR,
                      marginBottom: Platform.OS == "android" ? 5 : 15
                    }}
                    containerStyle={{
                      marginTop:  10
                    }}
                    dividerStyle={{
                      marginBottom: Platform.OS == "android" ? 5 : 15
                    }}
                  >
                    <Text style={styles.title_carddetail}>
                      Name :{" "}
                      <Text style={styles.carddetail}>
                        {" "}
                        {this.state.client_bank.ClntBankNm}{" "}
                      </Text>{" "}
                    </Text>
                    <Text
                      style={[
                        styles.title_carddetail,
                        { marginTop: Platform.OS == "android" ? 3 : 7 }
                      ]}
                    >
                      Branch :{" "}
                      <Text style={styles.carddetail}>
                        {" "}
                        {this.state.client_bank.BankBrnNm}{" "}
                      </Text>{" "}
                    </Text>
                    <Text
                      style={[
                        styles.title_carddetail,
                        { marginTop: Platform.OS == "android" ? 3 : 7 }
                      ]}
                    >
                      Account No. :{" "}
                      <Text style={styles.carddetail}>
                        {" "}
                        {this.state.client_info.client_acc_no}{" "}
                      </Text>{" "}
                    </Text>
                    <Text
                      style={[
                        styles.title_carddetail,
                        { marginTop: Platform.OS == "android" ? 3 : 7 }
                      ]}
                    >
                      IFSC :{" "}
                      <Text style={styles.carddetail}>
                        {" "}
                        {this.state.client_info.client_ifsc}{" "}
                      </Text>{" "}
                    </Text>
                    <Text
                      style={[
                        styles.title_carddetail,
                        { marginTop: Platform.OS == "android" ? 3 : 7 }
                      ]}
                    >
                      MICR :{" "}
                      <Text style={styles.carddetail}>
                        {" "}
                        {this.state.client_info.client_acc_micro}{" "}
                      </Text>{" "}
                    </Text>
                    <Text
                      style={[
                        styles.title_carddetail,
                        { marginTop: Platform.OS == "android" ? 3 : 7 }
                      ]}
                    >
                      Account Type :{" "}
                      <Text style={styles.carddetail}>
                        {" "}
                        {this.state.client_bank.BankAcctType}{" "}
                      </Text>{" "}
                    </Text>
                  </Card>
                </View>
              </ScrollView>
            </Animated.View>
          </Animated.View>
          <Tabs
            locked={false}
            prerenderingSiblingsNumber={3}
            onChangeTab={({ i }) => {
              this.setState({ height: this.heights[i], activeTab: i });
            }}
            renderTabBar={props => (
              <Animated.View
                style={{
                  transform: [{ translateY: this.tabY }],
                  zIndex: 1,
                  width: "100%",
                  backgroundColor: "white"
                }}
              >
                <ScrollableTab
                  {...props}
                  renderTab={(name, page, active, onPress, onLayout) => (
                    <TouchableOpacity
                      key={page}
                      onPress={() => {
                        onPress(page);
                      }}
                      onLayout={onLayout}
                      activeOpacity={0.4}
                    >
                      <Animated.View
                        style={{
                          flex: 1,
                          height: 100,
                          backgroundColor: this.tabBg
                        }}
                      >
                        <TabHeading
                          scrollable
                          style={{
                            backgroundColor: "transparent",
                            width: SCREEN_WIDTH / 2
                          }}
                          active={active}
                        >
                          <Animated.Text
                            style={{
                              fontWeight: active ? "bold" : "normal",
                              color: this.textColor,
                              fontSize: 14,marginTop:12
                            }}
                          >
                            {name}
                          </Animated.Text>
                        </TabHeading>
                      </Animated.View>
                    </TouchableOpacity>
                  )}
                  underlineStyle={{ backgroundColor: this.textColor }}
                />
              </Animated.View>
            )}
          >
            <Tab heading="Portfolio Snapshot">
              <PortfolioSnapshot />
            </Tab>
            <Tab heading="Portfolio Summary">
              <PortfolioSummary />
            </Tab>
            <Tab heading="Portfolio Analytics">
              <PortfolioAnalytics />
            </Tab>
            <Tab heading="Gain Loss Report">
              <GainLossReport />
            </Tab>
            <Tab heading="SIP History">
              <SipHistory />
            </Tab>
            <Tab heading="Account Statement">
              <AccountStatement />
            </Tab>
            <Tab heading="Order History">
              <OrderHistory />
            </Tab>
            <Tab heading="Mandate Registration">
              <MandateRegistration />
            </Tab>
            <Tab heading="SIP Calendar">
              <SipCalendar />
            </Tab>
          </Tabs>
        </Animated.ScrollView>
        {this.state.isLoading && <View style={{alignItems: 'center', justifyContent: 'center',position:'absolute',top:0,left:0,bottom:0,right:0,flex:1}}>
            <Image source={require('../assets/images/loader.gif')} style={{height:30,width:30,resizeMode:"contain"}}/>
            </View>}
      </View>
    );
  }
}

export default ManageAccount;
