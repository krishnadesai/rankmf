import React, { Component } from "react";
import {
  AsyncStorage,
  Platform,
  TouchableOpacity,
  Text,
  View,
  Image,
  ScrollView
} from "react-native";
import { Card } from "react-native-elements";
import { Actions } from "react-native-router-flux";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";
const bank_allowed = 0,
  atom_allowed = 0,
  ledger_balance = 0;

function cleantext(strInputCode) {
  cleanText = strInputCode.replace("/n", "\n");
  cleanText = cleanText.replace("/n", "\n");
  cleanText = cleanText.replace("/n", "\n");
  cleanText = cleanText.replace("/n", "\n");
  cleanText = cleanText.replace("/n", "\n");
  return cleanText;
}

class LumpsumBasket3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      data: {},
      order_status: "",
      isLoading: true,
      opacity: 0,
      currentPage: 0,
      height3: 0
    };
    
  }

  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      bank_allowed = await AsyncStorage.getItem("bank_allowed");
      atom_allowed = await AsyncStorage.getItem("atom_allowed");
      ledger_balance = await AsyncStorage.getItem("ledger_balance");

      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    //  alert(`api=1.6&access_token=${access_token}&user=${clientid}&client_id=${clientid}&amount=${
    //     this.props.amount
    //   }&ledger_balance=${parseInt(ledger_balance)}&atom_allowed=${parseInt(
    //     atom_allowed
    //   )}&bank_allowed=${parseInt(
    //     bank_allowed
    //   )}&order_type=lumpsum&scheme_code=${this.props.basket_id}&basket_order_id=${
    //     this.props.unique_code
    //   }&quantity=${this.props.quantity}`)
    fetch(Apis.Mf_order, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.6&access_token=${access_token}&user=${clientid}&client_id=${clientid}&amount=${
        this.props.amount
      }&ledger_balance=${parseInt(ledger_balance)}&atom_allowed=${parseInt(
        atom_allowed
      )}&bank_allowed=${parseInt(
        bank_allowed
      )}&order_type=lumpsum&scheme_code=${this.props.basket_id}&basket_order_id=${
        this.props.unique_code
      }&quantity=${this.props.quantity}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson["status"] === "success") {
          this.setState({
            data: responseJson["data"]["response"],
            order_status: responseJson["data"]["order_status"]
          });
          this.setState({ isLoading: false, opacity: 1 });
          //  alert(bank_allowed)
        }
      })
      .catch(error => {
        alert(error);
      });
  }

  render() {
    return (
      <ScrollView
        style={[styles.containerFlex, { backgroundColor: color.LIGHT_GREY }]}
      >
        <View>
          <Card containerStyle={{opacity:this.state.opacity}}>
            <View
              style={{
                backgroundColor: color.lumpsumbg,
                borderColor: color.lumpsumborder,
                borderWidth: 1,
                borderRadius: 5
              }}
            >
              <View style={{ padding: 20 }}>
                <Text style={{ color: color.lumpsumtext, fontWeight: "700" }}>
                  Your Lump-Sum has been successfully Placed.
                </Text>

                <Text
                  style={{
                    color: color.lumpsumtext,
                    marginTop: 15,
                    fontWeight: "400"
                  }}
                >
                  Scheme Name -{" "}
                  <Text style={{ fontWeight: "700" }}>
                    {this.props.basket_name}
                  </Text>
                </Text>
                <Text
                  style={{
                    color: color.lumpsumtext,
                    marginTop: 15,
                    fontWeight: "400"
                  }}
                >
                  Amount -{" "}
                  <Text style={{ fontWeight: "700" }}>
                    {"\u20B9"}
                    {this.props.amount}
                  </Text>
                </Text>
              </View>
            </View>
            <Text
              style={{
                marginTop: 20,
                alignSelf: "center",
                textAlign: "center",
                fontWeight: "700",
                fontSize: 15
              }}
            >
              Track Your Order
            </Text>

            <View style={{ marginTop: 15 }}>
              <View style={{ flexDirection: "row", marginRight: 10 }}>
                <View>
                  <Image
                    source={require("../../assets/images/check-mark.png")}
                    style={{ height: 30, width: 30, resizeMode: "contain" }}
                  />
                  {this.state.data.step_2_title && (
                    <View
                      style={[
                        styles.dashcolumn,
                        {
                          margin: 5,
                          height: 50,
                          alignItems: "center",
                          alignSelf: "center"
                        }
                      ]}
                    />
                  )}
                </View>
                <View
                  style={{
                    marginLeft: 10,
                    paddingRight: 10,
                    alignItems: "center"
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <Text
                      style={{
                        color: color.toolbar,
                        fontSize: 16,
                        fontWeight: "700",
                        flex: 0.7
                      }}
                    >
                      {this.state.data.step_1_title}
                    </Text>
                    <Text
                      style={{
                        color: color.GREY,
                        fontSize: 13,
                        alignSelf: "flex-end",
                        alignItems: "flex-end",
                        flex: 0.3,
                        textAlign: "right"
                      }}
                    >
                      {this.state.data.step_1_day}
                    </Text>
                  </View>
                  <Text
                    style={{
                      color: color.BLACK,
                      fontSize: 13,
                      flexGrow: 1,
                      marginTop: 5
                    }}
                  >
                    {this.state.data.step_1_msg}
                  </Text>
                </View>
              </View>

              {this.state.data.step_2_title && (
                <View style={{ flexDirection: "row", marginRight: 10 }}>
                  <View>
                    <Image
                      source={require("../../assets/images/next.png")}
                      style={{ height: 30, width: 30, resizeMode: "contain" }}
                    />
                    {this.state.data.step_3_title && (
                      <View
                        style={[
                          styles.dashcolumn,
                          {
                            margin: 5,
                            height: 50,
                            alignItems: "center",
                            alignSelf: "center"
                          }
                        ]}
                      />
                    )}
                  </View>
                  <View
                    style={{
                      marginLeft: 10,
                      paddingRight: 10,
                      alignItems: "center"
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          color: color.toolbar,
                          fontSize: 16,
                          fontWeight: "700",
                          flex: 0.7
                        }}
                      >
                        {this.state.data.step_2_title}
                      </Text>
                      <Text
                        style={{
                          color: color.GREY,
                          fontSize: 13,
                          alignSelf: "flex-end",
                          alignItems: "flex-end",
                          flex: 0.3,
                          textAlign: "right"
                        }}
                      >
                        {this.state.data.step_2_day}
                      </Text>
                    </View>
                    <Text
                      style={{
                        color: color.BLACK,
                        fontSize: 13,
                        flexGrow: 1,
                        marginTop: 5
                      }}
                    >
                      {this.state.data.step_2_msg}
                    </Text>
                   {this.state.data.step_2_link  && <TouchableOpacity
                    onPress={() => Actions.LumpsumBasketWeb({url:this.state.data.step_2_link})}
                    style={[styles.button, { backgroundColor: color.toolbar,margin:5,alignSelf:"flex-start" }]}
                  >
                    <Text
                      style={[
                        styles.instructions,
                        { fontSize:13,padding:5 }
                      ]}
                    >
                      TRANSFER NOW
                    </Text>
                  </TouchableOpacity>}
                  </View>
                </View>
              )}

              {this.state.data.step_3_title && (
                <View style={{ flexDirection: "row", marginRight: 10 }}>
                  <View>
                    <Image
                      source={require("../../assets/images/pending.png")}
                      style={{ height: 30, width: 30, resizeMode: "contain" }}
                    />
                    {this.state.data.step_4_title && (
                      <View
                        style={[
                          styles.dashcolumn,
                          {
                            margin: 5,
                            height: this.state.height3,
                            alignItems: "center",
                            alignSelf: "center"
                          }
                        ]}
                      />
                    )}
                  </View>
                  <View
                    style={{
                      marginLeft: 10,
                      paddingRight: 10,
                      alignItems: "center"
                    }}
                    onLayout={event => {
                      this.setState({
                        height3: event.nativeEvent.layout.height
                      });
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          color: color.toolbar,
                          fontSize: 16,
                          fontWeight: "700",
                          flex: 0.7
                        }}
                      >
                        {this.state.data.step_3_title}
                      </Text>
                      <Text
                        style={{
                          color: color.GREY,
                          fontSize: 13,
                          alignSelf: "flex-end",
                          alignItems: "flex-end",
                          flex: 0.3,
                          textAlign: "right"
                        }}
                      >
                        {this.state.data.step_3_day}
                      </Text>
                    </View>
                    <Text
                      style={{
                        color: color.BLACK,
                        fontSize: 13,
                        flexGrow: 1,
                        marginTop: 5
                      }}
                    >
                      {cleantext(this.state.data.step_3_msg)}
                    </Text>
                  </View>
                </View>
              )}

              {this.state.data.step_4_title && (
                <View style={{ flexDirection: "row", marginRight: 10 }}>
                  <View>
                    <Image
                      source={require("../../assets/images/pending.png")}
                      style={{ height: 30, width: 30, resizeMode: "contain" }}
                    />
                    {this.state.data.step_5_title && (
                      <View
                        style={[
                          styles.dashcolumn,
                          {
                            margin: 5,
                            height: 50,
                            alignItems: "center",
                            alignSelf: "center"
                          }
                        ]}
                      />
                    )}
                  </View>
                  <View
                    style={{
                      marginLeft: 10,
                      paddingRight: 10,
                      alignItems: "center"
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          color: color.toolbar,
                          fontSize: 16,
                          fontWeight: "700",
                          flex: 0.7
                        }}
                      >
                        {this.state.data.step_4_title}
                      </Text>
                      <Text
                        style={{
                          color: color.GREY,
                          fontSize: 13,
                          alignSelf: "flex-end",
                          alignItems: "flex-end",
                          flex: 0.3,
                          textAlign: "right"
                        }}
                      >
                        {this.state.data.step_4_day}
                      </Text>
                    </View>
                    <Text
                      style={{
                        color: color.BLACK,
                        fontSize: 13,
                        flexGrow: 1,
                        marginTop: 5
                      }}
                    >
                      {this.state.data.step_4_msg}
                    </Text>
                  </View>
                </View>
              )}

              {this.state.data.step_5_title && (
                <View style={{ flexDirection: "row", marginRight: 10 }}>
                  <View>
                    <Image
                      source={require("../../assets/images/pending.png")}
                      style={{ height: 30, width: 30, resizeMode: "contain" }}
                    />
                    <View
                      style={[
                        styles.dashcolumn,
                        {
                          margin: 5,
                          height: 50,
                          alignItems: "center",
                          alignSelf: "center"
                        }
                      ]}
                    />
                  </View>
                  <View
                    style={{
                      marginLeft: 10,
                      paddingRight: 10,
                      alignItems: "center"
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          color: color.toolbar,
                          fontSize: 16,
                          fontWeight: "700",
                          flex: 0.7
                        }}
                      >
                        {this.state.data.step_5_title}
                      </Text>
                      <Text
                        style={{
                          color: color.GREY,
                          fontSize: 13,
                          alignSelf: "flex-end",
                          alignItems: "flex-end",
                          flex: 0.3,
                          textAlign: "right"
                        }}
                      >
                        {this.state.data.step_5_day}
                      </Text>
                    </View>
                    <Text
                      style={{
                        color: color.BLACK,
                        fontSize: 13,
                        flexGrow: 1,
                        marginTop: 5
                      }}
                    >
                      {this.state.data.step_5_msg}
                    </Text>
                  </View>
                </View>
              )}
            </View>
          </Card>
          {this.state.isLoading && (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                position: "absolute",
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                flex: 1
              }}
            >
              <Image
                source={require("../../assets/images/loader.gif")}
                style={{ height: 30, width: 30, resizeMode: "contain" }}
              />
            </View>
          )}
        </View>
      </ScrollView>
    );
  }
}

export default LumpsumBasket3;
