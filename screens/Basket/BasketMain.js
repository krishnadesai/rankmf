import React, { Component } from "react";
import {
  Animated,
  AsyncStorage,
  View,
  Text,
  Image,
  Dimensions,
  FlatList,
  TouchableOpacity
} from "react-native";
import { Actions } from "react-native-router-flux";
import StarRating from "react-native-star-rating";
import { Card } from "react-native-elements"; // 0.18.5
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";
const clientid="",access_token=""
const { width } = Dimensions.get("window");

export default class BasketMain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      isLoading: true,
      opacity:0.5,
      baskets: [],
      categories: []
    };

    //this.fetchData = this.fetchData.bind(this);
  }

  componentDidMount() {
      
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      name = await AsyncStorage.getItem("name");
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      this.fetchData()
      
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchDataBank() {
    // alert(this.props.unique_code);
    fetch(Apis.Mf_order, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson["status"] === "success") {
          this.setState({ data: responseJson["data"] });
          
          this._storeData();
        }
         //  alert(this.state.data.atom_allowed)
      })
      .catch(error => {
        console.error(error);
      });
  }

  _storeData = async () => {
    try {
      await AsyncStorage.setItem(
        "bank_allowed",
        this.state.data.bank_allowed + ""
      );
      await AsyncStorage.setItem(
        "atom_allowed",
        this.state.data.atom_allowed + ""
      );
      await AsyncStorage.setItem(
        "ledger_balance",
        this.state.data.ledger_balance + ""
      );
    } catch (error) {
      // Error saving data
      // alert(1)
    }
  };

  fetchData() {
      
    fetch(Apis.MfBasket, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&access_token=${access_token}&user=${clientid}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
       
        if (responseJson["status"] === "Success") {
          responseJson["data"]["category_list"].map(item => {
            var data = [];

            responseJson["data"]["basket_master"].map((item1, index) => {
              if (item.id === item1.cat_id) {
                data.push(item1);
              }
            });
            this.state.baskets.push(data);
          });

          //alert(this.state.baskets.length)
          this.setState({ categories: responseJson["data"]["category_list"] });
          this.setState({
            isLoading:false,
            opacity:1
        });
          //  alert(this.state.baskets[0][0].id+"---"+this.state.baskets[1][0].id)
        }
        this.fetchDataBank()
      })
      .catch(error => {
        alert(error);
      });
  }

  renderCarousel(item1, index) {
    return (
      <View>
        <Text
          style={{
            marginTop: 15,
            color: color.toolbar,
            marginLeft: 10,
            fontSize: 16
          }}
        >
          {item1.name}
        </Text>

        <FlatList
          style={{ marginTop: 5 }}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={this.state.baskets[index]}
          renderItem={({ item, index }) => this.renderRow(item, index)}
        />
      </View>
    );
  }

  renderRow(item, index) {
    return (
      <Animated.View>
        <Card
          containerStyle={{
            borderRadius: 5,
            padding: 2,
            margin: 5,
            marginLeft: 10,
            width: width - 60,
            overflow: "hidden"
          }}
        >
          <TouchableOpacity
            onPress={() => Actions.BasketDetail({ id: item.id })}
          >
            <View style={{ marginRight: 2 }}>
              <View style={{ height: 80, width: width - 60 }}>
                <Image
                  source={{ uri: Apis.BaseImageURL + `${item.image_thumb}` }}
                  style={{
                    height: 80,
                    width: width - 60,
                    resizeMode: "stretch"
                  }}
                />
              </View>
              <View
                style={{
                  backgroundColor: color.BLACK,
                  opacity: 0.7,
                  position: "absolute",
                  bottom: 0,
                  width: width - 60
                }}
              >
                <Text
                  style={{
                    color: color.WHITE,
                    fontSize: 13,
                    padding: 3,
                    fontWeight: "500"
                  }}
                >
                  {item.basket_name}
                </Text>
              </View>
            </View>
            <View style={{ flexDirection: "row", marginTop: 10 }}>
              <View>
                <Text style={styles.title}>Popularity</Text>
                <StarRating
                  containerStyle={{ marginLeft: 5, marginTop: 10 }}
                  fullStar={require("../../assets/images/popularity.png")}
                  emptyStar={require("../../assets/images/popularity-grey.png")}
                  disabled={true}
                  maxStars={5}
                  starSize={18}
                  rating={item.popular}
                />
              </View>
              <View style={{ flex: 1, alignItems: "flex-end", marginRight: 5 }}>
                <Text style={styles.title}>Minimum Amount</Text>
                <Text
                  style={{
                    color: color.toolbar,
                    fontSize: 18,
                    marginTop: 5,
                    marginLeft: 5
                  }}
                >
                  {"\u20B9"}
                  {item.basket_min_amt_sip_total != "0"
                    ? item.basket_min_amt_sip_total
                    : item.basket_min_amt_lumpsum_total}
                </Text>
              </View>
            </View>
            <View style={[styles.dash, { margin: 5, marginTop: 10 }]} />
          </TouchableOpacity>
          <View
            style={{
              flexDirection: "row",
              marginBottom: 5,
              marginLeft: 15,
              marginRight: 15
            }}
          >
            <View style={{ flex: 0.5 }} />
            <View
              style={{
                flex: 0.5,
                flexDirection: "row",
                alignItems: "flex-end",
                justifyContent: "flex-end"
              }}
            >
              <TouchableOpacity onPress={()=>Actions.BasketDetail({ id: item.id })}>
                <Text
                  style={{
                    fontFamily: "Roboto",
                    color: color.toolbar,
                    marginLeft: 15,
                    fontWeight: "800"
                  }}
                >
                  Invest Now
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Card>
      </Animated.View>
    );
  }

  render() {
    return (
      <View style={styles.containerFlex}>
        <FlatList
          style={{ marginTop: 10, marginBottom: 10,opacity:this.state.opacity }}
          data={this.state.categories}
          renderItem={({ item, index }) => this.renderCarousel(item, index)}
        />
         {this.state.isLoading && <View style={{alignItems: 'center', justifyContent: 'center',position:'absolute',top:0,left:0,bottom:0,right:0,flex:1}}>
            <Image source={require('../../assets/images/loader.gif')} style={{height:30,width:30,resizeMode:"contain"}}/>
            </View>}
      </View>
    );
  }
}
