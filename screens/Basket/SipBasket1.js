import React, { Component } from "react";
import {
  AsyncStorage,
  FlatList,
  Alert,
  Text,
  View,
  Image,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  TextInput,
  KeyboardAvoidingView
} from "react-native";
import PopupDialog, {
    DialogTitle,
    SlideAnimation
  } from "react-native-popup-dialog";
import moment from "moment";
import { Actions } from "react-native-router-flux";
import { Calendar } from "react-native-calendars";
import { Card } from "react-native-elements";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";
const slideAnimation = new SlideAnimation({ slideFrom: "bottom" });
var datetoday = new Date();
var currentday = datetoday ? ("0" + datetoday.getDate()).slice(-2) : "";
var currentmonth = datetoday
  ? ("0" + (datetoday.getMonth() + 1)).slice(-2)
  : "";
var year = datetoday ? datetoday.getFullYear() : "";
const currentDate = year + "-" + currentmonth + "-" + currentday;
const maxDate = year + 1 + "-" + currentmonth + "-" + currentday;
const minDate = year + "-" + currentmonth + "-" + "01";
function cleantext(strInputCode) {
    cleanText = strInputCode.replace("<ul><li>", "\u2022 ");
    cleanText = cleanText.replace("<ul></ul>", "");
    cleanText = cleanText.replace("</li><li>", "\n\n\u2022 ");
    cleanText = cleanText.replace("</li><li>", "\n\n\u2022 ");
    cleanText = cleanText.replace("</li><li>", "\n\n\u2022 ");
    cleanText = cleanText.replace("</li><li>", "\n\n\u2022 ");
    cleanText = cleanText.replace("</li><li>", "\n\n\u2022 ");
    cleanText = cleanText.replace("</li><li>", "\n\n\u2022 ");
    cleanText = cleanText.replace("</li><li>", "\n\n\u2022 ");
    cleanText = cleanText.replace("</li><li>", "\n\n\u2022 ");
    cleanText = cleanText.replace("</li><li>", "\n\n\u2022 ");
    cleanText = cleanText.replace("</li></ul>", "");
    return cleanText;
  }

class LumpsumBasket1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qty: "1",
      fundimage: true,
      investimage: false,
      error: null,
      amterror: false,
      multipliererror: false,
      data: {},
      mandate: {},
      event: [],
      date: "",
      disabled_dates: [],
      allowed_dates: [],
      markedDates: [],
      disableddates: [],
      dateerror: false,
      basket_min_amt_sip_total: this.props.basket_min_amt_sip_total
    };
    this.onDayPress = this.onDayPress.bind(this);
  }

  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    this.setState({
      isLoading: true,
      opacity: 0,
      disabled_dates: [],
      allowed_dates: [],
      event: [],
      mandate: {},
      markedDates: []
    });
    //   alert(`api=1.4&access_token=${access_token}&user=${clientid}&client_id=${clientid}&amount=${
    //     this.props.basket_min_amt_sip_total
    //   }&order_type=basket&scheme_code=${this.props.basket_id}`);
    fetch(Apis.Mf_order, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.4&access_token=${access_token}&user=${clientid}&client_id=${clientid}&amount=${
        this.props.basket_min_amt_sip_total
      }&order_type=basket&scheme_code=${this.props.basket_id}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        //  alert(responseJson["status"])
        if (responseJson["status"] === "success") {
          this.setState({
            isLoading: false,
            opacity: 1,
            disabled_dates: responseJson["data"]["disabled_dates"],
            allowed_dates: responseJson["data"]["allowed_dates"],
            event: responseJson["data"]["event"],
            mandate: responseJson["data"]["mandate"],
            markedDates: this.getDaysInMonth(
              moment().month() + 1,
              moment().year(),
              responseJson["data"]["disabled_dates"],
              responseJson["data"]["event"]
            )
          });
          mandate_deficiency = this.state.mandate.deficiency
            ? this.state.mandate.deficiency
            : "No";
          mandate_uploaded = this.state.mandate.uploaded
            ? this.state.mandate.uploaded
            : "No";
          mandate_status = this.state.mandate.mandate_status
            ? this.state.mandate.mandate_status
            : "No";
          mandate_link = this.state.mandate.mandate_link
            ? this.state.mandate.mandate_link
            : "No";
          //  alert(mandate_link)
          this._storeData();
        }
          
      })
      .catch(error => {
        alert(error);
      });
  }

  _storeData = async () => {
    try {
      await AsyncStorage.setItem("mandate_uploaded", mandate_uploaded);
      await AsyncStorage.setItem("mandate_status", mandate_status);
      await AsyncStorage.setItem("mandate_deficiency", mandate_deficiency);
      await AsyncStorage.setItem("mandate_id", this.state.mandate.mandate);
      await AsyncStorage.setItem(
        "mandate_link",
        this.state.mandate.mandate_link
      );
    } catch (error) {
      // Error saving data
      // alert(1)
    }
  };
 
  showSlideAnimationDialog = () => {
    this.slideAnimationDialog.show();
  };

  onDayPress(day) {
    // this.setState({
    //   selected: day.dateString
    // });
    
    var data = [],
      flag = 0;
    this.state.event.map(e => {
      if (e.start.split("-")[2] == day.dateString.split("-")[2]) {
        data.push(e);
        flag = 1;
      }
    });

    var message = "";

    data.map(
      e =>
        (message +=
          "\n" +
          e.description +
          "\n" +
          e.title +
          "\n" +
          e.order_type +
          "\n" +
          cleantext(e.type) +          
          "\n" +
          "-----------")
    );
    message = message.substring(0, message.lastIndexOf("\n"));

    if (flag === 1) {
      Alert.alert(day.dateString, message, [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => {
            if (
              moment().format("k") >= 20 &&
              moment().format("MM") ==
                moment(day.dateString, "YYYY-MM-DD").format("MM")
            ) {
              alert(
                "Order cannot be processed for this day. Kindly pick another date."
              );
            } else if (this.state.disableddates.indexOf(day.dateString) == -1) {
              this.setState({
                date: day.dateString,
                dateerror: false
              });
              this.slideAnimationDialog.dismiss();
            } else {
              Alert.alert(
                "",
                "Order cannot be processed for this day. Kindly pick another date."
              );
            }
          }
        }
      ]);
    } else {
      if (
        moment().format("k") >= 20 &&
        moment().format("MM") ==
          moment(day.dateString, "YYYY-MM-DD").format("MM")
      ) {
        Alert.alert(
          "",
          "Order Cannot be processed for this day. Kindly pick another Date."
        );
      } else if (this.state.disableddates.indexOf(day.dateString) == -1) {
        this.setState({
          date: day.dateString,
          dateerror: false
        });
        this.slideAnimationDialog.dismiss();
      }
    }
  }

  getDaysInMonth(month, year, days, event) {
    //const disableddates = this.state.disableddates;
    let dates = {};
    //alert(pivot.date(days[0]).format("YYYY-MM-DD"))
    const sip = {
      customStyles: {
        container: {
          backgroundColor: color.toolbar
        },
        text: {
          color: color.WHITE
        }
      }
    };
    const basket = {
      customStyles: {
        container: {
          backgroundColor: color.basketcolor
        },
        text: {
          color: color.WHITE
        }
      }
    };
    const disabled = { disabled: true };
    if (month + "" === currentmonth + "") {
      for (var i = 1; i <= 31; i++) {
        if (i < currentDate.split("-")[2]) {
          d1 = i ? ("0" + i).slice(-2) : "";
          d = year + "-" + month + "-" + d1;
          dates[d] = disabled;
          this.state.disableddates.push(d);
        }
      }
    }

    days.forEach(day => {
      d1 = day ? ("0" + day).slice(-2) : "";
      d = year + "-" + month + "-" + d1;
      dates[d] = disabled;
      this.state.disableddates.push(d);
    });

    dates[currentDate] = disabled;
    this.state.disableddates.push(currentDate);

    event.forEach(item => {
      dt = item.start.split("-")[2];
      d = year + "-" + month + "-" + dt;

      if (
        new Date(d) >= new Date(currentDate) &&
        new Date(d) <= new Date(maxDate)
      ) {
        if (item.order_type === "SIP") {
          dates[d] = sip;
        } else if (item.order_type === "basket") {
          dates[d] = basket;
        }
      }

      if (
        new Date(d).getMonth() == new Date(currentDate).getMonth() ||
        new Date(d).getMonth() == new Date(maxDate).getMonth()
      ) {
        if (item.order_type === "SIP") {
          dates[d] = sip;
        } else if (item.order_type === "basket") {
          dates[d] = basket;
        }
      }
    });
//alert(this.state.disableddates)
    return dates;
  }

  confirm() {
    Actions.SipBasket2({
        image_thumb: this.props.image_thumb,
        basket_name: this.props.basket_name,
        sub_basket: this.props.sub_basket,
        basket_min_amt_sip_total: this.props.basket_min_amt_sip_total,
        unique_code: this.props.unique_code,
        basket_id:this.props.basket_id,
        amount:this.state.basket_min_amt_sip_total,
        date:this.state.date,
        quantity:this.state.qty,
        mandate_id:this.state.mandate.mandate
    });
  }



  renderRowLine(item, index) {
    return (
      <Card containerStyle={{ margin: 0, marginBottom: 8, padding: 0 }}>
        <View style={{ flexDirection: "row" }}>
          <Image
            source={{
              uri: Apis.BaseImageURL + `${item.Amc_code}.jpg`
            }}
            style={{
              height: 40,
              width: 80,
              resizeMode: "stretch",
              flex: 0.2,
              margin: 7
            }}
          />
          <Text
            style={{ color: color.BLACK, fontSize: 12, flex: 0.8, margin: 7 }}
          >
            {item.Scheme_Name}
          </Text>
        </View>
      </Card>
    );
  }

  render() {
    return (
      <ScrollView
        style={[styles.containerFlex, { backgroundColor: color.LIGHT_GREY }]}
      >
        <KeyboardAvoidingView behavior="padding">
          <View>
            <StatusBar
              backgroundColor={color.toolbar}
              barStyle="light-content"
            />

            <Card containerStyle={{ opacity: this.state.opacity }}>
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <View
                  style={{
                    borderRadius: 5,
                    borderColor: color.GREY,
                    borderWidth: 1,
                    justifyContent: "center"
                  }}
                >
                  <Image
                    source={{
                      uri: Apis.BaseImageURL + `${this.props.image_thumb}`
                    }}
                    style={{
                      padding: 15,
                      height: 50,
                      width: 100,
                      resizeMode: "stretch"
                    }}
                  />
                </View>
                <Text
                  style={[
                    styles.welcome,
                    { textAlign: "center", marginTop: 15, color: color.toolbar }
                  ]}
                >
                  {this.props.basket_name}
                </Text>
              </View>

              <View style={[styles.dash, { margin: 7, marginTop: 15 }]} />

              <FlatList
                scrollEnabled={false}
                showsVerticalScrollIndicator={false}
                style={{ marginTop: 10, marginBottom: 10 }}
                data={this.props.sub_basket}
                renderItem={({ item, index }) =>
                  this.renderRowLine(item, index)
                }
              />

              <View
                style={{
                  marginTop: 15,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    borderBottomColor: color.BGREY,
                    borderBottomWidth: 1,
                    width: 100,
                    marginTop: 15
                  }}
                >
                  <TextInput
                    underlineColorAndroid="transparent"
                    keyboardType="numeric"
                    returnKeyType="done"
                    defaultValue={this.state.qty}
                    onChangeText={text => {
                      if (text === "") {
                        this.setState({
                          qty: "1",
                          basket_min_amt_sip_total: this.props
                            .basket_min_amt_sip_total
                        });
                      } else {
                        this.setState({
                          qty: text,
                          basket_min_amt_sip_total:
                            this.props.basket_min_amt_sip_total *
                            parseInt(text)
                        });
                      }
                    }}
                    style={{
                      flex: 0.9,
                      fontSize: 15,
                      textAlign: "center",
                      color: color.BLACK
                    }}
                  />
                  <View
                    style={{
                      flex: 0.1,
                      margin: 5,
                      alignItems: "flex-end"
                    }}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({
                          qty: parseInt(this.state.qty) + 1 + "",
                          basket_min_amt_sip_total:
                            this.props.basket_min_amt_sip_total *
                            (parseInt(this.state.qty) + 1)
                        })
                      }
                    >
                      <Image
                        source={require("../../assets/images/arrow-up.png")}
                        style={{
                          height: 15,
                          width: 15,
                          resizeMode: "contain"
                        }}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        if (parseInt(this.state.qty) > 1) {
                          this.setState({
                            qty: parseInt(this.state.qty) - 1 + "",
                            basket_min_amt_sip_total:
                              this.props.basket_min_amt_sip_total *
                              (parseInt(this.state.qty) - 1)
                          });
                        }
                      }}
                    >
                      <Image
                        source={require("../../assets/images/arrow-down.png")}
                        style={{
                          height: 15,
                          width: 15,
                          resizeMode: "contain"
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <Text
                  style={{
                    marginTop: 5,
                    color: color.GREY,
                    fontSize: 13,
                    textAlign: "center",
                    alignItems: "center"
                  }}
                >
                  Quantity
                </Text>
              </View>

              <View
                style={{
                  marginTop: 15,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    borderBottomColor: color.BGREY,
                    borderBottomWidth: 1,
                    textAlign: "center",
                    fontSize: 16,
                    color: color.BLACK,
                    width: 200,
                    marginTop: 15
                  }}
                >
                  {this.state.basket_min_amt_sip_total}
                </Text>
                <Text
                  style={{
                    marginTop: 5,
                    color: color.GREY,
                    fontSize: 13,
                    textAlign: "center",
                    alignItems: "center"
                  }}
                >
                  Amount
                </Text>
              </View>
              <View
              style={{
                marginTop: 15,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  borderBottomColor: color.BGREY,
                  borderBottomWidth: 1,
                  textAlign: "center",
                  color: color.BLACK,
                  width: 150,
                  marginTop: 15
                }}
                onPress={() => this.showSlideAnimationDialog()}
              >
                <Text
                  style={{
                    flex: 0.8,
                    textAlign: "center",
                    marginLeft: 10,
                    marginTop: 7
                  }}
                >
                  {this.state.date}
                </Text>
                <Image
                  source={require("../../assets/images/calendar.png")}
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: "contain",
                    margin: 5,
                    alignSelf: "flex-end",
                    flex: 0.2,
                    alignItems: "flex-end"
                  }}
                />
              </TouchableOpacity>

              <Text
                style={{
                  marginTop: 5,
                  color: color.BLACK,
                  fontSize: 13,
                  textAlign: "center",
                  alignItems: "center"
                }}
              >
                Start Date
              </Text>
            </View>
              <View style={[styles.buttonContainer, { marginTop: 15 }]}>
                <TouchableOpacity
                  onPress={() => this.confirm()}
                  style={[styles.button, { backgroundColor: color.toolbar }]}
                >
                  <Text style={styles.instructions}>CONFIRM AND CONTINUE</Text>
                </TouchableOpacity>
              </View>
            </Card>
            {/* {this.state.isLoading && (
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              flex: 1
            }}
          >
            <Image
              source={require("../../assets/images/loader.gif")}
              style={{ height: 30, width: 30, resizeMode: "contain" }}
            />
          </View>
        )} */}
          </View>
        </KeyboardAvoidingView>
        <PopupDialog
          dialogStyle={{
            height: 400,
            width: 400,
            top: 150,
            position: "absolute"
          }}
          ref={popupDialog => {
            this.slideAnimationDialog = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
          <Calendar
            markingType={"custom"}
            onDayPress={this.onDayPress}
            style={styles.calendar}
            minDate={minDate}
            markedDates={this.state.markedDates}
            maxDate={maxDate}
            onMonthChange={date => {
              this.setState({
                markedDates: this.getDaysInMonth(
                  date.month ? ("0" + date.month).slice(-2) : "",
                  date.year,
                  this.state.disabled_dates,
                  this.state.event
                )
              });
            }}
          />

          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              marginTop: 15
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <View
                style={{
                  height: 10,
                  width: 10,
                  backgroundColor: color.toolbar,
                  alignItems: "center",
                  marginTop: 3
                }}
              />
              <Text style={{ fontSize: 13, marginLeft: 5 }}>SIP</Text>
            </View>
            <View style={{ flexDirection: "row", marginLeft: 15 }}>
              <View
                style={{
                  height: 10,
                  width: 10,
                  backgroundColor: color.basketcolor,
                  alignItems: "center",
                  marginTop: 3
                }}
              />
              <Text style={{ fontSize: 13, marginLeft: 5 }}>BASKET</Text>
            </View>
          </View>
        </PopupDialog>
      </ScrollView>
    );
  }
}

export default LumpsumBasket1;
