import React, { Component } from "react";
import {
  AsyncStorage,
  FlatList,
  ScrollView,
  Text,
  View,
  Image,
  TouchableOpacity
} from "react-native";
import { Card } from "react-native-elements";
import { Actions } from "react-native-router-flux";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";

class LumpsumBasket2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      data: {},
      isLoading: true,
      opacity: 0
    };
  }

  componentDidMount() {
     
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      bank_allowed = await AsyncStorage.getItem("bank_allowed");
      atom_allowed = await AsyncStorage.getItem("atom_allowed");
      ledger_balance = await AsyncStorage.getItem("ledger_balance");
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    fetch(Apis.Mf_order, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.2&access_token=${access_token}&user=${clientid}&client_id=${clientid}&amount=${
        this.props.amount
      }&ledger_balance=${parseInt(ledger_balance)}&atom_allowed=${parseInt(
        atom_allowed
      )}&bank_allowed=${parseInt(bank_allowed)}&order_type=lumpsum&is_basket=1` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson["status"] === "success") {
          this.setState({ data: responseJson["data"] });
          this.setState({ isLoading: false, opacity: 1 });
          //  alert(bank_allowed)
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  renderRowLine(item, index) {
    return (
      <Text
        style={{
          textAlign: "left",
          fontSize: 13,
          margin: 7,
          color: color.BLACK
        }}
      >
        {" "}
        {"\u2022"} {"\u20B9"}
        {parseFloat(item.min_amt_lumpsum_new) *
          parseInt(this.props.quantity)}{" "}
        in {item.Scheme_Name}
      </Text>
    );
  }

  redirect() {
    Actions.LumpsumBasket3({
        basket_name: this.props.basket_name,
        sub_basket: this.props.sub_basket,
        basket_min_amt_lumpsum_total: this.props.basket_min_amt_lumpsum_total,
        unique_code: this.props.unique_code,
        amount: this.props.basket_min_amt_lumpsum_total,
        quantity: this.props.quantity,
        basket_id:this.props.basket_id
    });
  }

  render() {
    return (
      <ScrollView
        style={[styles.containerFlex, { backgroundColor: color.LIGHT_GREY }]}
      >
        <Card containerStyle={{ opacity: this.state.opacity }}>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <View
              style={{
                borderRadius: 5,
                borderColor: color.GREY,
                borderWidth: 1,
                justifyContent: "center"
              }}
            >
              <Image
                source={{
                  uri: Apis.BaseImageURL + `${this.props.image_thumb}`
                }}
                style={{
                  padding: 15,
                  height: 50,
                  width: 100,
                  resizeMode: "stretch"
                }}
              />
            </View>
            <Text
              style={[
                styles.welcome,
                { textAlign: "center", marginTop: 15, color: color.toolbar }
              ]}
            >
              {this.props.basket_name}
            </Text>
          </View>

          <View style={[styles.dash, { margin: 7, marginTop: 15 }]} />
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row"
            }}
          >
            <Text
              style={{
                marginTop: 5,
                color: color.GREY,
                fontSize: 13,
                textAlign: "center",
                alignItems: "center"
              }}
            >
              Amount(Rs)
            </Text>
            <Text
              style={{
                borderBottomColor: color.BLACK,
                borderBottomWidth: 1,
                textAlign: "center",
                fontSize: 17,
                color: color.BLACK,
                width: 200,
                marginTop: 15
              }}
            >
              {this.props.amount}
            </Text>
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row"
            }}
          >
            <Text
              style={{
                marginTop: 5,
                color: color.GREY,
                fontSize: 13,
                textAlign: "center",
                alignItems: "center"
              }}
            >
              Schemes in Basket
            </Text>
            <FlatList
              scrollEnabled={false}
              showsVerticalScrollIndicator={false}
              style={{ marginTop: 5 }}
              data={this.props.sub_basket}
              renderItem={({ item, index }) => this.renderRowLine(item, index)}
            />
          </View>

          <View style={[styles.buttonContainer, { marginTop: 15 }]}>
            <TouchableOpacity
              onPress={() => this.redirect()}
              style={[styles.button, { backgroundColor: color.toolbar }]}
            >
              <Text style={styles.instructions}>CONFIRM AND CONTINUE</Text>
            </TouchableOpacity>
          </View>

          <Text style={{ fontWeight: "500", fontSize: 13, marginTop: 15 }}>
            Note:
            <Text
              style={{ fontWeight: "normal", fontSize: 11, paddingLeft: 10 }}
            >
              {this.state.data.note_1}.
            </Text>
          </Text>
          <Text style={{ fontWeight: "normal", fontSize: 11, marginTop: 15 }}>
            {this.state.data.note_2}
          </Text>
        </Card>
        {this.state.isLoading && (
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              flex: 1
            }}
          >
            <Image
              source={require("../../assets/images/loader.gif")}
              style={{ height: 30, width: 30, resizeMode: "contain" }}
            />
          </View>
        )}
      </ScrollView>
    );
  }
}

export default LumpsumBasket2;
