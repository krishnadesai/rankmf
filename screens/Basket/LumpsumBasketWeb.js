import React, { Component } from "react";
import {
  Dimensions,
  WebView,
  Platform,
  Text,
  View,
  StatusBar,
  TouchableOpacity
} from "react-native";
import { colors } from "react-native-elements";
import { Actions } from "react-native-router-flux";
import styles from "../../components/styles";
import color from "../../constants/Colors";

class LumpsumBasketWeb extends Component {
  webView = {
    canGoBack: false,
    ref: null,
    url: null,
    loading: true
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false
    };
  }

  //   runJSInBackground (code) {
  //     alert(code)
  //     this.webView.injectJavaScript(code)
  //   }

  componentDidMount() {
    //  alert(this.props.url)
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: color.toolbar }}>
        <StatusBar backgroundColor={color.toolbar} barStyle="light-content" />
        {/* {this.state.isLoading && (
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              flex: 1
            }}
          >
            <Image
              source={require("../../assets/images/loader.gif")}
              style={{ height: 30, width: 30, resizeMode: "contain" }}
            />
          </View>
        )} */}

        <WebView
          source={{ uri: this.props.url }}
          ref={webView => {
            this.webView.ref = webView;
          }}
          onNavigationStateChange={navState => {
            this.webView.canGoBack = navState.canGoBack;
            this.webView.url = navState.url;
            this.webView.loading = navState.loading;
            this.setState({ isLoading: navState.loading });
          }}
          style={[styles.webView]}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          startInLoadingState={false}
          scalesPageToFit={true}
        />
      </View>
    );
  }
}

export default LumpsumBasketWeb;
