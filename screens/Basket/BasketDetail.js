import React, { Component } from "react";
import {
  Platform,
  StatusBar,
  View,
  Text,
  Image,
  Dimensions,
  AsyncStorage,
  ScrollView,
  TouchableOpacity
} from "react-native";
import { Actions } from "react-native-router-flux";
import StarRating from "react-native-star-rating";
import SimpleTable from "../../components/SimpleTable";
import ChartView from "react-native-highcharts";
import { Card } from "react-native-elements"; // 0.18.5
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";

const { width } = Dimensions.get("window");
const columnsLumpsum = [
  {
    title: "Scheme",
    dataIndex: "scheme",
    width: 180,
    index: 0
  },
  {
    title: "Lumpsum Amt",
    dataIndex: "amt",
    width: 100,
    index: 1
  },
  {
    title: "Lumpsum Weightage",
    dataIndex: "weight",
    width: 100,
    index: 2
  }
];

const columnsSip = [
  {
    title: "Scheme",
    dataIndex: "scheme",
    width: 180,
    index: 0
  },
  {
    title: "SIP Amt",
    dataIndex: "amt",
    width: 100,
    index: 1
  },
  {
    title: "SIP Weightage",
    dataIndex: "weight",
    width: 100,
    index: 2
  }
];

export default class BasketDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      isLoading: true,
      piechart: [],
      basket_master: {},
      Returns: {},
      border: [color.LIGHT_GREY, color.LIGHT_GREY],
      table: "Sip",
      lumpsum: [],
      sip: [],
      sub_basket: []
    };

    this.fetchData = this.fetchData.bind(this);
  }

  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    fetch(Apis.MfBasket, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.2&access_token=${access_token}&user=${clientid}&client_id=${clientid}&unique_code=${
        this.props.id
      }` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson["status"] === "Success") {
          var e = 0,
            d = 0,
            o = 0,
            c = 0,
            h = 0;
          const border = this.state.border;
          responseJson["data"]["sub_basket"].map(item => {
            if (item.asset_type === "Equity") {
              e++;
            } else if (item.asset_type === "Debt") {
              d++;
            } else if (item.asset_type === "Others") {
              o++;
            } else if (item.asset_type === "Hybrid") {
              h++;
            } else if (item.asset_type === "Commodity") {
              c++;
            }

            if (item.min_amt_lumpsum_new != "0") {
              var lumpsumdata = {
                scheme: item.Scheme_Name,
                amt: item.min_amt_lumpsum_new,
                weight: item.lpm_weightage
              };
              this.state.lumpsum.push(lumpsumdata);
            }

            if (item.min_amt_sip_new != "0") {
              var sipdata = {
                scheme: item.Scheme_Name,
                amt: item.min_amt_sip_new,
                weight: item.sip_weightage
              };
              this.state.sip.push(sipdata);
            }
          });

          if (this.state.sip.length > 0) {
            this.setState({ table: "Sip" });
            border[0] = color.toolbar;
            border[1] = color.LIGHT_GREY;
          } else if (this.state.lumpsum.length > 0) {
            this.setState({ table: "lumpsum" });
            border[0] = color.LIGHT_GREY;
            border[1] = color.toolbar;
          }

          var dataStorage = [];
          if (e > 0) {
            var data = { name: "Equity", y: e };

            dataStorage.push(data);
          }
          if (d > 0) {
            var data = { name: "Debt", y: d };
            dataStorage.push(data);
          }
          if (o > 0) {
            var data = { name: "Others", y: o };
            dataStorage.push(data);
          }
          if (h > 0) {
            var data = { name: "Hybrid", y: h };
            dataStorage.push(data);
          }
          if (c > 0) {
            var data = { name: "Commodity", y: c };
            dataStorage.push(data);
          }

          var onem = 0.0,
            thm = 0.0,
            sixm = 0.0,
            oney = 0.0,
            thy = 0.0,
            fivy = 0.0;
          var om = 0,
            th = 0,
            sm = 0,
            oy = 0,
            ty = 0,
            fy = 0;
          responseJson["data"]["basket_scheme"].map(item => {
            if (item["1MONTHRET"] != "") {
              onem += parseFloat(item["1MONTHRET"]);
              om++;
            }
            if (item["3MONTHRET"] != "") {
              thm += parseFloat(item["3MONTHRET"]);
              th++;
            }
            if (item["6MONTHRET"] != "") {
              sixm += parseFloat(item["6MONTHRET"]);
              sm++;
            }
            if (item["1YRRET"] != "") {
              oney += parseFloat(item["1YRRET"]);
              oy++;
            }
            if (item["3YEARRET"] != "") {
              thy += parseFloat(item["3YEARRET"]);
              ty++;
            }
            if (item["5YEARRET"] != "") {
              fivy += parseFloat(item["5YEARRET"]);
              fy++;
            }
          });

          var dataStorage1 = {
            oneMONTHRET: onem / om,
            threeMONTHRET: thm / th,
            sixMONTHRET: sixm / sm,
            oneYRRET: oney / oy,
            threeYEARRET: thy / ty,
            fiveYEARRET: fivy / fy
          };
          this.setState({
            sub_basket: responseJson["data"]["sub_basket"],
            piechart: dataStorage,
            basket_master: responseJson["data"]["basket_master"],
            Returns: dataStorage1,
            border
          });

         // alert(responseJson["data"]["basket_master"].basket_id)
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  renderColor(item) {
    if (item < 0) {
      return color.RED;
    } else {
      return color.GREEN;
    }
  }

  Change(name, i) {
    const border = this.state.border;
    border.map((item, ii) => {
      if (item === color.toolbar) {
        border[ii] = color.LIGHT_GREY;
      }
    });

    border[i] = color.toolbar;

    if (name === "Sip") {
      this.setState({ table: "Sip" });
    } else if (name === "lumpsum") {
      this.setState({ table: "lumpsum" });
    }

    this.setState({ border });
  }

  render() {
    var Highcharts = "Highcharts";

    var conf = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie"
      },
      title: {
        text: `Asset Allocation`
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.y}</b>"
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: true,
            format: "<b>{point.name}</b>: {point.y}",
            style: {
              color:
                (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
                "black"
            }
          }
        }
      },
      series: [
        {
          name: "Series 1",
          colorByPoint: true,
          data: this.state.piechart
        }
      ]
    };

    const options = {
      global: {
        useUTC: false
      },
      lang: {
        decimalPoint: ".",
        thousandsSep: ","
      }
    };

    return (
      <View style={{ backgroundColor: color.WHITE }}>
        <StatusBar hidden />

        <View>
          <Image
            source={{
              uri: Apis.BaseImageURL + `${this.state.basket_master.image_thumb}`
            }}
            style={{
              height: Platform.OS === "ios" ? 120 : 100,
              opacity: 0.5,
              backgroundColor: color.BLACK
            }}
          />
          <TouchableOpacity
            style={{
              width: 20,
              marginLeft: 12,
              position: "absolute",
              marginTop: 50,
              opacity: 15
            }}
            onPress={() => Actions.BasketMain()}
          >
            <Image
              source={require("../../assets/images/back.png")}
              style={{ height: 20 }}
            />
          </TouchableOpacity>
          <View
            style={{
              width: Platform.OS === "ios" ? 80 : 70,
              height: Platform.OS === "ios" ? 80 : 70,
              elevation: 10,
              shadowColor: color.LIGHT_BLUE,
              borderRadius: Platform.OS === "ios" ? 80 / 2 : 70 / 2,
              alignSelf: "center",
              resizeMode: "contain",
              position: "absolute",
              marginTop: Platform.OS === "ios" ? 80 : 60,
              backgroundColor: color.WHITE
            }}
          >
            <Image
              source={require("../../assets/images/basket-tag-blue.png")}
              style={{
                width: Platform.OS === "ios" ? 60 : 50,
                height: Platform.OS === "ios" ? 60 : 50,
                resizeMode: "contain",
                alignSelf: "center",
                justifyContent: "center",
                marginTop: 10
              }}
            />
          </View>
        </View>
        <ScrollView
          contentInsetAdjustmentBehavior="always"
          style={{
            marginBottom: 120,
            marginTop: Platform.OS == "ios" ? 40 : 0
          }}
        >
          <View>
            <View style={{ alignItems: "center", justifyContent: "center" }}>
              <Text
                style={[
                  styles.welcome,
                  {
                    marginTop: Platform.OS === "ios" ? 50 : 40,
                    textAlign: "center",
                    fontSize: Platform.OS === "ios" ? 17 : 15,
                    marginLeft: 5,
                    marginRight: 5,
                    color: color.BLACK,
                    fontWeight: "600"
                  }
                ]}
              >
                {this.state.basket_master.basket_name}
              </Text>
              <View
                style={{
                  borderColor: color.ORANGE,
                  borderRadius: 5,
                  borderWidth: 1,
                  marginLeft: 5,
                  alignItems: "center",
                  justifyContent: "center",
                  marginTop: 10
                }}
              >
                <Text
                  style={[
                    styles.ExploreAllText,
                    { color: color.ORANGE, padding: 5 }
                  ]}
                >
                  {this.state.basket_master.name}
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                marginLeft: 30,
                marginRight: 30,
                marginTop: 20
              }}
            >
              <View>
                <Text style={styles.title}>Popularity</Text>
                <StarRating
                  containerStyle={{ marginLeft: 5, marginTop: 10 }}
                  fullStar={require("../../assets/images/popularity.png")}
                  emptyStar={require("../../assets/images/popularity-grey.png")}
                  disabled={true}
                  maxStars={5}
                  starSize={18}
                  rating={this.state.basket_master.popular}
                />
              </View>
              {this.state.basket_master.investment_mode === "1" ||
              this.state.basket_master.investment_mode === "3" ? (
                <View
                  style={{ flex: 1, alignItems: "flex-end", marginRight: 5 }}
                >
                  <Text style={styles.title}>Minimum SIP</Text>
                  <Text
                    style={{
                      color: color.toolbar,
                      fontSize: 18,
                      marginTop: 5,
                      marginLeft: 5,
                      textAlign: "center"
                    }}
                  >
                    {"\u20B9"}
                    {this.state.basket_master.basket_min_amt_sip_total}
                  </Text>
                </View>
              ) : (
                <View
                  style={{ flex: 1, alignItems: "flex-end", marginRight: 5 }}
                >
                  <Text style={styles.title}>Minimum lump-sum Investment</Text>
                  <Text
                    style={{
                      color: color.toolbar,
                      fontSize: 18,
                      marginTop: 5,
                      marginLeft: 5,
                      textAlign: "center"
                    }}
                  >
                    {"\u20B9"}
                    {this.state.basket_master.basket_min_amt_lumpsum_total}
                  </Text>
                </View>
              )}
            </View>
            {this.state.basket_master.investment_mode === "3" && (
              <View
                style={{
                  marginTop: 15,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <Text style={styles.title}>Minimum lump-sum Investment</Text>
                <Text
                  style={{
                    color: color.toolbar,
                    fontSize: 18,
                    marginTop: 5,
                    marginLeft: 5,
                    textAlign: "center"
                  }}
                >
                  {"\u20B9"}
                  {this.state.basket_master.basket_min_amt_lumpsum_total}
                </Text>
              </View>
            )}

            <View
              style={[
                styles.container2,
                { marginTop: 15, marginLeft: 5, marginRight: 5 }
              ]}
            >
              <View style={styles.buttonContainerPage1}>
                {this.state.basket_master.investment_mode === "2" ||
                this.state.basket_master.investment_mode === "3" ? (
                  <TouchableOpacity
                    onPress={() =>
                      Actions.LumpsumBasket1({
                        image_thumb: this.state.basket_master.image_thumb,
                        basket_name: this.state.basket_master.basket_name,
                        sub_basket: this.state.sub_basket,
                        basket_min_amt_lumpsum_total: this.state.basket_master
                          .basket_min_amt_lumpsum_total,
                        unique_code: this.props.id,
                        basket_id:this.state.basket_master.basket_id
                      })
                    }
                    style={[styles.button, { backgroundColor: color.GREEN }]}
                  >
                    <Text
                      style={[
                        styles.instructions,
                        { fontSize: Platform.OS == "ios" ? 15 : 12 }
                      ]}
                    >
                      Invest Now
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity
                    style={[
                      styles.button,
                      { backgroundColor: color.LIGHT_GREY }
                    ]}
                  >
                    <Text
                      style={[
                        styles.instructions,
                        {
                          fontSize: Platform.OS == "ios" ? 15 : 12,
                          color: color.GREY
                        }
                      ]}
                    >
                      Invest Now
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
              <View style={styles.buttonContainerPage1}>
                {this.state.basket_master.investment_mode === "1" ||
                this.state.basket_master.investment_mode === "3" ? (
                  <TouchableOpacity
                    onPress={() => Actions.SipBasket1({image_thumb: this.state.basket_master.image_thumb,
                      basket_name: this.state.basket_master.basket_name,
                      sub_basket: this.state.sub_basket,
                      basket_min_amt_sip_total: this.state.basket_master.basket_min_amt_sip_total,
                      unique_code: this.props.id,
                      basket_id:this.state.basket_master.basket_id})}
                    style={[styles.button, { backgroundColor: color.sipbg }]}
                  >
                    <Text
                      style={[
                        styles.instructions,
                        { fontSize: Platform.OS == "ios" ? 15 : 12 }
                      ]}
                    >
                      SIP
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity
                    style={[
                      styles.button,
                      { backgroundColor: color.LIGHT_GREY }
                    ]}
                  >
                    <Text
                      style={[
                        styles.instructions,
                        {
                          fontSize: Platform.OS == "ios" ? 15 : 12,
                          color: color.GREY
                        }
                      ]}
                    >
                      SIP
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </View>

            <Card title="Rational">
              <Text>{this.state.basket_master.rationale}</Text>
            </Card>

            <Card title="Returns">
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <Text style={{ flex: 1 }}>1M</Text>
                  <Text
                    style={{
                      color: this.renderColor(this.state.Returns.oneMONTHRET),
                      flex: 1
                    }}
                  >
                    {this.state.Returns.oneMONTHRET != ""
                      ? parseFloat(this.state.Returns.oneMONTHRET).toFixed(2)
                      : "-"}
                    %
                  </Text>
                </View>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <Text style={{ flex: 1 }}>3M</Text>
                  <Text
                    style={{
                      color: this.renderColor(this.state.Returns.threeMONTHRET),
                      flex: 1
                    }}
                  >
                    {this.state.Returns.threeMONTHRET != ""
                      ? parseFloat(this.state.Returns.threeMONTHRET).toFixed(2)
                      : "-"}
                    %
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <Text style={{ flex: 1 }}>6M</Text>
                  <Text
                    style={{
                      color: this.renderColor(this.state.Returns.sixMONTHRET),
                      flex: 1
                    }}
                  >
                    {this.state.Returns.sixMONTHRET != ""
                      ? parseFloat(this.state.Returns.sixMONTHRET).toFixed(2)
                      : "-"}
                    %
                  </Text>
                </View>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <Text style={{ flex: 1 }}>1Y</Text>
                  <Text
                    style={{
                      color: this.renderColor(this.state.Returns.oneYRRET),
                      flex: 1
                    }}
                  >
                    {this.state.Returns.oneYRRET != ""
                      ? parseFloat(this.state.Returns.oneYRRET).toFixed(2)
                      : "-"}
                    %
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <Text style={{ flex: 1 }}>3Y</Text>
                  <Text
                    style={{
                      color: this.renderColor(this.state.Returns.threeYEARRET),
                      flex: 1
                    }}
                  >
                    {this.state.Returns.threeYEARRET != ""
                      ? parseFloat(this.state.Returns.threeYEARRET).toFixed(2)
                      : "-"}
                    %
                  </Text>
                </View>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <Text style={{ flex: 1 }}>5Y</Text>
                  <Text
                    style={{
                      color: this.renderColor(this.state.Returns.fiveYEARRET),
                      flex: 1
                    }}
                  >
                    {this.state.Returns.fiveYEARRET != ""
                      ? parseFloat(this.state.Returns.fiveYEARRET).toFixed(2)
                      : "-"}
                    %
                  </Text>
                </View>
              </View>
            </Card>
            <Card title="Description">
              <Text>{this.state.basket_master.description}</Text>
              <ChartView
                style={{ height: 300 }}
                config={conf}
                options={options}
              />
              <View style={{ flexDirection: "row", margin: 10 }}>
                {this.state.sip.length > 0 && (
                  <TouchableOpacity
                    style={{ flex: 1 }}
                    onPress={() => this.Change("Sip", 0)}
                  >
                    <Text style={[styles.flexboxtext]}>SIP </Text>
                    <View
                      style={[
                        styles.dash,
                        {
                          borderBottomColor: this.state.border[0],
                          borderBottomWidth: 1
                        }
                      ]}
                    />
                  </TouchableOpacity>
                )}
                {this.state.lumpsum.length > 0 && (
                  <TouchableOpacity
                    style={{ flex: 1 }}
                    onPress={() => this.Change("lumpsum", 1)}
                  >
                    <Text style={[styles.flexboxtext]}>Lumpsum </Text>
                    <View
                      style={[
                        styles.dash,
                        {
                          borderBottomColor: this.state.border[1],
                          borderBottomWidth: 1
                        }
                      ]}
                    />
                  </TouchableOpacity>
                )}
              </View>
              {this.state.table === "Sip" && (
                <SimpleTable
                  height={"100%"}
                  columns={columnsSip}
                  dataSource={this.state.sip}
                />
              )}
              {this.state.table === "lumpsum" && (
                <SimpleTable
                  height={"100%"}
                  columns={columnsLumpsum}
                  dataSource={this.state.lumpsum}
                />
              )}
            </Card>
          </View>
        </ScrollView>
      </View>
    );
  }
}
