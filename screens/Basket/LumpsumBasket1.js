import React, { Component } from "react";
import {
  AsyncStorage,
  FlatList,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  TextInput,
  KeyboardAvoidingView
} from "react-native";
import StarRating from "react-native-star-rating";
import { Actions } from "react-native-router-flux";
import { Card } from "react-native-elements";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";

class LumpsumBasket1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qty: "1",
      fundimage: true,
      investimage: false,
      error: null,
      amterror: false,
      multipliererror: false,
      data: {},
      basket_min_amt_lumpsum_total: this.props.basket_min_amt_lumpsum_total
    };
  }

   
  confirm() {
    Actions.LumpsumBasket2({
      image_thumb: this.props.image_thumb,
      basket_name: this.props.basket_name,
      sub_basket: this.props.sub_basket,
      basket_min_amt_lumpsum_total: this.props.basket_min_amt_lumpsum_total,
      unique_code: this.props.unique_code,
      amount: this.state.basket_min_amt_lumpsum_total,
      quantity: this.state.qty,
      basket_id:this.props.basket_id
    });
  }

  renderRowLine(item, index) {
    return (
      <Card containerStyle={{ margin: 0, marginBottom: 8, padding: 0 }}>
        <View style={{ flexDirection: "row" }}>
          <Image
            source={{
              uri: Apis.BaseImageURL + `${item.Amc_code}.jpg`
            }}
            style={{
              height: 40,
              width: 80,
              resizeMode: "stretch",
              flex: 0.2,
              margin: 7
            }}
          />
          <Text
            style={{ color: color.BLACK, fontSize: 12, flex: 0.8, margin: 7 }}
          >
            {item.Scheme_Name}
          </Text>
        </View>
      </Card>
    );
  }

  render() {
    return (
      <ScrollView
        style={[styles.containerFlex, { backgroundColor: color.LIGHT_GREY }]}
      >
        <KeyboardAvoidingView behavior="padding">
          <View>
            <StatusBar
              backgroundColor={color.toolbar}
              barStyle="light-content"
            />

            <Card containerStyle={{ opacity: this.state.opacity }}>
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <View
                  style={{
                    borderRadius: 5,
                    borderColor: color.GREY,
                    borderWidth: 1,
                    justifyContent: "center"
                  }}
                >
                  <Image
                    source={{
                      uri: Apis.BaseImageURL + `${this.props.image_thumb}`
                    }}
                    style={{
                      padding: 15,
                      height: 50,
                      width: 100,
                      resizeMode: "stretch"
                    }}
                  />
                </View>
                <Text
                  style={[
                    styles.welcome,
                    { textAlign: "center", marginTop: 15, color: color.toolbar }
                  ]}
                >
                  {this.props.basket_name}
                </Text>
              </View>

              <View style={[styles.dash, { margin: 7, marginTop: 15 }]} />

              <FlatList
                scrollEnabled={false}
                showsVerticalScrollIndicator={false}
                style={{ marginTop: 10, marginBottom: 10 }}
                data={this.props.sub_basket}
                renderItem={({ item, index }) =>
                  this.renderRowLine(item, index)
                }
              />

              <View
                style={{
                  marginTop: 15,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    borderBottomColor: color.BGREY,
                    borderBottomWidth: 1,
                    width: 100,
                    marginTop: 15
                  }}
                >
                  <TextInput
                    underlineColorAndroid="transparent"
                    keyboardType="numeric"
                    returnKeyType="done"
                    defaultValue={this.state.qty}
                    onChangeText={text => {
                      if (text === "") {
                        this.setState({
                          qty: "1",
                          basket_min_amt_lumpsum_total: this.props
                            .basket_min_amt_lumpsum_total
                        });
                      } else {
                        this.setState({
                          qty: text,
                          basket_min_amt_lumpsum_total:
                            this.props.basket_min_amt_lumpsum_total *
                            parseInt(text)
                        });
                      }
                    }}
                    style={{
                      flex: 0.9,
                      fontSize: 15,
                      textAlign: "center",
                      color: color.BLACK
                    }}
                  />
                  <View
                    style={{
                      flex: 0.1,
                      margin: 5,
                      alignItems: "flex-end"
                    }}
                  >
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({
                          qty: parseInt(this.state.qty) + 1 + "",
                          basket_min_amt_lumpsum_total:
                            this.props.basket_min_amt_lumpsum_total *
                            (parseInt(this.state.qty) + 1)
                        })
                      }
                    >
                      <Image
                        source={require("../../assets/images/arrow-up.png")}
                        style={{
                          height: 15,
                          width: 15,
                          resizeMode: "contain"
                        }}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        if (parseInt(this.state.qty) > 1) {
                          this.setState({
                            qty: parseInt(this.state.qty) - 1 + "",
                            basket_min_amt_lumpsum_total:
                              this.props.basket_min_amt_lumpsum_total *
                              (parseInt(this.state.qty) - 1)
                          });
                        }
                      }}
                    >
                      <Image
                        source={require("../../assets/images/arrow-down.png")}
                        style={{
                          height: 15,
                          width: 15,
                          resizeMode: "contain"
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <Text
                  style={{
                    marginTop: 5,
                    color: color.GREY,
                    fontSize: 13,
                    textAlign: "center",
                    alignItems: "center"
                  }}
                >
                  Quantity
                </Text>
              </View>

              <View
                style={{
                  marginTop: 15,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    borderBottomColor: color.BGREY,
                    borderBottomWidth: 1,
                    textAlign: "center",
                    fontSize: 16,
                    color: color.BLACK,
                    width: 200,
                    marginTop: 15
                  }}
                >
                  {this.state.basket_min_amt_lumpsum_total}
                </Text>
                <Text
                  style={{
                    marginTop: 5,
                    color: color.GREY,
                    fontSize: 13,
                    textAlign: "center",
                    alignItems: "center"
                  }}
                >
                  Amount
                </Text>
              </View>

              <View style={[styles.buttonContainer, { marginTop: 15 }]}>
                <TouchableOpacity
                  onPress={() => this.confirm()}
                  style={[styles.button, { backgroundColor: color.toolbar }]}
                >
                  <Text style={styles.instructions}>CONFIRM AND CONTINUE</Text>
                </TouchableOpacity>
              </View>
            </Card>
            {/* {this.state.isLoading && (
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              flex: 1
            }}
          >
            <Image
              source={require("../../assets/images/loader.gif")}
              style={{ height: 30, width: 30, resizeMode: "contain" }}
            />
          </View>
        )} */}
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }
}

export default LumpsumBasket1;
