import React, { Component } from "react";
import {
  AsyncStorage,
  Alert,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  StatusBar,
  TextInput,
  KeyboardAvoidingView
} from "react-native";
import PopupDialog, {
  DialogTitle,
  SlideAnimation
} from "react-native-popup-dialog";
import StarRating from "react-native-star-rating";
import moment from "moment";
import { Actions } from "react-native-router-flux";
import { Calendar } from "react-native-calendars";
import { Card } from "react-native-elements";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";
const slideAnimation = new SlideAnimation({ slideFrom: "bottom" });
var datetoday = new Date();
var currentday = datetoday ? ("0" + datetoday.getDate()).slice(-2) : "";
var currentmonth = datetoday
  ? ("0" + (datetoday.getMonth() + 1)).slice(-2)
  : "";
var year = datetoday ? datetoday.getFullYear() : "";
const currentDate = year + "-" + currentmonth + "-" + currentday;
const maxDate = year + 1 + "-" + currentmonth + "-" + currentday;
const minDate = year + "-" + currentmonth + "-" + "01";
const mandate_uploaded = "",
  mandate_deficiency = "",
  mandate_status = "";
function cleantext(strInputCode) {
  cleanText = strInputCode.replace("<ul><li>", "\u2022 ");
  cleanText = cleanText.replace("<ul></ul>", "");
  cleanText = cleanText.replace("</li><li>", "\n\n\u2022 ");
  cleanText = cleanText.replace("</li><li>", "\n\n\u2022 ");
  cleanText = cleanText.replace("</li><li>", "\n\n\u2022 ");
  cleanText = cleanText.replace("</li></ul>", "");
  return cleanText;
}

function nth(d) {
  if (d > 3 && d < 21) return "th";
  switch (d % 10) {
    case 1:
      return "st";
    case 2:
      return "nd";
    case 3:
      return "rd";
    default:
      return "th";
  }
}

class Sip1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      opacity: 0,
      fundimage: true,
      investimage: false,
      starCount: 2.5,
      error: null,
      amterror: false,
      multipliererror: false,
      disabled_dates: [],
      allowed_dates: [],
      mandate: {},
      event: [],
      date: "",
      amount: 5000,
      markedDates: [],
      disableddates: [],
      dateerror: false,
      maxamterror: false
    };
    this.onDayPress = this.onDayPress.bind(this);
  }

  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    this.setState({
      isLoading: true,
      opacity: 0,
      disabled_dates: [],
      allowed_dates: [],
      event: [],
      mandate: {},
      markedDates: []
    });
    //  alert(`api=1.4&access_token=${access_token}&user=${clientid}&client_id=${clientid}&amount=${this.state.amount}&order_type=normal&scheme_code${this.props.Scheme_Code}`);
    fetch(Apis.Mf_order, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.4&access_token=${access_token}&user=${clientid}&client_id=${clientid}&amount=${
        this.state.amount
      }&order_type=normal&scheme_code=${this.props.Scheme_Code}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        //  alert(responseJson["status"])
        if (responseJson["status"] === "success") {
          this.setState({
            isLoading: false,
            opacity: 1,
            disabled_dates: responseJson["data"]["disabled_dates"],
            allowed_dates: responseJson["data"]["allowed_dates"],
            event: responseJson["data"]["event"],
            mandate: responseJson["data"]["mandate"],
            markedDates: this.getDaysInMonth(
              moment().month() + 1,
              moment().year(),
              responseJson["data"]["disabled_dates"],
              responseJson["data"]["event"]
            )
          });
          mandate_deficiency = this.state.mandate.deficiency
            ? this.state.mandate.deficiency
            : "No";
          mandate_uploaded = this.state.mandate.uploaded
            ? this.state.mandate.uploaded
            : "No";
          mandate_status = this.state.mandate.mandate_status
            ? this.state.mandate.mandate_status
            : "No";
          mandate_link = this.state.mandate.mandate_link
            ? this.state.mandate.mandate_link
            : "No";
          this._storeData();
        }
        //   alert(this.state.data.ledger_balance)
      })
      .catch(error => {
        alert(error);
      });
  }

  _storeData = async () => {
    try {
      await AsyncStorage.setItem("mandate_uploaded", mandate_uploaded);
      await AsyncStorage.setItem("mandate_status", mandate_status);
      await AsyncStorage.setItem("mandate_deficiency", mandate_deficiency);
      await AsyncStorage.setItem("mandate_id", this.state.mandate.mandate);
      await AsyncStorage.setItem(
        "mandate_link",
        this.state.mandate.mandate_link
      );
    } catch (error) {
      // Error saving data
      // alert(1)
    }
  };

  showSlideAnimationDialog = () => {
    this.slideAnimationDialog.show();
  };

  onDayPress(day) {
    // this.setState({
    //   selected: day.dateString
    // });

    var data = [],
      flag = 0;
    this.state.event.map(e => {
      if (e.start.split("-")[2] == day.dateString.split("-")[2]) {
        data.push(e);
        flag = 1;
      }
    });

    var message = "";

    data.map(
      e =>
        (message +=
          "\n" +
          e.title +
          "\n" +
          e.order_type +
          "\n" +
          cleantext(e.type) +
          "\n" +
          e.description +
          "\n" +
          "-----------")
    );
    message = message.substring(0, message.lastIndexOf("\n"));

    if (flag === 1) {
      Alert.alert(day.dateString, message, [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: () => {
            if (
              moment().format("k") >= 20 &&
              moment().format("MM") ==
                moment(day.dateString, "YYYY-MM-DD").format("MM")
            ) {
              alert(
                "Order cannot be processed for this day. Kindly pick another date."
              );
            } else if (this.state.disableddates.indexOf(day.dateString) == -1) {
              this.setState({
                date: day.dateString,
                dateerror: false
              });
              this.slideAnimationDialog.dismiss();
            } else {
              Alert.alert(
                "",
                "Order cannot be processed for this day. Kindly pick another date."
              );
            }
          }
        }
      ]);
    } else {
      if (
        moment().format("k") >= 20 &&
        moment().format("MM") ==
          moment(day.dateString, "YYYY-MM-DD").format("MM")
      ) {
        Alert.alert(
          "",
          "Order Cannot be processed for this day. Kindly pick another Date."
        );
      } else if (this.state.disableddates.indexOf(day.dateString) == -1) {
        this.setState({
          date: day.dateString,
          dateerror: false
        });
        this.slideAnimationDialog.dismiss();
      }
    }
  }

  getDaysInMonth(month, year, days, event) {
    //const disableddates = this.state.disableddates;
    let dates = {};
    //alert(pivot.date(days[0]).format("YYYY-MM-DD"))
    const sip = {
      customStyles: {
        container: {
          backgroundColor: color.toolbar
        },
        text: {
          color: color.WHITE
        }
      }
    };
    const basket = {
      customStyles: {
        container: {
          backgroundColor: color.basketcolor
        },
        text: {
          color: color.WHITE
        }
      }
    };
    const disabled = { disabled: true };
    if (month + "" === currentmonth + "") {
      for (var i = 1; i <= 31; i++) {
        if (i < currentDate.split("-")[2]) {
          d1 = i ? ("0" + i).slice(-2) : "";
          d = year + "-" + month + "-" + d1;
          dates[d] = disabled;
          this.state.disableddates.push(d);
        }
      }
    }

    days.forEach(day => {
      d1 = day ? ("0" + day).slice(-2) : "";
      d = year + "-" + month + "-" + d1;
      dates[d] = disabled;
      this.state.disableddates.push(d);
    });

    dates[currentDate] = disabled;
    this.state.disableddates.push(currentDate);

    event.forEach(item => {
      dt = item.start.split("-")[2];
      d = year + "-" + month + "-" + dt;

      if (
        new Date(d) >= new Date(currentDate) &&
        new Date(d) <= new Date(maxDate)
      ) {
        if (item.order_type === "SIP") {
          dates[d] = sip;
        } else if (item.order_type === "basket") {
          dates[d] = basket;
        }
      }

      if (
        new Date(d).getMonth() == new Date(currentDate).getMonth() ||
        new Date(d).getMonth() == new Date(maxDate).getMonth()
      ) {
        if (item.order_type === "SIP") {
          dates[d] = sip;
        } else if (item.order_type === "basket") {
          dates[d] = basket;
        }
      }
    });

    return dates;
  }

  confirm() {
    if (this.state.date === "") {
      this.setState({ dateerror: true });
    } else {
      if (this.state.amount < parseInt(this.props.sip_min_intallment_amt)) {
        this.setState({ amterror: true });
      } else if (this.state.amount > 10000000) {
        this.setState({ maxamterror: true });
      } else {
        this.setState({ amterror: false });
        this.setState({ maxamterror: false });
        if (
          parseFloat(this.state.amount) %
            parseFloat(this.props.sip_multiplier_amt) ===
          0
        ) {
          this.setState({ multipliererror: false });
          Actions.SipOrder2({
            Scheme_Code: this.props.Scheme_Code,
            Unique_No: this.props.Unique_No,
            Amc_code: this.props.Amc_code,
            Scheme_Name: this.props.Scheme_Name,
            amount: this.state.amount,
            date: this.state.date,
            mandate_id: this.state.mandate.mandate
          });
        } else {
          this.setState({ multipliererror: true });
        }
      }
    }
  }

  render() {
    return (
      <KeyboardAvoidingView
        style={[styles.containerFlex, { backgroundColor: color.LIGHT_GREY }]}
        behavior="padding"
      >
        <View>
          <StatusBar backgroundColor={color.toolbar} barStyle="light-content" />

          <Card containerStyle={{ opacity: this.state.opacity }}>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <View
                style={{
                  borderRadius: 5,
                  borderColor: color.GREY,
                  borderWidth: 1,
                  justifyContent: "center"
                }}
              >
                <Image
                  source={{
                    uri: Apis.BaseImageURL + `${this.props.Amc_code}.jpg`
                  }}
                  style={{
                    padding: 15,
                    height: 50,
                    width: 100,
                    resizeMode: "stretch"
                  }}
                />
              </View>
              <Text
                style={[
                  styles.welcome,
                  { textAlign: "center", marginTop: 15, color: color.toolbar }
                ]}
              >
                {this.props.Scheme_Name}
              </Text>
            </View>

            <View style={[styles.dash, { margin: 7, marginTop: 15 }]} />
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                marginLeft: 30,
                marginTop: 15
              }}
            >
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <Text style={[styles.ExploreAllText, { color: color.GREY }]}>
                  Rating
                </Text>

                <StarRating
                  starStyle={{ marginTop: 15 }}
                  fullStar={require("../../assets/images/star.png")}
                  emptyStar={require("../../assets/images/emptystar.png")}
                  disabled={true}
                  maxStars={5}
                  starSize={18}
                  halfStarEnabled={true}
                  rating={parseInt(this.props.rating)}
                />
              </View>
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                  marginLeft: 15
                }}
              >
                <Text style={[styles.ExploreAllText, { color: color.GREY }]}>
                  Nav
                </Text>

                <Text style={{ marginTop: 10 }}>
                  {"\u20B9"}
                  {this.props.Navrs}
                </Text>
              </View>
            </View>
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 0.5 }} />
              <Text
                style={{
                  flex: 0.5,
                  marginTop: 5,
                  color: color.BGREY,
                  fontSize: 13,
                  textAlign: "center",
                  alignSelf: "flex-end"
                }}
              >
                ({this.props.Navdate})
              </Text>
            </View>
            <View style={[styles.dash, { margin: 7, marginTop: 15 }]} />
            <View
              style={{
                marginTop: 15,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TextInput
                underlineColorAndroid="transparent"
                keyboardType="numeric"
                returnKeyType="done"
                defaultValue={"5000"}
                onSubmitEditing={() => {
                  if (
                    parseFloat(this.state.amount) <
                    parseFloat(this.props.sip_min_intallment_amt)
                  ) {
                    this.setState({ amterror: true });
                  } else {
                    this.setState({ amterror: false });
                    this.fetchData();
                  }
                }}
                onChangeText={text => this.setState({ amount: text })}
                style={{
                  borderBottomColor: color.BGREY,
                  borderBottomWidth: 1,
                  textAlign: "center",
                  color: color.BLACK,
                  width: 200,
                  marginTop: 15
                }}
              />
              <Text
                style={{
                  marginTop: 5,
                  color: color.BGREY,
                  fontSize: 13,
                  textAlign: "center",
                  alignItems: "center"
                }}
              >
                (Min amt Rs. {this.props.sip_min_intallment_amt})
              </Text>
            </View>
            <View
              style={{
                marginTop: 15,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  borderBottomColor: color.BGREY,
                  borderBottomWidth: 1,
                  textAlign: "center",
                  color: color.BLACK,
                  width: 150,
                  marginTop: 15
                }}
                onPress={() => this.showSlideAnimationDialog()}
              >
                <Text
                  style={{
                    flex: 0.8,
                    textAlign: "center",
                    marginLeft: 10,
                    marginTop: 7
                  }}
                >
                  {this.state.date}
                </Text>
                <Image
                  source={require("../../assets/images/calendar.png")}
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: "contain",
                    margin: 5,
                    alignSelf: "flex-end",
                    flex: 0.2,
                    alignItems: "flex-end"
                  }}
                />
              </TouchableOpacity>

              <Text
                style={{
                  marginTop: 5,
                  color: color.BLACK,
                  fontSize: 13,
                  textAlign: "center",
                  alignItems: "center"
                }}
              >
                Start Date
              </Text>
            </View>
            {this.state.date != "" && (
              <Text style={{ fontWeight: "500", fontSize: 13, marginTop: 15 }}>
                Note:
                <Text
                  style={{
                    fontWeight: "normal",
                    fontSize: 11,
                    paddingLeft: 10
                  }}
                >
                  Please note if you select{" "}
                  {new Date(this.state.date).getDate()}
                  {nth(new Date(this.state.date).getDate())} of each month as
                  SIP date, funds shall be deducted from your account 2 working
                  days before investment date since the units are allotted in
                  demat mode.
                </Text>
              </Text>
            )}
            {this.state.amterror && (
              <Text
                style={{
                  marginTop: 10,
                  color: color.RED,
                  fontSize: 13,
                  textAlign: "center",
                  alignItems: "center"
                }}
              >
                Minimum amount for this scheme is Rs{" "}
                {this.props.sip_min_intallment_amt}
              </Text>
            )}
            {this.state.maxamterror && (
              <Text
                style={{
                  marginTop: 10,
                  color: color.RED,
                  fontSize: 13,
                  textAlign: "center",
                  alignItems: "center"
                }}
              >
                Maximum amount for this scheme is Rs.10000000
              </Text>
            )}
            {this.state.dateerror && (
              <Text
                style={{
                  marginTop: 10,
                  color: color.RED,
                  fontSize: 13,
                  textAlign: "center",
                  alignItems: "center"
                }}
              >
                Please select start date.
              </Text>
            )}
            {this.state.mandate.status === "error" && (
              <Text
                style={{
                  marginTop: 10,
                  color: color.toolbar,
                  fontSize: 13,
                  textAlign: "center",
                  alignItems: "center"
                }}
              >
                {/* Your Maximum mandate amount is {'\u20B9'}{this.state.mandate.mandate_amt}.A new mandate will automatically be created if proceeded with this order. */}
                {this.state.mandate.msg}
              </Text>
            )}
            {this.state.multipliererror && (
              <Text
                style={{
                  marginTop: 10,
                  color: color.RED,
                  fontSize: 13,
                  textAlign: "center",
                  alignItems: "center"
                }}
              >
                Amount should be in multiples of {this.props.sip_multiplier_amt}
              </Text>
            )}
            <View style={[styles.buttonContainer, { marginTop: 15 }]}>
              <TouchableOpacity
                onPress={() => this.confirm()}
                style={[styles.button, { backgroundColor: color.toolbar }]}
              >
                <Text style={styles.instructions}>CONFIRM AND CONTINUE</Text>
              </TouchableOpacity>
            </View>
          </Card>
          {this.state.isLoading && (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                position: "absolute",
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                flex: 1
              }}
            >
              <Image
                source={require("../../assets/images/loader.gif")}
                style={{ height: 30, width: 30, resizeMode: "contain" }}
              />
            </View>
          )}
        </View>
        <PopupDialog
          dialogStyle={{
            height: 400,
            width: 400,
            top: 150,
            position: "absolute"
          }}
          ref={popupDialog => {
            this.slideAnimationDialog = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
          <Calendar
            markingType={"custom"}
            onDayPress={this.onDayPress}
            style={styles.calendar}
            minDate={minDate}
            markedDates={this.state.markedDates}
            maxDate={maxDate}
            onMonthChange={date => {
              this.setState({
                markedDates: this.getDaysInMonth(
                  date.month ? ("0" + date.month).slice(-2) : "",
                  date.year,
                  this.state.disabled_dates,
                  this.state.event
                )
              });
            }}
          />

          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              marginTop: 15
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <View
                style={{
                  height: 10,
                  width: 10,
                  backgroundColor: color.toolbar,
                  alignItems: "center",
                  marginTop: 3
                }}
              />
              <Text style={{ fontSize: 13, marginLeft: 5 }}>SIP</Text>
            </View>
            <View style={{ flexDirection: "row", marginLeft: 15 }}>
              <View
                style={{
                  height: 10,
                  width: 10,
                  backgroundColor: color.basketcolor,
                  alignItems: "center",
                  marginTop: 3
                }}
              />
              <Text style={{ fontSize: 13, marginLeft: 5 }}>BASKET</Text>
            </View>
          </View>
        </PopupDialog>
      </KeyboardAvoidingView>
    );
  }
}

export default Sip1;
