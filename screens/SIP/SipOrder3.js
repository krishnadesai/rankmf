import React, {Component} from 'react';
import {AsyncStorage,Platform, TouchableOpacity, Text, View, Image,ScrollView} from 'react-native';
import { Card } from "react-native-elements"
import PopupDialog, {    DialogTitle,    DialogButton,    SlideAnimation,    ScaleAnimation,    FadeAnimation,  } from 'react-native-popup-dialog';
import { Actions } from "react-native-router-flux";
import styles from '../../components/styles';
import color from '../../constants/Colors';
import Apis from "../../constants/Apis";
const slideAnimation = new SlideAnimation({ slideFrom: 'bottom' });
const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
const Purchase_Allowed="";
const mandate_uploaded="",mandate_deficiency="",mandate_id="",mandate_link="";
function nth(d) {
    if (d > 3 && d < 21) return 'th'; 
    switch (d % 10) {
      case 1:  return "st";
      case 2:  return "nd";
      case 3:  return "rd";
      default: return "th";
    }
  }

class SipOrder3 extends Component {


constructor(props) {
    super(props)
    this.state = {
        error: null,
        data:{},
        order_status:"",
        currentPage:0,
        height3:0,
        opacity: 0,
        isLoading:true,
        showPlaceOrder:true
    }
    this.viewabilityConfig = {itemVisiblePercentThreshold: 40}
} 

componentDidMount() {
  this.retrieveData()
  this.slideAnimationDialog.show()
}

retrieveData = async () => {
  try {

    clienttype = await AsyncStorage.getItem("clienttype");
    clientid = await AsyncStorage.getItem("clientid");
    access_token = await AsyncStorage.getItem("access_token");
    Purchase_Allowed = await AsyncStorage.getItem("Purchase_Allowed");
    mandate_uploaded = await AsyncStorage.getItem("mandate_uploaded");
    mandate_deficiency = await AsyncStorage.getItem("mandate_deficiency");
    mandate_id = await AsyncStorage.getItem("mandate_id");
    mandate_link = await AsyncStorage.getItem("mandate_link");
    this.fetchData()
   
  } catch (error) {
    // Error retrieving data
    //console.warn("1");
  }
};

componentWillReceiveProps(){
  
    this.setState({showPlaceOrder:true})
  
}

fetchData() {
    fetch(Apis.Mf_order, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.5&access_token=${access_token}&user=${clientid}&client_id=${clientid}&installment_amount=${
        this.props.amount
      }&unique_no=${this.props.Unique_No}&scheme_code=${this.props.Scheme_Code}&amc_code=${this.props.Amc_code}&mandate_id=${mandate_id}&date=${this.props.date}&order_type=xsip` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson["status"] === "success") {
          if(mandate_id === "NOMANDATE"){
            this.setState({
              showPlaceOrder:false
            })
          }else{
            if(mandate_uploaded === "0"){
              this.setState({
                showPlaceOrder:false
              })
            }
          }
          //alert(mandate_link)
          this.setState({ data: responseJson["data"] });
          this.setState({ isLoading: false, opacity: 1 });
          this.slideAnimationDialog.dismiss()
          //  alert(bank_allowed)
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  render () {
    return (
        <ScrollView style={[styles.containerFlex,{backgroundColor:color.LIGHT_GREY}]}>
       <View>
         <Card containerStyle={{opacity:this.state.opacity}}>
         <View style={{ justifyContent: "center", alignItems: "center" }}>
            <View
              style={{
                borderRadius: 5,
                borderColor: color.GREY,
                borderWidth: 1,
                justifyContent: "center"
              }}
            >
              <Image
                source={{
                  uri: Apis.BaseImageURL + `${this.props.Amc_code}.jpg`
                }}
                style={{
                  padding: 15,
                  height: 50,
                  width: 100,
                  resizeMode: "stretch"
                }}
              />
            </View>
            <Text
              style={[
                styles.welcome,
                { textAlign: "center", marginTop: 15, color: color.toolbar }
              ]}
            >
              {this.props.Scheme_Name}
            </Text>
          </View>
          <View style={[styles.dash, { margin: 7, marginTop: 15 }]} />
         <View style={{ backgroundColor:color.lumpsumbg,borderColor:color.lumpsumborder,borderWidth:1,borderRadius:5,marginTop:15}}>
            <View style={{padding:20}}>
            <Text style={{color:color.lumpsumtext,fontWeight:"400",textAlign:"justify"}}>{this.state.data.success_msg}</Text>
            </View>
            </View>
       {Purchase_Allowed === "Y" && this.state.showPlaceOrder === true &&  <View style={{ justifyContent: "center", alignItems: "center" }}>
      <View style={{ justifyContent: "center", alignItems: "center" }}>  
     <Text style={{marginTop:20,alignSelf:"center",textAlign:"center",fontWeight:"700",fontSize:15}}>Make your First Investment Today</Text>

          <View style={{ backgroundColor:color.sipbg1,borderColor:color.sipborder,borderWidth:1,borderRadius:5,marginTop:15}}>
            <View style={{padding:20}}>
            <Text style={{color:color.siptext,fontWeight:"400",textAlign:"justify"}}>While your regular SIP in <Text style={{fontWeight:"700"}}>{this.props.Scheme_Name}</Text> can begin 
            from <Text style={{fontWeight:"700"}}>{new Date(this.props.date).getDate()}{nth(new Date(this.props.date).getDate())} {monthNames[new Date(this.props.date).getMonth()]}</Text>
            , you need not wait and can make an investment today.</Text>
            </View>
            </View></View>

         
            <View style={[styles.buttonContainer, { marginTop: 15 }]}>
            <TouchableOpacity
              onPress={() => Actions.SipOrder4({ 
                  Scheme_Code:this.props.Scheme_Code,
                Unique_No:this.props.Unique_No,
                Amc_code: this.props.Amc_code,
                Scheme_Name: this.props.Scheme_Name,
              amount:this.props.amount})}
              style={[styles.button, { backgroundColor: color.toolbar }]}
            >
              <Text style={styles.instructions}>
                PLACE FIRST ORDER TODAY
              </Text>
            </TouchableOpacity>
          </View>
          <Text style={[styles.instructions,{color:color.toolbar,}]} onPress={() => Actions.page1()}>
       Skip Step
     </Text>
          </View> }

              {this.state.showPlaceOrder === false && <View style={{ justifyContent: "center", alignItems: "center" }}>
         <View style={[styles.buttonContainer, { marginTop: 15 }]}>
         <TouchableOpacity
           onPress={() => Actions.SipMandate({url:mandate_link})}
           style={[styles.button, { backgroundColor: color.toolbar }]}
         >
           <Text style={styles.instructions}>
             COMPLETE MANDATE SETUP/AUTO DEBIT
           </Text>
         </TouchableOpacity>
       </View>
       <Text style={[styles.instructions,{color:color.toolbar,}]} onPress={() => this.setState({showPlaceOrder:true})}>
       Skip Step
     </Text></View>
       }
         </Card>
         
        </View>
        <PopupDialog
            dialogStyle={{height:235,width:320, position: 'absolute', top: 150,overflow:"hidden"}}
          ref={(popupDialog) => {
            this.slideAnimationDialog = popupDialog;
          }}
          dialogAnimation={slideAnimation}
          
        >
        <View >
        
           <Text style={{color:color.BLACK,fontSize:18,textAlign:"center",marginTop:10,fontWeight:"bold"}}>Order Processing...</Text>
          
          <View style={{height:100,justifyContent:"center"}}>
            
            <Image source={require('../../assets/images/loader.gif')} style={{ height: 50, width: 50, resizeMode: "contain",alignSelf:"center" }} />
                        
            </View>
        
       
        <Text style={{textAlign:"center",fontSize:15,margin:5,marginTop:0}}>Please wait while you're being redirected to your bank page...</Text>
         
        <View style={{backgroundColor:color.toolbar,marginTop:10}}>
            <Text style={{color:color.WHITE,padding:10,fontSize:14}}>Please be patient. This process might take some time, please do not close this window.</Text>
        </View>
        </View>
        </PopupDialog>
      </ScrollView>
    );
  }
}


export default SipOrder3;