import React, { Component } from "react";
import {
  AsyncStorage,
  WebView,
  Dimensions,
  Text,
  View,
  Image,
  TouchableOpacity
} from "react-native";
import { Card } from "react-native-elements";
import { Actions } from "react-native-router-flux";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";
const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
function nth(d) {
    if (d > 3 && d < 21) return 'th'; 
    switch (d % 10) {
      case 1:  return "st";
      case 2:  return "nd";
      case 3:  return "rd";
      default: return "th";
    }
  }

class SipOrder2 extends Component {


  constructor(props) {
    super(props);
    this.state = {
      error: null,
      data: {},
      isLoading: true,
      opacity: 0,
    };
  }

  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    fetch(Apis.Mf_order, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.2&access_token=${access_token}&user=${clientid}&client_id=${clientid}&amount=${
        this.props.amount
      }mandate_id=${this.props.mandate_id}&date=${this.props.date}&order_type=xsip&is_basket=0` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson["status"] === "success") {
          this.setState({ data: responseJson["data"] });
          this.setState({ isLoading: false, opacity: 1 });
          //  alert(bank_allowed)
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  

  redirect() {
    
            Actions.SipOrder3({Scheme_Code: this.props.Scheme_Code,
                Unique_No: this.props.Unique_No,
                Amc_code: this.props.Amc_code,
                Scheme_Name: this.props.Scheme_Name,
                amount: this.props.amount,
                date: this.props.date,})
       
  }


  render() {
    return (
      <View
        style={[styles.containerFlex, { backgroundColor: color.LIGHT_GREY }]}
      >
        <Card containerStyle={{ opacity: this.state.opacity }}>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <View
              style={{
                borderRadius: 5,
                borderColor: color.GREY,
                borderWidth: 1,
                justifyContent: "center"
              }}
            >
              <Image
                source={{
                  uri: Apis.BaseImageURL + `${this.props.Amc_code}.jpg`
                }}
                style={{
                  padding: 15,
                  height: 50,
                  width: 100,
                  resizeMode: "stretch"
                }}
              />
            </View>
            <Text
              style={[
                styles.welcome,
                { textAlign: "center", marginTop: 15, color: color.toolbar }
              ]}
            >
              {this.props.Scheme_Name}
            </Text>
          </View>
          <View style={[styles.dash, { margin: 7, marginTop: 15 }]} />
          <View style={{ justifyContent: "center", alignItems: "center",flexDirection:"row" }}>
          <Text
              style={{
                marginTop: 5,
                color: color.GREY,
                fontSize: 13,
                textAlign: "center",
                alignItems: "center"
              }}
            >
              Amount(Rs)
            </Text>
            <Text
              style={{
                borderBottomColor: color.BLACK,
                borderBottomWidth: 1,
                textAlign: "center",
                fontSize: 17,
                color: color.BLACK,
                width: 200,
                marginTop: 15
              }}
            >
              {this.props.amount}
            </Text>
            
          </View>
          <View style={{ justifyContent: "center", alignItems: "center",flexDirection:"row"  }}>
          <Text
              style={{
                marginTop: 5,
                color: color.GREY,
                fontSize: 13,
                textAlign: "center",
                alignItems: "center"
              }}
            >
             Installment Date
            </Text>
            <Text
              style={{
                borderBottomColor: color.BLACK,
                borderBottomWidth: 1,
                textAlign: "center",
                fontSize: 14,
                margin:7,
                color: color.BLACK,
                width: 200,
                marginTop: 15
              }}
            >
              {new Date(this.props.date).getDate()}{nth(new Date(this.props.date).getDate())} of each month. Beginning from the {new Date(this.props.date).getDate()}{nth(new Date(this.props.date).getDate())} of {monthNames[new Date(this.props.date).getMonth()]}
            </Text>
           
          </View>
          <View style={{ justifyContent: "center", alignItems: "center",flexDirection:"row"  }}>
          <Text
              style={{
                marginTop: 5,
                color: color.GREY,
                fontSize: 13,
                textAlign: "center",
                alignItems: "center"
              }}
            >
             Payment Date
            </Text>
            <Text
              style={{
                borderBottomColor: color.BLACK,
                borderBottomWidth: 1,
                textAlign: "center",
                fontSize: 14,
                margin:7,
                color: color.BLACK,
                width: 200,
                marginTop: 15
              }}
            >
             2 working days prior to the {new Date(this.props.date).getDate()}{nth(new Date(this.props.date).getDate())} of the month
            </Text>
            
          </View>
          <View style={[styles.buttonContainer, { marginTop: 15 }]}>
            <TouchableOpacity
              onPress={() => this.redirect()}
              style={[styles.button, { backgroundColor: color.toolbar }]}
            >
              <Text style={styles.instructions}>
                CONFIRM AND CONTINUE
              </Text>
            </TouchableOpacity>
          </View>

          <Text style={{ fontWeight: "500", fontSize: 13, marginTop: 15 }}>
            Note:
            <Text
              style={{ fontWeight: "normal", fontSize: 11, paddingLeft: 10 }}
            >
              {this.state.data.note_1}.
            </Text>
          </Text>

          
        </Card>
        {this.state.isLoading && (
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              flex: 1
            }}
          >
            <Image
              source={require("../../assets/images/loader.gif")}
              style={{ height: 30, width: 30, resizeMode: "contain" }}
            />
          </View>
        )}

       
      </View>
    );
  }
}

export default SipOrder2;
