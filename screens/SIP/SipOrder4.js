import React, { Component } from "react";
import {
  AsyncStorage,
  WebView,
  Dimensions,
  Text,
  View,
  Image,
  TouchableOpacity
} from "react-native";
import { Card } from "react-native-elements";
import { Actions } from "react-native-router-flux";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";
const bank_allowed = 0,
  atom_allowed = 0,
  ledger_balance = 0;

class SipOrder4 extends Component {


  constructor(props) {
    super(props);
    this.state = {
      error: null,
      data: {},
      isLoading: true,
      opacity: 0,
      redirect_payment_link: "",
      data1:{}
    };
  }

  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      bank_allowed = await AsyncStorage.getItem("bank_allowed");
      atom_allowed = await AsyncStorage.getItem("atom_allowed");
      ledger_balance = await AsyncStorage.getItem("ledger_balance");
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  // fetchData() {
  //   // alert(this.props.unique_code);
  //   fetch(Apis.Mf_order, {
  //     method: "POST",
  //     headers: new Headers({
  //       "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
  //     }),
  //     body: `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}` // <-- Post parameters
  //   })
  //     .then(response => response.json())
  //     .then(responseJson => {
  //       if(responseJson["status"] === "success"){
  //            this.setState({data1:responseJson["data"],amount:this.props.Minimum_Purchase_Amount})             
  //            this._storeData()
  //            this.fetchDatanote()
  //       }
  //    //   alert(this.state.data.ledger_balance)
  //     })
  //     .catch(error => {
  //       console.error(error);
  //     });
  // }

  // _storeData = async () => {
   
  //   try {
            
  //           await AsyncStorage.setItem('bank_allowed', this.state.data1.bank_allowed+"");
  //           await AsyncStorage.setItem('atom_allowed', this.state.data1.atom_allowed+"");
  //           await AsyncStorage.setItem('ledger_balance',this.state.data1.ledger_balance+"");
        
  //   } catch (error) {
  //     // Error saving data
  //    // alert(1)
  //   }
  // }


  fetchDatanote() {
    fetch(Apis.Mf_order, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.2&access_token=${access_token}&user=${clientid}&client_id=${clientid}&amount=${
        this.props.amount
      }&ledger_balance=${parseInt(ledger_balance)}&atom_allowed=${parseInt(
        atom_allowed
      )}&bank_allowed=${parseInt(bank_allowed)}&order_type=lumpsum&is_basket=0` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson["status"] === "success") {
          this.setState({ data: responseJson["data"] });
          this.setState({ isLoading: false, opacity: 1 });
           // alert(this.state.data.note_2)
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  fetchLink() {
    // alert(`api=1.3&access_token=${access_token}&user=${clientid}&client_id=${clientid}&amount=${this.props.amount}&ledger_balance=${parseInt(ledger_balance)}&atom_allowed=${parseInt(atom_allowed)}&bank_allowed=${parseInt(bank_allowed)}&order_type=lumpsum&scheme_code=${this.props.Scheme_Code}&unique_no=${this.props.Unique_No}`)
    this.setState({ isLoading: true, opacity: 0 });
    fetch(Apis.Mf_order, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.3&access_token=${access_token}&user=${clientid}&client_id=${clientid}&amount=${
        parseFloat(this.props.amount).toFixed(2)
      }&ledger_balance=${this.state.data1.ledger_balance}&atom_allowed=${this.state.data1.atom_allowed}&bank_allowed=${this.state.data1.bank_allowed}&order_type=lumpsum&scheme_code=${this.props.Scheme_Code}&unique_no=${
        this.props.Unique_No
      }` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson["status"] === "success") {
          this.setState({
            redirect_payment_link:
              responseJson["data"]["response"].redirect_payment_link,
            order_status: responseJson["data"]["order_status"],
            isLoading: false, opacity: 1
          });
          //alert(responseJson["data"]["response"].redirect_payment_link)
          Actions.LumpsumWebview({url:responseJson["data"]["response"].redirect_payment_link})
         
          //  alert(bank_allowed)
        }
      })
      .catch(error => {
        alert(error);
      });
  }

  redirect() {
    if (parseInt(this.state.data1.atom_allowed) === 0) {
      Actions.Lumpsum3({
        Scheme_Code: this.props.Scheme_Code,
        Unique_No: this.props.Unique_No,
        Scheme_Name: this.props.Scheme_Name,
        amount: this.props.amount
      });
    } else {
      this.fetchLink();
    }
  }


  render() {
    return (
      <View
        style={[styles.containerFlex, { backgroundColor: color.LIGHT_GREY }]}
      >
        <Card containerStyle={{ opacity: this.state.opacity }}>
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <View
              style={{
                borderRadius: 5,
                borderColor: color.GREY,
                borderWidth: 1,
                justifyContent: "center"
              }}
            >
              <Image
                source={{
                  uri: Apis.BaseImageURL + `${this.props.Amc_code}.jpg`
                }}
                style={{
                  padding: 15,
                  height: 50,
                  width: 100,
                  resizeMode: "stretch"
                }}
              />
            </View>
            <Text
              style={[
                styles.welcome,
                { textAlign: "center", marginTop: 15, color: color.toolbar }
              ]}
            >
              {this.props.Scheme_Name}
            </Text>
          </View>
          <View style={[styles.dash, { margin: 7, marginTop: 15 }]} />
          <View style={{ justifyContent: "center", alignItems: "center" }}>
            <Text
              style={{
                borderBottomColor: color.BLACK,
                borderBottomWidth: 1,
                textAlign: "center",
                fontSize: 17,
                color: color.BLACK,
                width: 200,
                marginTop: 15
              }}
            >
              {this.props.amount}
            </Text>
            <Text
              style={{
                marginTop: 5,
                color: color.GREY,
                fontSize: 13,
                textAlign: "center",
                alignItems: "center"
              }}
            >
              Amount(Rs)
            </Text>
          </View>
          <View style={[styles.buttonContainer, { marginTop: 15 }]}>
            <TouchableOpacity
              onPress={() => this.redirect()}
              style={[styles.button, { backgroundColor: color.toolbar }]}
            >
              <Text style={styles.instructions}>
                {parseInt(bank_allowed) === 0
                  ? "CONFIRM AND CONTINUE"
                  : "CONFIRM AND PAY VIA NET BANKING"}
              </Text>
            </TouchableOpacity>
          </View>

          <Text style={{ fontWeight: "500", fontSize: 13, marginTop: 15 }}>
            Note:
            <Text
              style={{ fontWeight: "normal", fontSize: 11, paddingLeft: 10 }}
            >
              {this.state.data.note_1}
            </Text>
          </Text>

          <Text style={{ fontWeight: "normal", fontSize: 11, marginTop: 15 }}>
            {this.state.data.note_2}
          </Text>
        </Card>
        {this.state.isLoading && (
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              flex: 1
            }}
          >
            <Image
              source={require("../../assets/images/loader.gif")}
              style={{ height: 30, width: 30, resizeMode: "contain" }}
            />
          </View>
        )}

       
      </View>
    );
  }
}

export default SipOrder4;
