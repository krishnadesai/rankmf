import React, { Component } from "react";
import {
  Image,
  AsyncStorage,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  FlatList
} from "react-native";
import { Card } from "react-native-elements";
import ModalDatePicker from "react-native-datepicker-modal";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Table from "react-native-simple-table";
import Apis from "../../constants/Apis";
const THEME_COLOR = "rgba(85,186,255, 1)";

var date = new Date();
var day = date ? ("0" + date.getDate()).slice(-2) : "";
var month = date ? ("0" + (date.getMonth() + 1)).slice(-2) : "";
var year = date ? date.getFullYear() : "";

const dateFinal = year + "-" + month + "-" + day;

const columnsScheme = [
  {
    title: "Fund",
    dataIndex: "fund",
    width: 120,
    index: 0
  },
  {
    title: "Scheme Name",
    dataIndex: "scheme",
    width: 190,
    index: 1
  },
  {
    title: "Opening Unit Balance",
    dataIndex: "opbal",
    width: 100,
    index: 2
  },
  {
    title: "Closing Unit Balance",
    dataIndex: "clobal",
    width: 100,
    index: 3
  },
  {
    title: "Nav as on",
    dataIndex: "navason",
    width: 100,
    index: 4
  },
  {
    title: "NAV",
    dataIndex: "nav",
    width: 100,
    index: 5
  },
  {
    title: "Valuation As On",
    dataIndex: "val",
    width: 100,
    index: 6
  },
  {
    title: "Amount",
    dataIndex: "amt",
    width: 100,
    index: 7
  }
];

const columnsSchemeDetail = [
  {
    title: "Type",
    dataIndex: "type",
    width: 100,
    index: 0
  },
  {
    title: "Date",
    dataIndex: "date",
    width: 100,
    index: 1
  },
  {
    title: "Amount",
    dataIndex: "amt",
    width: 100,
    index: 2
  },
  {
    title: "No. of Units",
    dataIndex: "nounits",
    width: 100,
    index: 3
  },
  {
    title: "Price",
    dataIndex: "price",
    width: 100,
    index: 4
  },
  {
    title: "Unit Balance",
    dataIndex: "unibal",
    width: 100,
    index: 5
  }
];

class AccountStatement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading:true,
      opacity:0,
      backdetail: "#D3D3D3",
      backsummary: color.toolbar,
      detailorsummary: "summary",
      summaryhide: false,
      detailhide: true,
      summary: [],
      master_array: [],
      norecords: false
    };
  }

  ChangeTable = () => {
    if (this.state.master_array.length > 0) {
      if (this.state.detailorsummary === "summary") {
        this.setState({ detailorsummary: "detail" });
        this.setState({ detailhide: false });
        this.setState({ summaryhide: true });
        this.setState({ backdetail: color.toolbar });
        this.setState({ backsummary: "#D3D3D3" });
      } else if (this.state.detailorsummary === "detail") {
        this.setState({ detailorsummary: "summary" });
        this.setState({ backsummary: color.toolbar });
        this.setState({ backdetail: "#D3D3D3" });
        this.setState({ detailhide: true });
        this.setState({ summaryhide: false });
      }
    } else {
      if (this.state.detailorsummary === "summary") {
        this.setState({ detailorsummary: "detail" });
        this.setState({ detailhide: true });
        this.setState({ summaryhide: true });
        this.setState({ backdetail: color.toolbar });
        this.setState({ backsummary: "#D3D3D3" });
      } else if (this.state.detailorsummary === "detail") {
        this.setState({ detailorsummary: "summary" });
        this.setState({ backsummary: color.toolbar });
        this.setState({ backdetail: "#D3D3D3" });
        this.setState({ detailhide: true });
        this.setState({ summaryhide: true });
      }
    }
  };

  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    //alert(`api=1.1&client_id=${clientid}&invest_type=portfolio_summary&from_date=${dateFinal}`);
    this.setState({ summary: [], master_array: [] ,opacity:0,isLoading:true});
    fetch(Apis.Minvest, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&invest_type=account_statement&from_date=${dateFinal}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (
          responseJson["data"]["account_statement"]["master_summary"].length > 0
        ) {
          responseJson["data"]["account_statement"]["master_summary"].map(
            item => {
              var data = {
                fund: item.FUND,
                scheme: item.Scheme_Name,
                opbal: item.Opening_Unit_Bal,
                clobal: item.Closing_Unit_Balance,
                navason: item.Nav_As_on,
                nav: item.NAVRS,
                val: item.Valuation_As_on,
                amt: item.Amount
              };

              this.state.summary.push(data);
            }
          );

          //this.setState({chartdata:responseJson["data"]["portfolio_overview"]["graph_data"],data:responseJson["data"]["portfolio_overview"]["response"]["Table2"][0]})

          for (key in responseJson["data"]["account_statement"]["master_array"]) {
            this.state.master_array.push(
              responseJson["data"]["account_statement"]["master_array"][key]
            );
          }
          this.setState({ norecords: false,detailorsummary:"detail"});
        
      
            this.ChangeTable()
         
         
        } else {
          this.setState({
            norecords: true,
            summaryhide: true,
            detailhide: true
          });
        }
        this.setState({opacity:1,isLoading:false})
      })
      
      .catch(error => {
        console.error(error);
      });
  }

  renderDetail(item1, index) {
    var dataSourceDetail = [];
    item1.map(item => {
      var data = {
        type: item.Transaction_Type,
        date: item.Date,
        amt: item.Amount,
        nounits: item.Units,
        price: item.Price,
        unibal: item.Unit_Balance
      };
      dataSourceDetail.push(data);
    });
    return (
      <View style={{ marginTop: 20 }}>
        <Text
          style={{ fontWeight: "bold", color: "black", fontSize: 15, flex: 1 }}
        >
          {item1[0].Scheme_Name}
        </Text>
        <View style={{ marginTop: 10 }}>
          <Table
            height={"100%"}
            columns={columnsSchemeDetail}
            dataSource={dataSourceDetail}
          />
        </View>
      </View>
    );
  }

  render() {
    const DatePicker = ({ style, ...props }) => (
      <ModalDatePicker
        style={[styles.containerFlex, style]}
        renderDate={({ year, month, day, date }) => {
          if (!date) {
            return <Text style={styles.dateinput}>{dateFinal}</Text>;
          }

          dateFinal = `${day}-${month}-${year}`;

          return <Text style={styles.dateinput}>{dateFinal}</Text>;
        }}
        {...props}
      />
    );
    return (
      
        <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.containerFlex}>
          <View>
            <Card title="Filters" titleStyle={{ color: THEME_COLOR }}>
              <View style={{ flexDirection: "row" }}>
                <DatePicker style={{ marginBottom: 15 }} />
                <View style={styles.buttonContainer}>
                  <TouchableOpacity
                    style={[styles.button, { backgroundColor: color.toolbar }]}
                    onPress={() => this.fetchData()}
                  >
                    <Text style={{ color: "white", padding: 8 }}>APPLY</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Card>

            <Card >
              <View style={{ alignItems: "center",opacity:this.state.opacity}}>
                <View
                  style={{
                    flexDirection: "row",
                    borderRadius: 10,
                    backgroundColor: "#D3D3D3",
                    alignItems: "center",
                    marginTop: 10,
                    overflow: "hidden"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 14,
                      padding: 10,
                      color: "#FFFFFF",
                      backgroundColor: this.state.backsummary,
                      borderBottomLeftRadius: 10,
                      borderBottomRightRadius: 0,
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 0
                    }}
                    onPress={this.ChangeTable}
                  >
                    Summary
                  </Text>

                  <Text
                    style={{
                      fontSize: 14,
                      padding: 10,
                      color: "#FFFFFF",
                      backgroundColor: this.state.backdetail,
                      borderBottomLeftRadius: 0,
                      borderBottomRightRadius: 10,
                      borderTopLeftRadius: 0,
                      borderTopRightRadius: 10
                    }}
                    onPress={this.ChangeTable}
                  >
                    Details
                  </Text>
                </View>
              </View>

              {!this.state.summaryhide ? (
                <View style={{ marginTop: 10,opacity:this.state.opacity }}>
                  <Table
                    height={"100%"}
                    columns={columnsScheme}
                    dataSource={this.state.summary}
                  />
                </View>
              ) : null}

              {!this.state.detailhide ? (
                <FlatList
                  style={{ marginTop: 15, marginBottom: 25,opacity:this.state.opacity }}
                  data={this.state.master_array}
                  renderItem={({ item, index }) =>
                    this.renderDetail(item, index)
                  }
                />
              ) : null}
              {this.state.norecords && (
                <Text style={{ fontSize: 14, padding: 10, color: color.RED }}>
                  No Records Found
                </Text>
              )}
               {this.state.isLoading && <View style={{alignItems: 'center', justifyContent: 'center',position:'absolute',top:0,left:0,bottom:0,right:0}}>
            <Image source={require('../../assets/images/loader.gif')} style={{height:30,width:30,resizeMode:"contain"}}/>
            </View>}
            </Card>
           
          </View>
        </ScrollView>
    
    );
  }
}

export default AccountStatement;
