import React, { Component } from "react";
import {
  Image,
  Platform,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import { Card } from "react-native-elements";
import ModalDatePicker from "react-native-datepicker-modal";
import Table from "react-native-simple-table";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";
const THEME_COLOR = "rgba(85,186,255, 1)";

var date = new Date();
var day = date ? ("0" + date.getDate()).slice(-2) : "";
var month = date ? ("0" + (date.getMonth() + 1)).slice(-2) : "";
var year = date ? date.getFullYear() : "";

const currentDate = day + "-" + month + "-" + year;
const dateFinal = year + "-" + month + "-" + day;

const columns = [
  {
    title: "Scheme Name",
    dataIndex: "scheme",
    width: 120,
    index: 0
  },
  {
    title: "Units",
    dataIndex: "units",
    width: 100,
    index: 1
  },
  {
    title: "Purchase Amount",
    dataIndex: "pamt",
    width: 100,
    index: 2
  },
  {
    title: "Sell Amount",
    dataIndex: "samt",
    width: 100,
    index: 3
  },
  {
    title: "G/L ST[Non Equity]",
    dataIndex: "glsne",
    width: 100,
    index: 4
  },
  {
    title: "G/L ST[Equity]",
    dataIndex: "glse",
    width: 100,
    index: 5
  },
  {
    title: "G/L LT[Non Equity]",
    dataIndex: "gllne",
    width: 100,
    index: 5
  },
  {
    title: "G/L LT[Equity]",
    dataIndex: "glle",
    width: 100,
    index: 6
  },
  {
    title: "STT",
    dataIndex: "stt",
    width: 100,
    index: 7
  },
  {
    title: "Absolute Return",
    dataIndex: "abreturn",
    width: 100,
    index: 8
  }
];

class GainLossReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading:true,
      opacity:0,
      datasource: [],
      norecords: false
    };
  }
  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
     
    this.setState({ datasource: [],isLoading:true,opacity:0 });
    fetch(Apis.Minvest, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&invest_type=profit_loss_statement&from_date=${dateFinal}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {

        const datasource = this.state.datasource;
        responseJson["data"]["profit_loss_statement"]["response"]["Table"].map(
          (item, i) => {
            if (!item.DataValues) {
              var data = {
                scheme: item.Scheme_Name,
                units: item.Units,
                pamt: item.Purchase_Amount,
                samt: item.Sell_Amount,
                glsne: item.GL_ST_NonEquity ? item.GL_ST_NonEquity : 0,
                glse: item.GL_ST_Equity ? item.GL_ST_Equity : 0,
                gllne: item.GL_LT_NonEquity ? item.GL_LT_NonEquity : 0,
                glle: item.GL_LT_Equity ? item.GL_LT_Equity : 0,
                stt: item.STT,
                abreturn: item.AbsReturn
              };

              datasource.push(data);
              
            } else {
              this.setState({ norecords: true });
            }
          }
        );

        for (key in responseJson["data"]["profit_loss_statement"]["response"]) {
          if (key === "Table1") {
            responseJson["data"]["profit_loss_statement"]["response"][
              "Table1"
            ].map((item, i) => {
              var data = {
                scheme: item.Scheme_Name ? item.Scheme_Name : "Total",
                units: item.Units,
                pamt: item.Purchase_Amount,
                samt: item.Sell_Amount,
                glsne: item.GL_ST_NonEquity ? item.GL_ST_NonEquity : 0,
                glse: item.GL_ST_Equity ? item.GL_ST_Equity : 0,
                gllne: item.GL_LT_NonEquity ? item.GL_LT_NonEquity : 0,
                glle: item.GL_LT_Equity ? item.GL_LT_Equity : 0,
                stt: item.STT,
                abreturn: item.AbsReturn
              };

              datasource.push(data);
             
            });
            this.setState({ norecords: false });
          }
        }

        this.setState({datasource,opacity:1,isLoading:false})
        // alert(this.state.datasource)
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    const DatePicker = ({ style, ...props }) => (
      <ModalDatePicker
        style={[styles.container, style]}
        renderDate={({ year, month, day, date }) => {
          if (!date) {
            return (
              <Text style={[styles.dateinput, { marginTop: 5 }]}>
                {dateFinal}
              </Text>
            );
          }

          dateFinal = `${year}-${month}-${day}`;

          return (
            <Text style={[styles.dateinput, { marginTop: 5 }]}>
              {dateFinal}
            </Text>
          );
        }}
        {...props}
      />
    );

    return (
      
        <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.containerFlex}>
          <View>
            <Card title="Filters" titleStyle={{ color: color.toolbar }}>
              <View style={{ flexDirection: "row" }}>
                <DatePicker style={{ marginBottom: 15, flex: 1 }} />
                <View
                  style={[
                    styles.buttonContainer,
                    { flex: 1, alignItems: "flex-end" }
                  ]}
                >
                  <TouchableOpacity
                    style={[styles.button, { backgroundColor: color.toolbar }]}
                    onPress={() => this.fetchData()}
                  >
                    <Text style={{ color: "white", padding: 8 }}>APPLY</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Card>

            <Card containerStyle={{opacity:this.state.opacity}}>
              <Table
                height={"100%"}
                columns={columns}
                dataSource={this.state.datasource}
              />
              {this.state.norecords && (
                <Text style={{ fontSize: 14, padding: 10, color: color.RED }}>
                  No Records Found
                </Text>
              )}
            </Card>
            {this.state.isLoading && <View style={{alignItems: 'center', justifyContent: 'center',position:'absolute',top:0,left:0,bottom:0,right:0}}>
            <Image source={require('../../assets/images/loader.gif')} style={{height:30,width:30,resizeMode:"contain"}}/>
            </View>}
          </View>
        </ScrollView>
    
    );
  }
}

export default GainLossReport;
