import React, { Component } from "react";
import {
  Image,
  Platform,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import { Card } from "react-native-elements";
import ModalDatePicker from "react-native-datepicker-modal";
import Table from "react-native-simple-table";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";
const THEME_COLOR = "rgba(85,186,255, 1)";

var date = new Date();
var day = date ? ("0" + date.getDate()).slice(-2) : "";
var month = date ? ("0" + (date.getMonth() + 1)).slice(-2) : "";
var year = date ? date.getFullYear() : "";
const dateFinal = year + "-" + month + "-" + day;

const columns = [
  {
    title: "Scheme Name",
    dataIndex: "scheme",
    width: 180,
    index: 0
  },
  {
    title: "SIP Amount",
    dataIndex: "samt",
    width: 100,
    index: 1
  },
  {
    title: "Frequency",
    dataIndex: "Frequency",
    width: 100,
    index: 2
  },
  {
    title: "Start Date",
    dataIndex: "sdate",
    width: 100,
    index: 3
  },
  {
    title: "End Date",
    dataIndex: "edate",
    width: 100,
    index: 4
  },
  {
    title: "Invested Amount",
    dataIndex: "iamt",
    width: 100,
    index: 5
  },
  {
    title: "Dividend Amount",
    dataIndex: "damt",
    width: 100,
    index: 6
  },
  {
    title: "Total Units",
    dataIndex: "units",
    width: 100,
    index: 7
  },
  {
    title: "Avg Purchase Price",
    dataIndex: "avgprice",
    width: 100,
    index: 8
  },
  {
    title: "Current NAV",
    dataIndex: "curnav",
    width: 100,
    index: 9
  },
  {
    title: "Present Value",
    dataIndex: "presentval",
    width: 100,
    index: 10
  },
  {
    title: "Net Gain/Loss",
    dataIndex: "net",
    width: 100,
    index: 11
  },
  {
    title: "XIRR(%)",
    dataIndex: "xirr",
    width: 100,
    index: 12
  }
];

class SipHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datasource: [],
      norecords: false
    };
  }

  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    // alert(this.props.unique_code);
    this.setState({ datasource: [],isLoading:true,opacity:0 });
    fetch(Apis.Minvest, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&invest_type=sip_summary&from_date=${dateFinal}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        const datasource = this.state.datasource;
        responseJson["data"]["sip_summary"]["response"]["Table1"].map(
          (item, i) => {
            if (!item.DataValues) {
              var data = {
                scheme: item.Scheme_Name,
                samt: item.SIP_Amount,
                Frequency: item.Freq,
                sdate: item.Start_Date,
                edate: item.End_Date,
                iamt: item.Amount_Invested,
                damt: item.Dividend_Amount,
                units: item.Total_Units,
                avgprice: item.Avg_Purchase_Price,
                curnav: item.Current_NAV,
                presentval: item.Market_Value,
                net: item.Net_Gain,
                xirr: item.XIRR
              };

              datasource.push(data);
            } else {
              this.setState({ norecords: true });
            }
          }
        );

        for (key in responseJson["data"]["sip_summary"]["response"]) {
          if (key === "Table2") {
            responseJson["data"]["sip_summary"]["response"]["Table2"].map(
              (item, i) => {
                var data = {
                  scheme: item.Scheme_Name ? item.Scheme_Name : "Total",
                  samt: item.SIP_Amount,
                  Frequency: item.Freq,
                  sdate: item.Start_Date,
                  edate: item.End_Date,
                  iamt: item.Amount_Invested,
                  damt: item.Dividend_Amount,
                  units: item.Total_Units,
                  avgprice: item.Avg_Purchase_Price,
                  curnav: item.Current_NAV,
                  presentval: item.Market_Value,
                  net: item.Net_Gain,
                  xirr: item.XIRR
                };

                datasource.push(data);
              }
            );
            this.setState({ norecords: false });
          }
          
        }
        this.setState({datasource,opacity:1,isLoading:false})
        // alert(this.state.datasource)
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    const DatePicker = ({ style, ...props }) => (
      <ModalDatePicker
        style={[styles.container, style]}
        renderDate={({ year, month, day, date }) => {
          if (!date) {
            return <Text style={styles.dateinput}>{dateFinal}</Text>;
          }

          dateFinal = `${year}-${month}-${day}`;

          return <Text style={styles.dateinput}>{dateFinal}</Text>;
        }}
        {...props}
      />
    );

    return (
     
        <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.containerFlex}>
          <View>
            <Card title="Filters" titleStyle={{ color: THEME_COLOR }}>
              <View style={{ flexDirection: "row" }}>
                <DatePicker style={{ marginBottom: 15, flex: 1 }} />
                <View
                  style={[
                    styles.buttonContainer,
                    { flex: 1, alignItems: "flex-end" }
                  ]}
                >
                  <TouchableOpacity
                    style={[styles.button, { backgroundColor: color.toolbar }]}
                    onPress={() => this.fetchData()}
                  >
                    <Text style={{ color: "white", padding: 8 }}>APPLY</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Card>

            <Card containerStyle={{opacity:this.state.opacity}}>
              <Table
                height={"100%"}
                columns={columns}
                dataSource={this.state.datasource}
              />
            </Card>
            {this.state.isLoading && <View style={{alignItems: 'center', justifyContent: 'center',position:'absolute',top:0,left:0,bottom:0,right:0}}>
            <Image source={require('../../assets/images/loader.gif')} style={{height:30,width:30,resizeMode:"contain"}}/>
            </View>}
          </View>
        </ScrollView>
     
    );
  }
}

export default SipHistory;
