import React, { Component } from "react";
import {
  Dimensions,
  StyleSheet,
  Platform,
  Text,
  View,
  ScrollView,
  AsyncStorage
} from "react-native";
import { Card } from "react-native-elements";
import ChartView from "react-native-highcharts";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";
const clientid = "";
const THEME_COLOR = "rgba(85,186,255, 1)";

class PortfolioSnapshot extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chartdata: [],
      data: {}
    };
  }

  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    // alert(`api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&invest_type=portfolio_overview`);
    fetch(Apis.Minvest, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&invest_type=portfolio_overview` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        //alert(responseJson["portfolio_overview"])
        this.setState({
          chartdata: responseJson["data"]["portfolio_overview"]["graph_data"],
          data:responseJson["data"]["portfolio_overview"]["response"]["Table2"][0]
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    var Highcharts = "Highcharts";
    const options = {
      global: {
        useUTC: false
      },
      lang: {
        decimalPoint: ".",
        thousandsSep: ","
      }
    };

    var conf = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie"
      },
      credits: {
        enabled: false
      },
      title: {
        text: ""
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>"
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: true,
            format: "<b>{point.name}</b>: {point.percentage:.1f} %",
            style: {
              color:
                (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
                "black"
            }
          }
        }
      },
      exporting: {
        enabled: false
      },
      series: [
        {
          name: "Portfolio Allocation",
          colorByPoint: true,
          data: this.state.chartdata
        }
      ]
    };

    return (
    
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.containerFlex}
        >
          <View>
            <Card title="Overall Total" titleStyle={{ color: THEME_COLOR }}>
              <View style={{ flexDirection: "row" }}>
                <Text style={{ flex: 1 }}>Amount Invested</Text>
                <Text style={{ flex: 1, textAlign: "right" }}>
                  {"\u20B9"}
                  {this.state.data.Total_Amount}
                </Text>
              </View>
              <View style={[styles.dash, { marginTop: 15 }]} />
              <View style={{ flexDirection: "row", marginTop: 15 }}>
                <Text style={{ flex: 1 }}>Current Value</Text>
                <Text style={{ flex: 1, textAlign: "right" }}>
                  {"\u20B9"}
                  {this.state.data.Current_Invest_Amount}
                </Text>
              </View>
            </Card>

            <Card title="Total Gain" titleStyle={{ color: THEME_COLOR }}>
              <View style={{ flexDirection: "row" }}>
                <Text style={{ flex: 1 }}>Realised Gain</Text>
                <Text style={{ flex: 1, textAlign: "right" }}>
                  {"\u20B9"}
                  {this.state.data.Realized_Profit_on_Red}
                </Text>
              </View>
              <View style={[styles.dash, { marginTop: 15 }]} />
              <View style={{ flexDirection: "row", marginTop: 15 }}>
                <Text style={{ flex: 1 }}>Unrealised Gain</Text>
                <Text style={{ flex: 1, textAlign: "right" }}>
                  {"\u20B9"}
                  {this.state.data.UnRealized_Profit}
                </Text>
              </View>
            </Card>

            <Card title="Dividends" titleStyle={{ color: THEME_COLOR }}>
              <View style={{ flexDirection: "row" }}>
                <Text style={{ flex: 1 }}>Dividend Re-Invested</Text>
                <Text style={{ flex: 1, textAlign: "right" }}>
                  {"\u20B9"}
                  {this.state.data.Dividend_Reinvested}
                </Text>
              </View>
              <View style={[styles.dash, { marginTop: 15 }]} />
              <View style={{ flexDirection: "row", marginTop: 15 }}>
                <Text style={{ flex: 1 }}>Dividend Paid Out</Text>
                <Text style={{ flex: 1, textAlign: "right" }}>
                  {"\u20B9"}
                  {this.state.data.Dividend_Paid_out}
                </Text>
              </View>
            </Card>

            <Card title="Returns" titleStyle={{ color: THEME_COLOR }}>
              <View style={{ flexDirection: "row" }}>
                <Text style={{ flex: 1 }}>Absolute Return</Text>
                <Text style={{ flex: 1, textAlign: "right" }}>
                  {this.state.data.Abs_Returns}%
                </Text>
              </View>
              <View style={[styles.dash, { marginTop: 15 }]} />
              <View style={{ flexDirection: "row", marginTop: 15 }}>
                <Text style={{ flex: 1 }}>Annualized Return</Text>
                <Text style={{ flex: 1, textAlign: "right" }}>
                  {this.state.data.XIRR_Ann_Returns}%
                </Text>
              </View>
            </Card>

            <Card
              title="Portfolio Allocation"
              titleStyle={{ color: THEME_COLOR }}
            >
              <ChartView
                style={{ height: 300 }}
                config={conf}
                options={options}
              />
            </Card>
          </View>
        </ScrollView>
     
    );
  }
}

export default PortfolioSnapshot;
