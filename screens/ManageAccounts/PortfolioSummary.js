import React from "react";
import {
  Image,
  Platform,
  Dimensions,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Linking,
  AsyncStorage,
  FlatList
} from "react-native";
import { Card } from "react-native-elements";
import ModalDatePicker from "react-native-datepicker-modal";
import styles from "../../components/styles";
import Table from "react-native-simple-table";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";

let scrollYPos = 0;
const THEME_COLOR = "rgba(85,186,255, 1)";

var date = new Date();
var day = date ? ("0" + date.getDate()).slice(-2) : "";
var month = date ? ("0" + (date.getMonth() + 1)).slice(-2) : "";
var year = date ? date.getFullYear() : "";

const currentDate = year + "-" + month + "-" + day;
const dateFinal = currentDate;

const columnsSchemeDetail = [
  {
    title: "Type",
    dataIndex: "type",
    width: 100,
    index: 0
  },
  {
    title: "Date",
    dataIndex: "date",
    width: 100,
    index: 1
  },
  {
    title: "Price",
    dataIndex: "price",
    width: 100,
    index: 2
  },
  {
    title: "No. of Units",
    dataIndex: "nounits",
    width: 100,
    index: 3
  },
  {
    title: "Investment Amount",
    dataIndex: "iamt",
    width: 100,
    index: 4
  },
  {
    title: "Present Value",
    dataIndex: "preval",
    width: 100,
    index: 5
  },
  {
    title: "Profit/Loss",
    dataIndex: "proloss",
    width: 100,
    index: 6
  },
  {
    title: "Dividend Payout",
    dataIndex: "divpay",
    width: 100,
    index: 7
  },
  {
    title: "Absolute Return",
    dataIndex: "abret",
    width: 100,
    index: 8
  },
  {
    title: "XIRR(%)",
    dataIndex: "xirr",
    width: 100,
    index: 9
  }
];

class PortfolioSummary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading:true,
      opacity:0,
      backdetail: "#D3D3D3",
      backsummary: color.toolbar,
      detailorsummary: "summary",
      summaryhide: false,
      detailhide: true,
      summary: [],
      screenHeight: Dimensions.get("window").height,
      screenWidth: Dimensions.get("window").width,
      master_array: [],
      norecords: false
    };
  }

  ChangeTable = () => {
    if (this.state.detailorsummary === "summary") {
      this.setState({ detailorsummary: "detail" });
      this.setState({ detailhide: false });
      this.setState({ summaryhide: true });
      this.setState({ backdetail: color.toolbar });
      this.setState({ backsummary: "#D3D3D3" });
    } else if (this.state.detailorsummary === "detail") {
      this.setState({ detailorsummary: "summary" });
      this.setState({ backsummary: color.toolbar });
      this.setState({ backdetail: "#D3D3D3" });
      this.setState({ detailhide: true });
      this.setState({ summaryhide: false });
    }
  };

  handleClick = () => {
    Linking.canOpenURL(
      "http://mf.samco.in/mf_client_reports/exportPortfolioSummary/2018-08-09"
    ).then(supported => {
      if (supported) {
        Linking.openURL(
          "http://mf.samco.in/mf_client_reports/exportPortfolioSummary/2018-08-09"
        );
      } else {
        //console.log("Don't know how to open URI: " + 'http://mf.samco.in/mf_client_reports/exportPortfolioSummary/2018-08-09');
      }
    });
  };

  handleScroll() {
    //console.warn(event.nativeEvent.contentOffset.y);
  }
  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    this.setState({opacity:0,isLoading:true})
   //alert(`api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&invest_type=portfolio_summary&from_date=${dateFinal}`);
    this.setState({ summary: [], master_array: [] });
    fetch(Apis.Minvest, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&invest_type=portfolio_summary&from_date=${dateFinal}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        for (key in responseJson["data"]["portfolio_summary"]["response"]) {
          this.setState({ summary: [], master_array: [] });
          if (key != "Table") {
            responseJson["data"]["portfolio_summary"]["response"]["Table1"].map(
              item => {
                var data = {
                  scheme: item.Scheme_Name,
                  iamt: item.Current_Invest_Amount,
                  punit: item.Present_Units,
                  preval: item.Present_Value,
                  regl: item.Realized_Profit_on_Red,
                  unregl: item.Notional_GL,
                  xirr: item.XIRR_Ann_Returns,
                  abret: item.Abs_Returns,
                  redeem: (
                    <View style={styles.buttonContainer}>
                      <TouchableOpacity
                        onPress={() => alert(item.Folio_No)}
                        style={[styles.button, { backgroundColor: "green" }]}
                      >
                        <Text style={[styles.redeemtext, { fontSize: 10 }]}>
                          REDEEM
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )
                };

                this.state.summary.push(data);
              }              
            );

            this.setState({detailorsummary:"detail",detailhide:true})
            this.ChangeTable()
          } else {
            this.setState({ norecords: true, summaryhide: true });
          }
        }

        //this.setState({chartdata:responseJson["data"]["portfolio_overview"]["graph_data"],data:responseJson["data"]["portfolio_overview"]["response"]["Table2"][0]})

        for (key in responseJson["data"]["portfolio_summary"]["master_array"]) {
          this.state.master_array.push(
            responseJson["data"]["portfolio_summary"]["master_array"][key]
          );
        }
        this.setState({ summaryhide: false,isLoading:false,opacity:1 });
      })
      .catch(error => {
        console.error(error);
      });
  }

  componentWillUpdate() {
    //alert(11);
    this.scroller.scrollTo({ x: 0, y: 0 });
  }

  renderDetail(item1, index) {
    var dataSourceDetail = [];
    item1.map(item => {
      var data = {
        type: item.Transaction_Type ? item.Transaction_Type : "Total",
        date: item.Transaction_Date ? item.Transaction_Date : "--",
        price: item.Transaction_Price ? item.Transaction_Price : "--",
        nounits: item.No_of_Units,
        iamt: item.Purchase_Value,
        preval: item.Current_Amount,
        proloss: item.Profit_Loss,
        divpay: item.Dividend_Payout,
        abret: item.Absolute_Return,
        xirr: item.XIRR_CAGR
      };
      dataSourceDetail.push(data);
    });
    return (
      <View style={{ marginTop: 20 }}>
        <Text
          style={{ fontWeight: "bold", color: "black", fontSize: 15, flex: 1 }}
        >
          {item1[0].Scheme_Name}
        </Text>
        <View style={{ marginTop: 10 }}>
          <Table
            height={"100%"}
            columns={columnsSchemeDetail}
            dataSource={dataSourceDetail}
          />
        </View>
      </View>
    );
  }

  render() {
    const columnsScheme = [
      {
        title: "Scheme Name",
        dataIndex: "scheme",
        width: 190,
        index: 0
      },
      {
        title: "Invested Amount",
        dataIndex: "iamt",
        width: 100,
        index: 1
      },
      {
        title: "Present Units",
        dataIndex: "punit",
        width: 100,
        index: 2
      },
      {
        title: "Present Value",
        dataIndex: "preval",
        width: 100,
        index: 3
      },
      {
        title: "Realised Gain/Loss",
        dataIndex: "regl",
        width: 100,
        index: 4
      },
      {
        title: "Unrealised Gain/Loss",
        dataIndex: "unregl",
        width: 100,
        index: 5
      },
      {
        title: "XIRR(%)",
        dataIndex: "xirr",
        width: 100,
        index: 6
      },
      {
        title: "Absolute Return",
        dataIndex: "abret",
        width: 100,
        index: 7
      },
      {
        title: "Redeem",
        dataIndex: "redeem",
        width: 100,
        index: 8
      }
    ];

    const DatePicker = ({ style, ...props }) => (
      <ModalDatePicker
        style={[styles.container, style]}
        renderDate={({ year, month, day, date }) => {
          if (!date) {
            return <Text style={styles.dateinput}>{dateFinal}</Text>;
          }

          dateFinal = `${year}-${month}-${day}`;

          return <Text style={styles.dateinput}>{dateFinal}</Text>;
        }}
        {...props}
      />
    );
    return (
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        ref={scroller => {
          this.scroller = scroller;
        }}
        scrollEventThrottle={16}
        onScroll={this.handleScroll}
      >
        <View style={styles.containerFlex}>
          <View>
            <Card title="Filters" titleStyle={{ color: color.toolbar }}>
              <View style={{ flexDirection: "row" }}>
                <DatePicker style={{ marginBottom: 15, flex: 1 }} />
                <View
                  style={[
                    styles.buttonContainer,
                    { flex: 1, alignItems: "flex-end" }
                  ]}
                >
                  <TouchableOpacity
                    onPress={() => this.fetchData()}
                    style={[styles.button, { backgroundColor: color.toolbar }]}
                  >
                    <Text style={{ color: "white", padding: 8 }}>APPLY</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Card>

            <Card>
              <View style={{ alignItems: "center" ,opacity:this.state.opacity}}>
                <View
                  style={{
                    flexDirection: "row",
                    borderRadius: 10,
                    backgroundColor: "#D3D3D3",
                    alignItems: "center",
                    marginTop: 10,
                    overflow: "hidden"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 14,
                      padding: 10,
                      color: "#FFFFFF",
                      backgroundColor: this.state.backsummary,
                      borderBottomLeftRadius: 10,
                      borderBottomRightRadius: 0,
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 0
                    }}
                    onPress={this.ChangeTable}
                  >
                    Summary
                  </Text>

                  <Text
                    style={{
                      fontSize: 14,
                      padding: 10,
                      color: "#FFFFFF",
                      backgroundColor: this.state.backdetail,
                      borderBottomLeftRadius: 0,
                      borderBottomRightRadius: 10,
                      borderTopLeftRadius: 0,
                      borderTopRightRadius: 10
                    }}
                    onPress={this.ChangeTable}
                  >
                    Details
                  </Text>
                </View>
              </View>

              {!this.state.summaryhide ? (
                <View style={{ marginTop: 10 }}>
                  <Table
                    height={"100%"}
                    columns={columnsScheme}
                    dataSource={this.state.summary}
                  />
                  {/* <View style={{marginTop:15}}>
                   <Table height={'100%'}  columns={columnsRedemption} dataSource={dataSourceRedemption} /> 
                   </View> */}
                </View>
              ) : null}

              {!this.state.detailhide ? (
                <FlatList
                  style={{ marginTop: 15, marginBottom: 25 }}
                  data={this.state.master_array}
                  renderItem={({ item, index }) =>
                    this.renderDetail(item, index)
                  }
                />
              ) : null}

              {this.state.norecords && (
                <Text style={{ fontSize: 14, padding: 10, color: color.RED }}>
                  No Records Found
                </Text>
              )}
               {this.state.isLoading && <View style={{alignItems: 'center', justifyContent: 'center',position:'absolute',top:0,left:0,bottom:0,right:0}}>
            <Image source={require('../../assets/images/loader.gif')} style={{height:30,width:30,resizeMode:"contain"}}/>
            </View>}
            </Card>
           
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default PortfolioSummary;
