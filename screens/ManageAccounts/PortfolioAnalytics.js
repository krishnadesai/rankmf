import React, { Component } from "react";
import {
  StyleSheet,
  Platform,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import styles from "../../components/styles";
import { Card } from "react-native-elements";
import Table from "react-native-simple-table";
import SimpleTable from "../../components/SimpleTable";
import ChartView from "react-native-highcharts";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";

const THEME_COLOR = "rgba(85,186,255, 1)";

const columns = [
  {
    title: "Sector Holding",
    dataIndex: "holding",
    width: 150,
    index: 0
  },
  {
    title: "% of Net Assets",
    dataIndex: "assets",
    width: 150,
    index: 1
  }
];

const columns1 = [
  {
    title: "Asset",
    dataIndex: "Asset",
    width: 100,
    index: 0
  },
  {
    title: "AUM (Rs)",
    dataIndex: "aum",
    width: 100,
    index: 1
  },
  {
    title: "(%)",
    dataIndex: "per",
    width: 100,
    index: 2
  }
];

const columnsmarketcap = [
  {
    title: "Type",
    dataIndex: "type",
    width: 100,
    index: 0
  },
  {
    title: "Total",
    dataIndex: "total",
    width: 100,
    index: 1
  },
  {
    title: "(%)",
    dataIndex: "per",
    width: 100,
    index: 2
  }
];

const columnsfund = [
  {
    title: "Fund",
    dataIndex: "fund",
    width: 120,
    index: 0
  },
  {
    title: "Cost",
    dataIndex: "cost",
    width: 100,
    index: 1
  },
  {
    title: "Net Gain",
    dataIndex: "net",
    width: 80,
    index: 2
  },
  {
    title: "XIRR",
    dataIndex: "xirr",
    width: 80,
    index: 3
  },
  {
    title: "Holding",
    dataIndex: "holding",
    width: 100,
    index: 4
  }
];

class PortfolioSummary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sector: [],
      compall: [],
      assetpiedata: [],
      assetallocation: [],
      marketcapallocation: [],
      marketcappiedata: [],
      fundallocation: [],
      fundpiedata: []
    };
  }

  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    // alert(this.props.unique_code);
    fetch(Apis.Minvest, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&invest_type=portfolio_analytics` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        var assett = 0,
          cap = 0,
          cost = 0,
          net = 0,
          xirr = 0,
          hol = 0;
        responseJson["data"]["portfolio_analytics"]["sectorAllocation"][
          "Table"
        ].map(item => {
          var data = { holding: item.Sector, assets: item.Percentage };
          this.state.sector.push(data);
        });
        var data = { holding: "Total", assets: 100 };
        this.state.sector.push(data);

        responseJson["data"]["portfolio_analytics"]["compAllocation"][
          "Table"
        ].map(item => {
          var data = { holding: item.Company, assets: item.Percentage };
          this.state.compall.push(data);
        });
        var data = { holding: "Total", assets: 100 };
        this.state.compall.push(data);

        responseJson["data"]["portfolio_analytics"]["assetAllocation"][
          "Table"
        ].map(item => {
          var data = {
            name: item.Asset_type,
            y: item.AUM_Perc,
            selected: true
          };
          var d = { Asset: item.Asset_type, aum: item.Aum, per: item.AUM_Perc };
          this.state.assetpiedata.push(data);
          this.state.assetallocation.push(d);
          assett += item.Aum;
        });
        var d = { Asset: "Total", aum: assett, per: 100 };
        this.state.assetallocation.push(d);

        responseJson["data"]["portfolio_analytics"]["capAllocation"][
          "Table"
        ].map(item => {
          var data = {
            name: item.CType,
            y: item.Holding,
            selected: true
          };
          var d = {
            type: item.CType,
            total: item.InvAmtTot,
            per: item.Holding
          };
          this.state.marketcappiedata.push(data);
          this.state.marketcapallocation.push(d);
          cap += item.InvAmtTot;
        });
        var d = { type: "Total", total: cap, per: 100 };
        this.state.marketcapallocation.push(d);

        responseJson["data"]["portfolio_analytics"]["fundPortFolioBreakUp"][
          "Table"
        ].map(item => {
          var data = {
            name: item.FUND,
            y: parseFloat(item.Holding),
            selected: true
          };
          var d = {
            fund: item.FUND,
            cost: item.Cost,
            net: item.NetGain,
            xirr: item.XIRR,
            holding: item.Holding
          };
          this.state.fundpiedata.push(data);
          this.state.fundallocation.push(d);
          cost += parseFloat(item.Cost);
          net += parseFloat(item.NetGain);
          xirr += parseFloat(item.XIRR);
          hol += parseFloat(item.Holding);
        });
        var d = {
          fund: "Total",
          cost: parseFloat(cost).toFixed(2),
          net: parseFloat(net).toFixed(2),
          xirr: parseFloat(xirr).toFixed(2),
          holding: parseFloat(hol).toFixed(0)
        };
        this.state.fundallocation.push(d);
        //  alert(this.state.fundpiedata[0].name)
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    var Highcharts = "Highcharts";

    var assetpie = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie"
      },
      credits: {
        enabled: false
      },
      title: {
        text: ""
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>"
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: true,
            format: "<b>{point.name}</b>: {point.percentage:.1f} %",
            style: {
              color:
                (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
                "black"
            }
          }
        }
      },
      exporting: {
        enabled: false
      },
      series: [
        {
          name: "Portfolio Analytics",
          colorByPoint: true,
          data: this.state.assetpiedata
        }
      ]
    };

    var marketcappie = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie"
      },
      credits: {
        enabled: false
      },
      title: {
        text: ""
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>"
      },
      exporting: {
        enabled: false
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: true,
            format: "<b>{point.name}</b>: {point.percentage:.1f} %",
            style: {
              color:
                (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
                "black"
            }
          }
        }
      },
      series: [
        {
          name: "Portfolio Analytics",
          colorByPoint: true,
          data: this.state.marketcappiedata
        }
      ]
    };

    var fundpie = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie"
      },
      title: {
        text: ""
      },
      credits: {
        enabled: false
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>"
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: true,
            format: "<b>{point.name}</b>: {point.percentage:.1f} %",
            style: {
              color:
                (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
                "black"
            }
          }
        }
      },
      exporting: {
        enabled: false
      },
      series: [
        {
          name: "Portfolio Analytics",
          colorByPoint: true,
          data: this.state.fundpiedata
        }
      ]
    };

    const options = {
      global: {
        useUTC: false
      },
      lang: {
        decimalPoint: ".",
        thousandsSep: ","
      }
    };

    return (
      
        <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.containerFlex}>
          <View>
            <Card
              title="Top 10 Sector-wise Holdings"
              titleStyle={{ color: THEME_COLOR }}
            >
              <View style={{ alignItems: "center" }}>
                <SimpleTable
                  height={"100%"}
                  columns={columns}
                  dataSource={this.state.sector}
                />
              </View>
            </Card>
            <Card
              title="Top 10 Company-wise Holdings"
              titleStyle={{ color: THEME_COLOR }}
            >
              <View style={{ alignItems: "center" }}>
                <SimpleTable
                  height={"100%"}
                  columns={columns}
                  dataSource={this.state.compall}
                />
              </View>
            </Card>
            <Card title="Asset Allocation" titleStyle={{ color: THEME_COLOR }}>
              <View style={{ alignItems: "center" }}>
                <SimpleTable
                  height={"100%"}
                  columns={columns1}
                  dataSource={this.state.assetallocation}
                />
              </View>
              <ChartView
                style={{ height: 300 }}
                config={assetpie}
                options={options}
              />
            </Card>

            <Card
              title="Market Cap Allocation of Equity Exposure"
              titleStyle={{ color: THEME_COLOR }}
            >
              <View style={{ alignItems: "center" }}>
                <SimpleTable
                  height={"100%"}
                  columns={columnsmarketcap}
                  dataSource={this.state.marketcapallocation}
                />
              </View>
              <ChartView
                style={{ height: 300 }}
                config={marketcappie}
                options={options}
              />
            </Card>

            <Card title="Fund House" titleStyle={{ color: THEME_COLOR }}>
              <View style={{ alignItems: "center" }}>
                <Table
                  height={"100%"}
                  columns={columnsfund}
                  dataSource={this.state.fundallocation}
                />
              </View>
              <ChartView
                style={{ height: 300 }}
                config={fundpie}
                options={options}
              />
            </Card>
          </View>
        </ScrollView>
      
    );
  }
}

export default PortfolioSummary;
