import React, { Component, Props } from "react";
import {
  AsyncStorage,
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ScrollView,
  FlatList,
  TouchableOpacity,
  Image,
  Button
} from "react-native";
import { Card } from "react-native-elements";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";
import { DocumentPicker, ImagePicker } from "expo";
import { Actions } from "react-native-router-flux";
//import {DocumentPicker,DocumentPickerUtil} from 'react-native-document-picker';
//import RNFetchBlob from 'rn-fetch-blob';
//import uuid from 'uuid';
//import * as firebase from 'firebase';

const THEME_COLOR = "rgba(85,186,255, 1)";

console.disableYellowBox = true;

// const url =
//   'https://firebasestorage.googleapis.com/v0/b/blobtest-36ff6.appspot.com/o/Obsidian.jar?alt=media&token=93154b97-8bd9-46e3-a51f-67be47a4628a';

// const firebaseConfig = {
//   apiKey: 'AIzaSyAlZruO2T_JNOWn4ysfX6AryR6Dzm_VVaA',
//   authDomain: 'blobtest-36ff6.firebaseapp.com',
//   databaseURL: 'https://blobtest-36ff6.firebaseio.com',
//   storageBucket: 'blobtest-36ff6.appspot.com',
//   messagingSenderId: '506017999540',
// };

// firebase.initializeApp(firebaseConfig);

// async function uploadImageAsync(uri) {
//   const response = await fetch(uri);
//   const blob = await response.blob();
//   const ref = firebase
//     .storage()
//     .ref()
//     .child(uuid.v4());

//   const snapshot = await ref.put(blob);
//   return snapshot.downloadURL;
// }

_pickDocument = async () => {
  let result = await DocumentPicker.getDocumentAsync({});

  //console.log(result);
  if (!result.cancelled) {
    this.setState({ image: result.uri });
    // uploadResponse = await uploadImageAsync(result.uri);
    // uploadResult = await uploadResponse.json();
  }
};

class MandateRegistration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      isLoading: true,
      opacity:0,
      file: undefined,
      mandates: [],
      image: null,
      mandates_rejected: [],
      red:[],
      mandate_amt:"10000"
    };
  }

  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    //  alert(`api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&invest_type=mandate_registration`);
    fetch(Apis.Minvest, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&invest_type=mandate_registration` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        //alert(responseJson["portfolio_overview"])
        //  this.setState({
        //   mandates:responseJson["data"]["mandate_registration"]["mandates"]
        //  });
        const mandates_rejected = this.state.mandates_rejected;
        responseJson["data"]["mandate_registration"][
          "mandates_rejected"
        ].forEach(element => {
          mandates_rejected[element.mandate_id] = {
            reason: element.reason,
            remark: element.remark
          };
        });

        this.setState({ mandates_rejected });
        var d = [];
        const red= this.state.red;
        responseJson["data"]["mandate_registration"]["mandates"].forEach(
          element => {
            var data = {},
              mandate_status = "",
              samco_status = "",
              reason_display = "";
            responseJson["data"]["mandate_registration"]["mandates_generated"].forEach(item => {
              if (element.mandate_id === item.mandate_id) {
                
                if (item.status === "1") {
                  samco_status = "Pending";
                  red[parseInt(element.mandate_id)] = color.BLACK
                } else if (item.status === "2") {
                  if (
                    this.state.mandates_rejected[item.mandate_id].reason ==
                    "Other"
                  ) {
                    reason_display = this.state.mandates_rejected[item.mandate_id].remark;
                  } else {
                    reason_display = this.state.mandates_rejected[item.mandate_id].reason;
                  }
                  samco_status = `Rejected [${reason_display}] Please re-generate.`;
                  red[parseInt(element.mandate_id)] = color.RED
                  //show_generate =1;
                } else if (item.status === "3") {
                  samco_status = element.mandate_status;
                  red[parseInt(element.mandate_id)] = color.BLACK
                } else if (element.cancelled_by === "1") {
                  samco_status = "Cancelled on request";
                  mandate_status = "CANCELLED";
                  red[parseInt(element.mandate_id)] = color.BLACK
                } else if (element.mandate_option === "0") {
                  red[parseInt(element.mandate_id)] = color.BLACK
                  samco_status = "Not Generated";
                  //show_generate =1;
                }
              }else{
                red[parseInt(element.mandate_id)] = color.BLACK
                samco_status = "Not Generated";
              }
            });
            data = {
              mandate_amt: element.mandate_amt,
              mandate_id: element.mandate_id,
              mandate_status: mandate_status,
              cancelled_by: element.cancelled_by,
              mandate_option: element.mandate_option,
              created: element.created,
              samco_status: samco_status
            };
            //alert(data.mandate_amt)
            d.push(data);
          }
        );
        this.setState({ mandates: d,opacity:1,isLoading:false,red });
         //alert(this.state.red)
      })
      .catch(error => {
        alert(error);
      });
  }

  

  upload() {
    // RNFetchBlob.fetch('POST', 'https://content.dropboxapi.com/2/files/upload', {
    //     // dropbox upload headers
    //     Authorization : "Bearer access-token...",
    //     'Dropbox-API-Arg': JSON.stringify({
    //       path : '/img-from-react-native.png',
    //       mode : 'add',
    //       autorename : true,
    //       mute : false
    //     }),
    //     'Content-Type' : 'application/octet-stream',
    //     // Change BASE64 encoded data to a file path with prefix `RNFetchBlob-file://`.
    //     // Or simply wrap the file path with RNFetchBlob.wrap().
    //   }, RNFetchBlob.wrap(this.state.image))
    //   .then((res) => {
    //     console.log(res.text())
    //   })
    //   .catch((err) => {
    //     // error handling ..
    //   })
  }

  // react-native-document-picker when remove expo

  // openfile(){
  //    DocumentPicker.show({
  //   filetype: [DocumentPickerUtil.allFiles()],
  // },(error,res) => {
  //   // Android
  //   console.log(
  //      res.uri,
  //      res.type, // mime type
  //      res.fileName,
  //      res.fileSize
  //   );
  // });
  // }

  renderRow(item) {
    //  alert(item.mandate_id)
    return (
      <Card>
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1 }}>
            <Text style={styles.title_carddetail}>
              Mandate ID :{" "}
              <Text style={styles.carddetail}>{item.mandate_id}</Text>{" "}
            </Text>
            <Text style={styles.title_carddetail}>
              Registration date :{" "}
              <Text style={styles.carddetail}>{item.created}</Text>{" "}
            </Text>
            <Text style={styles.title_carddetail}>
              Amount : <Text style={styles.carddetail}>{item.mandate_amt}</Text>{" "}
            </Text>
            <Text style={styles.title_carddetail}>
              Mandate Status :{" "}
              <Text style={[styles.carddetail,{color:this.state.red[parseInt(item.mandate_id)]}]}>{item.samco_status}</Text>
            </Text>
          </View>

          <View style={{ flex: 1 }}>
            <View
              style={[styles.buttonContainerPage1, { alignItems: "flex-end" }]}
            >
              <TouchableOpacity
                style={styles.download}
                onPress={this._pickDocument}
              >
                <Image
                  source={require("../../assets/images/upload.png")}
                  style={{
                    height: 10,
                    width: 10,
                    resizeMode: "contain",
                    alignItems: "center",
                    marginLeft: 5,
                    marginBottom: 5
                  }}
                />
                <Text style={{ padding: 5, fontSize: 10, color: "#000000" }}>
                  UPLOAD
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={[styles.buttonContainerPage1, { alignItems: "flex-end" }]}
            >
              <TouchableOpacity style={styles.download}>
                <Image
                  source={require("../../assets/images/downloadimage.png")}
                  style={{
                    height: 10,
                    width: 10,
                    resizeMode: "contain",
                    alignItems: "center",
                    marginLeft: 5,
                    marginBottom: 5
                  }}
                />
                <Text style={{ padding: 5, fontSize: 10, color: "#000000" }}>
                  DOWNLOAD
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Card>
    );
  }

  render() {
    let { image } = this.state;

    return (
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        contentContainerStyle={[styles.containerFlex, { flexDirection: "column"}]}
      >
        <StatusBar backgroundColor={color.toolbar} barStyle="light-content" />
        <View style={{ marginTop: 15 ,opacity:this.state.opacity}}>
          <View style={styles.buttonContainerPage1}>
            <TouchableOpacity
              onPress={() => Actions.CreateMandate()}
              style={[styles.button, { backgroundColor: "green" }]}
            >
              <Text style={styles.instructions}>CREATE A NEW MANDATE</Text>
            </TouchableOpacity>
          </View>

          <View>
            <FlatList
              data={this.state.mandates}
              renderItem={({ item }) => this.renderRow(item)}
            />
          </View>
        </View>
        {this.state.isLoading && (
          <View
            style={{
              alignItems:"center",
              position: "absolute",
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              flex: 1
            }}
          >
            <Image
              source={require("../../assets/images/loader.gif")}
              style={{ height: 30, width: 30, resizeMode: "contain" }}
            />
          </View>
        )}
      </ScrollView>
    );
  }
}

export default MandateRegistration;
