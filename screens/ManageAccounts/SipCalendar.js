import React, { Component } from "react";
import {
  StyleSheet,
  Platform,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Alert
} from "react-native";
import { Calendar } from "react-native-calendars";
import styles from "../../components/styles";

var date = new Date();
var day = date ? ("0" + date.getDate()).slice(-2) : "";
var month = date ? ("0" + (date.getMonth() + 1)).slice(-2) : "";
var year = date ? date.getFullYear() : "";
const currentDate = year + "-" + month + "-" + day;


export default class SipCalendar extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      marked: false
    };
    this.onDayPress = this.onDayPress.bind(this);
  }

  componentDidMount() {
    //this.anotherFunc();
  }

  // anotherFunc = () => {
  // var obj = nextDay.reduce((c, v) => Object.assign(c, {[v]: {selected: true,marked: true}}), {});
  // this.setState({ marked : obj});
  // }

  render() {
    return (
      <ScrollView style={{ marginTop: 10 }}>
        <Calendar
          onDayPress={this.onDayPress}
          style={styles.calendar}
          current={currentDate}
          markedDates={this.state.marked}
        />
      </ScrollView>
    );
  }

  onDayPress(day) {
    this.setState({
      selected: day.dateString
    });
    event.map(e =>
      e.start == day.dateString
        ? Alert.alert(
            day.dateString,
            "\n" + e.title + "\n" + e.type + "\n" + e.description
          )
        : ""
    );
  }
}
