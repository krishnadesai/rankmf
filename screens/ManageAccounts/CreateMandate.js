import React, { Component, Props } from "react";
import {
  AsyncStorage,
  TextInput,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ScrollView,
  FlatList,
  TouchableOpacity,
  Image,
  Button
} from "react-native";
import { Actions } from "react-native-router-flux";
import { Card } from "react-native-elements";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";

const regex = /(<([^>]+)>)/ig;
function cleantext(strInputCode){
    cleanText = strInputCode.replace(regex, '');
    return cleanText;
}

class CreateMandate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      isLoading: false,
      opacity:1,
      file: undefined,
      mandates: [],
      image: null,
      mandates_rejected: [],
      red:[],
      mandate_amt:"10000",
      amterror:false
    };
  }

  componentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  

  SubmitMandate() {
    
    fetch(Apis.Mf_mandate, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.2&access_token=${access_token}&user=${clientid}&client_id=${clientid}&mandate_amt=${this.state.mandate_amt}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson["status"] === "error"){
                this.setState({mandates:responseJson["show_unuploaded_arr"]})
        }else{
            this.setState({mandates:[]})
            this.getLink(responseJson["mandate"])
        }
      })
      .catch(error => {
        alert(error);
      });
  }

  getLink(mandate_id) {
    //alert(`api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&mandate_amt=${this.state.mandate_amt}&mandate_id=${mandate_id}`)
    fetch(Apis.Mf_mandate, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&mandate_amt=${this.state.mandate_amt}&mandate_id=${mandate_id}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson["status"] === "success"){
            Actions.MandateWebview({url:responseJson["data"]})
        }
        
      })
      .catch(error => {
        alert(error);
      });
  }


  submit(){
      if(parseFloat(this.state.mandate_amt) < 10000){
          this.setState({amterror:true})
      }else{
        this.setState({amterror:false})        
        //test git        
        //this.SubmitMandate()
        Actions.MandateWebview({url:'https://www.rankmf.com/'})
      }
  }
 

  renderRow(item) {
    //  alert(item.mandate_id)
    return (
      <Card>
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1 }}>
            <Text style={styles.title_carddetail}>
              Mandate ID :{" "}
              <Text style={styles.carddetail}>{item.mandate_id}</Text>{" "}
            </Text>
            <Text style={styles.title_carddetail}>
              Amount : <Text style={styles.carddetail}>{item.mandate_amt}</Text>{" "}
            </Text>
            <Text style={styles.title_carddetail}>
              Mandate Status :{" "}
            {item.samco_status.indexOf("red") != -1?  <Text style={[styles.carddetail,{color:color.RED}]}>{cleantext(item.samco_status)}</Text>: <Text style={[styles.carddetail]}>{cleantext(item.samco_status)}</Text>}
            </Text>
          </View>

          <View style={{ flex: 1 }}>
            <View
              style={[styles.buttonContainerPage1, { alignItems: "flex-end" }]}
            >
              <TouchableOpacity
                style={styles.download}
                onPress={this._pickDocument}
              >
                <Image
                  source={require("../../assets/images/upload.png")}
                  style={{
                    height: 10,
                    width: 10,
                    resizeMode: "contain",
                    alignItems: "center",
                    marginLeft: 5,
                    marginBottom: 5
                  }}
                />
                <Text style={{ padding: 5, fontSize: 10, color: "#000000" }}>
                  UPLOAD
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={[styles.buttonContainerPage1, { alignItems: "flex-end" }]}
            >
              <TouchableOpacity style={styles.download}>
                <Image
                  source={require("../../assets/images/downloadimage.png")}
                  style={{
                    height: 10,
                    width: 10,
                    resizeMode: "contain",
                    alignItems: "center",
                    marginLeft: 5,
                    marginBottom: 5
                  }}
                />
                <Text style={{ padding: 5, fontSize: 10, color: "#000000" }}>
                  DOWNLOAD
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Card>
    );
  }

  render() {
    
    return (
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={[styles.containerFlex]}
      >
      <View>
        <StatusBar backgroundColor={color.toolbar} barStyle="light-content" />
        <View style={{ marginTop: 15 ,opacity:this.state.opacity,paddingBottom:20}}>

        <Text
                style={[
                  styles.welcome,
                  { textAlign: "left", color: color.BLACK,fontSize:17,fontWeight:"700" ,marginLeft:10}
                ]}
              >
                Enter the amount for Mandate/Auto Debit Facility
              </Text>
              <Text
                style={[
                  styles.welcome,
                  { textAlign: "left", color: color.BLACK,fontSize:14,marginTop:10,marginLeft:10 }
                ]}
              >
               Once you setup an auto debit facility, your SIP amounts shall be automatically deducted from your linked Bank account.{'\n'}{'\n'}
               This facility shall be used only for SIP payments
              </Text>

              <View
              style={{
                marginTop: 15,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
            <Text
                style={{
                  marginTop: 5,
                  color: color.GREY,
                  fontSize: 15,
                  textAlign: "center",
                  alignItems: "center"
                }}
              >
                Amount
              </Text>
              <TextInput
                underlineColorAndroid="transparent"
                keyboardType="numeric"
                returnKeyType="done"
                defaultValue={this.state.mandate_amt}
                onChangeText={text => this.setState({ mandate_amt: text })}
                style={{
                  borderBottomColor: color.BGREY,
                  borderBottomWidth: 1,
                  textAlign: "center",
                  color: color.BLACK,
                  width: 200,
                  marginTop: 15
                }}
              />
              
            </View>
            {this.state.amterror && (
              <Text
                style={{
                  marginTop: 10,
                  color: color.RED,
                  fontSize: 13,
                  textAlign: "center",
                  alignItems: "center"
                }}
              >
                Minimum amount should be 10000!
              </Text>
            )}

            <TouchableOpacity
              onPress={() => this.submit()}
              style={[styles.button, { backgroundColor: color.toolbar ,margin:20,}]}
            >
              <Text style={styles.instructions}>SUBMIT</Text>
            </TouchableOpacity>
            
           
         
            { this.state.mandates.length>0 &&    <Text
                style={[
                  styles.welcome,
                  { textAlign: "left", color: color.BLACK,fontSize:14,marginTop:10,marginLeft:10 }
                ]}
              >
               You have pending mandates of higher amount. Please generate the mandate for further processing.
              </Text>}
      { this.state.mandates.length>0 &&   
          
            <FlatList
              data={this.state.mandates}
              renderItem={({ item }) => this.renderRow(item)}
            />
          }
        </View>
        {this.state.isLoading && (
          <View
            style={{
              alignItems:"center",
              position: "absolute",
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              flex: 1
            }}
          >
            <Image
              source={require("../../assets/images/loader.gif")}
              style={{ height: 30, width: 30, resizeMode: "contain" }}
            />
          </View>
        )}
        </View>
      </ScrollView>
    );
  }
}

export default CreateMandate;
