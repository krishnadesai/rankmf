import React, { Component, PropTypes } from "react";
import {
  StyleSheet,
  Platform,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Picker,
  AsyncStorage
} from "react-native";
import { Card } from "react-native-elements";
import ModalDatePicker from "react-native-datepicker-modal";
import PopupDialog, {
  DialogTitle,
  SlideAnimation
} from "react-native-popup-dialog";
//import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";
import Table from "react-native-simple-table";
import Apis from "../../constants/Apis";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import { Thumbnail } from "native-base";
const slideAnimation = new SlideAnimation({ slideFrom: "bottom" });
const THEME_COLOR = "rgba(85,186,255, 1)";

var date = new Date();
var day = date ? ("0" + date.getDate()).slice(-2) : "";
var month = date ? ("0" + (date.getMonth() + 1)).slice(-2) : "";
var year = date ? date.getFullYear() : "";

const datestart = year + "-" + month + "-" + day;
const dateend = year + "-" + month + "-" + day;
const label = "";
const columnsScheme = [
  {
    title: "Order Number",
    dataIndex: "no",
    width: 80,
    index: 0
  },
  {
    title: "Order Type",
    dataIndex: "type",
    width: 100,
    index: 1
  },
  {
    title: "Scheme Name",
    dataIndex: "name",
    width: 120,
    index: 2
  },
  {
    title: "Buy/Sell",
    dataIndex: "bs",
    width: 50,
    index: 3
  },
  {
    title: "Amount(Rs)",
    dataIndex: "amt",
    width: 100,
    index: 4
  },
  {
    title: "Order Placed Date",
    dataIndex: "date",
    width: 100,
    index: 5
  },
  {
    title: "Order Response",
    dataIndex: "response",
    width: 160,
    index: 6
  },
  {
    title: "Order Payment Status",
    dataIndex: "paystatus",
    width: 100,
    index: 7
  },
  {
    title: "Start Date",
    dataIndex: "sdate",
    width: 100,
    index: 8
  },
  {
    title: "Order Status",
    dataIndex: "ostatus",
    width: 100,
    index: 9
  },
  {
    title: "Payment Mode",
    dataIndex: "mode",
    width: 50,
    index: 10
  },
  {
    title: "Action",
    dataIndex: "action",
    width: 50,
    index: 11
  }
];

class OrderHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      backdetail: "#D3D3D3",
      backpro: "#D3D3D3",
      backsummary: color.toolbar,
      detailorsummary: "summary",
      summaryhide: true,
      detailhide: true,
      prohide: true,
      orderstatus: "",
      ordertype: "",
      ordertypelabel: "Choose your option",
      orderstatuslabel: "Choose your option",
      types: [
        { label: "Buy              ", value: 0 },
        { label: "Sell", value: 1 }
      ],
      val: -1,
      norecords: false,
      buy_sell_value: "",
      summary: [],
      detail: [],
      provisional: []
    };
  }

  componentDidMount() {
    this.retrieveData();
    setTimeout(() => {
      this.fetchData();
    }, 500);
  }

  retrieveData = async () => {
    try {
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  getbuysell(buy_sell) {
    if (buy_sell === "P") {
      return "Buy";
    } else if (buy_sell === "R") {
      return "Sell";
    } else {
      return "";
    }
  }

  getorderstaus(order_status) {
    if (order_status === "0") {
      return "Order Placed";
    } else if (order_status === "1") {
      return "Order Failed";
    } else if (order_status === "2") {
      return "";
    } else if (order_status === "3") {
      return "Order Cancelled";
    }
  }

  fetchData() {
    alert(
      `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&invest_type=order_summary&from_date=${datestart}&to_date=${dateend}&order_summary_type=order_history&order_status=${
        this.state.orderstatus
      }&order_type=${this.state.ordertype}&buy_sell=${
        this.state.buy_sell_value
      }`
    );
    this.setState({ summary: [], master_array: [] });
    fetch(Apis.Minvest, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&invest_type=order_summary&from_date=${datestart}&to_date=${dateend}&order_summary_type=order_history&order_status=${
        this.state.orderstatus
      }&order_type=${this.state.ordertype}&buy_sell=${
        this.state.buy_sell_value
      }` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson["data"]["order_history"].clientOrders.length > 0) {
          responseJson["data"]["order_history"].clientOrders.map(item => {
            var data = {
              no: item.order_id,
              type: item.order_type,
              name: item.Scheme_Name,
              bs: this.getbuysell(item.buy_sell),
              amt: item.amount,
              date: item.date_created,
              response: item.order_response,
              paystatus: item.order_payment_status,
              sdate: item.start_date,
              ostatus: this.getorderstaus(item.order_status),
              mode: item.mfi ? "Samco" : "Bank",
              action: "Pay"
            };

            this.state.summary.push(data);
          });

          this.setState({ norecords: false, summaryhide: true });
        } else {
          this.setState({
            norecords: true,
            summaryhide: false,
            detailhide: false,
            prohide: false
          });
        }
        this.fetchDetailData();
      })
      .catch(error => {
        console.error(error);
      });
  }

  fetchDetailData() {
    //alert(`api=1.1&client_id=${clientid}&invest_type=order_summary&from_date=${datestart}&to_date=${dateend}&order_summary_type=order_history&order_status=${this.state.orderstatus}&order_type=${this.state.ordertype}&buy_sell=${this.state.buy_sell_value}`);
    this.setState({ detail: [] });
    fetch(Apis.Minvest, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&invest_type=order_summary&from_date=${datestart}&to_date=${dateend}&order_summary_type=basket_order&order_status=${
        this.state.orderstatus
      }&order_type=${this.state.ordertype}&buy_sell=${
        this.state.buy_sell_value
      }` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson["data"]["basket_order"].clientOrders.length > 0) {
          responseJson["data"]["basket_order"].clientOrders.map(item => {
            var data = {
              no: item.order_id,
              type: item.order_type,
              name: item.Scheme_Name,
              bs: this.getbuysell(item.buy_sell),
              amt: item.amount,
              date: item.date_created,
              response: item.order_response,
              paystatus: item.order_payment_status,
              sdate: item.start_date,
              ostatus: this.getorderstaus(item.order_status),
              mode: item.mfi ? "Samco" : "Bank",
              action: "Pay"
            };

            this.state.detail.push(data);
          });
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  fetchProvisionalData() {
    //alert(`api=1.1&client_id=${clientid}&invest_type=order_summary&from_date=${datestart}&to_date=${dateend}&order_summary_type=order_history&order_status=${this.state.orderstatus}&order_type=${this.state.ordertype}&buy_sell=${this.state.buy_sell_value}`);
    this.setState({ provisional: [] });
    fetch(Apis.Minvest, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}&invest_type=order_summary&from_date=${datestart}&to_date=${dateend}&order_summary_type=provisional_orders&order_status=${
        this.state.orderstatus
      }&order_type=${this.state.ordertype}&buy_sell=${
        this.state.buy_sell_value
      }` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (
          responseJson["data"]["provisional_orders"].clientOrders.length > 0
        ) {
          responseJson["data"]["provisional_orders"].clientOrders.map(item => {
            var data = {
              no: item.order_id,
              type: item.order_type,
              name: item.Scheme_Name,
              bs: this.getbuysell(item.buy_sell),
              amt: item.amount,
              date: item.date_created,
              response: item.order_response,
              paystatus: item.order_payment_status,
              sdate: item.start_date,
              ostatus: this.getorderstaus(item.order_status),
              mode: item.mfi ? "Samco" : "Bank",
              action: "Pay"
            };

            this.state.provisional.push(data);
          });
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  ChangeTable(name) {
    if (name === "summary") {
      if (this.state.summary.length === 0) {
        this.setState({
          norecords: true,
          summaryhide: false,
          detailhide: false,
          prohide: false
        });
      } else {
        this.setState({
          norecords: false,
          summaryhide: true,
          detailhide: false,
          prohide: false
        });
      }
      this.setState({ detailorsummary: name });
      this.setState({ backdetail: color.BGREY });
      this.setState({ backpro: color.BGREY });
      this.setState({ backsummary: color.toolbar });
    } else if (name === "detail") {
      if (this.state.detail.length === 0) {
        this.setState({
          norecords: true,
          summaryhide: false,
          detailhide: false,
          prohide: false
        });
      } else {
        this.setState({
          norecords: false,
          summaryhide: false,
          detailhide: true,
          prohide: false
        });
      }
      this.setState({ detailorsummary: name });
      this.setState({ backsummary: color.BGREY });
      this.setState({ backdetail: color.toolbar });
      this.setState({ backpro: color.BGREY });
    } else if (name === "provisional") {
      if (this.state.provisional.length === 0) {
        this.setState({
          norecords: true,
          summaryhide: false,
          detailhide: false,
          prohide: false
        });
      } else {
        this.setState({
          norecords: false,
          summaryhide: false,
          detailhide: false,
          prohide: true
        });
      }
      this.setState({ detailorsummary: name });
      this.setState({ backsummary: color.BGREY });
      this.setState({ backdetail: color.BGREY });
      this.setState({ backpro: color.toolbar });
    }
  }

  showSlideAnimationDialog1 = () => {
    this.slideAnimationDialog1.show();
  };

  onPickerValueChange1 = (value, index) => {
    if (value === "0") {
      label = "Success";
    } else if (value === "1") {
      label = "Failure";
    } else if (value === "3") {
      label = "Cancelled";
    } else if (value === "-1") {
      label = "Choose your option";
    }
    // alert(label+"--"+value)

    this.setState(
      {
        orderstatus: value,
        orderstatuslabel: label
      },
      () => {
        // here is our callback that will be fired after state change.
        // Alert.alert("Throttlemode", index+" "+this.state.throttlemode);
        this.slideAnimationDialog1.dismiss();
      }
    );
  };

  showSlideAnimationDialog2 = () => {
    this.slideAnimationDialog2.show();
  };

  onPickerValueChange2 = (value, index) => {
    if (value === "lumpsum") {
      label = "Lumpsum";
    } else if (value === "xsip-isip") {
      label = "SIP";
    } else if (value === "Choose your option") {
      label = "Choose your option";
    }

    this.setState(
      {
        ordertype: value,
        ordertypelabel: label
      },
      () => {
        // here is our callback that will be fired after state change.
        // Alert.alert("Throttlemode", index+" "+this.state.throttlemode);
        this.slideAnimationDialog2.dismiss();
      }
    );
  };

  onSelect(index, value) {
    this.setState({
      buy_sell_value: value
    });
  }

  render() {
    const DatePickerFrom = ({ style, ...props }) => (
      <ModalDatePicker
        style={[styles.container, style]}
        renderDate={({ year, month, day, date }) => {
          if (!date) {
            return <Text style={styles.dateinput}>{datestart}</Text>;
          }

          datestart = `${year}-${month}-${day}`;

          return <Text style={styles.dateinput}>{datestart}</Text>;
        }}
        {...props}
      />
    );

    const DatePickerTo = ({ style, ...props }) => (
      <ModalDatePicker
        style={[styles.container, style]}
        renderDate={({ year, month, day, date }) => {
          if (!date) {
            return <Text style={styles.dateinput}>{dateend}</Text>;
          }

          dateend = `${year}-${month}-${day}`;

          return <Text style={styles.dateinput}>{dateend}</Text>;
        }}
        {...props}
      />
    );

    return (
     
        <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.containerFlex}>
          <View>
            <Card title="Filters" titleStyle={{ color: THEME_COLOR }}>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Text style={styles.title}>From</Text>
                  <DatePickerFrom style={{ marginBottom: 15, marginTop: 3 }} />
                </View>
                <View style={{ flex: 1, marginLeft: 15 }}>
                  <Text style={styles.title}>To</Text>
                  <DatePickerTo style={{ marginBottom: 15, marginTop: 3 }} />
                </View>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Text style={styles.title}>Order Status</Text>
                  <Text
                    onPress={this.showSlideAnimationDialog1}
                    style={[
                      styles.input,
                      { marginBottom: 15, marginTop: 3, textAlign: "center" }
                    ]}
                  >
                    {this.state.orderstatuslabel}
                  </Text>
                </View>
                <View style={{ flex: 1, marginLeft: 15 }}>
                  <Text style={styles.title}>Order Type</Text>
                  <Text
                    onPress={this.showSlideAnimationDialog2}
                    style={[
                      styles.input,
                      { marginBottom: 15, marginTop: 3, textAlign: "center" }
                    ]}
                  >
                    {this.state.ordertypelabel}
                  </Text>
                </View>
              </View>

              <View style={{ alignItems: "center" }}>
                {/* <RadioForm
            radio_props={this.state.types}
            initial={-1}
            formHorizontal={true}
            labelHorizontal={true}
            buttonColor={'#2196f3'}
            animation={true}
            style={{marginTop:15}}
            onPress={(value) => {this.setState({val:value})}}
            /> */}

                <RadioGroup
                  style={{ flexDirection: "row" }}
                  onSelect={(index, value, label) =>
                    this.onSelect(index, value, label)
                  }
                >
                  <RadioButton value={"P"}>
                    <Text>Buy</Text>
                  </RadioButton>

                  <RadioButton value={"R"}>
                    <Text>Sell</Text>
                  </RadioButton>
                </RadioGroup>
              </View>
              <View style={styles.buttonContainer}>
                <TouchableOpacity
                  style={[styles.button, { backgroundColor: color.toolbar }]}
                  onPress={() => this.fetchData()}
                >
                  <Text style={{ color: "white", padding: 8 }}>APPLY</Text>
                </TouchableOpacity>
              </View>
            </Card>

            <Card>
              <View style={{ alignItems: "center" }}>
                <View
                  style={{
                    flexDirection: "row",
                    borderRadius: 10,
                    backgroundColor: color.WHITE,
                    alignItems: "center",
                    marginTop: 10,
                    overflow: "hidden"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 10,
                      padding: 16,
                      color: "#FFFFFF",
                      backgroundColor: this.state.backsummary,
                      borderBottomLeftRadius: 10,
                      flex: 1,
                      borderBottomRightRadius: 0,
                      borderTopLeftRadius: 10,
                      borderTopRightRadius: 0
                    }}
                    onPress={() => this.ChangeTable("summary")}
                  >
                    Order History
                  </Text>

                  <Text
                    style={{
                      fontSize: 10,
                      padding: 10,
                      color: "#FFFFFF",
                      flex: 1,
                      backgroundColor: this.state.backdetail
                    }}
                    onPress={() => this.ChangeTable("detail")}
                  >
                    Basket Order History
                  </Text>

                  <Text
                    style={{
                      fontSize: 10,
                      padding: 10,
                      color: "#FFFFFF",
                      backgroundColor: this.state.backpro,
                      borderBottomLeftRadius: 0,
                      flex: 1,
                      borderBottomRightRadius: 10,
                      borderTopLeftRadius: 0,
                      borderTopRightRadius: 10
                    }}
                    onPress={() => this.ChangeTable("provisional")}
                  >
                    Provisional Order History
                  </Text>
                </View>
              </View>
              {this.state.summaryhide ? (
                <View style={{ marginTop: 10 }}>
                  <Table
                    height={"100%"}
                    columns={columnsScheme}
                    dataSource={this.state.summary}
                  />
                </View>
              ) : null}

              {this.state.detailhide ? (
                <View style={{ marginTop: 10 }}>
                  <Table
                    height={"100%"}
                    columns={columnsScheme}
                    dataSource={this.state.detail}
                  />
                </View>
              ) : null}

              {this.state.prohide ? (
                <View style={{ marginTop: 10 }}>
                  <Table
                    height={"100%"}
                    columns={columnsScheme}
                    dataSource={this.state.provisional}
                  />
                </View>
              ) : null}
              {this.state.norecords === true && (
                <Text style={{ fontSize: 14, padding: 10, color: color.RED }}>
                  No Records Found
                </Text>
              )}
            </Card>
            <PopupDialog
          dialogStyle={{
            height: Platform.OS === "ios" ? 300 : 150,
            width: 300,
            position: "absolute",
            top: 50
          }}
          dialogTitle={<DialogTitle title="Select" />}
          ref={popupDialog => {
            this.slideAnimationDialog1 = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
          <View>
            <Picker
              style={{ justifyContent: "center" }}
              selectedValue={this.state.orderstatus}
              onValueChange={this.onPickerValueChange1}
            >
              <Picker.Item label="Choose your option" value="-1" />
              <Picker.Item label="Success" value="0" />
              <Picker.Item label="Failure" value="1" />
              <Picker.Item label="Cancelled" value="3" />
            </Picker>
          </View>
        </PopupDialog>
        <PopupDialog
          dialogStyle={{
            height: Platform.OS === "ios" ? 300 : 150,
            width: 300,
            position: "absolute",
            top: 50
          }}
          dialogTitle={<DialogTitle title="Select" />}
          ref={popupDialog => {
            this.slideAnimationDialog2 = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
          <View>
            <Picker
              style={{ justifyContent: "center" }}
              selectedValue={this.state.ordertype}
              onValueChange={this.onPickerValueChange2}
            >
              <Picker.Item
                label="Choose your option"
                value="Choose your option"
              />
              <Picker.Item label="Lumpsum" value="lumpsum" />
              <Picker.Item label="SIP" value="xsip-isip" />
            </Picker>
          </View>
        </PopupDialog>
          
          </View>
        </ScrollView>

        
     
    );
  }
}

export default OrderHistory;
