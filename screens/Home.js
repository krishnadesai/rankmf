import React, {Component} from 'react';
import { BackHandler,Platform, Text,  View,ScrollView,AsyncStorage} from 'react-native';

class Home extends Component {

    onAndroidBackPress = () => {
     
     BackHandler.exitApp();         
      
        return true;
      }
    
      componentWillMount() {
       
        if (Platform.OS === 'android') {
          BackHandler.addEventListener('hardwareBackPress', this.onAndroidBackPress);
        }
     
    
      }
    
      componentWillUnmount() {
        if (Platform.OS === 'android') {
          BackHandler.removeEventListener('hardwareBackPress');
        }
        this._storeData();
      }
  
      _storeData = async () => {
        try {
           
                await AsyncStorage.setItem('clienttype', "open");
                
        } catch (error) {
          // Error saving data
        }
      }

  render () {

    
    return (   
        
     
      <View >
       
      <ScrollView contentInsetAdjustmentBehavior="automatic">
                <Text>Home</Text>
       
        </ScrollView>
     
        
      </View>
     
    );
  }
}




export default Home;