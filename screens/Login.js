import React, {Component,Props} from 'react';
import {Dimensions,Platform, StyleSheet, TextInput, View,Text, Image,TouchableOpacity,KeyboardAvoidingView,StatusBar,AsyncStorage,ImageBackground } from 'react-native';
import CustomToast from '../components/CustomToast';
import {Actions} from 'react-native-router-flux';
import color from '../constants/Colors';
import Apis from '../constants/Apis';
const access_token="",at_expire_time="";
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default class Login extends Component {

    constructor(props)
    {
        super(props);
        this.state = {
         username: '',
            password: '',
            error: null,
            clienttype:''
        }
        
    }

    static navigationOptions =
    {
       title: 'Login',
    };

    // componentDidMount(){
    //     this.retrieveData();
    //   }
    
    //   retrieveData = async () => {
    //     try {
    //      clientid = await AsyncStorage.getItem('clientid');
    //      access_token =await AsyncStorage.getItem('access_token');  
          
    //      } catch (error) {
    //        // Error retrieving data
    //        //console.warn("1");
    //      }
    //   }
    
  isValid() {
    const { username, password } = this.state;
    let valid = false;

    if (username.length > 0 && password.length > 0) {
        if (!this.validateEmail(this.state.username)) {
                valid = false;
                this.setState({ error: 'You must enter a valid email address' });
                this.refs.error.ShowToastFunction('You must enter a valid email address!');
        }else{
            valid = true;
        }    
    }

    if (username.length === 0) {
      this.setState({ error: 'You must enter an email address' });
      this.refs.error.ShowToastFunction('You must enter an email address!');
    } else if (password.length === 0) {
      this.setState({ error: 'You must enter a password' });
      this.refs.error.ShowToastFunction('You must enter a password!');
    }

    return valid;
  }

  _storeData = async (data,clienttype) => {
    try {
        await AsyncStorage.setItem('access_token_expire', at_expire_time);
        await AsyncStorage.setItem('access_token', access_token);
        if(clienttype === "lead")
        { 
            await AsyncStorage.setItem('clienttype', "lead");
            await AsyncStorage.setItem('username', this.state.username);
            await AsyncStorage.setItem('password',this.state.password);
            await AsyncStorage.setItem('clientid',data.id);
            await AsyncStorage.setItem('FirstLogin',"Yes");
            await AsyncStorage.setItem('email',data.email);
            await AsyncStorage.setItem('name',data.name);
            await AsyncStorage.setItem('mobile',data.mobile);
            await AsyncStorage.setItem('city',data.city);
        }else{
            await AsyncStorage.setItem('clienttype', "client");
            await AsyncStorage.setItem('username', this.state.username);
            await AsyncStorage.setItem('password',this.state.password);
            await AsyncStorage.setItem('clientid',data.client_id);
            await AsyncStorage.setItem('email',data.email_id);
            await AsyncStorage.setItem('name',data.name);
            await AsyncStorage.setItem('mobile',data.mobile);
            await AsyncStorage.setItem('encrypted_client_id',data.encrypted_client_id);
            
            await AsyncStorage.setItem('crms_user_name',data.crms_user_name);
            await AsyncStorage.setItem('FirstLogin',"Yes");
        }
        Actions.drawer();
    } catch (error) {
      // Error saving data
    }
  }
  
  

    fetchData() {
        
         fetch(Apis.BaseURL, {
           method: 'POST',
           headers: new Headers({
                       'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
               }),
           body: `api=2.1&access_token=${access_token}&user=${this.state.username}&login_username=${this.state.username}&login_password=${this.state.password}`// <-- Post parameters
               })
               .then((response) => response.json())
               .then((responseJson) => {
                  
                if(responseJson["status"] === "Success"){
                    if(responseJson["data"]["user_type"] === "lead"){
                        this.setState({clienttype:"lead"})
                        this._storeData(responseJson["data"]["data"],this.state.clienttype);
                       
                    }else{
                        this.setState({clienttype:"client"})
                        this._storeData(responseJson["data"]["data"],this.state.clienttype);
                      
                    }
                    
                }else{
                    this.refs.error.ShowToastFunction(responseJson["data"]["msg"]);
                }
                   
                  
               })
               .catch((error) => {
                   console.error(error);
               });
       }

       fetchAccessToken(){
        var app_key ="",app_secret="";
        if(Platform.OS === "ios"){
            app_key="rankmf-ios-app"
            app_secret=Apis.app_secret_ios
        }else{
          app_key="rankmf-android-app"
          app_secret=Apis.app_secret_android
        }
              
        fetch(Apis.AuthToken, {
            method: 'POST',
            headers: new Headers({
                        'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
                }),
            body: `app_key=${app_key}&app_secret=${app_secret}&user=${this.state.username}`// <-- Post parameters
                })
                .then((response) => response.json())
                .then((responseJson) => {    
                     if(responseJson["status"] === "success"){
                        access_token = responseJson["response"].access_token
                        at_expire_time=responseJson["response"].at_expire_time
                      
                        this.fetchData();
                     }
                   
                   
                })
                .catch((error) => {
                    console.error(error);
                });
       }
     

    validateEmail = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(email);
      };
      
    render() {
       
        return (
   
            <KeyboardAvoidingView style={styles.container} >

            <StatusBar
                    backgroundColor={color.toolbar}
                    barStyle="light-content"/>
            <View style={{justifyContent:"center",alignItems:"center",flex:1}}>
                <Image source={require('../assets/images/Splash/iphoneX.jpg')} style={{width: '100%', height: '100%',opacity:0.8,justifyContent:"center",alignItems:"center"}}/>
               
               <View style={{backgroundColor:color.WHITE,justifyContent:"center",alignItems:"center",borderRadius:5,padding:40,marginLeft:20,marginRight:20,position:"absolute"}}>
               <Image source={require('../assets/images/rankmf.png')} />
               <TextInput style = {[styles.input,{marginTop:25}]} 
                    autoCapitalize="none" 
                    onSubmitEditing={() => this.passwordInput.focus()} 
                    autoCorrect={false} 
                    keyboardType='email-address' 
                    returnKeyType="next" 
                    textContentType="emailAddress"
                    placeholder='User Name' 
                    placeholderTextColor={color.GREY}
                    onChangeText={(text) => this.setState({username:text})}/>

              <TextInput style = {styles.input}   
                returnKeyType="done" 
                ref={(input)=> this.passwordInput = input} 
                placeholder='Password' 
                placeholderTextColor={color.GREY}
                secureTextEntry
                onChangeText={(text) => this.setState({password:text})}/>

                <TouchableOpacity style={styles.buttonContainer} onPress={() => this.fetchAccessToken() }>
                            <Text  style={styles.buttonText}>LOGIN</Text>
                </TouchableOpacity> 
                
               </View>
               <CustomToast ref = "success" backgroundColor='#4CAF50' position = "bottom" />
                <CustomToast ref = "error" backgroundColor="red" position = "bottom" />
                </View>
           </KeyboardAvoidingView>
          
        );
    }
}




const styles = StyleSheet.create({
    container: {
     flex:1,
    
     backgroundColor:color.toolbar
    },
    input:{
        height: 40,
        backgroundColor: 'rgba(225,225,225,0.2)',
        marginBottom: 10,
        padding: 10,
        width: width/1.4,
        color: color.BLACK
    },
    buttonContainer:{
        backgroundColor: color.toolbar,
        paddingVertical: 15,
        marginBottom:15,
        width: width/1.4,
        borderRadius:5
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },
    loginContainer:{
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    logo: {
        position: 'absolute',
        width: 300,
        height: 100
    },
    
});