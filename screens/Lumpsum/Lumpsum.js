import React, { Component } from "react";
import {
  AsyncStorage,
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  TextInput,
  KeyboardAvoidingView
} from "react-native";
import StarRating from "react-native-star-rating";
import { Actions } from "react-native-router-flux";
import { Card } from "react-native-elements";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";
const like = require("../../assets/images/like.png");
const dislike = require("../../assets/images/dislike.png");

class Lumpsum extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      opacity: 0,
      fundimage: true,
      investimage: false,
      starCount: 2.5,
      error: null,
      amterror: false,
      multipliererror: false,
      data: {}
    };
  }

  componentDidMount() {
    //   this.retrieveData()
    this.setState({
      amount: this.props.Minimum_Purchase_Amount,
      isLoading: false,
      opacity: 1
    });
  }

  // retrieveData = async () => {
  //   try {

  //     clienttype = await AsyncStorage.getItem("clienttype");
  //     clientid = await AsyncStorage.getItem("clientid");
  //     access_token = await AsyncStorage.getItem("access_token");
  //   //  this.fetchData();
  //   } catch (error) {
  //     // Error retrieving data
  //     //console.warn("1");
  //   }
  // };

  confirm() {
    if (this.state.amount < parseInt(this.props.Minimum_Purchase_Amount)) {
      this.setState({ amterror: true });
    } else {
      this.setState({ amterror: false });
      // alert(this.state.amount)
      if (
        parseFloat(this.state.amount) %
          parseFloat(this.props.Purchase_Amount_Multiplier) ===
        0
      ) {
        this.setState({ multipliererror: false });
        Actions.Lumpsum2({
          Scheme_Code: this.props.Scheme_Code,
          Unique_No: this.props.Unique_No,
          Amc_code: this.props.Amc_code,
          Scheme_Name: this.props.Scheme_Name,
          amount: this.state.amount
        });
      } else {
        this.setState({ multipliererror: true });
      }
    }
  }

  render() {
    return (
      <KeyboardAvoidingView
        keyboardVerticalOffset = {60}
        style={[styles.containerFlex, { backgroundColor: color.LIGHT_GREY }]}
        behavior="padding"
        enabled
      >
        <ScrollView>
          <StatusBar backgroundColor={color.toolbar} barStyle="light-content" />

          <Card containerStyle={{ opacity: this.state.opacity }}>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <View
                style={{
                  borderRadius: 5,
                  borderColor: color.GREY,
                  borderWidth: 1,
                  justifyContent: "center"
                }}
              >
                <Image
                  source={{
                    uri: Apis.BaseImageURL + `${this.props.Amc_code}.jpg`
                  }}
                  style={{
                    padding: 15,
                    height: 50,
                    width: 100,
                    resizeMode: "stretch"
                  }}
                />
              </View>
              <Text
                style={[
                  styles.welcome,
                  { textAlign: "center", marginTop: 15, color: color.toolbar }
                ]}
              >
                {this.props.Scheme_Name}
              </Text>
            </View>

            <View style={[styles.dash, { margin: 7, marginTop: 15 }]} />
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                marginLeft: 30,
                marginTop: 15
              }}
            >
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <Text style={[styles.ExploreAllText, { color: color.GREY }]}>
                  Rating
                </Text>

                <StarRating
                  starStyle={{ marginTop: 15 }}
                  fullStar={require("../../assets/images/star.png")}
                  emptyStar={require("../../assets/images/emptystar.png")}
                  disabled={true}
                  maxStars={5}
                  starSize={18}
                  halfStarEnabled={true}
                  rating={parseInt(this.props.rating)}
                />
              </View>
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                  marginLeft: 15
                }}
              >
                <Text style={[styles.ExploreAllText, { color: color.GREY }]}>
                  Nav
                </Text>

                <Text style={{ marginTop: 10 }}>
                  {"\u20B9"}
                  {this.props.Navrs}
                </Text>
              </View>
            </View>
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 0.5 }} />
              <Text
                style={{
                  flex: 0.5,
                  marginTop: 5,
                  color: color.BGREY,
                  fontSize: 13,
                  textAlign: "center",
                  alignSelf: "flex-end"
                }}
              >
                ({this.props.Navdate})
              </Text>
            </View>
            <View style={[styles.dash, { margin: 7, marginTop: 15 }]} />
            <View
              style={{
                marginTop: 15,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TextInput
                underlineColorAndroid="transparent"
                keyboardType="numeric"
                returnKeyType="done"
                defaultValue={this.props.Minimum_Purchase_Amount}
                onChangeText={text => this.setState({ amount: text })}
                style={{
                  borderBottomColor: color.BGREY,
                  borderBottomWidth: 1,
                  textAlign: "center",
                  color: color.BLACK,
                  width: 200,
                  marginTop: 15
                }}
              />
              <Text
                style={{
                  marginTop: 5,
                  color: color.BGREY,
                  fontSize: 13,
                  textAlign: "center",
                  alignItems: "center"
                }}
              >
                (Min amt Rs. {this.props.Minimum_Purchase_Amount})
              </Text>
            </View>
            {this.state.amterror && (
              <Text
                style={{
                  marginTop: 10,
                  color: color.RED,
                  fontSize: 13,
                  textAlign: "center",
                  alignItems: "center"
                }}
              >
                Minimum amount for this scheme is Rs{" "}
                {this.props.Minimum_Purchase_Amount}
              </Text>
            )}
            {this.state.multipliererror && (
              <Text
                style={{
                  marginTop: 10,
                  color: color.RED,
                  fontSize: 13,
                  textAlign: "center",
                  alignItems: "center"
                }}
              >
                Amount should be in multiples of{" "}
                {this.props.Purchase_Amount_Multiplier}
              </Text>
            )}
            <View style={[styles.buttonContainer, { marginTop: 15 }]}>
              <TouchableOpacity
                onPress={() => this.confirm()}
                style={[styles.button, { backgroundColor: color.toolbar }]}
              >
                <Text style={styles.instructions}>CONFIRM AND CONTINUE</Text>
              </TouchableOpacity>
            </View>
          </Card>
          {this.state.isLoading && (
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                position: "absolute",
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                flex: 1
              }}
            >
              <Image
                source={require("../../assets/images/loader.gif")}
                style={{ height: 30, width: 30, resizeMode: "contain" }}
              />
            </View>
          )}
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

export default Lumpsum;
