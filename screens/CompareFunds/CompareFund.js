import React from 'react';
import {Platform,Dimensions, FlatList, Text, View, ScrollView,AsyncStorage,TouchableOpacity,Image,TouchableHighlight} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import PopupDialog, {  DialogTitle,  SlideAnimation} from 'react-native-popup-dialog';
import Autocomplete from 'react-native-autocomplete-input';
import StarRating from 'react-native-star-rating';
import ChartView from 'react-native-highcharts';
import styles from '../../components/styles';
import color from '../../constants/Colors';
import Apis from '../../constants/Apis';
import { Card } from 'native-base';
const like = require('../../assets/images/thumbsup.png');
const newicon = require('../../assets/images/new-icon.png');
const dislike = require('../../assets/images/thumbsdown.png');
const slideAnimation = new SlideAnimation({ slideFrom: 'bottom' });
const DEVICE_WIDTH = Dimensions.get(`window`).width;
const DEVICE_HEIGHT = Dimensions.get(`window`).height;
const regex = /(<([^>]+)>)/ig;
const clienttype="";
const SLIDER_1_FIRST_ITEM = 1;
function cleantext(strInputCode){
    cleanText = strInputCode.replace('<ul><li>', '\u2022 ');
    cleanText = cleanText.replace('</li><li>', '\n\n\u2022 ');
    cleanText = cleanText.replace('</li></ul>', '');
    return cleanText;
}

class CompareFunds extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            isLoading: true,
            selected_codes:[],
            selected_names:[],
            popcolor:[color.LIGHT_GREY,color.LIGHT_GREY,color.LIGHT_GREY,color.LIGHT_GREY,color.toolbar,color.LIGHT_GREY],
            poptext:[color.toolbar,color.toolbar,color.toolbar,color.toolbar,color.LIGHT_GREY,color.toolbar],
            growthyr:"5",
            chartdata:[],
            categoryfunddata:[],
            schemedata:[],
            data_to_pass:[],
            fund_tooltip:"",
            time_tooltip:"",
            rating_tooltip:"",
            fundimage:"",
            portfolio_quality:"",
            risk_volatility:"",
            corelation_becnhmark:"",
            asset_type:"",
            slider1ActiveSlide: SLIDER_1_FIRST_ITEM,
            marginTop:5
           }
           this.onViewableItemsChanged.bind(this)
           this.fetchData=this.fetchData.bind(this);
    }
  
  componentDidMount() {
    this.state.selected_codes.push(this.props.schemecode)
 
    
    this.retrieveData();
}

  retrieveData = async () => {
    try {
     clienttype = await AsyncStorage.getItem('clienttype');
     client_id = await AsyncStorage.getItem('clientid');
     access_token =await AsyncStorage.getItem('access_token');  
   
    this.fetchData();
     } catch (error) {
      
     }
  }

  fetchData(){
    
    var codes ="";
    if(this.state.selected_codes.length >1){
        this.state.selected_codes.map((item) =>{
            codes+= item+"-"

        })
        codes.substring(0, codes.length-1)
    }else{
            codes = this.state.selected_codes
         
    }
      
      
    fetch(Apis.BaseURL, {
        method: 'POST',
        headers: new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
            }),
        body: `api=1.6&access_token=${access_token}&user=${client_id}&client_id=${client_id}&compare_type=1&selected_codes=${codes}`// <-- Post parameters
            })
            .then((response) => response.json())
            .then((responseJson) => {    
               if(responseJson["status"] === "Success"){
               
                this.setState({selected_codes:responseJson["data"]["compare_data"].Unique_No,selected_names:responseJson["data"]["compare_data"].Unique_Name,data_to_pass:responseJson["data"]["compare_data"].data_to_pass})
              
               
                    if(this.state.selected_codes.length >1){
                      
                        this.state.selected_codes.map((item,index) =>  this.fetchChartData(item,this.state.growthyr,this.state.selected_codes.indexOf(parseInt(item))))
                    }else{
                       
                        
                        this.fetchChartData(codes,this.state.growthyr,this.state.selected_codes.indexOf(parseInt(codes)))
                      
                        
                    }
               
                
                
               }
               
               
            })
            .catch((error) => {
                alert(error);
            });
}

fetchChartData(schemeid,yr,index){
  
    fetch(Apis.BaseURL, {
        method: 'POST',
        headers: new Headers({
                    'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
            }),
        body: `api=1.6&access_token=${access_token}&user=${client_id}&client_id=${client_id}&compare_type=2&schemeid=${schemeid}&year_selector_container1=${yr}`// <-- Post parameters
            })
            .then((response) => response.json())
            .then((responseJson) => {    
                const chartdata = this.state.chartdata;
                const schemedata = this.state.schemedata;
                var dataStorage1={}
               if(responseJson["status"] === "Success"){
                var dataStorage = {name:responseJson["data"]["getSchemeData"]["result"][0].Scheme_Name, data:responseJson["data"]["getChartData"]}
               
                chartdata[index]=dataStorage
                 dataStorage1={Unique_No:responseJson["data"]["getSchemeData"]["result"][0].Unique_No,
                Scheme_Name:responseJson["data"]["getSchemeData"]["result"][0].Scheme_Name,
                rank_srno:responseJson["data"]["getSchemeData"]["result"][0].rank_srno,
                rating:responseJson["data"]["getSchemeData"]["result"][0].rating,
                time_flag:responseJson["data"]["getSchemeData"]["result"][0].time_flag,
                time_tooptip:responseJson["data"]["getSchemeData"]["scheme_tool_tips"][0].time_tooptip,
                Navrs:responseJson["data"]["getSchemeData"]["result"][0].Navrs,
                asset_type:responseJson["data"]["getSchemeData"]["result"][0].asset_type,
                classname:responseJson["data"]["getSchemeData"]["result"][0].classname,
                option:responseJson["data"]["getSchemeData"]["option"],
                return_3m:responseJson["data"]["getSchemeData"]["return_3m"],
                return_6m:responseJson["data"]["getSchemeData"]["return_6m"],
                return_1y:responseJson["data"]["getSchemeData"]["return_1y"],
                return_3y:responseJson["data"]["getSchemeData"]["return_3y"],
                return_5y:responseJson["data"]["getSchemeData"]["return_5y"],
                return_10y:responseJson["data"]["getSchemeData"]["return_10y"],
                risk_score:responseJson["data"]["getSchemeData"]["result"][0].risk_score,
                exp_ratio:responseJson["data"]["getSchemeData"]["exp_ratio"],
                EXITLOAD:responseJson["data"]["getSchemeData"]["load"].REMARKS,
                turnover_perc:responseJson["data"]["getSchemeData"]["portfolio_turnover"][0].turnover_perc,
                amc:responseJson["data"]["getSchemeData"]["result"][0].amc,
                fund_tooltip:responseJson["data"]["getSchemeData"]["scheme_tool_tips"][0].fund_tooltip,
                rating_tooltip:responseJson["data"]["getSchemeData"]["scheme_tool_tips"][0].rating_tooltip,
                portfolio_quality:responseJson["data"]["getSchemeData"]["result"][0].portfolio_quality,
                risk_volatility:responseJson["data"]["getSchemeData"]["result"][0].risk_volatility,
                corelation_becnhmark:responseJson["data"]["getSchemeData"]["result"][0].corelation_becnhmark
                }
                schemedata[index] =dataStorage1
               // this.state.chartdata.push(dataStorage)
                this.setState({chartdata,schemedata})
               }
              
             
             
              
            })
            .catch((error) => {
                alert(error);
            });
}

ChangeYear(yr,index1){
    
        this.setState({growthyr:yr})
        const popcolor = this.state.popcolor;
        const poptext = this.state.poptext
        this.state.popcolor.map((item,index) =>{           
            if(index === index1){                
                popcolor[index] = color.toolbar
                poptext[index] = color.LIGHT_GREY
            }else{                
                popcolor[index] = color.LIGHT_GREY
                poptext[index] = color.toolbar
            }
        })
       // this.state.selected_codes.push("29146")
        this.setState({popcolor,poptext})
    
        //this.state.selected_codes.map((item,index) =>  this.fetchChartData(item,yr,index))
      
        this.fetchData()
       
}

removeScheme(index,item)
{
    
    if(this.state.selected_codes.length >1){
        // chartdata[this.state.selected_codes.indexOf(code)] = {}
        // schemedata[this.state.selected_codes.indexOf(code)] = {}
      //  alert(this.state.selected_codes.indexOf(code))
   
    this.state.selected_names.pop(item)
    this.state.chartdata.splice(index,1)
    this.state.schemedata.splice(index, 1)
    this.state.selected_codes.splice(index, 1)
    if(this.state.selected_codes.length === 1){
        this.setState({marginTop:5})
    }
    setTimeout(() => {
        this.fetchData()
       }, 500);
    }else{

       this.setState({selected_codes:[],selected_names:[],chartdata:[],schemedata:[],marginTop:5})
    }
  //  this.setState({chartdata,schemedata})
   
       
    
}



showSlideAnimationDialog = () => {
    this.slideAnimationDialog.show();
  }

  renderAutoComplete(text,id){
    // alert(text)
     return (<TouchableOpacity onPress={() => {
      
       if(this.state.selected_names.indexOf(text) === -1 ){
         
      
         this.state.selected_names.push(text)
         this.state.selected_codes.push(id)
         
         this.setState({text1:"",marginTop:25})
         this.searchCategoryFund("no match")
         
       
     }else{
       alert(`${text} is already added`)
     }
       
     setTimeout(() => {
       //  this.props.navigation.navigate('TutorialSlider');
     this.fetchData()
       //this.fetchChartData(id,this.state.growthyr,this.state.selected_names.length-1)
       this.slideAnimationDialog.dismiss()
     },500)
         }} >
     <Text style={{margin:5,fontSize:10}}>
       {text} 
     </Text>
     <View style={styles.dash}/>
   </TouchableOpacity>)
   
   }

   searchCategoryFund(text){
    if(text.length >=3){
      
                       
         fetch(Apis.BaseURL, {
           method: 'POST',
           headers: new Headers({
                       'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
               }),
           body: `api=1.8&access_token=${access_token}&user=${client_id}&keyword=${text.toUpperCase()}&schemes=${this.props.selected_codes}&data_to_pass[0]=${this.state.data_to_pass[0]}&data_to_pass[1]=${this.state.data_to_pass[1]}`// <-- Post parameters
               })
               .then((response) => response.json())
               .then((responseJson) => {

                this.setState({categoryfunddata:responseJson["data"]["get_scheme_list"]["data"]})
                
                //alert(responseJson["data"])
                
               })
               .catch((error) => {
                   console.error(error);
               });
       
    }
    //this.setState({text1:""})
  }

  renderfundimage(fundimage,fund_tooltip,flag){       
    if(clienttype === "client")  
      { 
          if(fundimage >= 3) {
      return (
        <View style={{height:50,justifyContent:"center",alignItems:"center"}}>
          <TouchableOpacity style={[styles.circle,{backgroundColor:color.GREEN,alignItems:"center"}]} onPress={() => {flag ===1 ? this.renderFundTooltip(fund_tooltip,fundimage) : null}}>
              <Image
              style={{width: 15, height: 15,resizeMode:"contain",alignItems:"center",marginTop:7}} 
                  source={ like }
              />
      </TouchableOpacity>
      </View>
          );
      }else{
          return (
            <View style={{height:50,justifyContent:"center",alignItems:"center"}} >
              <TouchableOpacity style={[styles.circle,{backgroundColor:color.RED,alignItems:"center"}]} onPress={() => {flag ===1 ? this.renderFundTooltip(fund_tooltip,fundimage) : null}}>
                  <Image
                  style={{width:15, height: 15,resizeMode:"contain",alignItems:"center",marginTop:7}} 
                      source={ dislike }
                  />
          </TouchableOpacity>
          </View>
              );
      }
  }else{
      return (  
        <View style={{height:50,justifyContent:"center",alignItems:"center"}} >
        <TouchableOpacity style={{height:50,justifyContent:"center",alignItems:"center"}}>
      <Image
      style={{width:30, height: 30,resizeMode:"contain",alignItems:"center"}} 
          source={ require('../../assets/images/fund-lock.png') }
      /></TouchableOpacity> </View>);
  }
  }

  rendertimeimage(timeimage,time_tooltip){       
      if(clienttype === "client")  
      {   
          if(timeimage === "1") {
      return (
        <View style={{height:70,justifyContent:"center",alignItems:"center"}}>
          <TouchableOpacity style={[styles.circle,{backgroundColor:color.GREEN,alignItems:"center"}]} onPress={() => this.renderTimeTooltip(time_tooltip)}>
              <Image
              style={{width: 15, height: 15,resizeMode:"contain",alignItems:"center",marginTop:7}} 
                  source={ like }
              />
      </TouchableOpacity>
      </View>
          );
      }else if(time_tooltip === "Enough data is not available for this scheme."){
          return (
            <View style={{height:70,justifyContent:"center",alignItems:"center"}}>
                  <Image
                  style={{width: 30, height: 30,resizeMode:"contain",alignItems:"center"}} 
                      source={ newicon }
                  />
         </View>
              );
      }else{
          return (
            <View style={{height:70,justifyContent:"center",alignItems:"center"}}>
              <TouchableOpacity style={[styles.circle,{backgroundColor:color.RED,alignItems:"center"}]} onPress={() => this.renderTimeTooltip(time_tooltip)}>
                  <Image
                  style={{width: 15, height: 15,resizeMode:"contain",alignItems:"center",marginTop:7}} 
                      source={ dislike }
                  />
          </TouchableOpacity>
          </View>
              );
      }
  }else{
      return (  
        <View style={{height:70,justifyContent:"center",alignItems:"center"}}>
      <Image
      style={{width:30, height: 30,resizeMode:"contain",alignItems:"center"}} 
          source={ require('../../assets/images/time-to-invest-lock.png') }
      /></View>);
  }
   }

   renderStrength(riskscore,portfolio_quality,risk_volatility,corelation_becnhmark,asset_type){     
     
    if(clienttype === "client")  
    {
        if(riskscore === "0") {
        return (
            <TouchableOpacity style={{height:50,justifyContent:"center",alignItems:"center"}} onPress={() => this.renderStrengthTooltip1(portfolio_quality,risk_volatility,corelation_becnhmark,asset_type)}>
                <Image source={require('../../assets/images/very_low_strength.png')} 
                style={{width: 30, height:30,resizeMode:"contain",alignItems:"center"}}                   />
        </TouchableOpacity>
            );
        }else if(riskscore === "1"){
            return (
                <TouchableOpacity style={{height:50,justifyContent:"center",alignItems:"center"}} onPress={() => this.renderStrengthTooltip1(portfolio_quality,risk_volatility,corelation_becnhmark,asset_type)}>
                <Image source={require('../../assets/images/low_strength.png')}
                style={{width: 30, height:30,resizeMode:"contain",alignItems:"center"}}   /></TouchableOpacity>
                );
        }else if(riskscore === "2"){
            return (
                <TouchableOpacity style={{height:50,justifyContent:"center",alignItems:"center"}} onPress={() => this.renderStrengthTooltip1(portfolio_quality,risk_volatility,corelation_becnhmark,asset_type)}>
            <Image source={require('../../assets/images/moderate_strength.png')}
            style={{width: 30, height:30,resizeMode:"contain",alignItems:"center"}}   />
            </TouchableOpacity>
                );
        }else if(riskscore === "3"){
            return (
                <TouchableOpacity style={{height:50,justifyContent:"center",alignItems:"center"}} onPress={() => this.renderStrengthTooltip1(portfolio_quality,risk_volatility,corelation_becnhmark,asset_type)}>
            <Image
            source={require('../../assets/images/high_strength.png')}
            style={{width: 30, height:30,resizeMode:"contain",alignItems:"center"}}   />
           </TouchableOpacity>     );
        }else if(riskscore === "4"){
            return (
                <TouchableOpacity style={{height:50,justifyContent:"center",alignItems:"center"}} onPress={() => this.renderStrengthTooltip1(portfolio_quality,risk_volatility,corelation_becnhmark,asset_type)}>
            <Image
            source={require('../../assets/images/very_high_strength.png')}
            style={{width: 30, height:30,resizeMode:"contain",alignItems:"center"}}      />
              </TouchableOpacity>  );
        }
    }else{
        return (
            <TouchableOpacity style={{height:50,justifyContent:"center",alignItems:"center"}} onPress={() => this.renderStrengthTooltip1(portfolio_quality,risk_volatility,corelation_becnhmark,asset_type)}> 
        <Image
        style={{width:40, height: 40,resizeMode:"contain",alignItems:"center"}} 
            source={ require('../../assets/images/strength.png') }
        /></TouchableOpacity>);
    }
 }

 renderStrengthTooltip(portfolio_quality,risk_volatility,corelation_becnhmark,asset_type){

    const arr =[dislike,like];
    const arr1=[color.RED,color.GREEN];

    if(asset_type != "Debt"){
        return(
            <View style={{padding:15,justifyContent:"center",marginTop:15}}>
     
                <View style={{flexDirection:"row"}}>
                   <View style={[styles.circle,{backgroundColor:arr1[risk_volatility],alignItems:"center",height:30,width:30}]}>
                        <Image
                        style={{width:15, height: 15,resizeMode:"contain",alignItems:"center",marginTop:9}} 
                            source={arr[risk_volatility]}
                        />
                        </View>
                        <Text style={[styles.item,{marginLeft:15}]}>Resilience in volatility</Text>
                </View>
                <View style={{flexDirection:"row",marginTop:10}}>
                   <View style={[styles.circle,{backgroundColor:arr1[corelation_becnhmark],alignItems:"center",height:30,width:30}]}>
                        <Image
                        style={{width:15, height: 15,resizeMode:"contain",alignItems:"center",marginTop:9}} 
                            source={arr[corelation_becnhmark]}
                        />
                        </View>
                        <Text style={[styles.item,{marginLeft:15,flexGrow:1}]}>Ability to generate higher return at lower risk</Text>
                </View>
                { asset_type === "Equity" || asset_type === "Hybrid"    &&  <View style={{flexDirection:"row",marginTop:10}}>
                   <View style={[styles.circle,{backgroundColor:arr1[portfolio_quality],alignItems:"center",height:30,width:30}]}>
                        <Image
                        style={{width:15, height: 15,resizeMode:"contain",alignItems:"center",marginTop:9}} 
                            source={arr[portfolio_quality]}
                        />
                        </View>
                        <Text style={[styles.item,{marginLeft:15}]}>Quality of Portfolio</Text>
                </View>}
            </View>
        );
    }else{
        return(
            <View style={{padding:5}}>
     
     <View style={{flexDirection:"row"}}>
                   <View style={[styles.circle,{backgroundColor:arr1[portfolio_quality],alignItems:"center",height:30,width:30}]}>
                        <Image
                        style={{width:15, height: 15,resizeMode:"contain",alignItems:"center",marginTop:9}} 
                            source={arr[portfolio_quality]}
                        />
                        </View>
                        <Text style={[styles.item,{marginLeft:15}]}>Quality of Portfolio</Text>
                </View>
                <View style={{flexDirection:"row",marginTop:10}}>
                   <View style={[styles.circle,{backgroundColor:arr1[corelation_becnhmark],alignItems:"center",height:30,width:30}]}>
                        <Image
                        style={{width:15, height: 15,resizeMode:"contain",alignItems:"center",marginTop:9}} 
                            source={arr[corelation_becnhmark]}
                        />
                        </View>
                        <Text style={[styles.item,{marginLeft:15}]}>Interest Rate Risk</Text>
                </View>
                
            </View>
        );
    }
 }

 renderStrengthTooltip1 = (portfolio_quality,risk_volatility,corelation_becnhmark,asset_type) => {
    this.setState({portfolio_quality:portfolio_quality,risk_volatility:risk_volatility,corelation_becnhmark:corelation_becnhmark,asset_type:asset_type})
    setTimeout(() => {
        this.slideAnimationDialogStrength.show();
    }, 300);
    
    
 }

 renderFundTooltip = (fund_tooltip,fundimage) => {
    this.setState({fund_tooltip:fund_tooltip,fundimage:fundimage})
    setTimeout(() => {
        this.slideAnimationDialogFund.show();
    }, 300);
    
    
 }

 renderTimeTooltip = (time_tooltip) => {
    this.setState({time_tooltip:time_tooltip})
    setTimeout(() => {
        this.slideAnimationDialogTime.show();
    }, 300);
    
    
 }
 
 renderRatingTooltip = (rating_tooltip) =>{
    this.setState({rating_tooltip:rating_tooltip})
    setTimeout(() => {
        this.slideAnimationDialogRating.show();
    }, 300);
 }

  renderRow(item,index){
   
      return(<Card style={{width:DEVICE_WIDTH/2 -20}}>
          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:120}]} onPress={()=> Actions.page1({unique_code:item.Unique_No,from:"CompareFund"})}>{item.Scheme_Name}</Text>                        
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                         <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:50}]}>{item.rank_srno}</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                        <TouchableOpacity style={{height:50,justifyContent:"center"}}  onPress={() => this.renderRatingTooltip(cleantext(item.rating_tooltip))} >
                          <StarRating       
                          containerStyle={{width:DEVICE_WIDTH/3,alignItems:"center",alignSelf:"center"}}                        
                                fullStar= {require('../../assets/images/star.png')}
                                emptyStar= {require('../../assets/images/emptystar.png')}
                                disabled={true}
                                maxStars={5}
                                starSize={18}
                                halfStarEnabled={false}
                                rating={item.rating} /> 
                         </TouchableOpacity>
                        <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                         {this.renderfundimage(item.rating,item.fund_tooltip,1)}   
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                        {this.rendertimeimage(item.time_flag,item.time_tooptip)}   
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                           <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:50}]}>{item.Navrs}</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:50}]}>{item.asset_type}</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:70}]}>{item.classname}</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:50}]}>{item.option}</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:50}]}>{item.return_3m}</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:50}]}>{item.return_6m}</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:50}]}>{item.return_1y}</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:50}]}>{item.return_3y}</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:50}]}>{item.return_5y}</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:50}]}>{item.return_10y}</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                         {this.renderStrength(item.risk_score,item.portfolio_quality,item.risk_volatility,item.corelation_becnhmark,item.asset_type)}
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:50}]}>{item.exp_ratio}</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:70}]}>{item.EXITLOAD ? item.EXITLOAD : 'NIL'}</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:50}]}>{item.turnover_perc}</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"center",height:100}]}>{item.amc}</Text>
                          <TouchableOpacity  style={[styles.button,{backgroundColor:color.GREEN,marginLeft:15,marginRight:15,marginBottom:15}]} >
                        <Text style={[styles.instructions,{fontSize:Platform.OS == 'ios'? 15:12}]}>Invest Now</Text>
                    </TouchableOpacity>
                        
      </Card>);
  }

  renderRowLine(item,index){
    return(
        <View style={[styles.CompareFundsBadge,{flexDirection:"row",alignSelf:"flex-start",marginRight:10,marginLeft:10}]}>
        <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:13,marginLeft:10,width:0,flexGrow:1}]}>{this.state.selected_names[index]}</Text>
    <TouchableOpacity style={{marginLeft:5,marginTop:2,marginRight:10}}
        onPress={() => this.removeScheme(index,item)}>
        <Image source={require('../../assets/images/cross.png')} style={{height:10,width:10,resizeMode:"contain"}}/>
        </TouchableOpacity>
    </View>
    );
  }
  
  onViewableItemsChanged({viewableItems, changed}) {
    //console.log('viewableItems', viewableItems)
    //console.log('changed', changed)
}

viewabilityConfig = {viewAreaCoveragePercentThreshold: 50}
  

  render() {

    var Highcharts='Highcharts';

    var conf={
        chart: {
            type: 'spline',
            animation: Highcharts.svg, // don't animate in old IE
            marginRight: 10,
            events: {
                load: function () {
                   
                }
            }
        },
        title: {
            text: ''
        },
        xAxis: {
          type: 'datetime',
          dateTimeLabelFormats: { 
          month: '%b %y',
          },
          tickPixelInterval: 50
        },
        yAxis: {
            title:'',
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        credits: {
            enabled: false
        },
        tooltip: {
          headerFormat: '<b>{series.name}</b><br>',
          pointFormat: '{point.x:%e %b %y }: {point.y:.2f} '
        },
        plotOptions: {
          spline: {
              marker: {
                  enabled: false
              },
              
          },
          
      },
        legend: {
            enabled: true
        },
        exporting: {
            enabled: false
        },
        series: this.state.chartdata
    };

    const options = {
        global: {
            useUTC: false
        },
        lang: {
            decimalPoint: '.',
            thousandsSep: ','
        },
       
    };

    return (

        
      <View >
      <ScrollView>
      <View style={[{paddingLeft:10,paddingRight:10,backgroundColor:color.WHITE}]}>
         <Text style={{fontSize:23,fontWeight:"bold",fontFamily:"Roboto",marginTop:20}}>Compare Funds</Text>
         <Card style={{marginTop:20}}>
            <Text style={[styles.item,{marginLeft:10,marginTop:15}]}>Equity</Text>
            <View style={{flexDirection:"column"}}>
          

             {this.state.selected_names.length>0 ? 
     ( <FlatList    scrollEnabled={false}
      showsVerticalScrollIndicator={false}
                    style={{marginTop:10,marginBottom:10,height:(this.state.selected_names.length*60),minHeight:(this.state.selected_names.length*60)}}                   
                    data={this.state.selected_names}
                    renderItem={( {item,index }) => this.renderRowLine(item,index) }
                    
            />) : null
      }
            
           {this.state.selected_codes.length < 3 && <TouchableOpacity style={{alignSelf:"flex-start",width:DEVICE_WIDTH/2,marginLeft:10}}  onPress={this.showSlideAnimationDialog}>
           
               <Image source={require('../../assets/images/add-find-chip.png')} style={{width:DEVICE_WIDTH/2,height:60,resizeMode:"contain"}}/>
               
               
            </TouchableOpacity>
           }
           </View>
           </Card>
           {this.state.selected_codes.length >= 1 &&  <View><Card>
           <Text style={[styles.item,{marginLeft:10,marginTop:15}]} onPress={() => this.add()}>Growth of {'\u20B9'}10000</Text>
           <View style={{backgroundColor:color.LIGHT_GREY,margin:10}}>
           <Text style={[styles.RankBadgeText,{color:color.BLACK,fontSize:13,marginLeft:10}]}>How your investments would have grown in </Text>
           <View style={{flexDirection:"row",marginBottom:10}}>
            <TouchableHighlight style={[styles.CompareFundsBadge,{flex:0.5,backgroundColor:this.state.popcolor[0],marginLeft:10,borderRadius:20}]} onPress={() => this.ChangeYear("1",0)}>   
                 <Text style={[styles.RankBadgeText,{color:this.state.poptext[0]}]}>1Y</Text>                
            </TouchableHighlight>
            <TouchableHighlight style={[styles.CompareFundsBadge,{flex:0.5,backgroundColor:this.state.popcolor[1],marginLeft:10,borderRadius:20}]} onPress={() => this.ChangeYear("2",1)}>   
                 <Text style={[styles.RankBadgeText,{color:this.state.poptext[1]}]}>2Y</Text>                
            </TouchableHighlight>
            <TouchableHighlight style={[styles.CompareFundsBadge,{flex:0.5,backgroundColor:this.state.popcolor[2],marginLeft:10,borderRadius:20}]} onPress={() => this.ChangeYear("3",2)}>   
                 <Text style={[styles.RankBadgeText,{color:this.state.poptext[2]}]}>3Y</Text>                
            </TouchableHighlight>
            <TouchableHighlight style={[styles.CompareFundsBadge,{flex:0.5,backgroundColor:this.state.popcolor[3],marginLeft:10,borderRadius:20}]} onPress={() => this.ChangeYear("4",3)}>   
                 <Text style={[styles.RankBadgeText,{color:this.state.poptext[3]}]}>4Y</Text>                
            </TouchableHighlight>
            <TouchableHighlight style={[styles.CompareFundsBadge,{flex:0.5,backgroundColor:this.state.popcolor[4],marginLeft:10,borderRadius:20}]} onPress={() => this.ChangeYear("5",4)}>   
                 <Text style={[styles.RankBadgeText,{color:this.state.poptext[4]}]}>5Y</Text>                
            </TouchableHighlight>
            <TouchableHighlight style={[styles.CompareFundsBadge,{flex:1,backgroundColor:this.state.popcolor[5],marginLeft:10,borderRadius:20,width:0,marginRight:10}]} onPress={() => this.ChangeYear("inc",5)}>   
                 <Text style={[styles.RankBadgeText,{color:this.state.poptext[5]}]}>Inception</Text>                
            </TouchableHighlight>
            </View>
           </View>
           <View style={styles.itemchart}>
            <ChartView style={{height:300}} config={conf} options={options}></ChartView>
            
          </View>
           </Card>

            <Card containerStyle={{marginTop:15,flexDirection:"column"}}>
            
                <View style={{flexDirection:"row"}}>
                    <View style={{margin:5,backgroundColor:color.LIGHT_BLUE,flex:1,flexDirection:"column",marginTop:this.state.marginTop}}>
                           <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:120}]}>Scheme</Text>                        
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:50}]}>Rank</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:50}]}>Rating</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:50}]}>Fund Sahi Hai?</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:70,flexGrow:1}]}>Time to Invest Sahi Hai?</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:50}]}>NAV</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:50}]}>Asset Class</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:70}]}>Category</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:50}]}>Sub Category</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:50}]}>3 Month Return</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:50}]}>6 Month Return</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:50}]}>1 Year Return</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:50}]}>3 Year Return</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:50}]}>5 Year Return</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:50}]}>10 Year Return</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:50}]}>Strength</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:50}]}>Expense Ratio</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:70}]}>Exit Load</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:50}]}>Portfolio Turnover</Text>
                         <View style={[styles.dash,{backgroundColor:color.compareline}]}/>
                          <Text style={[styles.RankBadgeText,{color:color.toolbar,fontSize:15,fontWeight:"700",padding:10,textAlign:"left",height:100}]}>Fund House</Text>
                            <View style={{height:58,backgroundColor:color.WHITE}}></View>
                    </View>

                      <View  style={{flex:1,marginRight:5}}>
                      <Pagination
                     
                  dotsLength={this.state.schemedata.length}
                  activeDotIndex={this.state.slider1ActiveSlide}
                  containerStyle={styles.paginationContainer}
                  dotColor={color.toolbar}
                  dotStyle={styles.paginationDot}
                  inactiveDotColor={color.BLACK}
                  carouselRef={this._slider1Ref}
                  tappableDots={!!this._slider1Ref}
                />
                      <Carousel
                     
                  ref={c => this._slider1Ref = c}
                  data={this.state.schemedata}
                  renderItem={( {item,index }) =>  this.renderRow(item,index)}
                  sliderWidth={DEVICE_WIDTH/2 -10}
                  itemWidth={DEVICE_WIDTH/2 -10}
                  inactiveSlideScale={0.94}
                  firstItem={SLIDER_1_FIRST_ITEM}
                  inactiveSlideOpacity={0.7}
                  // inactiveSlideShift={20}
                  onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
                />
                
                  </View>    
                </View>
                
            </Card>
            </View>
        }
        </View>
        </ScrollView>
      <PopupDialog
       dialogStyle={{width:300, position: 'absolute', top: 50}}
          dialogTitle={<DialogTitle title="Search Fund" />}
          ref={(popupDialog) => {
            this.slideAnimationDialog = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
         
          <Autocomplete
          autoCapitalize="none"
          autoCorrect={false}
          clearTextOnFocus={true}
          containerStyle={{borderBottomColor:(Platform.OS=="ios"?"black":null),borderBottomWidth:(Platform.OS=="ios"?1:0),textAlign:"center",margin:10}}
          data={this.state.categoryfunddata}
          listContainerStyle={{position:"relative"}}
          onChangeText={(text) => this.searchCategoryFund(text)}
          placeholder="Search"
          defaultValue=""
          renderItem={( { text,id,asset_type } ) => this.renderAutoComplete(text,id,asset_type)} 
        />
         
        </PopupDialog>
        <PopupDialog
            dialogStyle={{width:"70%",height:"40%", position: 'absolute', top: 100}}
          ref={(popupDialog) => {
            this.slideAnimationDialogFund = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
        <View style={{flexDirection:"column",justifyContent:"center",alignItems:"center",padding:10}}>
          {this.state.fundimage >= 3 ?
        <View style={{height:50,justifyContent:"center",alignItems:"center",marginTop:15}}>
          <View style={[styles.circle,{backgroundColor:color.GREEN,alignItems:"center"}]} >
              <Image
              style={{width: 20, height: 20,resizeMode:"contain",alignItems:"center",marginTop:9}} 
                  source={ like }
              />
      </View>
      </View> :
         
            <View style={{height:50,justifyContent:"center",alignItems:"center",marginTop:15}} >
              <View style={[styles.circle,{backgroundColor:color.RED,alignItems:"center"}]}>
                  <Image
                  style={{width: 20, height: 20,resizeMode:"contain",alignItems:"center",marginTop:9}} 
                      source={ dislike }
                  />
          </View>
          </View>
              
      }
  
        
        
          <Text style={[styles.item,{marginTop:10}]}>{this.state.fund_tooltip}</Text>
        
          </View>
        </PopupDialog>
        <PopupDialog
            dialogStyle={{width:"80%",height:"40%", position: 'absolute', top: 100}}
          ref={(popupDialog) => {
            this.slideAnimationDialogTime = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
         
          <Text style={[styles.item,{padding:10}]}>{this.state.time_tooltip}</Text>
         
        </PopupDialog>
        <PopupDialog
       dialogStyle={{width:"80%",height:"40%", position: 'absolute', top: 100}}
          ref={(popupDialog) => {
            this.slideAnimationDialogRating = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
         
          <Text style={[styles.item,{padding:10}]}>{this.state.rating_tooltip}</Text>
         
        </PopupDialog>
        <PopupDialog
       dialogStyle={{width:"80%",height:"40%", position: 'absolute', top: 100}}
          ref={(popupDialog) => {
            this.slideAnimationDialogStrength = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
         
          {this.renderStrengthTooltip(this.state.portfolio_quality,this.state.risk_volatility,this.state.corelation_becnhmark,this.state.asset_type)}
         
        </PopupDialog>
      </View>
    );
  }
}

export default CompareFunds;
