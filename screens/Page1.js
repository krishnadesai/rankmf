import React, { Component, Props } from "react";
import {
  Animated,
  Dimensions,
  Platform,
  Text,
  View,
  Image,
  TouchableOpacity,
  StatusBar,
  Picker,
  ScrollView,
  AsyncStorage,
  BackHandler
} from "react-native";
import {
  Body,
  Header,
  List,
  ListItem as Item,
  ScrollableTab,
  Tab,
  TabHeading,
  Tabs,
  Title
} from "native-base";
import { Actions } from "react-native-router-flux";
import * as Animatable from "react-native-animatable";
import PopupDialog, {
  DialogTitle,
  SlideAnimation
} from "react-native-popup-dialog";
import styles from "../components/styles";
import Overview from "../screens/QuotePage/OverView";
import Performance from "../screens/QuotePage/Performance";
import Sip from "../screens/QuotePage/SIP";
import Portfolio from "../screens/QuotePage/Portfolio";
import RiskStrength from "../screens/QuotePage/RiskStrength";
import Manager from "../screens/QuotePage/Manager_other";
import StarRating from "react-native-star-rating";
import CustomToast from "../components/CustomToast";
import color from "../constants/Colors";
import Apis from "../constants/Apis";
const like = require("../assets/images/like.png");
const dislike = require("../assets/images/dislike.png");
const thumbsup = require("../assets/images/thumbsup.png");
const thumbsdown = require("../assets/images/thumbsdown.png");

const email = "",
  password = "";
const slideAnimation = new SlideAnimation({ slideFrom: "bottom" });
const { width: SCREEN_WIDTH } = Dimensions.get("window");
const IMAGE_HEIGHT = Dimensions.get("window").height / 1.15;
const HEADER_HEIGHT = Platform.OS === "ios" ? 70 : 50;
const SCROLL_HEIGHT = Platform.select({
  ios: Dimensions.get("window").height - 140,
  android: Dimensions.get("window").height - 110
});
const THEME_COLOR = color.toolbar;
const FADED_THEME_COLOR = color.toolbar;
const TRANSPARENT = "rgba(255, 255, 255, 0)";
const regex = /(<([^>]+)>)/gi;
function cleantext(strInputCode) {
  cleanText = strInputCode.replace("<ul><li>", "\u2022 ");
  cleanText = cleanText.replace("</li><li>", "\n\n\u2022 ");
  cleanText = cleanText.replace("</li></ul>", "");
  return cleanText;
}
const user_acc_id = "",
  client_id = "",
  clienttype = "";

class Page1 extends Component {
  nScroll = new Animated.Value(0);
  scroll = new Animated.Value(0);

  textColor = this.scroll.interpolate({
    inputRange: [0, SCROLL_HEIGHT / 5, SCROLL_HEIGHT],
    outputRange: [THEME_COLOR, FADED_THEME_COLOR, "white"],
    extrapolate: "clamp"
  });
  tabBg = this.scroll.interpolate({
    inputRange: [0, SCROLL_HEIGHT],
    outputRange: ["white", THEME_COLOR],
    extrapolate: "clamp"
  });
  tabY = this.nScroll.interpolate({
    inputRange: [0, SCROLL_HEIGHT, SCROLL_HEIGHT + 1],
    outputRange: [0, 0, 1]
  });
  headerBg = this.scroll.interpolate({
    inputRange: [0, SCROLL_HEIGHT, SCROLL_HEIGHT + 1],
    outputRange: [TRANSPARENT, THEME_COLOR, THEME_COLOR],
    extrapolate: "clamp"
  });
  imgScale = this.nScroll.interpolate({
    inputRange: [-25, 0],
    outputRange: [1.1, 1],
    extrapolateRight: "clamp"
  });
  imgOpacity = this.nScroll.interpolate({
    inputRange: [0, SCROLL_HEIGHT],
    outputRange: [1, 0]
  });
  tabContent = (x, i) => (
    <View style={{ height: this.state.height }}>
      <List
        onLayout={({
          nativeEvent: {
            layout: { height }
          }
        }) => {
          this.heights[i] = height;
          if (this.state.activeTab === i)
            this.setState({ height: this.heights[i] });
        }}
      >
        {new Array(x).fill(null).map((_, i) => (
          <Item key={i}>
            <Text>Item {i}</Text>
          </Item>
        ))}
      </List>
    </View>
  );
  heights = [500, 500];

  state = {
    activeTab: 0,
    height: 500
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      response: {},
      value: 18,
      fundimage: true,
      investimage: false,
      starCount: 2.5,
      error: null,
      emailtext: null,
      pwd: null,
      hideNavBar: false,
      hideDrawerButton: true,
      mfname: "",
      loading: false,
      showFund: true,
      showRate: false,
      showInvest: false,
      showStrength: false,
      highLow52Week: {},
      watchlist: false,
      from: "",
      myRequestedRefs: "",
      initpriced: "1 year",
      throttlemode: "1 year",
      nav: "1YRNAV",
      chartyr: "1",
      opacity: 0,
      line: "1 year",
      bar1: "Month End",
      year: "Financial",
      chartyrper: "1",
      performance: "5 years",
      dateYear: "5",
      per_period: "m_end",
      period_year: "financial",
      sipperiod: "5",
      responsenew: {},
      scheme: {},
      data1:{}
    };

    this.nScroll.addListener(
      Animated.event([{ value: this.scroll }], { useNativeDriver: false })
    );
  }

  onAndroidBackPress = () => {
    this.redirect();
  };

  componentWillMount() {
    if (Platform.OS === "android") {
      BackHandler.addEventListener(
        "hardwareBackPress",
        this.onAndroidBackPress
      );
    }
  }

  componentWillUnmount() {
    if (Platform.OS === "android") {
      BackHandler.removeEventListener("hardwareBackPress");
    }
  }

  toggleNavBar = () => {
    this.setState(
      prevState => ({ hideNavBar: !prevState.hideNavBar }),
      () => Actions.refresh({ hideNavBar: this.state.hideNavBar })
    );
  };

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      if (clienttype === "client") {
        this.setState({ showFund: true });
      } else {
        this.setState({ showFund: false });
      }

      if (clienttype === "client") {
        client_id = clientid;
      } else if (clienttype === "lead") {
        user_acc_id = clientid;
      }
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  componentDidMount() {
    this.toggleNavBar();
    this.retrieveData();
    
    // this.props.navigation.addListener(
    //     'didFocus',
    //     payload => {
    //         this.toggleNavBar();
    //     }
    //   );

    // BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  
  fetchDataLedger() {
    // alert(this.props.unique_code);
    fetch(Apis.Mf_order, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&access_token=${access_token}&user=${clientid}&client_id=${clientid}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson["status"] === "success"){
             this.setState({data1:responseJson["data"]})             
             this._storeData()
        }
     //   alert(this.state.data.ledger_balance)
      })
      .catch(error => {
        console.error(error);
      });
  }


  fetchData() {
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.2&access_token=${access_token}&user=${client_id}&unique_code=${
        this.props.unique_code
      }&type=nav` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson["status"] === "Success") {
          this.setState({
            highLow52Week: responseJson["data"]["highLow52Week"][0],
            watchlist: this.props.watchlist
          });

          this.setState({
            response: responseJson["data"]["aggregate"],
            from: this.props.from,
            responsenew: responseJson["data"]["response"][0],
            scheme: responseJson["data"]["scheme"]
          });
          //  alert(this.state.highLow52Week["52WeekhHigh"])
          if (
            this.props.from === "CompareFund" ||
            this.props.from === "Search"
          ) {
            this.fetchWatchlist();
          }
          this.setState({ isLoading: false, opacity: 1 });
          this.fetchDataLedger();
          //this._storeData();
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  _storeData = async () => {
   
    try {
            
            await AsyncStorage.setItem('Purchase_Allowed', this.state.response.Purchase_Allowed);
            await AsyncStorage.setItem('SIP_FLAG', this.state.response.SIP_FLAG);
            await AsyncStorage.setItem('bank_allowed', this.state.data1.bank_allowed+"");
            await AsyncStorage.setItem('atom_allowed', this.state.data1.atom_allowed+"");
            await AsyncStorage.setItem('ledger_balance',this.state.data1.ledger_balance+"");
    } catch (error) {
      // Error saving data
     // alert(1)
    }
  }

  fetchWatchlist() {
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.7&access_token=${access_token}&user=${client_id}&client_id=${client_id}&lead_id=${user_acc_id}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        responseJson["data"]["shortlistedSchemes"].map(item1 => {
          if (item1.Unique_No === this.props.unique_code) {
            this.setState({ watchlist: true });
          }
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  addtoWatchlist(schemeid, uniqueno, client_id, user_acc_id) {
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.5&access_token=${access_token}&user=${client_id}&client_id=${clientid}&type=add&schemeid=${schemeid}&uniqueno=${uniqueno}&client_id=${client_id}&user_acc_id=${user_acc_id}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson["data"]["watchlist"].status === "success") {
          this.refs.success.ShowToastFunction("Added to Watchlist");
        } else {
          this.refs.error.ShowToastFunction(
            responseJson["data"]["watchlist"].msg
          );
        }
        this.setState({ watchlist: true });
      })
      .catch(error => {
        console.error(error);
      });
  }

  RemoveWatchlist(schemeid, uniqueno, client_id, user_acc_id) {
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.5&access_token=${access_token}&user=${client_id}&client_id=${clientid}&type=remove&schemeid=${schemeid}&uniqueno=${uniqueno}&client_id=${client_id}&user_acc_id=${user_acc_id}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson["data"]["watchlist"].status === "success") {
          this.refs.error.ShowToastFunction("Removed from Watchlist");
        } else {
          this.refs.error.ShowToastFunction(
            responseJson["data"]["watchlist"].msg
          );
        }
        this.setState({ watchlist: false });
      })
      .catch(error => {
        console.error(error);
      });
  }

  getVal(val) {
    console.warn(val);
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
    // console.warn(rating);
  }

  renderfundimage(fundimage) {
    if (clienttype === "client") {
      if (fundimage >= 3) {
        return (
          <Image
            style={{
              width: Platform.OS === "ios" ? 40 : 30,
              height: Platform.OS === "ios" ? 40 : 30,
              resizeMode: "contain",
              alignItems: "center",
              marginTop: 9
            }}
            source={like}
          />
        );
      } else {
        return (
          <Image
            style={{
              width: Platform.OS === "ios" ? 40 : 30,
              height: Platform.OS === "ios" ? 40 : 30,
              resizeMode: "contain",
              alignItems: "center",
              marginTop: 9
            }}
            source={dislike}
          />
        );
      }
    } else {
      return (
        <Image
          style={{
            width: 40,
            height: 40,
            resizeMode: "contain",
            alignItems: "center",
            marginTop: 10
          }}
          source={require("../assets/images/fund-lock.png")}
        />
      );
    }
  }

  renderinvestimage(timeimage) {
    if (clienttype === "client") {
      if (timeimage === "1") {
        return (
          <Image
            style={{
              width: Platform.OS === "ios" ? 40 : 30,
              height: Platform.OS === "ios" ? 40 : 30,
              resizeMode: "contain",
              alignItems: "center",
              marginTop: 9
            }}
            source={like}
          />
        );
      } else {
        return (
          <Image
            style={{
              width: Platform.OS === "ios" ? 40 : 30,
              height: Platform.OS === "ios" ? 40 : 30,
              resizeMode: "contain",
              alignItems: "center",
              marginTop: 10
            }}
            source={dislike}
          />
        );
      }
    } else {
      return (
        <Image
          style={{
            width: 40,
            height: 40,
            resizeMode: "contain",
            alignItems: "center",
            marginTop: 10
          }}
          source={require("../assets/images/time-to-invest-lock.png")}
        />
      );
    }
  }

  renderColor = item => {
    if (item === "1") {
      return color.Equity;
    } else if (item === "2") {
      return color.Hybrid;
    } else if (item === "3") {
      return color.Debt;
    } else if (item === "4") {
      return color.Commodity;
    } else if (item === "5") {
      return color.Other;
    }
  };

  renderStrength(riskscore) {
    if (clienttype === "client") {
      if (riskscore === "0") {
        return (
          <Image
            source={require("../assets/images/very_low_strength.png")}
            style={{
              width: Platform.OS === "ios" ? 40 : 30,
              height: Platform.OS === "ios" ? 40 : 30,
              resizeMode: "contain",
              alignItems: "center",
              marginTop: 9
            }}
          />
        );
      } else if (riskscore === "1") {
        return (
          <Image
            source={require("../assets/images/low_strength.png")}
            style={{
              width: Platform.OS === "ios" ? 40 : 30,
              height: Platform.OS === "ios" ? 40 : 30,
              resizeMode: "contain",
              alignItems: "center",
              marginTop: 9
            }}
          />
        );
      } else if (riskscore === "2") {
        return (
          <Image
            source={require("../assets/images/moderate_strength.png")}
            style={{
              width: Platform.OS === "ios" ? 40 : 30,
              height: Platform.OS === "ios" ? 40 : 30,
              resizeMode: "contain",
              alignItems: "center",
              marginTop: 9
            }}
          />
        );
      } else if (riskscore === "3") {
        return (
          <Image
            source={require("../assets/images/high_strength.png")}
            style={{
              width: Platform.OS === "ios" ? 40 : 30,
              height: Platform.OS === "ios" ? 40 : 30,
              resizeMode: "contain",
              alignItems: "center",
              marginTop: 9
            }}
          />
        );
      } else if (riskscore === "4") {
        return (
          <Image
            source={require("../assets/images/very_high_strength.png")}
            style={{
              width: Platform.OS === "ios" ? 40 : 30,
              height: Platform.OS === "ios" ? 40 : 30,
              resizeMode: "contain",
              alignItems: "center",
              marginTop: 9
            }}
          />
        );
      }
    } else {
      return (
        <Image
          style={{
            width: 40,
            height: 40,
            resizeMode: "contain",
            alignItems: "center",
            marginTop: 10
          }}
          source={require("../assets/images/strength.png")}
        />
      );
    }
  }

  renderStrengthTooltip(
    portfolio_quality,
    risk_volatility,
    corelation_becnhmark,
    asset_type
  ) {
    const arr = [thumbsdown, thumbsup];
    const arr1 = [color.RED, color.GREEN];

    if (asset_type != "Debt") {
      return (
        <View style={{ padding: Platform.OS === "ios" ? 5 : 3 }}>
          <View style={{ flexDirection: "row" }}>
            <View
              style={[
                styles.circle,
                {
                  backgroundColor: arr1[risk_volatility],
                  alignItems: "center",
                  height: 20,
                  width: 20
                }
              ]}
            >
              <Image
                style={{
                  width: 10,
                  height: 10,
                  resizeMode: "contain",
                  alignItems: "center",
                  marginTop: 5
                }}
                source={arr[risk_volatility]}
              />
            </View>
            <Text style={[styles.flexboxtext, { marginLeft: 15 }]}>
              Resilience in volatility
            </Text>
          </View>
          <View style={{ flexDirection: "row", marginTop: 10 }}>
            <View
              style={[
                styles.circle,
                {
                  backgroundColor: arr1[corelation_becnhmark],
                  alignItems: "center",
                  height: 20,
                  width: 20
                }
              ]}
            >
              <Image
                style={{
                  width: 10,
                  height: 10,
                  resizeMode: "contain",
                  alignItems: "center",
                  marginTop: 5
                }}
                source={arr[corelation_becnhmark]}
              />
            </View>
            <Text style={[styles.flexboxtext, { marginLeft: 15 }]}>
              Ability to generate higher return at lower risk
            </Text>
          </View>
          {asset_type === "Equity" ||
            (asset_type === "Hybrid" && (
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <View
                  style={[
                    styles.circle,
                    {
                      backgroundColor: arr1[portfolio_quality],
                      alignItems: "center",
                      height: 20,
                      width: 20
                    }
                  ]}
                >
                  <Image
                    style={{
                      width: 10,
                      height: 10,
                      resizeMode: "contain",
                      alignItems: "center",
                      marginTop: 5
                    }}
                    source={arr[portfolio_quality]}
                  />
                </View>
                <Text style={[styles.flexboxtext, { marginLeft: 15 }]}>
                  Quality of Portfolio
                </Text>
              </View>
            ))}
        </View>
      );
    } else {
      return (
        <View style={{ padding: 5 }}>
          <View style={{ flexDirection: "row" }}>
            <View
              style={[
                styles.circle,
                {
                  backgroundColor: arr1[portfolio_quality],
                  alignItems: "center",
                  height: 20,
                  width: 20
                }
              ]}
            >
              <Image
                style={{
                  width: 10,
                  height: 10,
                  resizeMode: "contain",
                  alignItems: "center",
                  marginTop: 5
                }}
                source={arr[portfolio_quality]}
              />
            </View>
            <Text style={[styles.flexboxtext, { marginLeft: 15 }]}>
              Quality of Portfolio
            </Text>
          </View>
          <View style={{ flexDirection: "row", marginTop: 10 }}>
            <View
              style={[
                styles.circle,
                {
                  backgroundColor: arr1[corelation_becnhmark],
                  alignItems: "center",
                  height: 20,
                  width: 20
                }
              ]}
            >
              <Image
                style={{
                  width: 10,
                  height: 10,
                  resizeMode: "contain",
                  alignItems: "center",
                  marginTop: 5
                }}
                source={arr[corelation_becnhmark]}
              />
            </View>
            <Text style={[styles.flexboxtext, { marginLeft: 15 }]}>
              Interest Rate Risk
            </Text>
          </View>
        </View>
      );
    }
  }

  updatestateFund() {
    if (clienttype === "client") {
      if (this.state.showFund === true) {
        this.setState({ showFund: false });
      } else {
        this.setState({ showFund: true });
        if (this.state.showRate === true) {
          this.setState({ showRate: false });
        }
        if (this.state.showInvest === true) {
          this.setState({ showInvest: false });
        }
        if (this.state.showStrength === true) {
          this.setState({ showStrength: false });
        }
      }
    }
  }

  updatestateRate() {
    if (clienttype === "client") {
      if (this.state.showRate === true) {
        this.setState({ showRate: false });
      } else {
        this.setState({ showRate: true });
        if (this.state.showFund === true) {
          this.setState({ showFund: false });
        }
        if (this.state.showInvest === true) {
          this.setState({ showInvest: false });
        }
        if (this.state.showStrength === true) {
          this.setState({ showStrength: false });
        }
      }
    }
  }

  updatestateInvest() {
    if (clienttype === "client") {
      if (this.state.showInvest === true) {
        this.setState({ showInvest: false });
      } else {
        this.setState({ showInvest: true });
        if (this.state.showFund === true) {
          this.setState({ showFund: false });
        }
        if (this.state.showRate === true) {
          this.setState({ showRate: false });
        }
        if (this.state.showStrength === true) {
          this.setState({ showStrength: false });
        }
      }
    }
  }

  updatestateStrength() {
    if (clienttype === "client") {
      if (this.state.showStrength === true) {
        this.setState({ showStrength: false });
      } else {
        this.setState({ showStrength: true });
        if (this.state.showFund === true) {
          this.setState({ showFund: false });
        }
        if (this.state.showInvest === true) {
          this.setState({ showInvest: false });
        }
        if (this.state.showRate === true) {
          this.setState({ showRate: false });
        }
      }
    }
  }

  componentWillReceiveProps() {
    setTimeout(() => {
      if (this.props.dialog === "nav") {
        this.showSlideAnimationDialogNav();
      } else if (this.props.dialog === "OverViewGro") {
        this.showSlideAnimationDialogOverViewGro();
      } else if (this.props.dialog === "line") {
        this.showSlideAnimationDialogPerGrow();
      } else if (this.props.dialog === "performance") {
        this.showSlideAnimationDialogPerFund();
      } else if (this.props.dialog === "bar1") {
        this.showSlideAnimationDialogbar();
      } else if (this.props.dialog === "year") {
        this.showSlideAnimationDialogyear();
      } else if (this.props.dialog === "sipperiod") {
        this.showSlideAnimationDialogsipperiod();
      }
    }, 200);
  }

  //OverView Dialogs

  showSlideAnimationDialogNav = () => {
    this.slideAnimationDialogNav.show();
  };

  onPickerValueChangeNav = (value, index) => {
    if (value === "1 year") {
      this.setState({ nav: "1YRNAV" });
    }
    if (value === "2 years") {
      this.setState({ nav: "2YRNAV" });
    }
    if (value === "3 years") {
      this.setState({ nav: "3YRNAV" });
    }
    if (value === "4 years") {
      this.setState({ nav: "4YRNAV" });
    }
    if (value === "5 years") {
      this.setState({ nav: "5YRNAV" });
    }
    if (value === "inception") {
      this.setState({ nav: "INCNAV " });
    }
    this.setState(
      {
        initpriced: value
      },
      () => {
        // here is our callback that will be fired after state change.
        // Alert.alert("Throttlemode", index+" "+this.state.throttlemode);
        this.slideAnimationDialogNav.dismiss();
      }
    );

    setTimeout(() => {
      //  this.props.navigation.navigate('TutorialSlider');

      this.refs.Overview.fetchChart();
    }, 500);
  };

  showSlideAnimationDialogOverViewGro = () => {
    this.slideAnimationDialogOverViewGro.show();
  };

  onPickerValueChangeOverViewGro = (value, index) => {
    if (value === "1 year") {
      this.setState({ chartyr: "1" });
    }
    if (value === "2 years") {
      this.setState({ chartyr: "2" });
    }
    if (value === "3 years") {
      this.setState({ chartyr: "3" });
    }
    if (value === "4 years") {
      this.setState({ chartyr: "4" });
    }
    if (value === "5 years") {
      this.setState({ chartyr: "5" });
    }
    if (value === "inception") {
      this.setState({ chartyr: "inc" });
    }
    this.setState(
      {
        throttlemode: value
      },
      () => {
        // here is our callback that will be fired after state change.
        // Alert.alert("Throttlemode", index+" "+this.state.throttlemode);
        this.slideAnimationDialogOverViewGro.dismiss();
      }
    );
    setTimeout(() => {
      //  this.props.navigation.navigate('TutorialSlider');

      this.refs.Overview.fetchChart();
    }, 500);
  };

  //Performance Dialogs

  showSlideAnimationDialogPerGrow = () => {
    this.slideAnimationDialogPerGrow.show();
  };

  onPickerValueChangePerGrow = (value, index) => {
    if (value === "1 year") {
      this.setState({ chartyrper: "1" });
    }
    if (value === "2 years") {
      this.setState({ chartyrper: "2" });
    }
    if (value === "3 years") {
      this.setState({ chartyrper: "3" });
    }
    if (value === "4 years") {
      this.setState({ chartyrper: "4" });
    }
    if (value === "5 years") {
      this.setState({ chartyrper: "5" });
    }
    if (value === "inception") {
      this.setState({ chartyrper: "inc" });
    }
    this.setState(
      {
        line: value
      },
      () => {
        // here is our callback that will be fired after state change.
        // Alert.alert("Throttlemode", index+" "+this.state.throttlemode);
        this.slideAnimationDialogPerGrow.dismiss();
      }
    );
    setTimeout(() => {
      //  this.props.navigation.navigate('TutorialSlider');

      this.refs.Performance.fetchLineChart();
    }, 500);
  };

  showSlideAnimationDialogPerFund = () => {
    this.slideAnimationDialogPerFund.show();
  };
  onPickerValueChangePerFund = (value, index) => {
    if (value === "1 year") {
      this.setState({ dateYear: "1" });
    }
    if (value === "2 years") {
      this.setState({ dateYear: "2" });
    }
    if (value === "3 years") {
      this.setState({ dateYear: "3" });
    }
    if (value === "4 years") {
      this.setState({ dateYear: "4" });
    }
    if (value === "5 years") {
      this.setState({ dateYear: "5" });
    }

    this.setState(
      {
        performance: value
      },
      () => {
        // here is our callback that will be fired after state change.
        // Alert.alert("Throttlemode", index+" "+this.state.throttlemode);
        this.slideAnimationDialogPerFund.dismiss();
      }
    );
    setTimeout(() => {
      //  this.props.navigation.navigate('TutorialSlider');

      this.refs.Performance.fetchPerformanceChart();
    }, 500);
  };

  showSlideAnimationDialogbar = () => {
    this.slideAnimationDialogbar.show();
  };
  onPickerValueChangebar = (value, index) => {
    if (value === "Month End") {
      this.setState({ per_period: "m_end" });
    }
    if (value === "Quarter End") {
      this.setState({ per_period: "q_end" });
    }

    this.setState(
      {
        bar1: value
      },
      () => {
        // here is our callback that will be fired after state change.
        // Alert.alert("Throttlemode", index+" "+this.state.throttlemode);
        this.slideAnimationDialogbar.dismiss();
      }
    );
    setTimeout(() => {
      //  this.props.navigation.navigate('TutorialSlider');

      this.refs.Performance.fetchAvgChart();
    }, 500);
  };

  showSlideAnimationDialogyear = () => {
    this.slideAnimationDialogyear.show();
  };

  onPickerValueChangeyear = (value, index) => {
    if (value === "Financial Year") {
      this.setState({ period_year: "financial" });
    }
    if (value === "Calendar Year") {
      this.setState({ period_year: "calender" });
    }

    this.setState(
      {
        year: value
      },
      () => {
        // here is our callback that will be fired after state change.
        // Alert.alert("Throttlemode", index+" "+this.state.throttlemode);
        this.slideAnimationDialogyear.dismiss();
      }
    );
    setTimeout(() => {
      //  this.props.navigation.navigate('TutorialSlider');

      this.refs.Performance.fetchFinanceChart();
    }, 500);
  };

  //Sip Dialogs

  showSlideAnimationDialogsipperiod = () => {
    this.slideAnimationDialogsipperiod.show();
  };

  onPickerValueChangesipperiod = (value, index) => {
    this.setState(
      {
        sipperiod: value
      },
      () => {
        // here is our callback that will be fired after state change.
        // Alert.alert("Throttlemode", index+" "+this.state.throttlemode);
        this.slideAnimationDialogsipperiod.dismiss();
      }
    );
  };

  redirect() {
    if (this.state.from === "Watchlist") {
      Actions.Watchlist({
        Unique_No: this.state.response.Unique_No,
        watchlist: this.state.watchlist
      });
      return true;
    } else if (this.state.from === "CompareFund") {
      Actions.CompareFund({ schemecode: this.state.response.Unique_No });
      return true;
    } else if (this.state.from === "Search") {
      Actions.SearchMutualFund();
      return true;
    } else {
      //alert(this.state.from)
      // Actions.pop();
      Actions.ExploreAll({
        Unique_No: this.state.response.Unique_No,
        watchlist: this.state.watchlist,
        from: "Quote"
      });
      return true;
    }
  }

  bounce = view =>
    view.bounce(800).then(
      endState => "1"
      //  console.log(endState.finished ? "bounce finished" : "bounce canceled")
    );

  render() {
    var viewRef, viewRef2;

    return (
      <View>
        <StatusBar backgroundColor={color.toolbar} barStyle="light-content" />
        <Animated.View
          style={{
            position: "absolute",
            width: Dimensions.get("window").width,
            backgroundColor: this.headerBg,
            zIndex: 1,
            borderWidth: 0,
            borderColor: TRANSPARENT
          }}
        >
          <View
            style={{
              backgroundColor: TRANSPARENT,
              height: 35,
              borderWidth: 0,
              borderColor: TRANSPARENT
            }}
          />
        </Animated.View>

        <Animated.ScrollView
          scrollEventThrottle={5}
          showsVerticalScrollIndicator={false}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.nScroll } } }],
            { useNativeDriver: true }
          )}
          ref="_ScrollView"
          style={{
            zIndex: 0,
            borderWidth: 0,
            borderColor: "transparent",
            marginTop: 0,
            padding: 0,
            backgroundColor: color.WHITE,
            opacity: this.state.opacity
          }}
        >
          <Animated.View
            style={{
              transform: [
                { translateY: Animated.multiply(this.nScroll, 0.65) },
                { scale: this.imgScale }
              ],
              backgroundColor: THEME_COLOR
            }}
          >
            <Animated.View
              style={{
                height: IMAGE_HEIGHT,
                width: "100%",
                opacity: this.imgOpacity
              }}
            >
              <View
                style={[
                  styles.containerFlex,
                  { flex: 1, flexDirection: "column" }
                ]}
              >
                <ScrollView contentInsetAdjustmentBehavior="automatic">
                  <View>
                    <Image
                      source={require("../assets/images/core-page-bg.jpg")}
                      style={{ height: Platform.OS === "ios" ? 120 : 100 }}
                    />
                    <TouchableOpacity
                      style={{
                        width: 20,
                        marginLeft: 20,
                        position: "absolute",
                        marginTop: 50
                      }}
                      onPress={() => this.redirect()}
                    >
                      <Image
                        source={require("../assets/images/back.png")}
                        style={{ height: 20 }}
                      />
                    </TouchableOpacity>
                    <Image
                      source={{
                        uri:
                          Apis.BaseImageURL +
                          `${this.state.response.Amc_code}.jpg`
                      }}
                      style={{
                        width: Platform.OS === "ios" ? 60 : 50,
                        height: Platform.OS === "ios" ? 60 : 50,
                        borderRadius: Platform.OS === "ios" ? 60 / 2 : 50 / 2,
                        alignSelf: "center",
                        resizeMode: "contain",
                        position: "absolute",
                        marginTop: Platform.OS === "ios" ? 90 : 70
                      }}
                    />
                  </View>

                  <Text
                    style={[
                      styles.welcome,
                      {
                        marginTop: Platform.OS === "ios" ? 35 : 20,
                        textAlign: "center",
                        fontSize: Platform.OS === "ios" ? 15 : 13,
                        marginLeft: 5,
                        marginRight: 5
                      }
                    ]}
                  >
                    {this.state.response.Scheme_Name}
                  </Text>

                  <View
                    style={{
                      flexDirection: "column",
                      marginLeft: 10,
                      marginRight: 15,
                      marginTop: 10
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <View
                        style={{
                          backgroundColor: this.renderColor(
                            this.state.response.asset_code
                          ),
                          borderRadius: 5,
                          marginLeft: 5
                        }}
                      >
                        <Text
                          style={[
                            styles.ExploreAllText,
                            {
                              color: color.WHITE,
                              padding: 5,
                              fontSize: Platform.OS === "ios" ? 13 : 10
                            }
                          ]}
                        >
                          {this.state.response.asset_type}
                        </Text>
                      </View>
                      <View
                        style={{
                          borderColor: this.renderColor(
                            this.state.response.asset_code
                          ),
                          borderRadius: 5,
                          borderWidth: 1,
                          marginLeft: 5,
                          width: 0,
                          flexGrow: 1
                        }}
                      >
                        <Text
                          style={[
                            styles.ExploreAllText,
                            {
                              color: this.renderColor(
                                this.state.response.asset_code
                              ),
                              padding: 5,
                              fontSize: Platform.OS === "ios" ? 13 : 10
                            }
                          ]}
                        >
                          {this.state.response.classname}
                        </Text>
                      </View>
                    </View>

                    <Text
                      style={[
                        styles.ExploreAllText,
                        {
                          color: this.renderColor(
                            this.state.response.asset_code
                          ),
                          padding: 5,
                          fontSize: Platform.OS === "ios" ? 13 : 10,
                          borderColor: this.renderColor(
                            this.state.response.asset_code
                          ),
                          borderRadius: 5,
                          borderWidth: 1,
                          marginLeft: 5,
                          marginTop: 3
                        }
                      ]}
                    >
                      {this.state.response.category_name}
                    </Text>
                  </View>

                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: 10,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "Roboto",
                        fontSize: Platform.OS === "ios" ? 23 : 18,
                        fontWeight: "bold",
                        textAlign: "center"
                      }}
                    >
                      Rank
                    </Text>
                    {clienttype === "client" ? (
                      <View
                        style={[
                          styles.RankBadge,
                          {
                            backgroundColor: this.renderColor(
                              this.state.response.asset_code
                            ),
                            marginLeft: 10
                          }
                        ]}
                      >
                        <Text
                          style={[
                            styles.RankBadgeText,
                            {
                              fontSize: Platform.OS === "ios" ? 18 : 15,
                              fontWeight: "bold",
                              padding: Platform.OS === "ios" ? 4 : 2
                            }
                          ]}
                        >
                          {this.state.response.rank_srno}
                        </Text>
                      </View>
                    ) : (
                      <Image
                        style={{
                          width: 40,
                          height: 40,
                          resizeMode: "contain",
                          alignItems: "center",
                          marginTop: 10
                        }}
                        source={require("../assets/images/rank-lock.png")}
                      />
                    )}
                  </View>

                  <View style={{ flexDirection: "row", marginTop: 5 }}>
                    <TouchableOpacity
                      style={{ flex: 1 }}
                      onPress={() => this.updatestateFund()}
                    >
                      <View style={styles.box}>
                        <Text style={styles.flexboxtext}>Fund</Text>
                        {/* <Image source={require('RankMF/assets/like.png')}
                            style={{width: 40, height: 40,resizeMode:"contain",marginTop:10}} /> */}
                        {this.renderfundimage(this.state.response.rating)}
                      </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={{ flex: 1 }}
                      onPress={() => this.updatestateInvest()}
                    >
                      <View style={styles.box}>
                        <Text style={styles.flexboxtext}>Time to Invest</Text>
                        {/* <Image source={require('RankMF/assets/dislike.png')}
                            style={{width: 40, height: 40,resizeMode:"contain",marginTop:10}} /> */}
                        {this.renderinvestimage(this.state.response.time_flag)}
                      </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={{ flex: 1 }}
                      onPress={() => this.updatestateStrength()}
                    >
                      <View style={styles.box}>
                        <Text style={styles.flexboxtext}>Strength</Text>
                        {/* <Image source={require('RankMF/assets/dislike.png')}
                            style={{width: 40, height: 40,resizeMode:"contain",marginTop:10}} /> */}
                        {this.renderStrength(this.state.response.risk_score)}
                      </View>
                    </TouchableOpacity>
                  </View>

                  {this.state.showFund &&
                    this.state.response.fund_tooltip != "" && (
                      <Animatable.View
                        duration={300}
                        animation="zoomIn"
                        style={{ marginTop: 5 }}
                      >
                        <View
                          style={{
                            justifyContent: "center",
                            alignItems: "center",
                            width: 125
                          }}
                        >
                          <View style={[styles.triangle]} />
                        </View>
                        <View
                          style={{
                            backgroundColor: color.LIGHT_BLUE,
                            padding: 3,
                            marginLeft: 0,
                            marginRight: 0
                          }}
                        >
                          <Text
                            style={[
                              styles.flexboxtext,
                              { padding: Platform.OS === "ios" ? 5 : 3 }
                            ]}
                          >
                            {this.state.response.fund_tooltip}
                          </Text>
                        </View>
                      </Animatable.View>
                    )}
                  {this.state.showInvest &&
                    this.state.response.time_tooptip != "" && (
                      <Animatable.View
                        duration={300}
                        animation="zoomIn"
                        style={{ marginTop: 5 }}
                      >
                        <View
                          style={{
                            justifyContent: "center",
                            alignItems: "center",
                            flex: 1
                          }}
                        >
                          <View style={[styles.triangle]} />
                        </View>
                        <View
                          duration={800}
                          style={{
                            backgroundColor: color.LIGHT_BLUE,
                            padding: 3
                          }}
                        >
                          <Text
                            style={[
                              styles.flexboxtext,
                              { padding: Platform.OS === "ios" ? 5 : 3 }
                            ]}
                          >
                            {this.state.response.time_tooptip}
                          </Text>
                        </View>
                      </Animatable.View>
                    )}
                  {this.state.showStrength && (
                    <Animatable.View
                      duration={300}
                      animation="zoomIn"
                      style={{ marginTop: 5 }}
                    >
                      <View
                        style={{
                          justifyContent: "flex-end",
                          alignItems: "flex-end",
                          marginRight: 58
                        }}
                      >
                        <View
                          style={[styles.triangle, { alignItems: "flex-end" }]}
                        />
                      </View>
                      <View
                        duration={400}
                        style={{ backgroundColor: color.LIGHT_BLUE, flex: 1 }}
                      >
                        {this.renderStrengthTooltip(
                          this.state.response.portfolio_quality,
                          this.state.response.risk_volatility,
                          this.state.response.corelation_becnhmark,
                          this.state.response.asset_type
                        )}
                      </View>
                    </Animatable.View>
                  )}

                  <View style={{ flexDirection: "row", marginTop: 10 }}>
                    <TouchableOpacity
                      style={styles.box}
                      onPress={() =>
                        this.updatestateRate(this.state.response.rating)
                      }
                    >
                      <Text style={[styles.flexboxtext]}>Rating</Text>

                      {clienttype === "client" ? (
                        <StarRating
                          containerStyle={{ marginBottom: 10 }}
                          starStyle={{ marginTop: 5 }}
                          fullStar={require("../assets/images/star.png")}
                          emptyStar={require("../assets/images/emptystar.png")}
                          disabled={true}
                          maxStars={5}
                          starSize={15}
                          rating={this.state.response.rating}
                        />
                      ) : (
                        <Image
                          style={{
                            resizeMode: "contain",
                            alignItems: "center",
                            marginTop: 10
                          }}
                          source={require("../assets/images/rating-lock.png")}
                        />
                      )}
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => this.bounce(viewRef2)}
                      style={{
                        flexDirection: "column",
                        flex: 1,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Animatable.View
                        ref={ref => (viewRef2 = ref)}
                        style={{
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <Text style={[styles.flexboxtext]}>NAV</Text>
                        <Text style={{ marginTop: 5 }}>
                          {"\u20B9"}
                          {this.state.response.Navrs}
                        </Text>
                      </Animatable.View>
                    </TouchableOpacity>
                  </View>

                  {this.state.showRate &&
                    this.state.response.rating_tooltip != "" && (
                      <Animatable.View
                        duration={300}
                        animation="zoomIn"
                        style={{ marginTop: 5 }}
                      >
                        <View
                          style={{
                            justifyContent: "center",
                            alignItems: "center",
                            width: 150
                          }}
                        >
                          <View style={[styles.triangle]} />
                        </View>
                        <View
                          style={{
                            backgroundColor: color.LIGHT_BLUE,
                            padding: 3,
                            marginLeft: 0,
                            marginRight: 0
                          }}
                        >
                          <Text style={[styles.flexboxtext, { padding: 5 }]}>
                            {cleantext(this.state.response.rating_tooltip)}
                          </Text>
                        </View>
                      </Animatable.View>
                    )}

                  <View style={[styles.dash, { marginTop: 10 }]} />
                  <View style={{ flexDirection: "row", marginTop: 10 }}>
                    <Text style={[styles.flexboxtext, { marginLeft: 15 }]}>
                      52Week
                    </Text>
                    <View
                      style={{
                        width: 10,
                        height: 10,
                        borderRadius: 10,
                        resizeMode: "contain",
                        backgroundColor: color.toolbar,
                        alignItems: "center",
                        marginTop: 5,
                        marginLeft: 30
                      }}
                    />
                    <Text style={[styles.flexboxtext, { marginLeft: 15 }]}>
                      Low : {parseFloat(this.state.highLow52Week["52WeekLow"]).toFixed(2)}
                    </Text>
                    <View
                      style={{
                        width: 10,
                        height: 10,
                        borderRadius: 10,
                        resizeMode: "contain",
                        backgroundColor: color.toolbar,
                        alignItems: "center",
                        marginTop: 5,
                        marginLeft: 30
                      }}
                    />
                    <Text style={[styles.flexboxtext, { marginLeft: 15 }]}>
                      High : {parseFloat(this.state.highLow52Week["52WeekhHigh"]).toFixed(2)}
                    </Text>
                  </View>
                  <View style={[styles.dash, { marginTop: 10 }]} />

                  <View style={[styles.container2]}>
                    {this.state.response.Purchase_Allowed === "Y" ? (
                      <View style={styles.buttonContainerPage1}>
                        <TouchableOpacity
                          onPress={() =>
                            Actions.Lumpsum({
                              Scheme_Code:this.state.response.Scheme_Code,
                              Unique_No:this.state.response.Unique_No,
                              Amc_code: this.state.response.Amc_code,
                              Scheme_Name: this.state.response.Scheme_Name,
                              rating: this.state.response.rating,
                              Navrs: this.state.response.Navrs,
                              Navdate: this.state.responsenew.Navdate,
                              Minimum_Purchase_Amount: this.state.response
                                .Minimum_Purchase_Amount,
                              Purchase_Amount_Multiplier: this.state.scheme
                                .Purchase_Amount_Multiplier
                            })
                            // Actions.Lumpsum3({
                            //   Scheme_Code: this.state.response.Scheme_Code,
                            //   Unique_No: this.state.response.Unique_No,
                            //   Scheme_Name: this.state.response.Scheme_Name,
                            //   amount: "500"
                            // })
                          }
                          style={[styles.button, { backgroundColor: "green" }]}
                        >
                          <Text
                            style={[
                              styles.instructions,
                              { fontSize: Platform.OS == "ios" ? 15 : 12 }
                            ]}
                          >
                            Invest Now
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ) : (
                      <View style={styles.buttonContainerPage1}>
                        <TouchableOpacity
                          style={[
                            styles.button,
                            { backgroundColor: color.LIGHT_GREY }
                          ]}
                        >
                          <Text
                            style={[
                              styles.instructions,
                              { fontSize: Platform.OS == "ios" ? 15 : 12 }
                            ]}
                          >
                            Invest Now
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}

                    {this.state.response.SIP_FLAG === "Y" ? (
                      <View style={styles.buttonContainerPage1}>
                        <TouchableOpacity
                          onPress={() =>
                            Actions.Sip1({
                              Scheme_Code:this.state.response.Scheme_Code,
                              Unique_No:this.state.response.Unique_No,
                              Amc_code: this.state.response.Amc_code,
                              Scheme_Name: this.state.response.Scheme_Name,
                              rating: this.state.response.rating,
                              Navrs: this.state.response.Navrs,
                              Navdate: this.state.responsenew.Navdate,
                              sip_min_intallment_amt: this.state.scheme.sip_min_intallment_amt,
                                sip_multiplier_amt: this.state.scheme.sip_multiplier_amt
                            })
                          }
                          style={[styles.button, { backgroundColor: "green" }]}
                        >
                          <Text
                            style={[
                              styles.instructions,
                              { fontSize: Platform.OS == "ios" ? 15 : 12 }
                            ]}
                          >
                            SIP
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ) : (
                      <View style={styles.buttonContainerPage1}>
                        <TouchableOpacity
                          style={[
                            styles.button,
                            { backgroundColor: color.LIGHT_GREY }
                          ]}
                        >
                          <Text
                            style={[
                              styles.instructions,
                              { fontSize: Platform.OS == "ios" ? 15 : 12 }
                            ]}
                          >
                            SIP
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                  </View>

                  <View style={[styles.container2]}>
                    <View style={styles.buttonContainerPage1}>
                      {this.state.watchlist === true ? (
                        <TouchableOpacity
                          onPress={() =>
                            this.RemoveWatchlist(
                              this.state.response.schemecode,
                              this.state.response.Unique_No,
                              client_id,
                              user_acc_id
                            )
                          }
                          style={{
                            flexDirection: "row",
                            borderColor: color.RED,
                            borderRadius: 10,
                            borderWidth: 1,
                            backgroundColor: color.RED,
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                        >
                          <Image
                            source={require("../assets/images/remove_watchlist_white.png")}
                            style={{
                              height: 15,
                              width: 15,
                              resizeMode: "contain"
                            }}
                          />
                          <Text
                            style={[
                              styles.instructions,
                              {
                                fontSize: Platform.OS == "ios" ? 15 : 12,
                                color: color.WHITE
                              }
                            ]}
                          >
                            Remove
                          </Text>
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity
                          onPress={() =>
                            this.addtoWatchlist(
                              this.state.response.schemecode,
                              this.state.response.Unique_No,
                              client_id,
                              user_acc_id
                            )
                          }
                          style={{
                            borderColor: color.ORANGE,
                            borderRadius: 10,
                            borderWidth: 1
                          }}
                        >
                          <Text
                            style={[
                              styles.instructions,
                              {
                                fontSize: Platform.OS == "ios" ? 15 : 12,
                                color: color.ORANGE
                              }
                            ]}
                          >
                            Add To Watchlist
                          </Text>
                        </TouchableOpacity>
                      )}
                    </View>
                    <View style={styles.buttonContainerPage1}>
                      <TouchableOpacity
                        onPress={() =>
                          Actions.CompareFund({
                            schemecode: this.state.response.Unique_No
                          })
                        }
                        style={{
                          borderColor: color.ORANGE,
                          borderRadius: 10,
                          borderWidth: 1
                        }}
                      >
                        <Text
                          style={[
                            styles.instructions,
                            {
                              fontSize: Platform.OS == "ios" ? 15 : 12,
                              color: color.ORANGE
                            }
                          ]}
                        >
                          Compare Fund
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </ScrollView>
              </View>
            </Animated.View>
            {/* this.state.myRequestedRefs._ScrollView.scrollTo({x: 0, y: 0, animated: true}); */}
          </Animated.View>

          <Tabs
            locked={false}
            prerenderingSiblingsNumber={3}
            onChangeTab={({ i }) => {
              this.setState({ height: this.heights[i], activeTab: i });

              // this.refs._ScrollView.scrollTo(0)
            }}
            renderTabBar={props => (
              <Animated.View
                style={{
                  transform: [{ translateY: this.tabY }],
                  zIndex: 1,
                  width: "100%",
                  backgroundColor: "white"
                }}
              >
                <ScrollableTab
                  {...props}
                  renderTab={(name, page, active, onPress, onLayout) => (
                    <TouchableOpacity
                      key={page}
                      onPress={() => {
                        onPress(page);
                      }}
                      onLayout={onLayout}
                      activeOpacity={0.4}
                    >
                      <Animated.View
                        style={{
                          flex: 1,
                          height: 100,
                          backgroundColor: this.tabBg
                        }}
                      >
                        <TabHeading
                          scrollable
                          style={{
                            backgroundColor: "transparent",
                            width: SCREEN_WIDTH / 2
                          }}
                          active={active}
                        >
                          <Animated.Text
                            style={{
                              fontWeight: active ? "bold" : "normal",
                              color: this.textColor,
                              fontSize: 14,
                              marginTop: 10
                            }}
                          >
                            {name}
                          </Animated.Text>
                        </TabHeading>
                      </Animated.View>
                    </TouchableOpacity>
                  )}
                  underlineStyle={{ backgroundColor: this.textColor }}
                />
              </Animated.View>
            )}
          >
            <Tab heading="Overview">
              <Overview
                ref="Overview"
                unique_code={this.props.unique_code}
                initpriced={this.state.initpriced}
                throttlemode={this.state.throttlemode}
                chartyr={this.state.chartyr}
                nav={this.state.nav}
              />
            </Tab>

            <Tab heading="Performance">
              <Performance
                unique_code={this.props.unique_code}
                ref="Performance"
                line={this.state.line}
                performance={this.state.performance}
                bar1={this.state.bar1}
                year={this.state.year}
                chartyr={this.state.chartyrper}
                dateYear={this.state.dateYear}
                per_period={this.state.per_period}
                period_year={this.state.period_year}
              />
            </Tab>

            <Tab heading="SIP">
              <Sip
                ref="Sip"
                unique_code={this.props.unique_code}
                sipperiod={this.state.sipperiod}
              />
            </Tab>
            <Tab heading="Portfolio">
              <Portfolio unique_code={this.props.unique_code} />
            </Tab>
            <Tab heading="Risk & Strength">
              <RiskStrength unique_code={this.props.unique_code} />
            </Tab>
            <Tab heading="Manager & Other Details">
              <Manager unique_code={this.props.unique_code} />
            </Tab>
          </Tabs>
        </Animated.ScrollView>
        <CustomToast
          ref="success"
          backgroundColor="#4CAF50"
          position="bottom"
          style={{ alignSelf: "center" }}
        />
        <CustomToast
          ref="error"
          backgroundColor="red"
          position="bottom"
          style={{ alignSelf: "center" }}
        />

        {/* Overview Dialogs */}
        <PopupDialog
          dialogStyle={{
            height: Platform.OS === "ios" ? 300 : 150,
            width: 250,
            position: "absolute",
            top: 100
          }}
          dialogTitle={<DialogTitle title="Select" />}
          ref={popupDialog => {
            this.slideAnimationDialogNav = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
          <View>
            <Picker
              selectedValue={this.state.initpriced}
              onValueChange={this.onPickerValueChangeNav}
            >
              <Picker.Item label="1 year" value="1 year" />
              <Picker.Item label="2 years" value="2 years" />
              <Picker.Item label="3 years" value="3 years" />
              <Picker.Item label="4 years" value="4 years" />
              <Picker.Item label="5 years" value="5 years" />
              <Picker.Item label="Inception" value="inception" />
            </Picker>
          </View>
        </PopupDialog>
        <PopupDialog
          dialogStyle={{
            height: Platform.OS === "ios" ? 300 : 150,
            width: 250,
            position: "absolute",
            top: 100
          }}
          dialogTitle={<DialogTitle title="Select" />}
          ref={popupDialog => {
            this.slideAnimationDialogOverViewGro = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
          <View>
            <Picker
              selectedValue={this.state.throttlemode}
              onValueChange={this.onPickerValueChangeOverViewGro}
            >
              <Picker.Item label="1 year ago" value="1 year" />
              <Picker.Item label="2 years ago" value="2 years" />
              <Picker.Item label="3 years ago" value="3 years" />
              <Picker.Item label="4 years ago" value="4 years" />
              <Picker.Item label="5 years ago" value="5 years" />
              <Picker.Item label="Inception" value="inception" />
            </Picker>
          </View>
        </PopupDialog>

        {/* Performance Dialogs */}

        <PopupDialog
          dialogStyle={{
            height: Platform.OS === "ios" ? 300 : 150,
            width: 300,
            position: "absolute",
            zIndex: 1,
            top: 100
          }}
          dialogTitle={<DialogTitle title="Select" />}
          ref={popupDialog => {
            this.slideAnimationDialogPerGrow = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
          <View>
            <Picker
              style={{ justifyContent: "center" }}
              selectedValue={this.props.line}
              onValueChange={this.onPickerValueChangePerGrow}
            >
              <Picker.Item label="1 year" value="1 year" />
              <Picker.Item label="2 years" value="2 years" />
              <Picker.Item label="3 years" value="3 years" />
              <Picker.Item label="4 years" value="4 years" />
              <Picker.Item label="5 years" value="5 years" />
              <Picker.Item label="Inception" value="inception" />
            </Picker>
          </View>
        </PopupDialog>
        <PopupDialog
          dialogStyle={{
            height: Platform.OS === "ios" ? 300 : 150,
            width: 250,
            position: "absolute",
            top: 100
          }}
          dialogTitle={<DialogTitle title="Select" />}
          ref={popupDialog => {
            this.slideAnimationDialogPerFund = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
          <View>
            <Picker
              style={{ justifyContent: "center" }}
              selectedValue={this.state.performance}
              onValueChange={this.onPickerValueChangePerFund}
            >
              <Picker.Item label="1 year" value="1 year" />
              <Picker.Item label="2 years" value="2 years" />
              <Picker.Item label="3 years" value="3 years" />
              <Picker.Item label="4 years" value="4 years" />
              <Picker.Item label="5 years" value="5 years" />
            </Picker>
          </View>
        </PopupDialog>
        <PopupDialog
          dialogStyle={{
            height: Platform.OS === "ios" ? 300 : 150,
            width: 250,
            position: "absolute",
            top: 100
          }}
          dialogTitle={<DialogTitle title="Select" />}
          ref={popupDialog => {
            this.slideAnimationDialogbar = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
          <View>
            <Picker
              style={{ justifyContent: "center" }}
              selectedValue={this.state.bar1}
              onValueChange={this.onPickerValueChangebar}
            >
              <Picker.Item label="Month End" value="Month End" />
              <Picker.Item label="Quarter End" value="Quarter End" />
            </Picker>
          </View>
        </PopupDialog>
        <PopupDialog
          dialogStyle={{
            height: Platform.OS === "ios" ? 300 : 150,
            width: 250,
            position: "absolute",
            top: 100
          }}
          dialogTitle={<DialogTitle title="Select" />}
          ref={popupDialog => {
            this.slideAnimationDialogyear = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
          <View>
            <Picker
              style={{ justifyContent: "center" }}
              selectedValue={this.state.year}
              onValueChange={this.onPickerValueChangeyear}
            >
              <Picker.Item label="Financial Year" value="Financial Year" />
              <Picker.Item label="Calendar Year" value="Calendar Year" />
            </Picker>
          </View>
        </PopupDialog>

        {/* Sip Dialogs */}

        <PopupDialog
          dialogStyle={{
            height: Platform.OS === "ios" ? 300 : 150,
            width: 250,
            position: "absolute",
            top: 100
          }}
          dialogTitle={<DialogTitle title="Select" />}
          ref={popupDialog => {
            this.slideAnimationDialogsipperiod = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
          <View>
            <Picker
              style={{ justifyContent: "center", textAlign: "center" }}
              selectedValue={this.state.sipperiod}
              onValueChange={this.onPickerValueChangesipperiod}
            >
              <Picker.Item label="5" value="5" />
              <Picker.Item label="10" value="10" />
              <Picker.Item label="15" value="15" />
              <Picker.Item label="20" value="20" />
              <Picker.Item label="25" value="25" />
              <Picker.Item label="30" value="30" />
            </Picker>
          </View>
        </PopupDialog>

        {this.state.isLoading && (
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              flex: 1
            }}
          >
            <Image
              source={require("../assets/images/loader.gif")}
              style={{ height: 30, width: 30, resizeMode: "contain" }}
            />
          </View>
        )}
      </View>
    );
  }
}

export default Page1;
