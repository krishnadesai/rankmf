import React, { Component, Props } from "react";
import { Card, SearchBar } from "react-native-elements";
import { Actions } from "react-native-router-flux";
import ScrollableTabView, {
  ScrollableTabBar
} from "react-native-scrollable-tab-view";
import {
  Dimensions,
  WebView,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  TouchableHighlight,
  Image
} from "react-native";
import PopupDialog, {
  DialogButton,
  DialogTitle,
  SlideAnimation
} from "react-native-popup-dialog";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";
import createReactClass from "create-react-class";
const access_token = "",
  client_id = "";
const slideAnimation = new SlideAnimation({ slideFrom: "bottom" });
const children = [];
const keys = [];
const responsedata = [];
const regex = /(<([^>]+)>)/gi;

function cleantext(strInputCode) {
  cleanText = strInputCode.replace(regex, "");
  return cleanText;
}

export default class SearchMutualFund extends Component {

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      isLoading: true,
      file: undefined,
      itemList: [],
      image: null,
      text: "",
      tabs: [],
      show: false,
      url: ""
    };
  }

  search = {
    ref: null
  };

  omponentDidMount() {
    this.retrieveData();
  }

  retrieveData = async () => {
    try {
      client_id = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData(text) {
    this.setState({ text: text });
    if (text === "") {
      this.setState({ show: false });
    } else {
      fetch(Apis.getSchemes, {
        method: "POST",
        headers: new Headers({
          "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
        }),
        body: `keyword= ${text}&access_token=${access_token}&user=${client_id}` // <-- Post parameters
      })
        .then(response => response.json())
        .then(responseJson => {
          responsedata = responseJson;

          var count = Object.keys(responseJson[0]).length;

          keys = Object.keys(responseJson[0]);
          if (Object.keys(responseJson[1]).length > 0) {
            keys.push("Articles");
            count = count + 1;
          }
          if (count == 0) {
            this.setState({ tabs: [] });
          } else if (count == 1) {
            this.setState({ tabs: [1] });
          } else if (count == 2) {
            this.setState({ tabs: [1, 2] });
          } else if (count == 3) {
            this.setState({ tabs: [1, 2, 3] });
          } else if (count == 4) {
            this.setState({ tabs: [1, 2, 3, 4] });
          } else if (count == 5) {
            this.setState({ tabs: [1, 2, 3, 4, 5] });
          } else if (count == 6) {
            this.setState({ tabs: [1, 2, 3, 4, 5, 6] });
          }

          if (keys.indexOf("Articles") > -1) {
            this.setState({ itemList: responseJson[1] });
          }
          if (keys.indexOf("Other") > -1) {
            var arr = [];
            for (var key in responseJson[0].Other) {
              arr.push(responseJson[0].Other[key]);
            }

            this.setState({ itemList: arr });
          }
          if (keys.indexOf("Commodity") > -1) {
            var arr = [];
            for (var key in responseJson[0].Commodity) {
              arr.push(responseJson[0].Commodity[key]);
            }

            this.setState({ itemList: arr });
          }
          if (keys.indexOf("Hybrid") > -1) {
            var arr = [];
            for (var key in responseJson[0].Hybrid) {
              arr.push(responseJson[0].Hybrid[key]);
            }

            this.setState({ itemList: arr });
          }
          if (keys.indexOf("Debt") > -1) {
            var arr = [];
            for (var key in responseJson[0].Debt) {
              arr.push(responseJson[0].Debt[key]);
            }

            this.setState({ itemList: arr });
          }

          if (keys.indexOf("Equity") > -1) {
            this.setState({ itemList: responseJson[0].Equity });
          }

          this.setState({ show: true });
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  showSlideAnimationDialog = () => {
    this.slideAnimationDialog.show();
  };

  handleChangeTab = ({ i, ref, from }) => {
    //  this.children[i].onEnter();
    //   this.children[from].onLeave();

    if (keys[i] === "Articles") {
      this.setState({ itemList: responsedata[1] });
    } else if (keys[i] === "Other") {
      var arr = [];
      for (var key in responsedata[0].Other) {
        arr.push(responsedata[0].Other[key]);
      }

      this.setState({ itemList: arr });
    } else if (keys[i] === "Commodity") {
      var arr = [];
      for (var key in responsedata[0].Commodity) {
        arr.push(responsedata[0].Commodity[key]);
      }

      this.setState({ itemList: arr });
    } else if (keys[i] === "Hybrid") {
      var arr = [];
      for (var key in responsedata[0].Hybrid) {
        arr.push(responsedata[0].Hybrid[key]);
      }

      this.setState({ itemList: arr });
    } else if (keys[i] === "Debt") {
      var arr = [];
      for (var key in responsedata[0].Debt) {
        arr.push(responsedata[0].Debt[key]);
      }

      this.setState({ itemList: arr });
    } else if (keys[i] === "Equity") {
      this.setState({ itemList: responsedata[0].Equity });
    }
  };

  renderTab(name, page, isTabActive, onPressHandler, onLayoutHandler) {
    if (isTabActive) {
      return (
        <TouchableHighlight
          key={`${name}_${page}`}
          onPress={() => onPressHandler(page)}
          // onLayout={onLayoutHandler}
          style={{
            width: 100,
            backgroundColor: color.WHITE,
            margin: 7,
            borderRadius: 5
          }}
          underlayColor={color.WHITE}
        >
          <Text
            style={{
              textAlign: "center",
              color: color.toolbar,
              justifyContent: "center",
              marginTop: 10
            }}
          >
            {name}
          </Text>
        </TouchableHighlight>
      );
    } else {
      return (
        <TouchableHighlight
          key={`${name}_${page}`}
          onPress={() => onPressHandler(page)}
          // onLayout={onLayoutHandler}
          style={{ width: 100 }}
          underlayColor={color.WHITE}
        >
          <Text
            style={{
              textAlign: "center",
              color: color.WHITE,
              justifyContent: "center",
              marginTop: 15
            }}
          >
            {name}
          </Text>
        </TouchableHighlight>
      );
    }
  }

  render() {
    
    const CustomRow = ({ id, text, opt_code, amc_code }) => (
      <TouchableOpacity
        onPress={() =>
          Actions.page1({ unique_code: opt_code.split("/")[5], from: "Search" })
        }
        style={{ flexDirection: "row", margin: 10, marginLeft: 15 }}
      >
        <Image
          source={{ uri: Apis.BaseImageURL + `${amc_code}.jpg` }}
          style={{ height: 30, width: 30, resizeMode: "contain" }}
        />
        <Text
          style={[
            styles.title_carddetail,
            {
              color: color.BLACK,
              marginLeft: 8,
              width: 0,
              flexGrow: 1,
              fontWeight: "normal"
            }
          ]}
        >
          {text}
        </Text>
      </TouchableOpacity>
    );

    const CustomRowArticle = ({ id, text, asset_code, opt_code }) => (
      <Card
        containerStyle={{ borderRadius: 3, backgroundColor: color.LIGHT_BLUE }}
      >
        <TouchableOpacity
          style={{ flexDirection: "row" }}
          onPress={() => {
            this.setState({ url: opt_code });
            this.showSlideAnimationDialog();
          }}
        >
          <Image
            source={require("../../assets/images/article.png")}
            style={{ height: 30, width: 30, resizeMode: "contain" }}
          />
          <View
            style={{
              flexDirection: "column",
              marginLeft: 8,
              width: 0,
              flexGrow: 1
            }}
          >
            <Text style={[styles.title_carddetail, { color: color.BLACK }]}>
              {asset_code}
            </Text>
            <Text
              style={[
                styles.title_carddetail,
                { color: color.BLACK, fontWeight: "normal" }
              ]}
            >
              {cleantext(text)}
            </Text>
          </View>
        </TouchableOpacity>
      </Card>
    );

    const Child = createReactClass({
      // onEnter() {
      //   console.warn('enter: ' + this.props.i); // eslint-disable-line no-console
      // },

      // onLeave() {
      // console.warn('leave: ' + this.props.i); // eslint-disable-line no-console
      // },

      render() {
        const tabLabel = this.props.tabLabel;
        const data1 = this.props.data;
        if (tabLabel === "Articles") {
          return (
            <FlatList
              data={data1}
              renderItem={({ item }) => (
                <CustomRowArticle
                  id={item.id}
                  text={item.text}
                  asset_code={item.asset_code}
                  opt_code={item.opt_code}
                />
              )}
            />
          );
        } else {
          return (
            <FlatList
              data={data1}
              renderItem={({ item }) => (
                <CustomRow
                  id={item.id}
                  text={item.text}
                  opt_code={item.opt_code}
                  amc_code={item.amc_code}
                />
              )}
            />
          );
        }
      }
    });

    return (
      <View style={{ flex: 1 }}>
        <SearchBar
          containerStyle={{ backgroundColor: color.toolbar }}
          inputStyle={{ backgroundColor: color.WHITE }}
          ref={search => {
            this.search.ref = search;
          }}
          lightTheme
          clearIcon={true}
          value={this.state.text}
          onChangeText={text => this.fetchData(text)}
          onClear={() => this.setState({ text: "" })}
          searchIcon={{ size: 24 }}
          // onChangeText={}
          placeholder="Type Here..."
        />

        {this.state.show && (
          <ScrollableTabView
            renderTabBar={() => (
              <ScrollableTabBar
                renderTab={this.renderTab}
                style={{ backgroundColor: color.toolbar }}
              />
            )}
            onChangeTab={this.handleChangeTab}
          >
            {this.state.tabs.map((tab, i) => {
              return (
                <Child
                  ref={ref => (children[i] = ref)}
                  tabLabel={keys[i]}
                  i={i}
                  data={this.state.itemList}
                  key={i}
                />
              );
            })}
          </ScrollableTabView>
        )}
        <PopupDialog
          dialogStyle={{
            width: "100%",
            height: "100%",
            position: "absolute",
            bottom: 0,
            borderRadius: 10
          }}
          ref={popupDialog => {
            this.slideAnimationDialog = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
          <Text
            style={[
              styles.item,
              {
                marginRight: 10,
                marginTop: 10,
                fontSize: 13,
                textAlign: "right",
                justifyContent: "flex-end"
              }
            ]}
            onPress={() => this.slideAnimationDialog.dismiss()}>
            Done
          </Text>
          <WebView 
          source={{ uri: this.state.url }}
          style={[styles.webView]}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          startInLoadingState={false}
          scalesPageToFit={true} />
        </PopupDialog>
      </View>
    );
  }
}

