import React, { Component, Props } from "react";
import {
  Platform,
  Dimensions,
  Image,
  View,
  Text,
  StatusBar,
  FlatList,
  AsyncStorage,
  TouchableOpacity
} from "react-native";
import { Card } from "react-native-elements";
import * as Animatable from "react-native-animatable";
import { Actions } from "react-native-router-flux";
import StarRating from "react-native-star-rating";
import CustomToast from "../../components/CustomToast";
import PopupDialog, {
  DialogTitle,
  SlideAnimation
} from "react-native-popup-dialog";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import Apis from "../../constants/Apis";
const like = require("../../assets/images/thumbsup.png");
const newicon = require("../../assets/images/new-icon.png");
const dislike = require("../../assets/images/thumbsdown.png");
const regex = /(<([^>]+)>)/gi;
const DEVICE_WIDTH = Dimensions.get(`window`).width;
const slideAnimation = new SlideAnimation({ slideFrom: "bottom" });
const user_acc_id = "",
  client_id = "",
  access_token = "",
  clientid = "";

function cleantext(strInputCode) {
  cleanText = strInputCode.replace("<ul><li>", "\u2022 ");
  cleanText = cleanText.replace("</li><li>", "\n\n\u2022 ");
  cleanText = cleanText.replace("</li><li>", "\n\n\u2022 ");
  cleanText = cleanText.replace("</li></ul>", "");
  return cleanText;
}

export default class ExploreAll extends Component {
  // view = {
  //     ref: null,
  //   }

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      isLoading: true,
      file: undefined,
      itemList: [],
      count: 0,
      hideNavBar: false,
      showFund: [],
      showRate: [],
      showInvest: [],
      showStrength: [],
      offset: 0,
      sort_value: "",
      sort_type: "",
      risk: ["very_low", "low", "moderate", "high", "very_high"],
      thumbs: ["thumbs-down", "thumbs-up"],
      rating: [],
      mininvt: [],
      asset_classcode: [],
      fund_house: [],
      watchlist: [],
      opacity: 0.5
    };

    this.fetchData = this.fetchData.bind(this);
  }

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      if (clienttype === "client") {
        client_id = clientid;
      } else if (clienttype === "lead") {
        user_acc_id = clientid;
      }
      this.fetchData();
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  fetchData() {
    if (this.state.count > 10) {
      this.setState({ isLoading: true, opacity: 0.5 });
    }

    if (this.state.offset === 0) {
      this.setState({ itemList: [] });
    }

    var rate = "",
      mininvt = "",
      asset_classcode = "",
      fund_house = "";

    if (this.props.rating != "") {
      this.props.rating.map((item, i) => {
        rate += `rating[${i}]=${item}&`;
      });
      rate.substring(0, rate.length - 1);
      this.setState({ rating: this.props.rating });
    }
    //  alert(rate);
    if (this.props.mininvt != "") {
      this.props.mininvt.map((item, i) => {
        mininvt += `mininvt[${i}]=${item}&`;
      });
      mininvt.substring(0, mininvt.length - 1);
      this.setState({ mininvt: this.props.mininvt });
    }
    if (this.props.asset_classcode != "") {
      this.props.asset_classcode.map((item, i) => {
        asset_classcode += `asset_classcode[${i}]=${item}&`;
      });
      asset_classcode.substring(0, asset_classcode.length - 1);
      this.setState({ asset_classcode: this.props.asset_classcode });
    }
    if (this.props.fund_house != "") {
      this.props.fund_house.map((item, i) => {
        fund_house += `fund_house[${i}]=${item}&`;
      });
      fund_house.substring(0, fund_house.length - 1);
      this.setState({ fund_house: this.props.fund_house });
    }
    // alert(
    //   `api=1.1&limit=10&access_token=${access_token}&user=${clientid}&client_id=${client_id}&lead_id=${user_acc_id}&offset=${
    //     this.state.offset
    //   }&sort_value=${this.state.sort_value}&sort_type=${
    //     this.state.sort_type
    //   }&filter=${
    //     this.props.filter
    //   }&${rate}&${mininvt}&${asset_classcode}&${fund_house}`
    // );
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&limit=10&access_token=${access_token}&user=${client_id}&client_id=${client_id}&lead_id=${user_acc_id}&offset=${
        this.state.offset
      }&sort_value=${this.state.sort_value}&sort_type=${
        this.state.sort_type
      }&filter=${
        this.props.filter
      }&${rate}&${mininvt}&${asset_classcode}&${fund_house}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        var arr = this.state.itemList;
        var cont = this.state.count;

        var watchlist = this.state.watchlist;
        // alert(responseJson["data"]["schemes"])
        if (responseJson["data"] != null) {
          responseJson["data"]["schemes"].map(item1 => {
            arr.push(item1);

            responseJson["data"].short_listed.map(item => {
              if (item === item1.Unique_No) {
                watchlist[parseInt(item1.Unique_No)] = true;
              }
            });
          });
          if (this.state.offset >= 10) {
            cont = this.state.count;
          } else {
            cont = responseJson["data"].total_rows;
          }
          var off = this.state.offset;
          off = off + 10;

          this.setState({
            itemList: arr,
            offset: off,
            count: cont,
            watchlist: watchlist,
            isLoading: false,
            opacity: 1
          });
        }
      })
      .catch(error => {
        // alert(error);
      });
  }

  addtoWatchlist(schemeid, uniqueno, client_id, user_acc_id) {
    this.setState({ isLoading: true, opacity: 0.5 });
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.5&access_token=${access_token}&user=${client_id}&type=add&schemeid=${schemeid}&uniqueno=${uniqueno}&client_id=${client_id}&user_acc_id=${user_acc_id}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        const watchlist = this.state.watchlist;
        if (responseJson["data"]["watchlist"].status === "success") {
          watchlist[parseInt(uniqueno)] = true;
          this.refs.success.ShowToastFunction("Added to Watchlist");
        } else {
          this.refs.error.ShowToastFunction(
            responseJson["data"]["watchlist"].msg
          );
        }
        this.setState({ watchlist, isLoading: false, opacity: 1 });
      })
      .catch(error => {
        console.error(error);
      });
  }

  RemoveWatchlist(schemeid, uniqueno, client_id, user_acc_id, index) {
    this.setState({ isLoading: true, opacity: 0.5 });
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.5&access_token=${access_token}&user=${client_id}&type=remove&schemeid=${schemeid}&uniqueno=${uniqueno}&client_id=${client_id}&user_acc_id=${user_acc_id}` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        const watchlist = this.state.watchlist;
        if (responseJson["data"]["watchlist"].status === "success") {
          watchlist[parseInt(uniqueno)] = false;
          this.refs.error.ShowToastFunction("Removed from Watchlist");
        } else {
          this.refs.error.ShowToastFunction(
            responseJson["data"]["watchlist"].msg
          );
        }
        this.setState({ watchlist, isLoading: false, opacity: 1 });
      })
      .catch(error => {
        console.error(error);
      });
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onRight: this.handleIconTouch
    });
    this.setState(
      prevState => ({ hideNavBar: !prevState.hideNavBar }),
      () => Actions.refresh({ hideNavBar: !this.state.hideNavBar })
    );
    // this.retrieveData();
  }

  componentWillReceiveProps() {
    if (this.props.from === "Quote") {
      const watchlist = this.state.watchlist;
      setTimeout(() => {
        watchlist[parseInt(this.props.Unique_No)] = this.props.watchlist;
        this.setState({ watchlist });
      }, 200);
    } else {
      this.setState({ offset: 0, isLoading: true, opacity: 0 });
      setTimeout(() => {
        this.retrieveData();
      }, 200);
    }
  }

  showSortDialog1 = () => {
    this.showSortDialog.show();
  };

  renderfundimage(fundimage) {
    if (clienttype === "client") {
      if (fundimage >= 3) {
        return (
          <View
            style={[
              styles.circle,
              {
                backgroundColor: color.GREEN,
                alignItems: "center",
                marginTop: 10
              }
            ]}
          >
            <Image
              style={{
                width: 15,
                height: 15,
                resizeMode: "contain",
                alignItems: "center",
                marginTop: 7
              }}
              source={like}
            />
          </View>
        );
      } else {
        return (
          <View
            style={[
              styles.circle,
              {
                backgroundColor: color.RED,
                alignItems: "center",
                marginTop: 10
              }
            ]}
          >
            <Image
              style={{
                width: 15,
                height: 15,
                resizeMode: "contain",
                alignItems: "center",
                marginTop: 7
              }}
              source={dislike}
            />
          </View>
        );
      }
    } else {
      return (
        <Image
          style={{
            width: 40,
            height: 40,
            resizeMode: "contain",
            alignItems: "center",
            marginTop: 10
          }}
          source={require("../../assets/images/fund-lock.png")}
        />
      );
    }
  }

  rendertimeimage(timeimage, time_tooptip) {
    if (clienttype === "client") {
      if (timeimage === "1") {
        return (
          <View
            style={[
              styles.circle,
              {
                backgroundColor: color.GREEN,
                alignItems: "center",
                marginTop: 10
              }
            ]}
          >
            <Image
              style={{
                width: 15,
                height: 15,
                resizeMode: "contain",
                alignItems: "center",
                marginTop: 7
              }}
              source={like}
            />
          </View>
        );
      } else if (
        time_tooptip === "Enough data is not available for this scheme."
      ) {
        return (
          <Image
            style={{
              width: 30,
              height: 30,
              resizeMode: "contain",
              alignItems: "center",
              marginTop: 10
            }}
            source={newicon}
          />
        );
      } else {
        return (
          <View
            style={[
              styles.circle,
              {
                backgroundColor: color.RED,
                alignItems: "center",
                marginTop: 10
              }
            ]}
          >
            <Image
              style={{
                width: 15,
                width: 15,
                resizeMode: "contain",
                alignItems: "center"
              }}
              source={dislike}
            />
          </View>
        );
      }
    } else {
      return (
        <Image
          style={{
            width: 30,
            height: 30,
            resizeMode: "contain",
            alignItems: "center",
            marginTop: 10
          }}
          source={require("../../assets/images/time-to-invest-lock.png")}
        />
      );
    }
  }

  renderStrength(riskscore) {
    if (clienttype === "client") {
      if (riskscore === "0") {
        return (
          <Image
            source={require("../../assets/images/very_low_strength.png")}
            style={{
              width: 30,
              height: 30,
              resizeMode: "contain",
              alignItems: "center",
              marginTop: 9
            }}
          />
        );
      } else if (riskscore === "1") {
        return (
          <Image
            source={require("../../assets/images/low_strength.png")}
            style={{
              width: 30,
              height: 30,
              resizeMode: "contain",
              alignItems: "center",
              marginTop: 9
            }}
          />
        );
      } else if (riskscore === "2") {
        return (
          <Image
            source={require("../../assets/images/moderate_strength.png")}
            style={{
              width: 30,
              height: 30,
              resizeMode: "contain",
              alignItems: "center",
              marginTop: 9
            }}
          />
        );
      } else if (riskscore === "3") {
        return (
          <Image
            source={require("../../assets/images/high_strength.png")}
            style={{
              width: 30,
              height: 30,
              resizeMode: "contain",
              alignItems: "center",
              marginTop: 9
            }}
          />
        );
      } else if (riskscore === "4") {
        return (
          <Image
            source={require("../../assets/images/very_high_strength.png")}
            style={{
              width: 30,
              height: 30,
              resizeMode: "contain",
              alignItems: "center",
              marginTop: 9
            }}
          />
        );
      }
    } else {
      return (
        <Image
          style={{
            width: 30,
            height: 30,
            resizeMode: "contain",
            alignItems: "center",
            marginTop: 10
          }}
          source={require("../../assets/images/strength.png")}
        />
      );
    }
  }

  renderColor = item => {
    if (item === "1") {
      return color.Equity;
    } else if (item === "2") {
      return color.Hybrid;
    } else if (item === "3") {
      return color.Debt;
    } else if (item === "4") {
      return color.Commodity;
    } else if (item === "5") {
      return color.Other;
    }
  };

  renderStrengthTooltip(
    portfolio_quality,
    risk_volatility,
    corelation_becnhmark,
    asset_type
  ) {
    const arr = [dislike, like];
    const arr1 = [color.RED, color.GREEN];

    if (asset_type != "Debt") {
      return (
        <View style={{ padding: 5 }}>
          <View style={{ flexDirection: "row" }}>
            <View
              style={[
                styles.circle,
                {
                  backgroundColor: arr1[risk_volatility],
                  alignItems: "center",
                  height: 20,
                  width: 20
                }
              ]}
            >
              <Image
                style={{
                  width: 15,
                  height: 15,
                  resizeMode: "contain",
                  alignItems: "center",
                  marginTop: 5
                }}
                source={arr[risk_volatility]}
              />
            </View>
            <Text style={{ marginLeft: 15 }}>Resilience in volatility</Text>
          </View>
          <View style={{ flexDirection: "row", marginTop: 10 }}>
            <View
              style={[
                styles.circle,
                {
                  backgroundColor: arr1[corelation_becnhmark],
                  alignItems: "center",
                  height: 20,
                  width: 20
                }
              ]}
            >
              <Image
                style={{
                  width: 15,
                  height: 15,
                  resizeMode: "contain",
                  alignItems: "center",
                  marginTop: 5
                }}
                source={arr[corelation_becnhmark]}
              />
            </View>
            <Text style={{ marginLeft: 15 }}>
              Ability to generate higher return at lower risk
            </Text>
          </View>
          {asset_type === "Equity" ||
            (asset_type === "Hybrid" && (
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <View
                  style={[
                    styles.circle,
                    {
                      backgroundColor: arr1[portfolio_quality],
                      alignItems: "center",
                      height: 20,
                      width: 20
                    }
                  ]}
                >
                  <Image
                    style={{
                      width: 15,
                      height: 15,
                      resizeMode: "contain",
                      alignItems: "center",
                      marginTop: 5
                    }}
                    source={arr[portfolio_quality]}
                  />
                </View>
                <Text style={{ marginLeft: 15 }}>Quality of Portfolio</Text>
              </View>
            ))}
        </View>
      );
    } else {
      return (
        <View style={{ padding: 5 }}>
          <View style={{ flexDirection: "row" }}>
            <View
              style={[
                styles.circle,
                {
                  backgroundColor: arr1[portfolio_quality],
                  alignItems: "center",
                  height: 20,
                  width: 20
                }
              ]}
            >
              <Image
                style={{
                  width: 15,
                  height: 15,
                  resizeMode: "contain",
                  alignItems: "center",
                  marginTop: 5
                }}
                source={arr[portfolio_quality]}
              />
            </View>
            <Text style={{ marginLeft: 15 }}>Quality of Portfolio</Text>
          </View>
          <View style={{ flexDirection: "row", marginTop: 10 }}>
            <View
              style={[
                styles.circle,
                {
                  backgroundColor: arr1[corelation_becnhmark],
                  alignItems: "center",
                  height: 20,
                  width: 20
                }
              ]}
            >
              <Image
                style={{
                  width: 15,
                  height: 15,
                  resizeMode: "contain",
                  alignItems: "center",
                  marginTop: 5
                }}
                source={arr[corelation_becnhmark]}
              />
            </View>
            <Text style={{ marginLeft: 15 }}>Interest Rate Risk</Text>
          </View>
        </View>
      );
    }
  }

  renderRow(item, index) {
    var viewRef, viewRef2, viewRef3;

    // alert(this.state.showFund[index]);
    return (
      <Card containerStyle={{ padding: 0, margin: 10 }}>
        <TouchableOpacity
          onPress={() =>
            Actions.page1({
              unique_code: item.Unique_No,
              watchlist: this.state.watchlist[item.Unique_No],
              from: "ExploreAll"
            })
          }
        >
          <View
            style={{
              flexDirection: "row",
              marginTop: 15,
              marginLeft: 15,
              marginRight: 15
            }}
          >
            <Image
              source={{ uri: Apis.BaseImageURL + `${item.Amc_code}.jpg` }}
              style={{ height: 40, width: 40, resizeMode: "contain" }}
            />
            <Text
              style={[
                styles.title_carddetail,
                { fontWeight: "bold", marginLeft: 10, width: 0, flexGrow: 1 }
              ]}
            >
              {item.Scheme_Name}
            </Text>
          </View>
          <View
            style={{
              flexDirection: "column",
              marginLeft: 10,
              marginRight: 15,
              marginTop: 10
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <View
                style={{
                  backgroundColor: this.renderColor(item.asset_code),
                  borderRadius: 5,
                  marginLeft: 5
                }}
              >
                <Text
                  style={[
                    styles.ExploreAllText,
                    { color: color.WHITE, padding: 5 }
                  ]}
                >
                  {item.asset_type}
                </Text>
              </View>
              <View
                style={{
                  borderColor: this.renderColor(item.asset_code),
                  borderRadius: 5,
                  borderWidth: 1,
                  marginLeft: 5,
                  width: 0,
                  flexGrow: 1
                }}
              >
                <Text
                  style={[
                    styles.ExploreAllText,
                    { color: this.renderColor(item.asset_code), padding: 5 }
                  ]}
                >
                  {item.classname}
                </Text>
              </View>
            </View>

            <Text
              style={[
                styles.ExploreAllText,
                {
                  color: this.renderColor(item.asset_code),
                  padding: 5,
                  borderColor: this.renderColor(item.asset_code),
                  borderRadius: 5,
                  borderWidth: 1,
                  marginLeft: 5,
                  marginTop: 3
                }
              ]}
            >
              {item.category_name}
            </Text>
          </View>
          {/* <View style={[styles.dash,{margin:7}]}/> */}
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              marginTop: 10,
              marginLeft: 10,
              marginRight: 10
            }}
          >
            <TouchableOpacity
              style={{
                flexDirection: "column",
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
              onPress={() => this.updatestateFund(index, viewRef3)}
            >
              <Text style={[styles.ExploreAllText, { color: color.GREY }]}>
                Fund
              </Text>
              {this.renderfundimage(item.rating)}
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flexDirection: "column",
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
              onPress={() => this.updatestateRate(index)}
            >
              <Text style={[styles.ExploreAllText, { color: color.GREY }]}>
                Rating
              </Text>

              {clienttype === "client" ? (
                <StarRating
                  starStyle={{ marginTop: 13 }}
                  fullStar={require("../../assets/images/star.png")}
                  emptyStar={require("../../assets/images/emptystar.png")}
                  disabled={true}
                  maxStars={5}
                  starSize={18}
                  halfStarEnabled={true}
                  rating={parseInt(item.rating)}
                />
              ) : (
                <Image
                  style={{
                    width: 60,
                    height: 40,
                    resizeMode: "contain",
                    alignItems: "center",
                    marginTop: 10
                  }}
                  source={require("../../assets/images/rating-lock.png")}
                />
              )}
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                if (clienttype === "client") {
                  this.bounce(viewRef);
                }
              }}
              style={{
                flexDirection: "column",
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Animatable.View
                ref={ref => (viewRef = ref)}
                style={{ justifyContent: "center", alignItems: "center" }}
              >
                <Text style={[styles.ExploreAllText, { color: color.GREY }]}>
                  Rank
                </Text>
                {clienttype === "client" ? (
                  <View
                    style={[
                      styles.RankBadge,
                      {
                        backgroundColor: this.renderColor(item.asset_code),
                        marginTop: 10
                      }
                    ]}
                  >
                    <Text style={styles.RankBadgeText}>{item.rank_srno}</Text>
                  </View>
                ) : (
                  <Image
                    style={{
                      width: 40,
                      height: 40,
                      resizeMode: "contain",
                      alignItems: "center",
                      marginTop: 10
                    }}
                    source={require("../../assets/images/rank-lock.png")}
                  />
                )}
              </Animatable.View>
            </TouchableOpacity>
          </View>
          {this.state.showFund[index] && item.fund_tooltip != "" && (
            <Animatable.View
              duration={300}
              animation="zoomIn"
              style={{ marginTop: 5 }}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  width: 125
                }}
              >
                <View style={[styles.triangle]} />
              </View>
              <View
                style={{
                  backgroundColor: color.LIGHT_BLUE,
                  padding: 3,
                  marginLeft: 0,
                  marginRight: 0
                }}
              >
                <Text style={{ padding: 5 }}>{item.fund_tooltip}</Text>
              </View>
            </Animatable.View>
          )}
          {this.state.showRate[index] && cleantext(item.rating_tooltip) != "" && (
            <Animatable.View
              duration={300}
              animation="zoomIn"
              style={{ marginTop: 5 }}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  flex: 1
                }}
              >
                <View style={[styles.triangle]} />
              </View>
              <View
                style={{
                  backgroundColor: color.LIGHT_BLUE,
                  padding: 3,
                  marginLeft: 0,
                  marginRight: 0
                }}
              >
                <Text style={{ padding: 5 }}>
                  {cleantext(item.rating_tooltip)}
                </Text>
              </View>
            </Animatable.View>
          )}

          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              marginTop: 10,
              marginLeft: 10,
              marginRight: 10
            }}
          >
            <TouchableOpacity
              style={{
                flexDirection: "column",
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
              onPress={() => this.updatestateInvest(index, item.time_tooptip)}
            >
              <Text style={[styles.ExploreAllText, { color: color.GREY }]}>
                Time To Invest
              </Text>
              {this.rendertimeimage(item.time_flag, item.time_tooptip)}
              {/* <Image source={require('../../assets/images/like.png')}  style={{height:30,width:30,resizeMode:"contain",marginTop:5}}/> */}
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flexDirection: "column",
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
              onPress={() => this.updatestateStrength(index)}
            >
              <Text style={[styles.ExploreAllText, { color: color.GREY }]}>
                Strength
              </Text>
              {this.renderStrength(item.risk_score)}
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                if (clienttype === "client") {
                  this.bounce(viewRef2);
                }
              }}
              style={{
                flexDirection: "column",
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Animatable.View
                ref={ref => (viewRef2 = ref)}
                style={{ justifyContent: "center", alignItems: "center" }}
              >
                <Text style={[styles.ExploreAllText, { color: color.GREY }]}>
                  NAV
                </Text>
                <Text style={{ marginTop: 15 }}>
                  {"\u20B9"}
                  {item.Navrs}
                </Text>
              </Animatable.View>
            </TouchableOpacity>
          </View>

          {this.state.showInvest[index] && item.time_tooptip != "" && (
            <Animatable.View
              duration={300}
              animation="zoomIn"
              style={{ marginTop: 5 }}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  width: 125
                }}
              >
                <View style={[styles.triangle]} />
              </View>
              <View
                duration={800}
                style={{ backgroundColor: color.LIGHT_BLUE, padding: 3 }}
              >
                <Text style={{ padding: 5 }}>{item.time_tooptip}</Text>
              </View>
            </Animatable.View>
          )}
          {this.state.showStrength[index] && (
            <Animatable.View
              duration={300}
              animation="zoomIn"
              style={{ marginTop: 5 }}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  flex: 1
                }}
              >
                <View style={[styles.triangle]} />
              </View>
              <View
                duration={400}
                style={{ backgroundColor: color.LIGHT_BLUE, padding: 3 }}
              >
                {this.renderStrengthTooltip(
                  item.portfolio_quality,
                  item.risk_volatility,
                  item.corelation_becnhmark,
                  item.asset_type
                )}
              </View>
            </Animatable.View>
          )}
          <View
            style={{
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              marginTop: 10,
              marginLeft: 15,
              marginRight: 15
            }}
          >
            <Text style={[styles.ExploreAllText, { color: color.GREY }]}>
              Return
            </Text>
            <View style={{ flexDirection: "row" }}>
              {item.oneYRRET != "" && (
                <View
                  style={{
                    flexDirection: "row",
                    marginLeft: 5,
                    borderRadius: 5,
                    borderColor: "#D3D3D3",
                    alignItems: "center",
                    marginTop: 10,
                    borderWidth: 1,
                    overflow: "hidden"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 11,
                      padding: 5,
                      color: "#FFFFFF",
                      backgroundColor: "#D3D3D3"
                    }}
                  >
                    1Y
                  </Text>

                  {/* parseFloat(.toFixed(4)) */}

                  <Text
                    style={{
                      fontSize: 11,
                      padding: 5,
                      color: color.GREY,
                      backgroundColor: color.WHITE
                    }}
                  >
                    {parseFloat(item.oneYRRET).toFixed(2)}%
                  </Text>
                </View>
              )}

              {item.threeYEARRET != "" && (
                <View
                  style={{
                    flexDirection: "row",
                    marginLeft: 5,
                    borderRadius: 5,
                    borderColor: "#D3D3D3",
                    alignItems: "center",
                    marginTop: 10,
                    borderWidth: 1,
                    overflow: "hidden"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 11,
                      padding: 5,
                      color: "#FFFFFF",
                      backgroundColor: "#D3D3D3"
                    }}
                  >
                    3Y
                  </Text>

                  <Text
                    style={{
                      fontSize: 11,
                      padding: 5,
                      color: color.GREY,
                      backgroundColor: color.WHITE
                    }}
                  >
                    {parseFloat(item.threeYEARRET).toFixed(2)}%
                  </Text>
                </View>
              )}
              {item.fiveYEARRET != "" && (
                <View
                  style={{
                    flexDirection: "row",
                    marginLeft: 5,
                    borderRadius: 5,
                    borderColor: "#D3D3D3",
                    alignItems: "center",
                    marginTop: 10,
                    borderWidth: 1,
                    overflow: "hidden"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 11,
                      padding: 5,
                      color: "#FFFFFF",
                      backgroundColor: "#D3D3D3"
                    }}
                  >
                    5Y
                  </Text>

                  <Text
                    style={{
                      fontSize: 11,
                      padding: 5,
                      color: color.GREY,
                      backgroundColor: color.WHITE
                    }}
                  >
                    {parseFloat(item.fiveYEARRET).toFixed(2)}%
                  </Text>
                </View>
              )}
              {item.INCRET != "" && (
                <View
                  style={{
                    flexDirection: "row",
                    marginLeft: 5,
                    borderRadius: 5,
                    borderColor: "#D3D3D3",
                    alignItems: "center",
                    marginTop: 10,
                    borderWidth: 1,
                    overflow: "hidden"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 11,
                      padding: 5,
                      color: "#FFFFFF",
                      backgroundColor: "#D3D3D3"
                    }}
                  >
                    Incp.
                  </Text>

                  <Text
                    style={{
                      fontSize: 11,
                      padding: 5,
                      color: color.GREY,
                      backgroundColor: color.WHITE
                    }}
                  >
                    {parseFloat(item.INCRET).toFixed(2)}%
                  </Text>
                </View>
              )}
            </View>
          </View>
        </TouchableOpacity>
        <View style={[styles.dash, { margin: 7, marginTop: 10 }]} />
        <View
          style={{
            flexDirection: "row",
            marginBottom: 10,
            marginLeft: 15,
            marginRight: 15
          }}
        >
          <View style={{ flex: 0.5 }} />
          <View
            style={{
              flex: 1.5,
              flexDirection: "row",
              alignItems: "flex-end",
              justifyContent: "flex-end"
            }}
          >
            {this.state.watchlist[parseInt(item.Unique_No)] === true ? (
              <TouchableOpacity
                onPress={() =>
                  this.RemoveWatchlist(
                    item.schemecode,
                    item.Unique_No,
                    client_id,
                    user_acc_id,
                    index
                  )
                }
                style={{ flexDirection: "row" }}
              >
                <Image
                  source={require("../../assets/images/remove_watchlist.png")}
                  style={{
                    height: 20,
                    width: 10,
                    resizeMode: "contain",
                    marginTop: 5
                  }}
                />
                <Text
                  style={{
                    fontFamily: "Roboto",
                    color: color.RED,
                    marginLeft: 7,
                    marginTop: 5,
                    fontWeight: "700"
                  }}
                >
                  Remove{" "}
                </Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() =>
                  this.addtoWatchlist(
                    item.schemecode,
                    item.Unique_No,
                    client_id,
                    user_acc_id
                  )
                }
              >
                <Text
                  style={{
                    fontFamily: "Roboto",
                    color: color.toolbar,
                    fontWeight: "700"
                  }}
                >
                  Add to Watchlist
                </Text>
              </TouchableOpacity>
            )}
            <TouchableOpacity
              onPress={() =>
                Actions.page1({
                  unique_code: item.Unique_No,
                  watchlist: this.state.watchlist[item.Unique_No],
                  from: "ExploreAll"
                })
              }
            >
              <Text
                style={{
                  fontFamily: "Roboto",
                  color: color.GREEN,
                  marginLeft: 15,
                  fontWeight: "700"
                }}
              >
                Invest Now
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Card>
    );
  }

  handleIconTouch = () => {
    Actions.SearchMutualFund();
    //  Actions.refresh({hideNavBar:true});
    //  margin={marginTop:75}
    //  this.searchHeader.ref.show();
  };

  updatestateFund(index) {
    const showFund = this.state.showFund;
    const showRate = this.state.showRate;
    const showInvest = this.state.showInvest;
    const showStrength = this.state.showStrength;

    if (clienttype === "client") {
      if (showFund[index] === true) {
        showFund[index] = false;
      } else {
        showFund[index] = true;
        if (showRate[index] === true) {
          showRate[index] = false;
        }
        if (showInvest[index] === true) {
          showInvest[index] = false;
        }
        if (showStrength[index] === true) {
          showStrength[index] = false;
        }
      }

      // update state
      this.setState({
        showFund,
        showInvest,
        showRate,
        showStrength
      });
    }
  }

  updatestateRate(index) {
    const showFund = this.state.showFund;
    const showRate = this.state.showRate;
    const showInvest = this.state.showInvest;
    const showStrength = this.state.showStrength;

    if (clienttype === "client") {
      if (showRate[index] === true) {
        showRate[index] = false;
      } else {
        showRate[index] = true;
        if (showFund[index] === true) {
          showFund[index] = false;
        }
        if (showInvest[index] === true) {
          showInvest[index] = false;
        }
        if (showStrength[index] === true) {
          showStrength[index] = false;
        }
      }

      // update state
      this.setState({
        showFund,
        showInvest,
        showRate,
        showStrength
      });
    }
  }

  updatestateInvest(index, time_tooptip) {
    const showFund = this.state.showFund;
    const showRate = this.state.showRate;
    const showInvest = this.state.showInvest;
    const showStrength = this.state.showStrength;

    if (clienttype === "client") {
      if (time_tooptip != "Enough data is not available for this scheme.") {
        if (showInvest[index] === true) {
          showInvest[index] = false;
        } else {
          showInvest[index] = true;
          if (showFund[index] === true) {
            showFund[index] = false;
          }
          if (showRate[index] === true) {
            showRate[index] = false;
          }
          if (showStrength[index] === true) {
            showStrength[index] = false;
          }
        }

        // update state
        this.setState({
          showFund,
          showInvest,
          showRate,
          showStrength
        });
      }
    }
  }

  updatestateStrength(index) {
    const showFund = this.state.showFund;
    const showRate = this.state.showRate;
    const showInvest = this.state.showInvest;
    const showStrength = this.state.showStrength;

    if (clienttype === "client") {
      if (showStrength[index] === true) {
        showStrength[index] = false;
      } else {
        showStrength[index] = true;
        if (showFund[index] === true) {
          showFund[index] = false;
        }
        if (showInvest[index] === true) {
          showInvest[index] = false;
        }
        if (showRate[index] === true) {
          showRate[index] = false;
        }
      }

      // update state
      this.setState({
        showFund,
        showInvest,
        showRate,
        showStrength
      });
    }
  }

  onSort(sort_type, sort_value) {
    //alert(sort_type)
    this.setState(
      { offset: 0, sort_type: sort_type, sort_value: sort_value, itemList: [] },
      () => {
        this.showSortDialog.dismiss();
      }
    );
    setTimeout(() => {
      //this.props.navigation.navigate('Tabs');
      this.fetchData();
    }, 1000);
  }

  bounce = view => view.bounce(800).then(endState => "1");
  _keyExtractor = (item, index) => item.id + "";

  render() {
    return (
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: color.LIGHT_GREY
        }}
      >
        <StatusBar backgroundColor={color.toolbar} barStyle="light-content" />
        <View style={{ opacity: this.state.opacity }}>
          <View
            style={{
              flexDirection: "row",
              marginTop: 15,
              marginLeft: 15,
              marginBottom: 10
            }}
          >
            <View style={{ flex: 1.1 }}>
              <Text style={styles.ExploreAllText}>
                Total Funds ({this.state.count})
              </Text>
            </View>
            <View style={{ flex: 0.9, flexDirection: "row" }}>
              <TouchableOpacity
                style={{ flexDirection: "row" }}
                onPress={() =>
                  Actions.Filter({
                    rating: this.props.rating,
                    mininvt: this.props.mininvt,
                    asset_classcode: this.props.asset_classcode,
                    fundhouse: this.props.fund_house,
                    Equity: this.props.Equity,
                    Debt: this.props.Debt,
                    Hybrid: this.props.Hybrid,
                    Commodity: this.props.Commodity,
                    Others: this.props.Others,
                    Fund: this.props.Fund,
                    popular: this.props.popular
                  })
                }
              >
                <Image
                  source={require("../../assets/images/filter.png")}
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: "contain",
                    marginRight: 5
                  }}
                />
                <Text style={styles.ExploreAllText}>Filter</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flexDirection: "row", marginLeft: 15 }}
                onPress={this.showSortDialog1}
              >
                <Image
                  source={require("../../assets/images/sort.png")}
                  style={{
                    height: 20,
                    width: 20,
                    resizeMode: "contain",
                    marginRight: 5
                  }}
                />
                <Text style={styles.ExploreAllText}>Sort</Text>
              </TouchableOpacity>
            </View>
          </View>

          <FlatList
            contentContainerStyle={{
              marginBottom: 25,
              justifyContent: "center"
            }}
            data={this.state.itemList}
            renderItem={({ item, index }) => this.renderRow(item, index)}
            keyExtractor={this._keyExtractor}
            extraData={this.state}
            onEndReached={this.fetchData}
            onEndReachedThreshold={0.5}
          />
          <PopupDialog
            dialogStyle={{
              height: 450,
              width: 340,
              position: "absolute",
              top: 50
            }}
            dialogTitle={<DialogTitle title="Sort" />}
            ref={popupDialog => {
              this.showSortDialog = popupDialog;
            }}
            dialogAnimation={slideAnimation}
          >
            <View
              style={{ flex: 1, flexDirection: "column", marginBottom: 15 }}
            >
              <View
                style={{
                  flexDirection: "row",
                  flex: 1,
                  margin: 10,
                  marginTop: 15
                }}
              >
                <TouchableOpacity
                  style={{ flex: 0.5, marginLeft: 25 }}
                  onPress={() => this.onSort("asc", "rank_srno")}
                >
                  <Image
                    source={require("../../assets/images/asc.png")}
                    style={[{ height: 20, width: 20, resizeMode: "contain" }]}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ flex: 0.5 }}
                  onPress={() => this.onSort("desc", "rank_srno")}
                >
                  <Image
                    source={require("../../assets/images/desc.png")}
                    style={[{ height: 20, width: 20, resizeMode: "contain" }]}
                  />
                </TouchableOpacity>
                <Text style={[styles.item, { flex: 1, fontSize: 15 }]}>
                  Rank
                </Text>
              </View>
              <View style={[styles.dash, { margin: 10 }]} />
              <View
                style={{
                  flexDirection: "row",
                  flex: 1,
                  margin: 10,
                  marginTop: 15
                }}
              >
                <TouchableOpacity
                  style={{ flex: 0.5, marginLeft: 25 }}
                  onPress={() => this.onSort("asc", "Navrs")}
                >
                  <Image
                    source={require("../../assets/images/asc.png")}
                    style={[{ height: 20, width: 20, resizeMode: "contain" }]}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ flex: 0.5 }}
                  onPress={() => this.onSort("desc", "Navrs")}
                >
                  <Image
                    source={require("../../assets/images/desc.png")}
                    style={[{ height: 20, width: 20, resizeMode: "contain" }]}
                  />
                </TouchableOpacity>
                <Text style={[styles.item, { flex: 1, fontSize: 15 }]}>
                  Nav
                </Text>
              </View>
              <View style={[styles.dash, { margin: 10 }]} />
              <View
                style={{
                  flexDirection: "row",
                  flex: 1,
                  margin: 10,
                  marginTop: 15
                }}
              >
                <TouchableOpacity
                  style={{ flex: 0.5, marginLeft: 25 }}
                  onPress={() => this.onSort("asc", "1YRRET")}
                >
                  <Image
                    source={require("../../assets/images/asc.png")}
                    style={[{ height: 20, width: 20, resizeMode: "contain" }]}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ flex: 0.5 }}
                  onPress={() => this.onSort("desc", "1YRRET")}
                >
                  <Image
                    source={require("../../assets/images/desc.png")}
                    style={[{ height: 20, width: 20, resizeMode: "contain" }]}
                  />
                </TouchableOpacity>
                <Text style={[styles.item, { flex: 1, fontSize: 15 }]}>1Y</Text>
              </View>
              <View style={[styles.dash, { margin: 10 }]} />
              <View
                style={{
                  flexDirection: "row",
                  flex: 1,
                  margin: 10,
                  marginTop: 15
                }}
              >
                <TouchableOpacity
                  style={{ flex: 0.5, marginLeft: 25 }}
                  onPress={() => this.onSort("asc", "3YEARRET")}
                >
                  <Image
                    source={require("../../assets/images/asc.png")}
                    style={[{ height: 20, width: 20, resizeMode: "contain" }]}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ flex: 0.5 }}
                  onPress={() => this.onSort("desc", "3YEARRET")}
                >
                  <Image
                    source={require("../../assets/images/desc.png")}
                    style={[{ height: 20, width: 20, resizeMode: "contain" }]}
                  />
                </TouchableOpacity>
                <Text style={[styles.item, { flex: 1, fontSize: 15 }]}>3Y</Text>
              </View>
              <View style={[styles.dash, { margin: 10 }]} />
              <View
                style={{
                  flexDirection: "row",
                  flex: 1,
                  margin: 10,
                  marginTop: 15
                }}
              >
                <TouchableOpacity
                  style={{ flex: 0.5, marginLeft: 25 }}
                  onPress={() => this.onSort("asc", "5YEARRET")}
                >
                  <Image
                    source={require("../../assets/images/asc.png")}
                    style={[{ height: 20, width: 20, resizeMode: "contain" }]}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ flex: 0.5 }}
                  onPress={() => this.onSort("desc", "5YEARRET")}
                >
                  <Image
                    source={require("../../assets/images/desc.png")}
                    style={[{ height: 20, width: 20, resizeMode: "contain" }]}
                  />
                </TouchableOpacity>
                <Text style={[styles.item, { flex: 1, fontSize: 15 }]}>5Y</Text>
              </View>
              <View style={[styles.dash, { margin: 10 }]} />
              <View
                style={{
                  flexDirection: "row",
                  flex: 1,
                  margin: 10,
                  marginTop: 15
                }}
              >
                <TouchableOpacity
                  style={{ flex: 0.5, marginLeft: 25 }}
                  onPress={() => this.onSort("asc", "INCRET")}
                >
                  <Image
                    source={require("../../assets/images/asc.png")}
                    style={[{ height: 20, width: 20, resizeMode: "contain" }]}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ flex: 0.5 }}
                  onPress={() => this.onSort("desc", "INCRET")}
                >
                  <Image
                    source={require("../../assets/images/desc.png")}
                    style={[{ height: 20, width: 20, resizeMode: "contain" }]}
                  />
                </TouchableOpacity>
                <Text style={[styles.item, { flex: 1, fontSize: 15 }]}>
                  Since Inception
                </Text>
              </View>
            </View>
          </PopupDialog>
        </View>
        <CustomToast
          ref="success"
          backgroundColor="#4CAF50"
          position="bottom"
        />
        <CustomToast ref="error" backgroundColor="red" position="bottom" />
        {this.state.isLoading && (
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              flex: 1
            }}
          >
            <Image
              source={require("../../assets/images/loader.gif")}
              style={{ height: 30, width: 30, resizeMode: "contain" }}
            />
          </View>
        )}
      </View>
    );
  }
}
