import React, { Component } from "react";
import {
  Platform,
  Text,
  View,
  Image,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  TouchableHighlight,
  FlatList,
  AsyncStorage
} from "react-native";
import PopupDialog, {
  DialogButton,
  DialogTitle,
  SlideAnimation
} from "react-native-popup-dialog";
import { CheckBox, SearchBar } from "react-native-elements";
import { Actions } from "react-native-router-flux";
import Apis from "../../constants/Apis";
import styles from "../../components/styles";
import color from "../../constants/Colors";
import { Left } from "native-base";

const slideAnimation = new SlideAnimation({ slideFrom: "bottom" });
const access_token = "",
  client_id = "",
  clienttype = "",
  user_acc_id = "",
  clientid = "";

class Filter extends Component {
  search = {
    ref: null
  };

  constructor(props) {
    super(props);
    this.state = {
      hideNavBar: false,
      showMain: true,
      showList: false,
      rating: [],
      ratecolor: [
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY
      ],
      popcolor: [
        color.LIGHT_BLUE,
        color.LIGHT_BLUE,
        color.LIGHT_BLUE,
        color.LIGHT_BLUE,
        color.LIGHT_BLUE
      ],
      poptext: [
        color.toolbar,
        color.toolbar,
        color.toolbar,
        color.toolbar,
        color.toolbar
      ],
      asset_classcode: [],
      popular: [],
      popularclear: [],
      mininvtcheck: [],
      mininvt: [],
      filtername: "",
      text: "",
      itemList: [],
      newData: [],
      checked: [],
      Equity: [],
      Commodity: [],
      Others: [],
      Debt: [],
      Hybrid: [],
      Fund: [],
      Equityclear: [],
      Commodityclear: [],
      Othersclear: [],
      Debtclear: [],
      Hybridclear: [],
      Fundclear: [],
      fundhouse: [],
      filter: 0,
      equitycheck: "Check All",
      hybridcheck: "Check All",
      debtcheck: "Check All",
      commoditycheck: "Check All",
      otherscheck: "Check All",
      fundcheck: "Check All",
      ratingclear: [],
      mininvtclear: [],
      asset_classcodeclear: [],
      fund_houseclear: [],
      filterclear: 0,
      alterView: false
    };
  }

  ClearData() {
    this.setState({
      rating: [],
      ratecolor: [
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY,
        color.LIGHT_GREY
      ],
      popcolor: [
        color.LIGHT_BLUE,
        color.LIGHT_BLUE,
        color.LIGHT_BLUE,
        color.LIGHT_BLUE,
        color.LIGHT_BLUE
      ],
      poptext: [
        color.toolbar,
        color.toolbar,
        color.toolbar,
        color.toolbar,
        color.toolbar
      ],
      asset_classcode: [],
      popular: [],
      mininvtcheck: [],
      mininvt: [],
      filtername: "",
      text: "",
      itemList: [],
      checked: [],
      Equity: [],
      Commodity: [],
      Others: [],
      Debt: [],
      Hybrid: [],
      Fund: [],
      filter: 0,
      equitycheck: "Check All",
      hybridcheck: "Check All",
      debtcheck: "Check All",
      commoditycheck: "Check All",
      otherscheck: "Check All",
      fundcheck: "Check All"
    });
  }

  toggle = () => {
    this.setState(
      prevState => ({ hideNavBar: !prevState.hideNavBar }),
      () => Actions.refresh({ hideNavBar: this.state.hideNavBar })
    );
  };

  componentWillReceiveProps() {
    if (this.props.rating != null) {
      const ratecolor = this.state.ratecolor;
      this.props.rating.map(item => {
        if (this.state.rating.indexOf(item) === -1) {
          this.state.rating.push(item);
          this.state.ratingclear.push(item);
        }
        ratecolor[parseInt(item) - 1] = color.YELLOW;
      });
      this.setState({ ratecolor });
    }

    if (this.props.mininvt != null) {
      const mininvtcheck = this.state.mininvtcheck;

      this.props.mininvt.map(item => {
        if (this.state.mininvt.indexOf(item) === -1) {
          this.state.mininvt.push(item);
          this.state.mininvtclear.push(item);
        }
        if (item === "500") {
          mininvtcheck[0] = true;
        }
        if (item === "5000") {
          mininvtcheck[1] = true;
        }
        if (item === "5001") {
          mininvtcheck[2] = true;
        }
      });

      this.setState({ mininvtcheck });
    }

    if (this.props.asset_classcode != null) {
      const checked = this.state.checked;
      const popcolor = this.state.popcolor;
      const poptext = this.state.poptext;
      this.setState({
        Equity: this.props.Equity,
        Equityclear: this.props.Equity,
        Debt: this.props.Debt,
        Debtclear: this.props.Debt,
        Hybrid: this.props.Hybrid,
        Hybridclear: this.props.Hybrid,
        Commodity: this.props.Commodity,
        Commodityclear: this.props.Commodity,
        Others: this.props.Others,
        Othersclear: this.props.Others,
        popular: this.props.popular,
        popularclear: this.props.popular,
        asset_classcode: this.props.asset_classcode,
      });
      this.props.asset_classcode.map(item => {
        checked[parseInt(item)] = true;
        this.state.asset_classcodeclear.push(item)
      });

      this.props.popular.map(popular => {
        if (popular === "elss") {
          popcolor[0] = color.toolbar;
          poptext[0] = color.WHITE;
        } else if (popular === "multicap") {
          popcolor[1] = color.toolbar;
          poptext[1] = color.WHITE;
        } else if (popular === "hybrid") {
          popcolor[2] = color.toolbar;
          poptext[2] = color.WHITE;
        } else if (popular === "liquid") {
          popcolor[3] = color.toolbar;
          poptext[3] = color.WHITE;
        } else if (popular === "debt") {
          popcolor[4] = color.toolbar;
          poptext[4] = color.WHITE;
        }
      });

      this.setState({ checked, popcolor, poptext });
    }

    if (this.props.Fund != null) {
      this.setState({ Fund: this.props.Fund, Fundclear: this.props.Fund });
    }

  }

  componentDidMount() {
    this.toggle();
    this.retrieveData();
  }
  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
      if (clienttype === "client") {
        client_id = clientid;
      } else if (clienttype === "lead") {
        user_acc_id = clientid;
      }
      // this.fetchData();
    } catch (error) {}
  };

  SelectRating(rate) {
    const ratecolor = this.state.ratecolor;
    if (this.state.rating.indexOf(rate) > -1) {
      //this.state.rating.pop(rate);
      this.state.rating.splice(this.state.rating.indexOf(rate), 1);
      ratecolor[rate - 1] = color.LIGHT_GREY;
    } else {
      this.state.rating.push(rate);
      //alert(ratecolor);
      ratecolor[rate - 1] = color.YELLOW;
    }
    this.setState({ ratecolor });
  }

  SelectPopular(popular, index) {
    const popcolor = this.state.popcolor;
    const poptext = this.state.poptext;
    const checked = this.state.checked;

    if (this.state.popular.indexOf(popular) != -1) {
      if (popular === "elss") {
        this.state.asset_classcode.splice(
          this.state.asset_classcode.indexOf("8"),
          1
        );
        checked[8] = false;
        this.state.Equity.pop("Equity - ELSS");
      } else if (popular === "multicap") {
        this.state.asset_classcode.splice(
          this.state.asset_classcode.indexOf("49"),
          1
        );
        //this.state.asset_classcode.pop("49");
        this.state.Equity.pop("Equity - Multi Cap Fund");
        checked[49] = false;
      } else if (popular === "hybrid") {
        this.state.asset_classcode.splice(
          this.state.asset_classcode.indexOf("12"),
          1
        );
        this.state.asset_classcode.splice(
          this.state.asset_classcode.indexOf("19"),
          1
        );
        this.state.asset_classcode.splice(
          this.state.asset_classcode.indexOf("75"),
          1
        );
        this.state.asset_classcode.splice(
          this.state.asset_classcode.indexOf("14"),
          1
        );
        this.state.asset_classcode.splice(
          this.state.asset_classcode.indexOf("74"),
          1
        );
        this.state.asset_classcode.splice(
          this.state.asset_classcode.indexOf("57"),
          1
        );
        this.state.asset_classcode.splice(
          this.state.asset_classcode.indexOf("54"),
          1
        );

        this.state.Hybrid.pop("Hybrid - Aggressive Hybrid Fund");
        this.state.Hybrid.pop("Hybrid - Arbitrage Fund");
        this.state.Hybrid.pop("Hybrid - Balanced Advantage");
        this.state.Hybrid.pop("Hybrid - Conservative Hybrid Fund");
        this.state.Hybrid.pop("Hybrid - Dynamic Asset Allocation");
        this.state.Hybrid.pop("Hybrid - Equity Savings");
        this.state.Hybrid.pop("Hybrid - Multi Asset Allocation");

        checked[12] = false;
        checked[19] = false;
        checked[75] = false;
        checked[14] = false;
        checked[74] = false;
        checked[57] = false;
        checked[54] = false;

        this.setState({ hybridcheck: "Check All" });
      } else if (popular === "liquid") {
        this.state.asset_classcode.splice(
          this.state.asset_classcode.indexOf("24"),
          1
        );
        // this.state.asset_classcode.pop("24");
        this.state.Debt.pop("Debt - Liquid Fund");
        checked[24] = false;
      } else if (popular === "debt") {
        this.state.asset_classcode.splice(
          this.state.asset_classcode.indexOf("66"),
          1
        );
        this.state.asset_classcode.splice(
          this.state.asset_classcode.indexOf("16"),
          1
        );
        this.state.asset_classcode.splice(
          this.state.asset_classcode.indexOf("17"),
          1
        );

        // this.state.asset_classcode.pop("66");
        // this.state.asset_classcode.pop("16");
        // this.state.asset_classcode.pop("17");

        this.state.Debt.pop("Debt - Low Duration Fund");
        this.state.Debt.pop("Debt - Short Duration Fund");
        this.state.Debt.pop("Debt - Ultra Short Duration Fund");
        checked[66] = false;
        checked[16] = false;
        checked[17] = false;
      }
      popcolor[index - 1] = color.LIGHT_BLUE;
      poptext[index - 1] = color.toolbar;
      //this.state.popular.pop(popular);
      this.state.popular.splice(this.state.popular.indexOf(popular), 1);

      this.setState({ popcolor, poptext, checked });
    } else {
      this.state.popular.push(popular);
      if (popular === "elss") {
        this.state.asset_classcode.push("8");
        this.state.Equity.push("Equity - ELSS");
        checked[8] = true;
      } else if (popular === "multicap") {
        this.state.asset_classcode.push("49");
        this.state.Equity.push("Equity - Multi Cap Fund");
        checked[49] = true;
      } else if (popular === "hybrid") {
        this.state.asset_classcode.push("12");
        this.state.asset_classcode.push("19");
        this.state.asset_classcode.push("75");
        this.state.asset_classcode.push("14");
        this.state.asset_classcode.push("74");
        this.state.asset_classcode.push("57");
        this.state.asset_classcode.push("54");

        this.state.Hybrid.push("Hybrid - Aggressive Hybrid Fund");
        this.state.Hybrid.push("Hybrid - Arbitrage Fund");
        this.state.Hybrid.push("Hybrid - Balanced Advantage");
        this.state.Hybrid.push("Hybrid - Conservative Hybrid Fund");
        this.state.Hybrid.push("Hybrid - Dynamic Asset Allocation");
        this.state.Hybrid.push("Hybrid - Equity Savings");
        this.state.Hybrid.push("Hybrid - Multi Asset Allocation");

        checked[12] = true;
        checked[19] = true;
        checked[75] = true;
        checked[14] = true;
        checked[74] = true;
        checked[57] = true;
        checked[54] = true;

        this.setState({ hybridcheck: "Uncheck All" });
      } else if (popular === "liquid") {
        this.state.asset_classcode.push("24");
        this.state.Debt.push("Debt - Liquid Fund");
        checked[24] = true;
      } else if (popular === "debt") {
        this.state.asset_classcode.push("66");
        this.state.asset_classcode.push("16");
        this.state.asset_classcode.push("17");

        this.state.Debt.push("Debt - Low Duration Fund");
        this.state.Debt.push("Debt - Short Duration Fund");
        this.state.Debt.push("Debt - Ultra Short Duration Fund");
        checked[66] = true;
        checked[16] = true;
        checked[17] = true;
      }
      //alert(ratecolor);
      popcolor[index - 1] = color.toolbar;
      poptext[index - 1] = color.WHITE;

      this.setState({ popcolor, poptext, checked });
    }
  }

  Selectmininvt(index) {
    const mininvtcheck = this.state.mininvtcheck;
    if (index === 0) {
      if (mininvtcheck[index] === true) {
        mininvtcheck[index] = false;
        this.state.mininvt.splice(this.state.mininvt.indexOf("500"), 1);
      } else {
        mininvtcheck[index] = true;
        this.state.mininvt.push("500");
      }
    } else if (index === 1) {
      if (mininvtcheck[index] === true) {
        mininvtcheck[index] = false;
        this.state.mininvt.splice(this.state.mininvt.indexOf("5000"), 1);
      } else {
        mininvtcheck[index] = true;
        this.state.mininvt.push("5000");
      }
    } else if (index === 2) {
      if (mininvtcheck[index] === true) {
        mininvtcheck[index] = false;
        this.state.mininvt.splice(this.state.mininvt.indexOf("5001"), 1);
      } else {
        mininvtcheck[index] = true;
        this.state.mininvt.push("5001");
      }
    }

    this.setState({ mininvtcheck });
  }

  getData(filter) {
    this.setState({
      itemList: [],
      showList: true,
      showMain: false,
      filtername: filter
    });
    this.fetchData(filter);
  }

  fetchData(filter) {
    fetch(Apis.BaseURL, {
      method: "POST",
      headers: new Headers({
        "Content-Type": "application/x-www-form-urlencoded" // <-- Specifying the Content-Type
      }),
      body: `api=1.1&limit=10&access_token=${access_token}&user=${clientid}&client_id=${client_id}&lead_id=${user_acc_id}&offset=0&filter=0` // <-- Post parameters
    })
      .then(response => response.json())
      .then(responseJson => {
        var arr = [];
        // alert(responseJson["data"])
        if (filter === "Equity") {
          for (var key in responseJson["data"].assets_class
            .asset_class_equity) {
            arr.push(responseJson["data"].assets_class.asset_class_equity[key]);
          }
        } else if (filter === "Commodity") {
          for (var key in responseJson["data"].assets_class
            .asset_class_commodity) {
            arr.push(
              responseJson["data"].assets_class.asset_class_commodity[key]
            );
          }
        } else if (filter === "Debt") {
          for (var key in responseJson["data"].assets_class.asset_class_debt) {
            arr.push(responseJson["data"].assets_class.asset_class_debt[key]);
          }
        } else if (filter === "Hybrid") {
          for (var key in responseJson["data"].assets_class
            .asset_class_hybrid) {
            arr.push(responseJson["data"].assets_class.asset_class_hybrid[key]);
          }
        } else if (filter === "Others") {
          for (var key in responseJson["data"].assets_class.asset_class_other) {
            arr.push(responseJson["data"].assets_class.asset_class_other[key]);
          }
        } else if (filter === "Fund House") {
          for (var key in responseJson["data"].assets_class.fund_house) {
            arr.push(responseJson["data"].assets_class.fund_house[key]);
          }
        }

        // console.log(this.state.itemList);

        this.setState({ itemList: arr });
        // alert(this.state.itemList.length)
      })
      .catch(error => {
        console.error(error);
      });
  }

  searchFilterFunction = text => {
    if (this.state.filtername === "Fund House") {
      const newData1 = this.state.itemList.filter(function(item) {
        const itemData = item.fund.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      this.setState({
        newData: newData1,
        text: text
      });
    } else {
      const newData1 = this.state.itemList.filter(function(item) {
        const itemData = item.classname.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      this.setState({
        newData: newData1,
        text: text
      });
    }
  };

  ClearText() {
    this.setState({ text: "" });
    this.fetchData(this.state.filtername);
  }

  selectAsset(classcode, classname) {
    const checked = this.state.checked;
    const popcolor = this.state.popcolor;
    const poptext = this.state.poptext;

    if (checked[classcode] === true) {
      this.state.asset_classcode.splice(this.state.asset_classcode.indexOf(classcode + ""),1);
      checked[classcode] = false;
      if (this.state.filtername === "Equity") {
        this.state.Equity.splice(this.state.Equity.indexOf(classname), 1);
      } else if (this.state.filtername === "Debt") {
        this.state.Debt.splice(this.state.Debt.indexOf(classname), 1);
      } else if (this.state.filtername === "Hybrid") {
        tthis.state.Hybrid.splice(this.state.Hybrid.indexOf(classname), 1);
      } else if (this.state.filtername === "Commodity") {
        this.state.Commodity.splice(this.state.Commodity.indexOf(classname), 1);
      } else if (this.state.filtername === "Others") {
        this.state.Others.splice(this.state.Others.indexOf(classname), 1);
      }
      if (classcode === 8) {
        this.state.popular.splice(this.state.popular.indexOf("elss"), 1);
        popcolor[0] = color.LIGHT_BLUE;
        poptext[0] = color.toolbar;
      } else if (classcode === 49) {
        this.state.popular.splice(this.state.popular.indexOf("multicap"), 1);

        popcolor[1] = color.LIGHT_BLUE;
        poptext[1] = color.toolbar;
      } else if (classcode === 24) {
        this.state.popular.splice(this.state.popular.indexOf("liquid"), 1);

        popcolor[3] = color.LIGHT_BLUE;
        poptext[3] = color.toolbar;
      }
      this.setState({ checked });
    } else {
      this.state.asset_classcode.push(classcode + "");
      checked[classcode] = true;
      if (this.state.filtername === "Equity") {
        this.state.Equity.push(classname);
      } else if (this.state.filtername === "Debt") {
        this.state.Debt.push(classname);
      } else if (this.state.filtername === "Hybrid") {
        this.state.Hybrid.push(classname);
      } else if (this.state.filtername === "Commodity") {
        this.state.Commodity.push(classname);
      } else if (this.state.filtername === "Others") {
        this.state.Others.push(classname);
      }
      if (classcode === 8) {
        this.state.popular.push("elss");
        popcolor[0] = color.toolbar;
        poptext[0] = color.WHITE;
      } else if (classcode === 49) {
        this.state.popular.push("multicap");
        popcolor[1] = color.toolbar;
        poptext[1] = color.WHITE;
      } else if (classcode === 24) {
        this.state.popular.push("liquid");
        popcolor[3] = color.toolbar;
        poptext[3] = color.WHITE;
      } else if (
        classcode === 12 ||
        classcode === 14 ||
        classcode === 19 ||
        classcode === 75 ||
        classcode === 74 ||
        classcode === 57 ||
        classcode === 54
      ) {
        this.state.popular.push("hybrid");
        popcolor[2] = color.toolbar;
        poptext[2] = color.WHITE;
      } else if (classcode === 66 || classcode === 16 || classcode === 17) {
        this.state.popular.push("debt");
        popcolor[4] = color.toolbar;
        poptext[4] = color.WHITE;
      }
      this.setState({ checked });
    }

    this.setState({ popcolor, poptext });
    this.fetchData(this.state.filtername);
    alert(this.state.asset_classcodeclear+"--"+this.state.asset_classcode)
  }

  selectFund(classcode, classname) {
    const checked = this.state.checked;

    if (checked[classcode] === true) {
      this.state.fundhouse.splice(
        this.state.fundhouse.indexOf(classcode + ""),
        1
      );
      checked[classcode] = false;
      this.state.Fund.splice(this.state.Fund.indexOf(classname), 1);
    } else {
      this.state.fundhouse.push(classcode + "");
      checked[classcode] = true;
      this.state.Fund.push(classname);
    }
    this.setState({ checked });
    this.fetchData(this.state.filtername);
  }

  renderRow(item) {
    // alert(this.state.checked[parseInt(item.classcode)])
    if (this.state.filtername === "Fund House") {
      return (
        <View style={{ flexDirection: "row", padding: 5 }}>
          <Text
            style={[
              styles.RankBadgeText,
              { color: color.BLACK, textAlign: "left", flex: 0.9 }
            ]}
          >
            {item.fund}
          </Text>
          <CheckBox
            containerStyle={{
              flex: 0.1,
              borderWidth: 0,
              backgroundColor: color.WHITE,
              alignItems: "flex-end"
            }}
            checked={this.state.checked[parseInt(item.Amc_code)]}
            iconRight
            checkedColor={color.toolbar}
            onPress={() => this.selectFund(parseInt(item.Amc_code), item.fund)}
          />
        </View>
      );
    } else {
      return (
        <View style={{ flexDirection: "row", padding: 5 }}>
          <Text
            style={[
              styles.RankBadgeText,
              { color: color.BLACK, textAlign: "left", flex: 0.9 }
            ]}
          >
            {item.classname}
          </Text>
          <CheckBox
            containerStyle={{
              flex: 0.1,
              borderWidth: 0,
              backgroundColor: color.WHITE,
              alignItems: "flex-end"
            }}
            checked={this.state.checked[parseInt(item.classcode)]}
            iconRight
            checkedColor={color.toolbar}
            onPress={() =>
              this.selectAsset(parseInt(item.classcode), item.classname)
            }
          />
        </View>
      );
    }
  }

  renderEquity() {
    if (this.state.Equity.length > 3) {
      return (
        <View style={{ marginLeft: 25 }}>
          <Text
            style={[
              styles.RankBadgeText,
              {
                color: color.toolbar,
                flex: 1,
                flexWrap: "wrap",
                textAlign: "left"
              }
            ]}
          >
            {this.state.Equity[0]}, {this.state.Equity[1]},{" "}
            {this.state.Equity[2]} {"\t"}
            <Text
              style={{ backgroundColor: color.toolbar, color: color.WHITE }}
            >
              {this.state.Equity.length - 3} more
            </Text>
          </Text>
        </View>
      );
    } else {
      return (
        <Text
          style={[
            styles.RankBadgeText,
            {
              color: color.toolbar,
              flex: 1,
              flexWrap: "wrap",
              textAlign: "left",
              marginLeft: 25
            }
          ]}
        >
          {this.state.Equity.join(", ")}
        </Text>
      );
    }
  }
  renderDebt() {
    if (this.state.Debt.length > 3) {
      return (
        <View style={{ marginLeft: 25 }}>
          <Text
            style={[
              styles.RankBadgeText,
              {
                color: color.toolbar,
                flex: 1,
                flexWrap: "wrap",
                textAlign: "left"
              }
            ]}
          >
            {this.state.Debt[0]}, {this.state.Debt[1]}, {this.state.Debt[2]}{" "}
            {"\t"}
            <Text
              style={{ backgroundColor: color.toolbar, color: color.WHITE }}
            >
              {this.state.Debt.length - 3} more
            </Text>
          </Text>
        </View>
      );
    } else {
      return (
        <Text
          style={[
            styles.RankBadgeText,
            {
              color: color.toolbar,
              flex: 1,
              flexWrap: "wrap",
              textAlign: "left",
              marginLeft: 25
            }
          ]}
        >
          {this.state.Debt.join(", ")}
        </Text>
      );
    }
  }
  renderCommodity() {
    if (this.state.Commodity.length > 3) {
      return (
        <View style={{ marginLeft: 25 }}>
          <Text
            style={[
              styles.RankBadgeText,
              {
                color: color.toolbar,
                flex: 1,
                flexWrap: "wrap",
                textAlign: "left"
              }
            ]}
          >
            {this.state.Commodity[0]}, {this.state.Commodity[1]},{" "}
            {this.state.Commodity[2]} {"\t"}
            <Text
              style={{ backgroundColor: color.toolbar, color: color.WHITE }}
            >
              {this.state.Commodity.length - 3} more
            </Text>
          </Text>
        </View>
      );
    } else {
      return (
        <Text
          style={[
            styles.RankBadgeText,
            {
              color: color.toolbar,
              flex: 1,
              flexWrap: "wrap",
              textAlign: "left",
              marginLeft: 25
            }
          ]}
        >
          {this.state.Commodity.join(", ")}
        </Text>
      );
    }
  }
  renderHybrid() {
    if (this.state.Hybrid.length > 3) {
      return (
        <View style={{ marginLeft: 25 }}>
          <Text
            style={[
              styles.RankBadgeText,
              {
                color: color.toolbar,
                flex: 1,
                flexWrap: "wrap",
                textAlign: "left"
              }
            ]}
          >
            {this.state.Hybrid[0]}, {this.state.Hybrid[1]},{" "}
            {this.state.Hybrid[2]} {"\t"}
            <Text
              style={{ backgroundColor: color.toolbar, color: color.WHITE }}
            >
              {this.state.Hybrid.length - 3} more
            </Text>
          </Text>
        </View>
      );
    } else {
      return (
        <Text
          style={[
            styles.RankBadgeText,
            {
              color: color.toolbar,
              flex: 1,
              flexWrap: "wrap",
              textAlign: "left",
              marginLeft: 25
            }
          ]}
        >
          {this.state.Hybrid.join(", ")}
        </Text>
      );
    }
  }
  renderOthers() {
    if (this.state.Others.length > 3) {
      return (
        <View style={{ marginLeft: 25 }}>
          <Text
            style={[
              styles.RankBadgeText,
              {
                color: color.toolbar,
                flex: 1,
                flexWrap: "wrap",
                textAlign: "left"
              }
            ]}
          >
            {this.state.Others[0]}, {this.state.Others[1]},{" "}
            {this.state.Others[2]} {"\t"}
            <Text
              style={{ backgroundColor: color.toolbar, color: color.WHITE }}
            >
              {this.state.Others.length - 3} more
            </Text>
          </Text>
        </View>
      );
    } else {
      return (
        <Text
          style={[
            styles.RankBadgeText,
            {
              color: color.toolbar,
              flex: 1,
              flexWrap: "wrap",
              textAlign: "left",
              marginLeft: 25
            }
          ]}
        >
          {this.state.Others.join(", ")}
        </Text>
      );
    }
  }
  renderFund() {
    if (this.state.Fund.length > 3) {
      return (
        <View style={{ marginLeft: 25 }}>
          <Text
            style={[
              styles.RankBadgeText,
              {
                color: color.toolbar,
                flex: 1,
                flexWrap: "wrap",
                textAlign: "left"
              }
            ]}
          >
            {this.state.Fund[0]}, {this.state.Fund[1]}, {this.state.Fund[2]}{" "}
            {"\t"}
            <Text
              style={{ backgroundColor: color.toolbar, color: color.WHITE }}
            >
              {this.state.Fund.length - 3} more
            </Text>
          </Text>
        </View>
      );
    } else {
      return (
        <Text
          style={[
            styles.RankBadgeText,
            {
              color: color.toolbar,
              flex: 1,
              flexWrap: "wrap",
              textAlign: "left",
              marginLeft: 25
            }
          ]}
        >
          {this.state.Fund.join(", ")}
        </Text>
      );
    }
  }

  showSlideAnimationDialog = () => {
    this.slideAnimationDialog.show();
  };

  redirect() {
    // alert(this.state.asset_classcode);
    this.setState({
      filter: 1,
      filterclear: 1,
      ratingclear: this.state.rating,
      mininvtclear: this.state.mininvt,
      asset_classcodeclear: this.state.asset_classcode,
      fund_houseclear: this.state.fundhouse,
      Equityclear: this.state.Equity,
      Debtclear: this.state.Debt,
      Hybridclear: this.state.Hybrid,
      Commodityclear: this.state.Commodity,
      Othersclear: this.state.Others,
      Fundclear: this.state.Fund,
      popularclear: this.state.popular
    });
    setTimeout(() => {
      Actions.ExploreAll({
        rating: this.state.rating,
        mininvt: this.state.mininvt,
        asset_classcode: this.state.asset_classcode,
        fund_house: this.state.fundhouse,
        filter: 1,
        Equity: this.state.Equity,
        Debt: this.state.Debt,
        Hybrid: this.state.Hybrid,
        Commodity: this.state.Commodity,
        Others: this.state.Others,
        Fund: this.state.Fund,
        popular: this.state.popular
      });
    }, 500);
  }

  discardChanges() {
    alert(this.state.asset_classcodeclear)
    this.setState({
      filter: 1,
      filterclear: 1,
      rating: this.state.ratingclear,
      mininvt: this.state.mininvtclear,
      asset_classcode: this.state.asset_classcodeclear,
      fundhouse: this.state.fund_houseclear,
      Equity: this.state.Equityclear,
      Debt: this.state.Debtclear,
      Hybrid: this.state.Hybridclear,
      Commodity: this.state.Commodityclear,
      Others: this.state.Othersclear,
      Fund: this.state.Fundclear,
      popular: this.state.popularclear
    });
    this.slideAnimationDialog.dismiss();
    setTimeout(() => {
      Actions.ExploreAll({
        rating: this.state.ratingclear,
        mininvt: this.state.mininvtclear,
        asset_classcode: this.state.asset_classcodeclear,
        fund_house: this.state.fund_houseclear,
        filter: this.state.filterclear,
        Equity: this.state.Equityclear,
        Debt: this.state.Debtclear,
        Hybrid: this.state.Hybridclear,
        Commodity: this.state.Commodityclear,
        Others: this.state.Othersclear,
        Fund: this.state.Fundclear,
        popular: this.state.popularclear
      });
    }, 500);
  }

  cancel() {
  
    if(this.state.rating.length > 0 ||
      this.state.mininvt.length > 0 ||
      this.state.asset_classcode.length > 0 ||
      this.state.fundhouse.length > 0)
    {
      if (
      this.state.rating != this.state.ratingclear ||
      this.state.mininvt != this.state.mininvtclear ||
      this.state.asset_classcode != this.state.asset_classcodeclear ||
      this.state.fundhouse != this.state.fund_houseclear
    ) {
        //alert(1);
      this.showSlideAnimationDialog();
    } else {
        // alert(2);
      this.discardChanges();
    }
  }else{
    this.discardChanges();
  }
  }

  checkAll(name) {
    const checked = this.state.checked;
    const popcolor = this.state.popcolor;
    const poptext = this.state.poptext;
    if (name === "Equity") {
      if (this.state.equitycheck === "Check All") {
        this.setState({ equitycheck: "Uncheck All" });

        this.state.itemList.map((item, i) => {
          checked[parseInt(item.classcode)] = true;
          this.state.Equity.push(item.classname);
          this.state.asset_classcode.push(item.classcode);
          this.setState({ checked });
        });
        this.state.popular.push("elss");
        popcolor[0] = color.toolbar;
        poptext[0] = color.WHITE;
        this.state.popular.push("multicap");
        popcolor[1] = color.toolbar;
        poptext[1] = color.WHITE;
      } else {
        this.setState({ equitycheck: "Check All" });
        this.state.itemList.map((item, i) => {
          checked[parseInt(item.classcode)] = false;
          this.state.Equity.pop(item.classname);
          this.state.asset_classcode.pop(item.classcode);
          this.setState({ checked });
        });
        this.state.popular.pop("elss");
        popcolor[0] = color.LIGHT_BLUE;
        poptext[0] = color.toolbar;
        this.state.popular.pop("multicap");
        popcolor[1] = color.LIGHT_BLUE;
        poptext[1] = color.toolbar;
      }
    } else if (name === "Debt") {
      if (this.state.debtcheck === "Check All") {
        this.setState({ debtcheck: "Uncheck All" });
        this.state.itemList.map((item, i) => {
          checked[parseInt(item.classcode)] = true;
          this.state.Debt.push(item.classname);
          this.state.asset_classcode.push(item.classcode);
          this.setState({ checked });
        });
        this.state.popular.push("liquid");
        popcolor[3] = color.toolbar;
        poptext[3] = color.WHITE;

        this.state.popular.push("debt");
        popcolor[4] = color.toolbar;
        poptext[4] = color.WHITE;
      } else {
        this.setState({ debtcheck: "Check All" });
        this.state.itemList.map((item, i) => {
          checked[parseInt(item.classcode)] = false;
          this.state.Debt.pop(item.classname);
          this.state.asset_classcode.pop(item.classcode);
          this.setState({ checked });
        });

        this.state.popular.pop("liquid");
        popcolor[3] = color.LIGHT_BLUE;
        poptext[3] = color.toolbar;
        this.state.popular.pop("debt");
        popcolor[4] = color.LIGHT_BLUE;
        poptext[4] = color.toolbar;
      }
    } else if (name === "Hybrid") {
      if (this.state.hybridcheck === "Check All") {
        this.setState({ hybridcheck: "Uncheck All" });
        this.state.itemList.map((item, i) => {
          checked[parseInt(item.classcode)] = true;
          this.state.Hybrid.push(item.classname);
          this.state.asset_classcode.push(item.classcode);
          this.setState({ checked });
        });
        this.state.popular.push("hybrid");
        popcolor[2] = color.toolbar;
        poptext[2] = color.WHITE;
      } else {
        this.setState({ hybridcheck: "Check All" });
        this.state.itemList.map((item, i) => {
          checked[parseInt(item.classcode)] = false;
          this.state.Hybrid.pop(item.classname);
          this.state.asset_classcode.pop(item.classcode);
          this.setState({ checked });
        });
        this.state.popular.pop("hybrid");
        popcolor[2] = color.LIGHT_BLUE;
        poptext[2] = color.toolbar;
      }
    } else if (name === "Commodity") {
      if (this.state.commoditycheck === "Check All") {
        this.setState({ commoditycheck: "Uncheck All" });
        this.state.itemList.map((item, i) => {
          checked[parseInt(item.classcode)] = true;
          this.state.Commodity.push(item.classname);
          this.state.asset_classcode.push(item.classcode);
          this.setState({ checked });
        });
      } else {
        this.setState({ commoditycheck: "Check All" });
        this.state.itemList.map((item, i) => {
          checked[parseInt(item.classcode)] = false;
          this.state.Commodity.pop(item.classname);
          this.state.asset_classcode.pop(item.classcode);
          this.setState({ checked });
        });
      }
    } else if (name === "Others") {
      if (this.state.otherscheck === "Check All") {
        this.setState({ otherscheck: "Uncheck All" });
        this.state.itemList.map((item, i) => {
          checked[parseInt(item.classcode)] = true;
          this.state.Others.push(item.classname);
          this.state.asset_classcode.push(item.classcode);
          this.setState({ checked });
        });
      } else {
        this.setState({ otherscheck: "Check All" });
        this.state.itemList.map((item, i) => {
          checked[parseInt(item.classcode)] = false;
          this.state.Others.pop(item.classname);
          this.state.asset_classcode.pop(item.classcode);
          this.setState({ checked });
        });
      }
    } else if (name === "Fund House") {
      if (this.state.fundcheck === "Check All") {
        this.setState({ fundcheck: "Uncheck All" });
        this.state.itemList.map((item, i) => {
          checked[parseInt(item.Amc_code)] = true;
          this.state.Fund.push(item.fund);
          this.state.asset_classcode.push(item.Amc_code);
          this.setState({ checked });
        });
      } else {
        this.setState({ fundcheck: "Check All" });
        this.state.itemList.map((item, i) => {
          checked[parseInt(item.Amc_code)] = false;
          this.state.Fund.pop(item.fund);
          this.state.asset_classcode.pop(item.Amc_code);
          this.setState({ checked });
        });
      }
    }
    this.setState({ popcolor, poptext });
    this.fetchData(name);
  }

  renderCheck(name) {
    if (name === "Equity") {
      return this.state.equitycheck;
    } else if (name === "Debt") {
      return this.state.debtcheck;
    } else if (name === "Hybrid") {
      return this.state.hybridcheck;
    } else if (name === "Commodity") {
      return this.state.commoditycheck;
    } else if (name === "Others") {
      return this.state.otherscheck;
    } else if (name === "Fund House") {
      return this.state.fundcheck;
    }
  }

  _keyExtractor = item => item.classcode + "";

  render() {
    return (
      <View
        style={{
          flexDirection: "column",
          flex: 1,
          backgroundColor: color.WHITE
        }}
      >
        <StatusBar backgroundColor={color.toolbar} barStyle="light-content" />

        {this.state.showMain && (
          <View
            style={{ marginTop: 45, marginLeft: 25, flexDirection: "column" }}
          >
            <TouchableOpacity onPress={() => this.cancel()}>
              <Image
                source={require("../../assets/images/cross.png")}
                style={{ height: 15, width: 15, resizeMode: "contain" }}
              />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 23,
                fontWeight: "bold",
                fontFamily: "Roboto",
                marginTop: 20
              }}
            >
              Filter
            </Text>
          </View>
        )}

        {this.state.showMain && (
          <ScrollView style={{ flex: 1.8 }}>
            <View>
              <View style={{ padding: 10, marginTop: 20 }}>
                <Text style={[styles.item, { marginLeft: 10 }]}>
                  Asset Class
                </Text>
                <View style={[styles.dash, { marginTop: 10 }]} />
                <TouchableOpacity onPress={() => this.getData("Equity")}>
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: "row",
                      marginLeft: 20
                    }}
                  >
                    <Text style={[styles.item, { flex: 1.8 }]}>Equity</Text>
                    <Image
                      source={require("../../assets/images/arrow_forward.png")}
                      style={{
                        height: 15,
                        width: 15,
                        resizeMode: "contain",
                        justifyContent: "center",
                        flex: 0.2,
                        marginTop: 10
                      }}
                    />
                  </View>
                  {this.state.Equity != "" && this.renderEquity()}
                </TouchableOpacity>
                <View style={[styles.dash, { marginTop: 10 }]} />
                <TouchableOpacity onPress={() => this.getData("Debt")}>
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: "row",
                      marginLeft: 20
                    }}
                  >
                    <Text style={[styles.item, { flex: 1.8 }]}>Debt</Text>
                    <Image
                      source={require("../../assets/images/arrow_forward.png")}
                      style={{
                        height: 15,
                        width: 15,
                        resizeMode: "contain",
                        justifyContent: "center",
                        flex: 0.2,
                        marginTop: 10
                      }}
                    />
                  </View>

                  {this.state.Debt != "" && this.renderDebt()}
                </TouchableOpacity>
                <View style={[styles.dash, { marginTop: 10 }]} />
                <TouchableOpacity onPress={() => this.getData("Commodity")}>
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: "row",
                      marginLeft: 20
                    }}
                  >
                    <Text style={[styles.item, { flex: 1.8 }]}>Commodity</Text>
                    <Image
                      source={require("../../assets/images/arrow_forward.png")}
                      style={{
                        height: 15,
                        width: 15,
                        resizeMode: "contain",
                        justifyContent: "center",
                        flex: 0.2,
                        marginTop: 10
                      }}
                    />
                  </View>

                  {this.state.Commodity != "" && this.renderCommodity()}
                </TouchableOpacity>
                <View style={[styles.dash, { marginTop: 10 }]} />

                <TouchableOpacity onPress={() => this.getData("Hybrid")}>
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: "row",
                      marginLeft: 20
                    }}
                  >
                    <Text style={[styles.item, { flex: 1.8 }]}>Hybrid</Text>
                    <Image
                      source={require("../../assets/images/arrow_forward.png")}
                      style={{
                        height: 15,
                        width: 15,
                        resizeMode: "contain",
                        justifyContent: "center",
                        flex: 0.2,
                        marginTop: 10
                      }}
                    />
                  </View>

                  {this.state.Hybrid != "" && this.renderHybrid()}
                </TouchableOpacity>
                <View style={[styles.dash, { marginTop: 10 }]} />
                <TouchableOpacity onPress={() => this.getData("Others")}>
                  <View
                    style={{
                      marginTop: 10,
                      flexDirection: "row",
                      marginLeft: 20
                    }}
                  >
                    <Text style={[styles.item, { flex: 1.8 }]}>Others</Text>
                    <Image
                      source={require("../../assets/images/arrow_forward.png")}
                      style={{
                        height: 15,
                        width: 15,
                        resizeMode: "contain",
                        justifyContent: "center",
                        flex: 0.2,
                        marginTop: 10
                      }}
                    />
                  </View>
                  {this.state.Others != "" && this.renderOthers()}
                </TouchableOpacity>
                <View style={[styles.dash, { marginTop: 10 }]} />
              </View>

              <View
                style={{ flexDirection: "column", marginTop: 15, padding: 10 }}
              >
                <Text style={[styles.item, { marginLeft: 10 }]}>Rating</Text>
                <View style={{ flexDirection: "row", marginTop: 10 }}>
                  <TouchableHighlight
                    style={[
                      styles.RankBadge,
                      {
                        backgroundColor: this.state.ratecolor[4],
                        marginLeft: 10
                      }
                    ]}
                    onPress={() => this.SelectRating("5")}
                  >
                    <View style={{ flexDirection: "row", padding: 3 }}>
                      <Image
                        source={require("../../assets/images/starblack.png")}
                        style={{
                          height: 15,
                          width: 15,
                          resizeMode: "contain",
                          marginLeft: Platform.OS === "ios" ? 7 : 5,
                          marginTop: Platform.OS === "ios" ? 7 : 5
                        }}
                      />
                      <Text
                        style={[styles.RankBadgeText, { color: color.BLACK }]}
                      >
                        5
                      </Text>
                    </View>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={[
                      styles.RankBadge,
                      {
                        backgroundColor: this.state.ratecolor[3],
                        marginLeft: 10
                      }
                    ]}
                    onPress={() => this.SelectRating("4")}
                  >
                    <View style={{ flexDirection: "row", padding: 3 }}>
                      <Image
                        source={require("../../assets/images/starblack.png")}
                        style={{
                          height: 15,
                          width: 15,
                          resizeMode: "contain",
                          marginLeft: Platform.OS === "ios" ? 7 : 5,
                          marginTop: Platform.OS === "ios" ? 7 : 5
                        }}
                      />
                      <Text
                        style={[styles.RankBadgeText, { color: color.BLACK }]}
                      >
                        4
                      </Text>
                    </View>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={[
                      styles.RankBadge,
                      {
                        backgroundColor: this.state.ratecolor[2],
                        marginLeft: 10
                      }
                    ]}
                    onPress={() => this.SelectRating("3")}
                  >
                    <View style={{ flexDirection: "row", padding: 3 }}>
                      <Image
                        source={require("../../assets/images/starblack.png")}
                        style={{
                          height: 15,
                          width: 15,
                          resizeMode: "contain",
                          marginLeft: Platform.OS === "ios" ? 7 : 5,
                          marginTop: Platform.OS === "ios" ? 7 : 5
                        }}
                      />
                      <Text
                        style={[styles.RankBadgeText, { color: color.BLACK }]}
                      >
                        3
                      </Text>
                    </View>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={[
                      styles.RankBadge,
                      {
                        backgroundColor: this.state.ratecolor[1],
                        marginLeft: 10
                      }
                    ]}
                    onPress={() => this.SelectRating("2")}
                  >
                    <View style={{ flexDirection: "row", padding: 3 }}>
                      <Image
                        source={require("../../assets/images/starblack.png")}
                        style={{
                          height: 15,
                          width: 15,
                          resizeMode: "contain",
                          marginLeft: Platform.OS === "ios" ? 7 : 5,
                          marginTop: Platform.OS === "ios" ? 7 : 5
                        }}
                      />
                      <Text
                        style={[styles.RankBadgeText, { color: color.BLACK }]}
                      >
                        2
                      </Text>
                    </View>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={[
                      styles.RankBadge,
                      {
                        backgroundColor: this.state.ratecolor[0],
                        marginLeft: 10
                      }
                    ]}
                    onPress={() => this.SelectRating("1")}
                  >
                    <View style={{ flexDirection: "row", padding: 3 }}>
                      <Image
                        source={require("../../assets/images/starblack.png")}
                        style={{
                          height: 15,
                          width: 15,
                          resizeMode: "contain",
                          marginLeft: Platform.OS === "ios" ? 7 : 5,
                          marginTop: Platform.OS === "ios" ? 7 : 5
                        }}
                      />
                      <Text
                        style={[styles.RankBadgeText, { color: color.BLACK }]}
                      >
                        1
                      </Text>
                    </View>
                  </TouchableHighlight>
                </View>
                <View style={[styles.dash, { marginTop: 10 }]} />
              </View>
              <View
                style={{ flexDirection: "column", marginTop: 15, padding: 10 }}
              >
                <Text style={[styles.item, { marginLeft: 10 }]}>
                  Popular Filter
                </Text>
                <View style={{ flexDirection: "row", marginTop: 10 }}>
                  <TouchableHighlight
                    style={[
                      styles.RankBadge,
                      {
                        flex: 0.2,
                        backgroundColor: this.state.popcolor[0],
                        marginLeft: 10,
                        borderRadius: 20,
                        width: 0
                      }
                    ]}
                    onPress={() => this.SelectPopular("elss", 1)}
                  >
                    <Text
                      style={[
                        styles.RankBadgeText,
                        { color: this.state.poptext[0] }
                      ]}
                    >
                      ELSS
                    </Text>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={[
                      styles.RankBadge,
                      {
                        flex: 0.4,
                        backgroundColor: this.state.popcolor[1],
                        marginLeft: 10,
                        borderRadius: 20,
                        width: 0
                      }
                    ]}
                    onPress={() => this.SelectPopular("multicap", 2)}
                  >
                    <Text
                      style={[
                        styles.RankBadgeText,
                        { color: this.state.poptext[1] }
                      ]}
                    >
                      Multicap Equity
                    </Text>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={[
                      styles.RankBadge,
                      {
                        flex: 0.2,
                        backgroundColor: this.state.popcolor[2],
                        marginLeft: 10,
                        borderRadius: 20,
                        width: 0,
                        marginRight: 10
                      }
                    ]}
                    onPress={() => this.SelectPopular("hybrid", 3)}
                  >
                    <Text
                      style={[
                        styles.RankBadgeText,
                        { color: this.state.poptext[2] }
                      ]}
                    >
                      Hybrid
                    </Text>
                  </TouchableHighlight>
                </View>
                <View style={{ flexDirection: "row", marginTop: 10 }}>
                  <TouchableHighlight
                    style={[
                      styles.RankBadge,
                      {
                        flex: 0.3,
                        backgroundColor: this.state.popcolor[3],
                        marginLeft: 10,
                        borderRadius: 20,
                        width: 0
                      }
                    ]}
                    onPress={() => this.SelectPopular("liquid", 4)}
                  >
                    <Text
                      style={[
                        styles.RankBadgeText,
                        { color: this.state.poptext[3] }
                      ]}
                    >
                      Liquid
                    </Text>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={[
                      styles.RankBadge,
                      {
                        flex: 0.4,
                        backgroundColor: this.state.popcolor[4],
                        marginLeft: 10,
                        borderRadius: 20,
                        width: 0
                      }
                    ]}
                    onPress={() => this.SelectPopular("debt", 5)}
                  >
                    <Text
                      style={[
                        styles.RankBadgeText,
                        { color: this.state.poptext[4] }
                      ]}
                    >
                      Short term debt
                    </Text>
                  </TouchableHighlight>
                </View>
                <View style={[styles.dash, { marginTop: 10 }]} />
              </View>
              <View
                style={{ flexDirection: "column", marginTop: 15, padding: 10 }}
              >
                <Text style={[styles.item, { marginLeft: 10 }]}>
                  Minimum Investment Range
                </Text>
                <View style={[styles.dash, { marginTop: 10 }]} />
                <CheckBox
                  title="Less than Rs.500"
                  checkedIcon="dot-circle-o"
                  uncheckedIcon="circle-o"
                  checkedColor={color.toolbar}
                  containerStyle={{
                    backgroundColor: color.WHITE,
                    borderWidth: 0
                  }}
                  checked={this.state.mininvtcheck[0]}
                  onPress={() => this.Selectmininvt(0)}
                />
                <View style={[styles.dash, { marginTop: 5 }]} />
                <CheckBox
                  title="Less than Rs.5000"
                  checkedIcon="dot-circle-o"
                  uncheckedIcon="circle-o"
                  checkedColor={color.toolbar}
                  containerStyle={{
                    backgroundColor: color.WHITE,
                    borderWidth: 0
                  }}
                  checked={this.state.mininvtcheck[1]}
                  onPress={() => this.Selectmininvt(1)}
                />
                <View style={[styles.dash, { marginTop: 5 }]} />
                <CheckBox
                  title="Greater than Rs.5000"
                  checkedIcon="dot-circle-o"
                  uncheckedIcon="circle-o"
                  checkedColor={color.toolbar}
                  containerStyle={{
                    backgroundColor: color.WHITE,
                    borderWidth: 0
                  }}
                  checked={this.state.mininvtcheck[2]}
                  onPress={() => this.Selectmininvt(2)}
                />
                <View style={[styles.dash, { marginTop: 5 }]} />

                <View style={{ padding: 10 }}>
                  <TouchableOpacity onPress={() => this.getData("Fund House")}>
                    <View
                      style={{
                        marginTop: 10,
                        flexDirection: "row",
                        marginLeft: 10
                      }}
                    >
                      <Text style={[styles.item, { flex: 1.8 }]}>
                        Fund House
                      </Text>
                      <Image
                        source={require("../../assets/images/arrow_forward.png")}
                        style={{
                          height: 15,
                          width: 15,
                          resizeMode: "contain",
                          justifyContent: "center",
                          flex: 0.2,
                          marginTop: 10
                        }}
                      />
                    </View>
                    {this.state.Fund != "" && this.renderFund()}
                  </TouchableOpacity>
                </View>
                <View style={[styles.dash, { marginTop: 10 }]} />
              </View>
            </View>
          </ScrollView>
        )}

        {this.state.showList && (
          <View
            style={{ marginTop: 45, marginLeft: 25, flexDirection: "column" }}
          >
            <TouchableOpacity
              onPress={() =>
                this.setState({ showList: false, showMain: true, newData: [] })
              }
            >
              <Image
                source={require("../../assets/images/backarrow.png")}
                style={{ height: 20, width: 20, resizeMode: "contain" }}
              />
            </TouchableOpacity>
            <View style={{ flexDirection: "row", marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 23,
                  fontWeight: "bold",
                  fontFamily: "Roboto",
                  flex: 1
                }}
              >
                {this.state.filtername}
              </Text>
              <Text
                style={{
                  fontSize: 18,
                  fontFamily: "Roboto",
                  color: color.toolbar,
                  alignItems: "flex-end",
                  flex: 1,
                  textAlign: "right",
                  marginRight: 10
                }}
                onPress={() => this.checkAll(this.state.filtername)}
              >
                {this.renderCheck(this.state.filtername)}
              </Text>
            </View>
          </View>
        )}
        {this.state.showList && (
          <View style={{ flex: 1.8 }}>
            <SearchBar
              containerStyle={{ backgroundColor: color.WHITE, marginTop: 10 }}
              inputStyle={{ backgroundColor: color.WHITE }}
              ref={search => {
                this.search.ref = search;
              }}
              lightTheme
              clearIcon={true}
              value={this.state.text}
              onChangeText={text => this.searchFilterFunction(text)}
              onClearText={() => this.ClearText()}
              searchIcon={{ size: 24 }}
              placeholder="Type Here..."
            />

            <FlatList
              data={
                this.state.newData.length === 0
                  ? this.state.itemList
                  : this.state.newData
              }
              renderItem={({ item }) => this.renderRow(item)}
              keyExtractor={this._keyExtractor}
            />
          </View>
        )}

        {this.state.showMain && (
          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-end",
              flex: 0.1,
              backgroundColor: color.LIGHT_GREY,
              paddingBottom: 5
            }}
          >
            <TouchableOpacity
              style={{ flex: 1, marginTop: 10 }}
              onPress={() => this.ClearData()}
            >
              <Text
                style={[
                  styles.item,
                  {
                    color: color.GREY,
                    flex: 1,
                    justifyContent: "center",
                    textAlign: "center"
                  }
                ]}
              >
                Clear
              </Text>
            </TouchableOpacity>
            <View style={[styles.dashcolumn, { height: 30, marginTop: 10 }]} />
            <TouchableOpacity
              style={{ flex: 1, marginTop: 10 }}
              onPress={() => this.redirect()}
            >
              <Text
                style={[
                  styles.item,
                  {
                    color: color.toolbar,
                    flex: 1,
                    justifyContent: "center",
                    textAlign: "center"
                  }
                ]}
              >
                Apply
              </Text>
            </TouchableOpacity>
          </View>
        )}

        <PopupDialog
          dialogStyle={{
            width: "100%",
            height: 100,
            position: "absolute",
            bottom: 0,
            borderRadius: 0
          }}
          ref={popupDialog => {
            this.slideAnimationDialog = popupDialog;
          }}
          dialogAnimation={slideAnimation}
        >
          <Text
            style={[
              styles.item,
              { marginLeft: 10, marginTop: 10, flex: 1, fontSize: 13 }
            ]}
          >
            Do you want to discard your changes?
          </Text>

          <View
            style={{
              flexDirection: "row",
              alignItems: "baseline",
              paddingBottom: 5,
              flex: 1
            }}
          >
            <TouchableOpacity
              style={{ flex: 1, marginTop: 10 }}
              onPress={() => this.redirect()}
            >
              <Text
                style={[
                  styles.item,
                  {
                    color: color.GREY,
                    flex: 1,
                    justifyContent: "center",
                    textAlign: "center",
                    fontSize: 15
                  }
                ]}
              >
                Apply Changes
              </Text>
            </TouchableOpacity>
            <View style={[styles.dashcolumn, { height: 30, marginTop: 10 }]} />
            <TouchableOpacity
              style={{ flex: 1, marginTop: 10 }}
              onPress={() => this.discardChanges()}
            >
              <Text
                style={[
                  styles.item,
                  {
                    color: color.BLACK,
                    flex: 1,
                    justifyContent: "center",
                    textAlign: "center",
                    fontSize: 15
                  }
                ]}
              >
                Discard Changes
              </Text>
            </TouchableOpacity>
          </View>
        </PopupDialog>
      </View>
    );
  }
}

export default Filter;
