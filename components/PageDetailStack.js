import React, {Component,Props} from 'react';
import {StyleSheet,TouchableOpacity,Image} from 'react-native';
import {createStackNavigator} from 'react-navigation';
import Page1 from '../screens/Page1';
import Buy from '../screens/Buy/Buy';
import Buy2 from '../screens/Buy/Buy2';
import Sipmain from '../screens/SIP/Sipmain';
import SipConfirm from '../screens/SIP/SipConfirm';
import SipOrder2 from '../screens/SIP/SipOrder2';
import SipOrder3 from '../screens/SIP/SipOrder3';
import SipOrder4 from '../screens/SIP/SipOrder4';
import Back from '../assets/images/back.png';



const DetailStack = createStackNavigator({
 
  Page1:{screen:Page1,navigationOptions:{header : null }},
  Sipmain:{ screen: Sipmain,navigationOptions:{title:'Invest Now',drawerLockMode:"locked-closed"}},
  Buy:{ screen: Buy,navigationOptions:{title:'Invest Now',drawerLockMode:"locked-closed"}},
  Buy2:{ screen: Buy2,navigationOptions:{title:'Order Details',drawerLockMode:"locked-closed"}},
  SipConfirm:{ screen: SipConfirm,navigationOptions:{title:'Order Details',drawerLockMode:"locked-closed"}},
  SipOrder2:{ screen: SipOrder2,navigationOptions:{title:'Order Details',drawerLockMode:"locked-closed"}},
  SipOrder3:{ screen: SipOrder3,navigationOptions:{title:'Order Details',drawerLockMode:"locked-closed"}},
  SipOrder4:{ screen: SipOrder4,navigationOptions:{title:'Order Details',drawerLockMode:"locked-closed"}},
  },{
        headerMode:"screen",
        navigationOptions: ({navigation}) => ({
          headerLeft: <TouchableOpacity onPress={ () => { navigation.goBack() }} >
          <Image source={Back} style={{height:20,width:20,marginLeft:20}}/>
          </TouchableOpacity>,
          headerStyle:{backgroundColor:color.toolbar},
          headerTintColor:"#FFFFFF",
          headerTitleStyle:{color:"#FFFFFF",fontWeight:"normal"},
         
          })
      
  })



class PageDetailStack extends Component {

    

  render () {
   
    return (
        
    
    //   <Stack
    //   key="Detail"
    //   title="Detail"
    //   titleStyle={{fontStyle:"italic" }} 
    //   icon={TabIcon}
    //   navigationBarStyle={{ backgroundColor: color.toolbar }}
    //   titleStyle={{ color: 'white', alignSelf: 'center' }} 
    //   >

    //   <Scene key="Page1" component={Page1}  initial/>
    //   <Scene key="Buy" component={Buy} title="Invest Now" back  hideDrawerButton  backButtonTintColor="#FFFFFF" backButtonImage={Back}/>
    //   <Scene key="Buy2" component={Buy2} title="Order Details" back  hideDrawerButton  backButtonTintColor="#FFFFFF" backButtonImage={Back} />                  
    //   <Scene key="Sipmain" component={Sipmain} title="Invest Now" back  hideDrawerButton  backButtonTintColor="#FFFFFF" backButtonImage={Back}/>
    //   <Scene key="SipConfirm" component={SipConfirm} title="Order Details" back  hideDrawerButton  backButtonTintColor="#FFFFFF" backButtonImage={Back} />                  
    //   <Scene key="SipOrder2" component={SipOrder2} title="Order Details" back  hideDrawerButton  backButtonTintColor="#FFFFFF" backButtonImage={Back} />                  
    //   <Scene key="SipOrder4" component={SipOrder4} title="Order Details" back  hideDrawerButton  backButtonTintColor="#FFFFFF" backButtonImage={Back} />                  
    // </Stack>

    <DetailStack/>
       
    
    );
  }
}





export default PageDetailStack;