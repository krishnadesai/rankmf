import React from 'react';
import { Platform,BackHandler,StyleSheet,View,Image,TouchableOpacity,AsyncStorage } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import {Actions} from 'react-native-router-flux';
import styles from './styles';

   
  const slides = [
    
    {
      key: 'somethun',
     
      image: require('../assets/images/Splash/iphoneX.jpg'),
      
    },
    {
      key: 'somethun-dos',
      image: require('../assets/images/Splash/iphoneX.jpg'),
    },
    {
      key: 'somethun1',
      image: require('../assets/images/Splash/iphoneX.jpg'),
    }
  ];
   
  export default class TutorialSlider extends React.Component {

    

    onAndroidBackPress = () => {
    
     
     
        BackHandler.exitApp();
        
    
      return true;
    }
  
    componentWillMount() {
     
      if (Platform.OS === 'android') {
        BackHandler.addEventListener('hardwareBackPress', this.onAndroidBackPress);
      }
   
  
    }
  
    componentWillUnmount() {
      if (Platform.OS === 'android') {
        BackHandler.removeEventListener('hardwareBackPress');
      }
      
    }
    _renderNextButton = () => {
        return (
          <View  style={styles.buttonCircle}>
           <Image source={require('../assets/images/right-arrow-forward.png')} style={{height:30,width:30,resizeMode:"contain"}} />
          </View>
        );
      }

      _renderDoneButton = () => {
        return (
          <View style={styles.buttonCircle}>
            {/* <Ionicons
              name="md-checkmark"
              color="rgba(255, 255, 255, .9)"
              size={24}
              style={{ backgroundColor: 'transparent' }}
              onDone={this._onDone}/> */}
              <TouchableOpacity onPress={this._onDone}>
              <Image source={require('../assets/images/tick.png')} style={{height:30,width:30,resizeMode:"contain"}}  />
              </TouchableOpacity>
          </View>
        );
      }

    _onDone = async() => {
      try{
        await AsyncStorage.setItem('TutorialDone',"Yes");
       
      }catch(error){}
     
      Actions.drawerWL();

      //try {
       // 
        
       
        // const  FirstLogin = await AsyncStorage.getItem('FirstLogin');
        // alert(FirstLogin);
        // 
        //   if (FirstLogin !== null) {
        //     setTimeout(() => {
            
        //         Actions.drawer();     
        //     },2500)
        //   }else{
        //     alert(13);
        //     setTimeout(() => {
              
        //       Actions.drawerWL();
        //     },2500)
        //   }
        //  } catch (error) {
        //  }
       
    }

    render() {
      return (
        <AppIntroSlider
          slides={slides}
          renderDoneButton={this._renderDoneButton}
          renderNextButton={this._renderNextButton}
          
        />
      );
    }
  }