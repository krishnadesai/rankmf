import React from 'react';
import PropTypes from 'prop-types';
import { Text ,StyleSheet} from 'react-native';

const propTypes = {
  focused: PropTypes.bool,
  title: PropTypes.string,
};

const defaultProps = {
  focused: false,
  title: '',
};

const TabIcon = props => <Text style={ props.focused ? styles.active : styles.inactive }>{props.title}</Text>;

const styles = StyleSheet.create({
  active: {
    fontSize: 15,
    height:30,
    paddingTop:5,
    borderRadius: 30/2,        
    backgroundColor:color.toolbar,
    paddingLeft:10,
    paddingRight:10,
    justifyContent:"center",
    color:"#FFFFFF"
  },
  inactive:{
    color:color.toolbar
  }
});

TabIcon.propTypes = propTypes;
TabIcon.defaultProps = defaultProps;

export default TabIcon;
