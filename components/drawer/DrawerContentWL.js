import React from 'react';
import PropTypes from 'prop-types';
import {Platform, StyleSheet, Text, View, ViewPropTypes } from 'react-native';
import Button from 'react-native-button';
import { Actions } from 'react-native-router-flux';
import styles from '../styles';

class DrawerContentWL extends React.Component {
  static propTypes = {
    name: PropTypes.string,
    sceneStyle: ViewPropTypes.style,
    title: PropTypes.string,
  };

  static contextTypes = {
    drawer: PropTypes.object,
  };

  
  render() {
    return (
      <View style={styles.containerDrawer}>
        {/* <Text>Drawer Content</Text>
        <Button onPress={Actions.closeDrawer}>Back</Button>  */}
        <Text style={styles.textstyle} onPress={()=>Actions.Home()}>Home</Text>
        <View style={[styles.dash,{marginTop:10}]}/> 

        <Text style={styles.textstyle} onPress={()=>Actions.OpenAcc()}>Open an Account</Text>
        <View style={[styles.dash,{marginTop:10}]}/> 
        
        <Text style={styles.textstyle} onPress={()=>Actions.Login()}>Login</Text>
        <View style={[styles.dash,{marginTop:10}]}/> 
      </View>
    );
  }
}

export default DrawerContentWL;
