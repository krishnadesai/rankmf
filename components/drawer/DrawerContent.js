import React from "react";
import PropTypes from "prop-types";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ViewPropTypes,
  AsyncStorage,
  ImageBackground,
  Image
} from "react-native";
import Button from "react-native-button";
import { Actions } from "react-native-router-flux";
import styles from "../styles";
import color from "../../constants/Colors";
const clienttype = "",
  name = "";

class DrawerContent extends React.Component {
  static propTypes = {
    name: PropTypes.string,
    sceneStyle: ViewPropTypes.style,
    title: PropTypes.string
  };

  static contextTypes = {
    drawer: PropTypes.object
  };
  constructor(props) {
    super(props);
    this.state = {
      data: {}
    };
  }

  componentDidMount() {
    this.retrieveData();
  }
  componentWillMount() {
    this.retrieveData();
  }

  logout = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      if (clienttype === "lead") {
        await AsyncStorage.setItem("clienttype", "");
        await AsyncStorage.setItem("username", "");
        await AsyncStorage.setItem("password", "");
        await AsyncStorage.setItem("FirstLogin", "");
        await AsyncStorage.setItem("email", "");
        await AsyncStorage.setItem("name", "");
        await AsyncStorage.setItem("mobile", "");
        await AsyncStorage.setItem("city", "");
      } else {
        await AsyncStorage.setItem("clienttype", "");
        await AsyncStorage.setItem("username", "");
        await AsyncStorage.setItem("password", "");
        await AsyncStorage.setItem("clientid", "");
        await AsyncStorage.setItem("email", "");
        await AsyncStorage.setItem("name", "");
        await AsyncStorage.setItem("mobile", "");
        await AsyncStorage.setItem("encrypted_client_id", "");

        await AsyncStorage.setItem("crms_user_name", "");
        await AsyncStorage.setItem("FirstLogin", "");
      }

      setTimeout(() => {
        Actions.Login();
      }, 500);
    } catch (error) {
      // Error saving data
    }
  };

  retrieveData = async () => {
    try {
      clienttype = await AsyncStorage.getItem("clienttype");
      name = await AsyncStorage.getItem("name");
      clienttype = await AsyncStorage.getItem("clienttype");
      clientid = await AsyncStorage.getItem("clientid");
      access_token = await AsyncStorage.getItem("access_token");
    
    } catch (error) {
      // Error retrieving data
      //console.warn("1");
    }
  };

  render() {
    return (
      <View style={styles.containerDrawer}>
        {/* <Text>Drawer Content</Text>
        <Button onPress={Actions.closeDrawer}>Back</Button>  */}

        <ImageBackground
          source={require("../../assets/images/drawer_bg.jpg")}
          style={{ width: "100%", height: 200 }}
        >
          <View style={{ alignItems: "baseline", alignSelf: "baseline" }}>
            <View
              style={{
                height: 90,
                width: 90,
                borderRadius: 45,
                borderWidth: 2,
                borderColor: color.WHITE,
                marginLeft: 10,
                marginTop: 70
              }}
            >
              <Image
                source={require("../../assets/images/user.png")}
                style={{
                  width: 86,
                  height: 86,
                  resizeMode: "contain",
                  alignSelf: "center"
                }}
              />
            </View>
            <Text
              style={[
                styles.textstyle,
                {
                  bottom: 0,
                  color: color.WHITE,
                  alignSelf: "baseline",
                  alignItems: "baseline",
                  marginTop: 10
                }
              ]}
            >
              {name}
            </Text>
          </View>
        </ImageBackground>
        <View style={{ flexDirection: "row", marginLeft: 10, marginTop: 15 }}>
          <Image
            source={require("../../assets/images/My_Investments.png")}
            style={{
              width: 20,
              height: 20,
              resizeMode: "contain",
              alignSelf: "center"
            }}
          />
          <Text style={styles.textstyle} onPress={() => Actions.LoggedInUser()}>
            Home
          </Text>
        </View>

        <View style={{ flexDirection: "row", marginLeft: 10, marginTop: 15 }}>
          <Image
            source={require("../../assets/images/Explore.png")}
            style={{
              width: 20,
              height: 20,
              resizeMode: "contain",
              alignSelf: "center"
            }}
          />
          <Text
            style={styles.textstyle}
            onPress={() =>
              Actions.ExploreAll({
                rating: [],
                mininvt: [],
                asset_classcode: [],
                fund_house: [],
                filter: 0,
                Equity: [],
                Debt: [],
                Hybrid: [],
                Commodity: [],
                Others: [],
                Fund: [],
                popular: []
              })
            }
          >
            Explore All MF Ranks
          </Text>
        </View>

        {clienttype === "client" && (
          <View style={{ flexDirection: "row", marginLeft: 10, marginTop: 15 }}>
            <Image
              source={require("../../assets/images/Baskets.png")}
              style={{
                width: 20,
                height: 20,
                resizeMode: "contain",
                alignSelf: "center"
              }}
            />
            <Text style={styles.textstyle} onPress={() => Actions.BasketMain()}>
              Baskets
            </Text>
          </View>
        )}

        <View style={{ flexDirection: "row", marginLeft: 10, marginTop: 15 }}>
          <Image
            source={require("../../assets/images/eye.png")}
            style={{
              width: 20,
              height: 20,
              resizeMode: "contain",
              alignSelf: "center"
            }}
          />
          <Text style={styles.textstyle} onPress={() => Actions.Watchlist()}>
            Watchlist
          </Text>
        </View>

        {clienttype === "client" && (
          <View style={{ flexDirection: "row", marginLeft: 10, marginTop: 15 }}>
            <Image
              source={require("../../assets/images/Compare_Funds.png")}
              style={{
                width: 20,
                height: 20,
                resizeMode: "contain",
                alignSelf: "center"
              }}
            />
            <Text
              style={styles.textstyle}
              onPress={() => Actions.CompareFund({ schemecode: "65" })}
            >
              Compare Funds
            </Text>
          </View>
        )}

        {clienttype === "client" && (
          <View style={{ flexDirection: "row", marginLeft: 10, marginTop: 15 }}>
            <Image
              source={require("../../assets/images/My_Investments.png")}
              style={{
                width: 20,
                height: 20,
                resizeMode: "contain",
                alignSelf: "center"
              }}
            />
            <Text
              style={styles.textstyle}
              onPress={() => Actions.ManageAccount()}
            >
              Manage Account
            </Text>
          </View>
        )}

        <View style={{ flexDirection: "row", marginLeft: 10, marginTop: 15 }}>
          <Image
            source={require("../../assets/images/Logout.png")}
            style={{
              width: 20,
              height: 20,
              resizeMode: "contain",
              alignSelf: "center"
            }}
          />
          <Text style={styles.textstyle} onPress={() => this.logout()}>
            Logout
          </Text>
        </View>
      </View>
    );
  }
}

export default DrawerContent;
