
import {StyleSheet,Platform,Dimensions} from 'react-native';
import color from '../constants/Colors';

const DEVICE_WIDTH = Dimensions.get(`window`).width;
const DEVICE_HEIGHT = Dimensions.get(`window`).height;
module.exports = StyleSheet.create({

alwaysred: {
    backgroundColor: 'red',
    height: 100,
    width: 100,
},
containerSplash: {
    alignItems:"center",
    flex: 1,
    backgroundColor: color.toolbar,
    justifyContent:"center"
},
item: {
    fontSize: 18,
    fontFamily:'Roboto',
},
line: {
    flex: 1,
    height: 0.3,
    backgroundColor: 'darkgray',
},
logo: {
    alignItems:"center",
    position:"relative",
    width: 300,
    height: 100,
    justifyContent:"center"
},
containerDrawer: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  textstyle:{
    fontSize:(Platform.OS === 'ios' ? 18:15),
    fontFamily:'Roboto',
    color:color.toolbar,
    marginLeft:10,
    alignSelf:"center",
  },
  dash: {
    alignSelf: 'stretch',
    width: null,
    backgroundColor: '#D3D3D3',
    height: 1,
  },
  MainContainer :{
        
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: (Platform.OS == 'ios') ? 20 : 0,
    margin:10
     
  },     
animatedToastView:
{
    marginHorizontal: 50,
    paddingHorizontal: 25,
    paddingVertical: 10,
    borderRadius: 25,
    zIndex: 9999,
    position: 'absolute',
    justifyContent: 'center'
},
    
ToastBoxInsideText:
{
    fontSize: 15,
    fontFamily:'Roboto',
    alignSelf: 'stretch',
    textAlign: 'center'
},
buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
},
image: {
    height:'800',width:'400',resizeMode:"contain"
},
containerRouter: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
},
tabBarStyle: {

    backgroundColor: '#eee',
},
tabBarSelectedItemStyle: {
    backgroundColor: '#ddd',
},
containerFlex: {
    flex: 1,
    backgroundColor:color.WHITE
},
container2: {
   
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
},
welcome: {
    fontSize: 15,    
    color:'#696969',
    fontFamily:'Roboto'
},
flexboxtext: {
  fontSize: Platform.OS === 'ios' ? 15:12,
  fontFamily:'Roboto',
  textAlign: 'center',
  color:'#696969',
},
buttonContainer: {
    backgroundColor:"#FFFFFF",
    alignItems:"center",
},
instructions: {
    textAlign: 'center',
    color: '#FFFFFF',
    marginLeft:15,
    fontFamily:'Roboto',
    marginEnd:15,
    padding:Platform.OS == 'ios'?10 :7,
    
},
toolbar:{
    backgroundColor:color.toolbar,
    height:40
},
box:{
    flex: 1,
    alignItems: 'center',
    justifyContent:'center',
    backgroundColor: '#FFFFFF',
    paddingTop: 0,
    flexDirection: 'column'
},
button:{
    borderRadius:10,
    borderWidth: 1,
    borderColor: '#fff',
},
screen: {
    backgroundColor: '#33cc33',
    flex: 1,
    paddingTop: 10,
    alignItems: 'center',
    //padding: 10
},
dashcolumn: {
    alignSelf: 'stretch',
    width: 1,
    backgroundColor: '#D3D3D3',
},
input:{
    marginTop:5,
    height: 30,
    backgroundColor: '#FFFFFF',
    borderRadius:10,
    borderWidth:1,
    borderColor:"#D3D3D3",
    marginBottom: 10,
    padding: 5,
    textAlign:"center",
    color:"#000000"
},
dateinput:{
    marginTop:5,
    height: 30,
    backgroundColor: '#FFFFFF',
    borderRadius:10,
    borderWidth:1,
    borderColor:"#D3D3D3",
    padding: 5,
    width:120,
    textAlign:"center",
    color:"#000000"
},
buttonContainerPage1: {
    flex: 1,
    marginTop:10,
    marginLeft:10,
    marginRight:10
},
buttonBuy:{
    borderRadius:10,
    borderWidth: 1,
    marginLeft:15,
    marginEnd:15,
    padding:10,
    borderColor: '#fff',
},
download:{
    borderRadius:15,
    borderWidth:1,
    backgroundColor:"#f2f2f2",
    alignItems:"flex-end",
    margin:5,
    flexDirection:"row"
},
title: {
    marginLeft:5,
  fontSize: 12,
  fontFamily:'Roboto',
  color: '#696969',
},
calendar: {
    borderTopWidth: 1,
    paddingTop: 5,
    borderBottomWidth: 1,
    borderColor: '#eee',
    height: 350
},
text: {
    textAlign: 'center',
    borderColor: '#bbb',
    padding: 10,
    fontFamily:'Roboto',
    backgroundColor: '#eee'
},
title_carddetail:{
    fontWeight:"bold",fontSize:14,color:"#404040",fontFamily:'Roboto'
},
carddetail:{
    fontWeight:"normal",fontSize:13,color:"#000000",fontFamily:'Roboto',
},
itemchart: {
  marginTop:10,
  backgroundColor: '#000000',
},
verylowstrength:{
  borderLeftWidth : 3, 
  borderBottomWidth:3,
  borderBottomColor:'#b71e23',
  borderLeftColor:'#b71e23',
},
strength:{
    borderRadius: 75,
    width:150,  
    height:150,
    borderRightWidth:(Platform.OS == 'ios') ? 3 : 0,
    borderTopWidth:(Platform.OS == 'ios') ? 0 : 3,    
    borderTopColor:(Platform.OS == 'ios') ? 'transparent' : '#b71e23' ,
    borderRightColor:(Platform.OS == 'ios') ? '#b71e23' : 'transparent',
    position:"relative",   
    marginTop:(Platform.OS == 'ios') ? 0 : 15,
    transform:(Platform.OS == 'ios') ? [{ rotate: '45deg'}] : [{ rotate: '0deg'}] 
},
body :{
    
    //font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; 
    fontWeight: "300",
    fontSize: 14,fontFamily:'Roboto',
  },
  redeemtext: {
      textAlign: 'center',
      color: '#FFFFFF',
      marginLeft:10,
      marginEnd:10,
      padding:5
  },
  slider: { backgroundColor: '#000', height: 350 },
  content1: {
    width: '100%',
    height: 50,
    marginBottom: 10,
    backgroundColor: '#000',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content2: {
    width: '100%',
    height: 100,
    marginTop: 10,
    backgroundColor: '#000',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentText: { color: '#fff' ,fontFamily:'Roboto',},
  customSlide: {
    backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    width:100
  },
  customImage: {
    width: 100,
    height: 100,
  },
  buttonSlide: {
    margin: 3,
    width: 15,
    height: 15,
    opacity: 0.9,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttons: {
    zIndex: 1,
    height: 15,
    marginTop: -25,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  buttonSelected: {
    opacity: 1,
    color: 'red',
  },contentContainer: {
   
    flex: 1,
    height:100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  RankBadge:{
    backgroundColor:color.LIGHT_BLUE,
    marginTop:5,
    width:40,
    borderRadius:10,
    alignItems:"center",
    justifyContent:"center"
  },
  RankBadgeText:{
    padding: (Platform.OS === 'ios'? 7:5) ,
    color:color.WHITE,
    textAlign:"center",
    fontFamily:'Roboto',
  },
  ExploreAllText:{
      fontFamily:"Roboto",
      color:color.toolbar,
      fontSize:12
  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 5,
    borderRightWidth: 5,
    borderBottomWidth: 8,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: color.LIGHT_BLUE
  },
  circle: {
    width: 30,
    height:30,
    borderRadius: 30/2,
    backgroundColor: 'red'
},CompareFundsBadge:{
    backgroundColor:color.LIGHT_BLUE,
    marginTop:10,
    borderRadius:20,
    alignItems:"center",
    justifyContent:"center"
  },
  paginationDot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: 2
},
paginationContainer: {
    paddingVertical: 2,
    margin:2,
    padding:2,
    marginTop:5
},
});