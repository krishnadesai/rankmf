import React, {Component} from 'react';
import {Platform,StyleSheet,View,Text,TouchableOpacity,Linking,Image,BackHandler,AsyncStorage} from 'react-native';
import styles from './styles';
import {Actions} from 'react-native-router-flux';
import Apis from '../constants/Apis';
const access_token="",at_expire_time="";
var date = new Date();
var day = date ? ("0" + date.getDate()).slice(-2) : '';
var month = date ?  ("0" + (date.getMonth() + 1)).slice(-2) : '';
var year = date ? date.getFullYear() : '';
var currentdate = year+"/"+month+"/"+day+" "+date.getHours()+":"+("0" + date.getMinutes()).slice(-2)+":"+ ("0" + date.getSeconds()).slice(-2) ;

export default class Splash extends Component {

    constructor(props) {
        super(props);        
    }

    componentDidMount() {
      this.getClientid()
 //    alert(new Date(currentdate))
       
    }

    getClientid = async () => {
      try {
      client_id = await AsyncStorage.getItem('clientid');
      access_token_expire = await AsyncStorage.getItem('access_token_expire');
      access_token= await AsyncStorage.getItem('access_token');
    //  alert(access_token)
    //  alert(access_token_expire)
      //alert(new Date(access_token_expire.replace(/-/g,"/")) < new Date(currentdate))
     //access_token_expire = access_token_expire.replace(/-/g,"/")
     
    if(access_token_expire != null)
    {
      if(new Date(currentdate) > new Date(access_token_expire.replace(/-/g,"/"))){
      //alert(1)
        this.fetchData()
      }
      else{
        //alert(2)
        this.retrieveData()
      }
    }else{
     // alert(3)
      this.fetchData()
    }
       } catch (error) {
        // alert(error)
       }
    }

    retrieveData = async () => {
        try {
          await AsyncStorage.setItem('access_token_expire', at_expire_time);
        await AsyncStorage.setItem('access_token', access_token);
      
        const  FirstLogin = await AsyncStorage.getItem('FirstLogin');
        const  TutorialDone = await AsyncStorage.getItem('TutorialDone');
          if (FirstLogin !== null) {
            setTimeout(() => {
                     
                //this.props.navigation.navigate('Tabs');       
                Actions.drawer();     
            },2000)
          }else{
            if(TutorialDone !== null){
            setTimeout(() => {
              //  this.props.navigation.navigate('TutorialSlider');
            
                Actions.Home();
             
            },2000)
          }else{
            setTimeout(() => {
            Actions.TutorialSlider();
          },2000)
          }
          }
         } catch (error) {
         }
      }


      fetchData(){
        var app_key ="",app_secret="";
        if(Platform.OS === "ios"){
            app_key="rankmf-ios-app"
            app_secret=Apis.app_secret_ios
        }else{
          app_key="rankmf-android-app"
          app_secret=Apis.app_secret_android
        }
              
        fetch(Apis.AuthToken, {
            method: 'POST',
            headers: new Headers({
                        'Content-Type': 'application/x-www-form-urlencoded', // <-- Specifying the Content-Type
                }),
            body: `app_key=${app_key}&app_secret=${app_secret}&user=${client_id}`// <-- Post parameters
                })
                .then((response) => response.json())
                .then((responseJson) => {    
                     if(responseJson["status"] === "success"){
                        access_token = responseJson["response"].access_token
                        at_expire_time=responseJson["response"].at_expire_time
                        
                        this.retrieveData();
                     }
                   
                   
                })
                .catch((error) => {
                    console.error(error);
                });
    }

    render() {
        return (
           
              <View >
              <Image style={{height:'100%',width:'100%'}}  source={ require('../assets/images/Splash/iphoneX.jpg')}/>
               
              </View>
           
        )
    }

}

