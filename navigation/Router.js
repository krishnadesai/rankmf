import React from "react";
import { Dimensions, Platform, Text, View, Image } from "react-native";
import CardStackStyleInterpolator from "react-navigation-stack/dist/views/StackView/StackViewStyleInterpolator";
import {
  Scene,
  Router,
  Actions,
  Reducer,
  ActionConst,
  Overlay,
  Tabs,
  Modal,
  Drawer,
  Stack,
  Lightbox
} from "react-native-router-flux";
import Splash from "../components/Splash";
import Login from "../screens/Login";
import TutorialSlider from "../components/TutorialSlider";
import ExploreAll from "../screens/ExplorePages/ExploreAll";
import SearchMutualFund from "../screens/ExplorePages/SearchMutualFund";
import Filter from "../screens/ExplorePages/Filter";
import DrawerContent from "../components/drawer/DrawerContent";
import DrawerContentWL from "../components/drawer/DrawerContentWL";
import ErrorModal from "../components/modal/ErrorModal";
import DemoLightbox from "../components/lightbox/DemoLightbox";
import MenuIcon from "../assets/images/menu.png";
import Page1 from "../screens/Page1";
import OpenAccount from "../screens/OpenAccount";
import Home from "../screens/Home";
import LoggedInUser from "../screens/LoggedInUser";
import ManageAccount from "../screens/ManageAccount";
import CreateMandate from "../screens/ManageAccounts/CreateMandate";
import MandateWebview from "../screens/ManageAccounts/MandateWebview";
import Watchlist from "../screens/Watchlist";
import Lumpsum from "../screens/Lumpsum/Lumpsum";
import Lumpsum2 from "../screens/Lumpsum/Lumpsum2";
import Lumpsum3 from "../screens/Lumpsum/Lumpsum3";
import LumpsumWebview from "../screens/Lumpsum/LumpsumWebview";
import Sip1 from "../screens/SIP/Sip1";
import SipMandate from "../screens/SIP/SipMandate";
import SipOrder2 from "../screens/SIP/SipOrder2";
import SipOrder3 from "../screens/SIP/SipOrder3";
import SipOrder4 from "../screens/SIP/SipOrder4";
import Back from "../assets/images/back.png";
import search from "../assets/images/search.png";
import color from "../constants/Colors";
import BasketMain from "../screens/Basket/BasketMain";
import BasketDetail from "../screens/Basket/BasketDetail";
import LumpsumBasket1 from "../screens/Basket/LumpsumBasket1";
import LumpsumBasket2 from "../screens/Basket/LumpsumBasket2";
import LumpsumBasket3 from "../screens/Basket/LumpsumBasket3";
import LumpsumBasketWeb from "../screens/Basket/LumpsumBasketWeb";
import SipBasket1 from "../screens/Basket/SipBasket1";
import SipBasket2 from "../screens/Basket/SipBasket2";
import SipBasket3 from "../screens/Basket/SipBasket3";
import CompareFund from "../screens/CompareFunds/CompareFund";

const reducerCreate = params => {
  const defaultReducer = new Reducer(params);
  return (state, action) => {
    //console.log('reducer: ACTION:', action);

    return defaultReducer(state, action);
  };
};

const stateHandler = (prevState, newState, action) => {
  currentRoute = action.key;
  //console.log('onStateChange: ACTION:', action);
};

const getSceneStyle = () => ({
  backgroundColor: "#F5FCFF",
  shadowOpacity: 1,
  shadowRadius: 3
});

// // on Android, the URI prefix typically contains a host in addition to scheme
// const prefix = Platform.OS === 'android' ? 'mychat://mychat/' : 'mychat://';
//uriPrefix={prefix}
const Router1 = () => (
  <Router
    createReducer={reducerCreate}
    onStateChange={stateHandler}
    getSceneStyle={getSceneStyle}
  >
    <Overlay key="overlay">
      <Modal
        key="modal"
        hideNavBar
        transitionConfig={() => ({
          screenInterpolator:
            Platform.OS == "ios"
              ? CardStackStyleInterpolator.ModalSlideFromBottomIOS
              : CardStackStyleInterpolator.forHorizontal
        })}
      >
        <Lightbox key="lightbox">
          <Stack
            key="root"
            titleStyle={{ alignSelf: "center" }}
            transitionConfig={() => ({
              screenInterpolator:
                Platform.OS == "ios"
                  ? CardStackStyleInterpolator.ModalSlideFromBottomIOS
                  : CardStackStyleInterpolator.forHorizontal
            })}
          >
            <Scene key="Splash" component={Splash} initial hideNavBar={true} />
            <Scene
              key="TutorialSlider"
              component={TutorialSlider}
              hideNavBar={true}
            />
            <Scene key="Login" component={Login} hideNavBar={true} />

            <Drawer
              hideNavBar
              key="drawerWL"
              contentComponent={DrawerContentWL}
              drawerImage={MenuIcon}
              drawerWidth={300}
              panHandlers={null}
            >
              <Scene
                hideNavBar
                navigationBarStyle={{ backgroundColor: color.toolbar }}
                titleStyle={{ color: "white", alignSelf: "center" }}
              >
                <Stack>
                  <Scene key="Home" component={Home} />
                  <Scene
                    key="OpenAcc"
                    component={OpenAccount}
                    title="Open an Account"
                    panHandlers={null}
                  />
                </Stack>
              </Scene>
            </Drawer>

            <Drawer
              hideNavBar
              key="drawer"
              contentComponent={DrawerContent}
              drawerImage={MenuIcon}
              drawerWidth={300}
              panHandlers={null}
            >
              {/*
                Wrapper Scene needed to fix a bug where the tabs would
                reload as a modal ontop of itself
              */}
              <Scene
                hideNavBar
                navigationBarStyle={{ backgroundColor: color.toolbar }}
                titleStyle={{ color: "white", alignSelf: "center" }}
              >
                <Scene>
                  <Scene
                    key="LoggedInUser"
                    title="LoggedInUser"
                    component={LoggedInUser}
                  />
                </Scene>
              </Scene>

              <Scene
                hideNavBar
                navigationBarStyle={{ backgroundColor: color.toolbar }}
                titleStyle={{ color: "white", alignSelf: "center" }}
              >
                <Scene>
                  <Scene
                    key="ExploreAll"
                    title="Explore All MF Ranks"
                    component={ExploreAll}
                    rightButtonImage={search}
                  />
                  <Scene
                    key="SearchMutualFund"
                    title="Search"
                    component={SearchMutualFund}
                    back
                    hideDrawerButton
                    panHandlers={null}
                    backButtonImage={Back}
                  />
                </Scene>
              </Scene>

              <Scene
                hideNavBar
                navigationBarStyle={{ backgroundColor: color.toolbar }}
                titleStyle={{ color: "white", alignSelf: "center" }}
              >
                <Scene>
                  <Scene
                    key="CompareFund"
                    title="Compare Funds"
                    component={CompareFund}
                  />
                </Scene>
              </Scene>

              <Scene
                hideNavBar
                navigationBarStyle={{ backgroundColor: color.toolbar }}
                titleStyle={{ color: "white", alignSelf: "center" }}
              >
                <Scene>
                  <Scene
                    key="BasketMain"
                    title="Basket"
                    component={BasketMain}
                  />
                </Scene>
              </Scene>

              <Scene
                hideNavBar
                navigationBarStyle={{ backgroundColor: color.toolbar }}
                titleStyle={{ color: "white", alignSelf: "center" }}
              >
                <Stack>
                  <Scene
                    key="ManageAccount"
                    title="Manage Account"
                    component={ManageAccount}
                  />

                  {/* <Scene key="tab_2_1" component={TabView} title="Tab #2_1" renderRightButton={() => <Text>Right</Text>} />
                    <Scene key="tab_2_2" component={TabView} title="Tab #2_2" back onBack={() => alert('onBack button!')} hideDrawerButton backTitle="Back!" panHandlers={null} /> */}
                </Stack>
              </Scene>
              <Scene
                navigationBarStyle={{ backgroundColor: color.toolbar }}
                titleStyle={{ color: "white", alignSelf: "center" }}
              >
                <Scene
                  key="Watchlist"
                  title="Watchlist"
                  component={Watchlist}
                />
              </Scene>
            </Drawer>
            <Scene
              hideNavBar
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            >
              <Scene>
                <Scene key="page1" component={Page1} />
              </Scene>
            </Scene>
            <Scene
              hideNavBar
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            >
              <Scene hideNavBar>
                <Scene key="BasketDetail" component={BasketDetail} />
              </Scene>
            </Scene>
            <Scene
              hideNavBar
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            >
              <Scene
                key="Filter"
                title="Filter"
                component={Filter}
                back
                hideDrawerButton
                panHandlers={null}
                backButtonImage={Back}
              />
            </Scene>

            <Scene
              key="CreateMandate"
              component={CreateMandate}
              title="Create Mandate"
              back
              panHandlers={null}
              backButtonImage={Back}
              backButtonTintColor={color.WHITE}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="MandateWebview"
              component={MandateWebview}
              title="Generate Mandate"
              panHandlers={null}
              backButtonImage={Back}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="Sip1"
              component={Sip1}
              title="Invest Now"
              back
              panHandlers={null}
              backButtonImage={Back}
              backButtonTintColor={color.WHITE}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="Lumpsum"
              component={Lumpsum}
              back
              title="Invest Now"
              panHandlers={null}
              backButtonImage={Back}
              backButtonTintColor={color.WHITE}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="Lumpsum2"
              component={Lumpsum2}
              title="Review Your Order"
              panHandlers={null}
              backButtonImage={Back}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="Lumpsum3"
              component={Lumpsum3}
              title="Track Order"
              onBack={() => Actions.page1()}
              panHandlers={null}
              backButtonImage={Back}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="LumpsumWebview"
              component={LumpsumWebview}
              title="Generate Mandate"
              onBack={() => Actions.page1()}
              panHandlers={null}
              backButtonImage={Back}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="SipMandate"
              component={SipMandate}
              back
              title="Order Details"
              panHandlers={null}
              onBack={() => Actions.SipOrder3({ showPlaceOrder: true })}
              backButtonImage={Back}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="SipOrder2"
              component={SipOrder2}
              back
              title="Review Your Order"
              panHandlers={null}
              backButtonImage={Back}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="SipOrder3"
              component={SipOrder3}
              back
              onBack={() => Actions.page1()}
              title="Order Details"
              panHandlers={null}
              backButtonImage={Back}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="SipOrder4"
              component={SipOrder4}
              back
              title="Order Details"
              panHandlers={null}
              backButtonImage={Back}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="LumpsumBasket1"
              component={LumpsumBasket1}
              back
              title="Invest Now"
              panHandlers={null}
              backButtonImage={Back}
              backButtonTintColor={color.WHITE}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="LumpsumBasket2"
              component={LumpsumBasket2}
              back
              title="Review Your Order"
              panHandlers={null}
              backButtonImage={Back}
              backButtonTintColor={color.WHITE}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="LumpsumBasket3"
              component={LumpsumBasket3}
              back
              onBack={() => Actions.BasketDetail()}
              title="Review Your Order"
              panHandlers={null}
              backButtonImage={Back}
              backButtonTintColor={color.WHITE}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="LumpsumBasketWeb"
              component={LumpsumBasketWeb}
              back
              onBack={() => Actions.BasketDetail()}
              title="Review Your Order"
              panHandlers={null}
              backButtonImage={Back}
              backButtonTintColor={color.WHITE}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="SipBasket1"
              component={SipBasket1}
              back
              title="Invest Now"
              panHandlers={null}
              backButtonImage={Back}
              backButtonTintColor={color.WHITE}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="SipBasket2"
              component={SipBasket2}
              back
              onBack={() => Actions.BasketDetail()}
              title="Review Your Order"
              panHandlers={null}
              backButtonImage={Back}
              backButtonTintColor={color.WHITE}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
            <Scene
              key="SipBasket3"
              component={SipBasket3}
              back
              title="Order Details"
              panHandlers={null}
              backButtonImage={Back}
              backButtonTintColor={color.WHITE}
              navigationBarStyle={{ backgroundColor: color.toolbar }}
              titleStyle={{ color: "white", alignSelf: "center" }}
            />
          </Stack>

          <Scene key="demo_lightbox" component={DemoLightbox} />
        </Lightbox>
        <Scene key="error" component={ErrorModal} />
      </Modal>

      {/* <Scene component={MessageBar} /> */}
    </Overlay>
  </Router>
);

export default Router1;

{
  /* <Stack key="login" path="login/:data" titleStyle={{ alignSelf: 'center' }}>
          <Scene key="loginModal" component={Login} title="Login" onExit={() => console.log('onExit')} leftTitle="Cancel" onLeft={Actions.pop} />
          <Scene key="loginModal2" component={Login2} title="Login2" backTitle="Back" panHandlers={null} duration={1} />
          <Scene key="loginModal3" hideNavBar component={Login3} title="Login3" panHandlers={null} duration={1} />
        </Stack> */
}
{
  /* <Stack
                    key="Detail"
                    title="Detail"
                    titleStyle={{fontStyle:"italic" }} 
                    icon={TabIcon}
                    navigationBarStyle={{ backgroundColor: color.toolbar }}
                    titleStyle={{ color: 'white', alignSelf: 'center' }} 
                    >

                    <Scene key="Page1" component={Page1}  />
                    <Scene key="Lumpsum" component={Lumpsum} title="Invest Now" back  hideDrawerButton  backButtonTintColor="#FFFFFF" backButtonImage={Back}/>
                    <Scene key="Lumpsum2" component={Lumpsum2} title="Order Details" back  hideDrawerButton  backButtonTintColor="#FFFFFF" backButtonImage={Back} />                  
                    <Scene key="Sipmain" component={Sipmain} title="Invest Now" back  hideDrawerButton  backButtonTintColor="#FFFFFF" backButtonImage={Back}/>
                    <Scene key="SipConfirm" component={SipConfirm} title="Order Details" back  hideDrawerButton  backButtonTintColor="#FFFFFF" backButtonImage={Back} />                  
                    <Scene key="SipOrder2" component={SipOrder2} title="Order Details" back  hideDrawerButton  backButtonTintColor="#FFFFFF" backButtonImage={Back} />                  
                    <Scene key="SipOrder4" component={SipOrder4} title="Order Details" back  hideDrawerButton  backButtonTintColor="#FFFFFF" backButtonImage={Back} />                  
                  </Stack>

                   <Stack
                    key="OverviewMain"
                    title="Overview"
                    titleStyle={{fontStyle:"italic" }} 
                    icon={TabIcon}
                    navigationBarStyle={{ backgroundColor: color.toolbar }}
                    titleStyle={{ color: 'white', alignSelf: 'center' }} 
                    >

                    <Scene key="Overview" component={Overview}  />
                  
                  </Stack>

                  <Stack
                    key="PerformanceMain"
                    title="Performance"
                    titleStyle={{fontStyle:"italic" }} 
                    icon={TabIcon}
                    navigationBarStyle={{ backgroundColor: color.toolbar }}
                    titleStyle={{ color: 'white', alignSelf: 'center' }} 
                    >

                    <Scene key="Performance" component={Performance}  />
                    
                  </Stack>

                  <Stack
                    key="SIPMain_1"
                    title="SIP"
                    titleStyle={{fontStyle:"italic" }} 
                    icon={TabIcon}
                    navigationBarStyle={{ backgroundColor: color.toolbar }}
                    titleStyle={{ color: 'white', alignSelf: 'center' }} 
                    >

                    <Scene key="SIP" component={Sip}  />
                     
                  </Stack>
                  
                  <Stack
                    key="PortfolioMain"
                    title="Portfolio"
                    titleStyle={{fontStyle:"italic" }} 
                    icon={TabIcon}
                    navigationBarStyle={{ backgroundColor: color.toolbar }}
                    titleStyle={{ color: 'white', alignSelf: 'center' }} 
                    >

                    <Scene key="Portfolio" component={Portfolio}  />
                     
                  </Stack>

                  <Stack
                    key="RiskMain"
                    title="Risk & Strength"
                    titleStyle={{fontStyle:"italic" }} 
                    icon={TabIcon}
                    navigationBarStyle={{ backgroundColor: color.toolbar }}
                    titleStyle={{ color: 'white', alignSelf: 'center' }} 
                    >

                    <Scene key="RiskStrength" component={RiskStrength}  />
                     
                  </Stack>

                  <Stack
                    key="ManageOtherMain"
                    title="Manager & Other Details"
                    titleStyle={{fontStyle:"italic" }} 
                    icon={TabIcon}
                    navigationBarStyle={{ backgroundColor: color.toolbar }}
                    titleStyle={{ color: 'white', alignSelf: 'center' }} 
                    >

                    <Scene key="ManagerOther" component={Manager}  />
                     
                  </Stack> */
}

{
  /* <Tabs
                  key="tabbar"
                  routeName="tabbar"
                  swipeEnabled={true}
                  showLabel={false}
                  tabs={true}
                  tabBarPosition="bottom"
                  hideNavBar
                  tabBarStyle={styles.tabBarStyle}
                  backgroundColor="white"
                  animationEnabled
                  swi
                >
                  

                   </Tabs> */
}
{
  /* <Scene key="echo" back clone component={EchoView} getTitle={({ navigation }) => navigation.state.key} />
            <Scene key="launch" component={Launch} title="Launch" initial type={ActionConst.RESET} />

            <Stack key="customNavBar" hideTabBar titleStyle={{ alignSelf: 'center' }}>
              <Scene key="customNavBar1" title="CustomNavBar 1" navBar={CustomNavBar} component={CustomNavBarView} back />
              <Scene key="customNavBar2" title="CustomNavBar 2" navBar={CustomNavBar} component={CustomNavBarView} back />
              <Scene key="customNavBar3" title="Another CustomNavBar" navBar={CustomNavBar2} component={CustomNavBarView} back />
              <Scene key="hiddenNavBar" title="hiddenNavBar" component={CustomNavBarView} hideNavBar={true} back />
            </Stack> */
}

{
  /* <Stack back backTitle="Back" key="register" duration={0} navTransparent>
              <Scene key="_register" component={Register} title="Register" />
              <Scene key="register2" component={Register} title="Register2" />
              <Scene key="home" component={Home} title="Replace" type={ActionConst.REPLACE} />
            </Stack> */
}

{
  /* <Scene key="tab_1_1" component={TabView} title="Tab #1_1" onRight={() => alert('Right button')} rightTitle="Right" /> 

                      <Scene key="tab_1_2" component={TabView} title="Tab #1_2" back titleStyle={{ color: 'black', alignSelf: 'center' }} />  */
}
{
  /* <Stack key="tab_2" title="Tab #2" icon={TabIcon} >
                    <Scene key="tab_2_1" component={TabView} title="Tab #2_1" renderRightButton={() => <Text>Right</Text>} />
                    <Scene key="tab_2_2" component={TabView} title="Tab #2_2" back onBack={() => alert('onBack button!')} hideDrawerButton backTitle="Back!" panHandlers={null} />
                  </Stack>

                  <Stack key="tab_3" icon={TabIcon} title="Tab #3">
                    <Scene key="tab_3_1" component={TabView} rightTitle="Reset to 'tabbar'" onRight={() => Actions.reset('tabbar')} />
                  </Stack>
                  <Scene key="tab_4_1" component={TabView} title="Tab #4" hideNavBar icon={TabIcon} />
                  <Stack key="tab_5" icon={TabIcon} title="Tab #5">
                    <Scene key="tab_5_1" component={TabView} />
                  </Stack> */
}
