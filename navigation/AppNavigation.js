import React from 'react'
import {StyleSheet, View ,TouchableOpacity,Image} from 'react-native'
import { createStackNavigator, createDrawerNavigator} from 'react-navigation'
import Login from '../screen/Login'
import Splash from '../components/Splash'
import TutorialSlider from '../components/TutorialSlider'
import TabPage from '../components/TabPage'
import PageDetailStack from '../components/PageDetailStack'
const back = require('../assets/images/right-arrow-forward.png');
const menu = require('../assets/images/menu.png');
//var imgSource = this.state.drawerstate? menu : back;
    

// drawer stack
const DrawerStack = createDrawerNavigator({
    Tabs: { screen: TabPage ,navigationOptions:{title:'Tabs'}},
    PageDetailStack:{screen:PageDetailStack,navigationOptions:{title:'PageDetailStack'}},
})

const DrawerNavigation = createStackNavigator({
  DrawerStack: { screen: DrawerStack },
}, {
  headerMode: 'screen',
  navigationOptions: ({navigation}) => ({
    headerStyle: {backgroundColor: 'green'},
    title: 'Logged In to your app!',
    header: <View style={[styles.toolbar,{flexDirection:"row"}]}>
     
    <TouchableOpacity activeOpacity = { .5 }  style={{marginTop:20,marginLeft:15}}  onPress={()=>navigation.toggleDrawer()}>
      <Image source={require('../assets/images/menu.png')}
             style={{height:15,width:20,resizeMode:"contain"}} />
      </TouchableOpacity>

      <Image source={require('../assets/images/samcologo.png')}
             style={{width:65,height:40,marginLeft:25,alignItems:"center",marginTop:1,resizeMode:"contain",flex:1}} />

    {/* <TouchableOpacity activeOpacity = { .5 } onPress={ () => alert("1")}  style={{marginTop:20,flex:1,alignItems:"flex-end",marginRight:15}}>  
      <Image source={require('../assets/images/search.png')}
             style={{height:20,width:20,resizeMode:"contain"}} />
      </TouchableOpacity>  */}
     
    </View>
   
  }),
 
})




// login stack
const LoginStack = createStackNavigator({
  Splash: { screen: Splash },
  TutorialSlider: { screen: TutorialSlider },
  Login: { screen: Login,navigationOptions:{title:'Login'} }
}, {
  headerMode: 'none', 

})



// Manifest of possible screens
const PrimaryNav = createStackNavigator({
  loginStack: { screen: LoginStack},
  drawerStack: { screen: DrawerNavigation  }, 
}, {
  // Default config for all screens
  headerMode:"none",
  navigationOptions: ({navigation}) => ({
  title: 'Main',
  initialRouteName: 'loginStack'
  })
})

const styles = StyleSheet.create({
    
    toolbar:{
        backgroundColor:color.toolbar,
        height:50
    },
   
  });

export default PrimaryNav