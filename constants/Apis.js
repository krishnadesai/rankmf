export default {
    app_secret_ios:'iJCFih2xQceFZaF6kjxC',
    app_secret_android:'ahu7jM5Mbd7v6hypSLuz',
    getSchemes : 'https://www.rankmf.com/home/getSchemes', // POST  {keyword}
    BaseImageURL : 'https://www.rankmf.com/images/',
    BaseURL:'http://mfadmin.rankmf.com/webservices/Mf_api', // POST {api=1.1,limit=10,offset,}
    Minvest:'http://mfadmin.rankmf.com/webservices/Mf_Minvest',
    MfBasket:'http://mfadmin.rankmf.com/webservices/Mf_Basket',
    AuthToken:'http://mfadmin.rankmf.com/webservices/Mf_api/getAccessToken',
    getSchemesCompare:'http://mfadmin.rankmf.com/Mf_compare/getSchemes',
    Mf_order:'http://mfadmin.rankmf.com/webservices/Mf_order',
    Mf_mandate:'http://mfadmin.rankmf.com/webservices/Mf_mandate',
}