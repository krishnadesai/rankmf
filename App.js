GLOBAL.self = GLOBAL;

import React from "react";
import { Platform, StatusBar, StyleSheet, View } from "react-native";
import { AppLoading, Asset, Font, Icon } from "expo";
import Router1 from "./navigation/Router";

export default class App extends React.Component {
  state = {
    isLoadingComplete: false
  };
  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
    });
    this.setState({ isLoadingComplete: true });
  }

  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <View style={styles.container}>
          {Platform.OS === "ios" && <StatusBar barStyle="default" />}
          <Router1 />
        </View>
      );
    }
  }

  _loadResourcesAsync = async () => {
    return Promise.all([
      Asset.loadAsync([
        require("./assets/images/robot-dev.png"),
        require("./assets/images/robot-prod.png"),
        require("./assets/images/like.png"),
        require("./assets/images/add-find-chip.png"),
        require("./assets/images/arrow_forward.png"),
        require("./assets/images/article.png"),
        require("./assets/images/asc.png"),
        require("./assets/images/back.png"),
        require("./assets/images/backarrow.png"),
        require("./assets/images/basket-tag-blue.png"),
        require("./assets/images/Baskets.png"),
        require("./assets/images/Buildyourwealth.png"),
        require("./assets/images/core-page-bg.jpg"),
        require("./assets/images/cross.png"),
        require("./assets/images/desc.png"),
        require("./assets/images/dislike.png"),
        require("./assets/images/double_like.png"),
        require("./assets/images/downloadimage.png"),
        require("./assets/images/drawer_bg.jpg"),
        require("./assets/images/drop-down-arrow.png"),
        require("./assets/images/emptystar.png"),
        require("./assets/images/star.png"),
        require("./assets/images/Explore.png"),
        require("./assets/images/eye.png"),
        require("./assets/images/filter.png"),
        require("./assets/images/FiveStars.jpg"),
        require("./assets/images/fund-lock.png"),
        require("./assets/images/header-png.png"),
        require("./assets/images/high_strength_large.png"),
        require("./assets/images/high_strength.png"),
        require("./assets/images/hold-grey.png"),
        require("./assets/images/icon.png"),
        require("./assets/images/like.png"),
        require("./assets/images/loader.gif"),
        require("./assets/images/Logout.png"),
        require("./assets/images/low_strength_large.png"),
        require("./assets/images/low_strength.png"),
        require("./assets/images/menu.png"),
        require("./assets/images/moderate_strength_large.png"),
        require("./assets/images/moderate_strength.png"),
        require("./assets/images/My_Investments.png"),
        require("./assets/images/new-icon.png"),
        require("./assets/images/padlock.png"),
        require("./assets/images/popularity-grey.png"),
        require("./assets/images/popularity.png"),
        require("./assets/images/rank-lock.png"),
        require("./assets/images/rankmf.png"),
        require("./assets/images/rating-lock.png"),
        require("./assets/images/rating.png"),
        require("./assets/images/register-to-view-details.png"),
        require("./assets/images/Register.png"),
        require("./assets/images/remove_watchlist_white.png"),
        require("./assets/images/remove_watchlist.png"),
        require("./assets/images/samcologo.png"),
        require("./assets/images/search.png"),
        require("./assets/images/sort.png"),
        require("./assets/images/splash.png"),
        require("./assets/images/starblack.png"),
        require("./assets/images/strength.png"),
        require("./assets/images/thumbsdown.png"),
        require("./assets/images/thumbsup.png"),
        require("./assets/images/tick.png"),
        require("./assets/images/time-to-invest-lock.png"),
        require("./assets/images/upload.png"),
        require("./assets/images/user.png"),
        require("./assets/images/very_high_strength_large.png"),
        require("./assets/images/very_high_strength.png"),
        require("./assets/images/very_low_strength.png"),
        require("./assets/images/very_low_strength_large.png"),
        require("./assets/images/check-mark.png"),
        require("./assets/images/pending.png"),
        require("./assets/images/calendar.png"),
        require("./assets/images/arrow-up.png"),
        require("./assets/images/arrow-down.png"),
        require("./assets/images/next.png"),
      ]),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Icon.Ionicons.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        "space-mono": require("./assets/fonts/SpaceMono-Regular.ttf")
      })
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  }
});
